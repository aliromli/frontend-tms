@extends('layouts.app')
@section('title', 'Under Development')
@section('ribbon')
<span class="ribbon-button-alignment">
    <span id="refresh" class="btn btn-ribbon" data-action="resetWidgets" data-title="refresh" rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true">
        <i class="fa fa-refresh"></i>
    </span>
</span>

<!-- breadcrumb -->
<ol class="breadcrumb">
    <li>Home</li>
    <li>Under Development</li>
</ol>
<!-- end breadcrumb -->
@endsection

@section('content')
<div class="content-wrap">
    <div class="row">
        <div class="col-md-12 text-center">
            <img height="300" src="{{ asset('img/development4.png') }}" />
            <div>
                <div style="font-size: 1.2em; background: white; display: inline; border-radius: 20px; padding: 5px"> Mohon maaf... Kami saat ini sedang membangun halaman ini sekuat tenaga...</div>
            </div>
        </div>
    </div>
</div>
@endsection

@extends('layouts.app')
@section('title', 'Home')
@section('ribbon')
@endsection


@section('content')
<div class="title-page">
    <div class="row">
            <div class="col-md-12">
                <ul class="breadcrumb mt-bread">
                    <li><a href="/">HOME </a></li>
                </ul>
            </div>
    </div> 
</div>
<div class="main-content-inner" style="">
        
        <div class="row">
            
            <div class="col-md-4">
                <div class="card mb-tng">
                    <div class="single-report mb-xs-30">
                        <div class="s-report-inner pr--20 pt--30 mb-3">
                            <!-- <div class="icon"><i class="fa fa-btc"></i></div> -->
                            <div class="s-report-title">
                                <h4 class="header-title mb-0">TOTAL TERMINAL</h4>
                                <div>{{$totalTerminal}}</div>
                            </div>
                            <div class="d-flex justify-content-between pb-2">
                                <div 
                                class="rounded-circle"
                                style="width: 73px;height: 71px;background-color: #D9D9D9;"    >
                                </div>
                                <div style="background: #fffff;">
                                    <p class="text-right text-success font-weight-bold">Online {{$online}}</p>
                                    <p class="text-right text-warning font-weight-bold">Ofline {{$offline}}</p>
                                    <p class="text-right text-danger font-weight-bold">Disconected {{$disconnected}}</p>
                                    <p class="text-right text-secondary">&nbsp;</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card mb-tng">
                           <div class="single-report mb-xs-30">
                                <div class="s-report-inner pr--20 pt--30 mb-3">
                                    <!-- <div class="icon"><i class="fa fa-btc"></i></div> -->
                                    <div class="s-report-title">
                                        <h4 class="header-title mb-0">DOWNLOAD TASK</h4>
                                        <div>INSTALL FMS</div>
                                    </div>
                                    <div class="d-flex justify-content-between pb-2">
                                        <div 
                                        class="rounded-circle"
                                        style="width: 73px;height: 71px;background-color: #D9D9D9;"    >
                                        </div>
                                        <div style="background: #fffff;">
                                            <p class="text-right text-secondary">Success</p>
                                            <p class="text-right text-secondary">FAILed</p>
                                            <p class="text-right text-secondary">Downloading</p>
                                            <p class="text-right text-secondary">Installing</p>
                                            
                                        </div>
                                        
                                    </div>
                                </div>
                                
                            </div>
                </div>
            </div>    
        </div>
</div>
@endsection

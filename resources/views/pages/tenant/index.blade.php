@extends('layouts.app')
@section('title', 'Tenant')
@section('ribbon')
@endsection

@section('content')

<div class="title-page">
    <div class="row">
            <div class="col-md-12">
                <ul class="breadcrumb mt-bread">
                    <li><a href="/tenant">TENANT</a></li>
                </ul>
            </div>
    </div> 
</div>
<div class="main-content-inner">
    <div class="row">
        <div class="col-12 mt-5">
            <div class="card mb-tng2">
                <div class="card-body">
                    
                    <div class="box-header no-border text-left filter" style="margin-bottom:20px;"> 
                        <a class="btn btn-rounded btn-secondary clear-search" style="width:auto;" href="#">Clear Search</a> &nbsp;
                        <a class="btn btn-rounded btn-secondary tugel" style="width:auto;" href="#"><i class="fa fa-chevron-down sign"></i> <span class="filter-title">Show</span><span class="filter-title hidden">Hide</span> Filter <i class="fa fa-filter"></i></a> &nbsp;
                        @if(Privilege::visible(Request::segment(1),'CREATE')=='Y')
                        <button class="btn btn-rounded btn-primary" id="btn-add-tenant" style="width:auto;" ><i class="fa fa-plus-circle"></i> New Tenant</button> 
                        @endif
                    </div>
                    <div class="form-filter">
                        <form class="form-horizontal">
                                <div class="form-group row" xstyle="margin-left:-28;">
                                    <div class="col-md-4"><input type="text" id="search-tenant" class="form-control input-xs" placeholder="Tenant Name..."></div>
                                    <div class="col-md-3">
                                        <a href="#" id="btn-cari-tenant" class="btn  btn-rounded btn-secondary btn-xs" style="height:3em;width:4em;" ><span style="font-size:16px;">Cari</span></a>
                                    </div>
                                </div>
                        </form>
                    </div>
                    <div class="data-tables">
                        <table id="dataTableTenant"  xclass="text-center" width="100%">
                            <thead class="bg-light text-capitalize">
                                <tr>
                                    <th style="width:30px;">No</th>
                                    <th>Version</th>
                                    <th>Name</th>
                                    <th>Super Tenant</th>
                                    <th>IsSuper</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                               
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>

@endsection

@section('modal')
    @include('pages.tenant.new-tenant')
   
@endsection

@section('script')
 <!-- Start datatable js -->
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js"></script>
<script src="https://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.18/js/dataTables.bootstrap4.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.2.3/js/responsive.bootstrap.min.js"></script>

<script src="{{ asset('assets/js/tenant.js') }}"></script>

@endsection

<div class="modal fade " id="modal-list-md-md">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="page-loader hidden">
                <div class="sk-circle">
                    <div class="sk-circle1 sk-child"></div>
                    <div class="sk-circle2 sk-child"></div>
                    <div class="sk-circle3 sk-child"></div>
                    <div class="sk-circle4 sk-child"></div>
                    <div class="sk-circle5 sk-child"></div>
                    <div class="sk-circle6 sk-child"></div>
                    <div class="sk-circle7 sk-child"></div>
                    <div class="sk-circle8 sk-child"></div>
                    <div class="sk-circle9 sk-child"></div>
                    <div class="sk-circle10 sk-child"></div>
                    <div class="sk-circle11 sk-child"></div>
                    <div class="sk-circle12 sk-child"></div>
                </div>
            </div>
            <form class="form-horizontal" id="form-user">
                <div class="modal-header no-border" style="padding:0.5rem;">
                    <h4 class="title-form" style="font-size:15px;"></h4> 
                </div>
                <div class="modal-body" style="padding:1.7rem 1.7rem 0.7rem 1.7rem;">
					<div class="input-group mb-3">
					  <input type="text" class="form-control form-control-sm input-xs" id="search-model" placeholder="Model .." aria-label="Model" aria-describedby="basic-addon2">
					  <div class="input-group-append">
						 <button class="btn  btn-outline-secondary btn-xs" type="button" id="btn-search-model">Search</button>
						 <button class="btn btn-outline-secondary btn-xs" type="button" id="btn-clear-model">Clear</button>
					  </div>
					</div> 
					<div class="form-group">
						 <div class="data-tables">
							<table id="dataTableApplication-list-device-model" width="100%">
								<thead class="bg-light text-capitalize">
									<tr>
										<th style="width:30px;">No</th>
										<th>@lang('device-model.version')</th>
										<th>@lang('device-model.model')</th>
										<th>Vendor</th>
										<th>Country</th>
										<th>@lang('general.action')</th>
									</tr>
								</thead>
								<tbody>
								</tbody>
							</table>
						</div>
					 </div>
                </div>
				<div class="form-group" style="margin-top:0px;float:right;padding:0rem 1.7rem 0.3rem 0rem;">
                        <button type="button" class="btn btn-primary btn-rounded btn-xs" id="btn-add-md-to-list"> Add</button>
						<button class="btn btn-secondary btn-rounded btn-xs" data-dismiss="modal">@lang('general.close')</button>
						
                </div>
               
            </form>
        </div>
    </div>
</div>

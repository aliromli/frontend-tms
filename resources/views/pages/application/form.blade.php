@extends('layouts.app')
@section('title', 'Form Application')
@section('ribbon')
@endsection

<!-- Start datatable css -->
<link rel="stylesheet" type="text/css" href="{{ asset('assets/css/plugins/theme-material.css')}}">  
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.css" />

<style>

</style>
@section('content')

<div class="title-page">
    <div class="row">
            <div class="col-md-12">
                <ul class="breadcrumb mt-bread">
                    <li><a href="/application">APPLICATION</a></li>
                </ul>
            </div>
    </div> 
</div>
<div class="main-content-inner">
    <div class="row">
        <div class="col-12">
            <div class="card mb-tng">
                <div class="card-body">
                <form class="form-horizontal" id="form-application">
                
                <div class="modal-body">
					<div class="form-group">
							  <h4 class="title-form">{{ $edit=='no' ? 'Add  Application' : 'Edit  Application ' }}</h4>
							  <input type="hidden" class="form-control input-xs" id="application-id" value="{{$data ? $data['id'] : ''}}">
					</div>
                    <div class="form-row mt-4">
							 <div class="form-group col-md-4">
								<label for="">App Name</label>
								<input type="text" class="form-control input-sm" id="application-name" xstyle="width:70%;"  value="{{$data ? $data['name'] : ''}}">
							 </div>
							<div class="form-group col-md-2 offset-2">
								<label for="" id="label-apk">Apk File</label>
								<input type="file" name="apk" {{ $edit=='no' ? '' : 'readOnly'}}  class="form-control input-sm" id="application-apk">
							</div>
							<div class="form-group col-md-2">
								<label for="" id="label-icon">Icon File</label>
								<input type="file" name="icon"  class="form-control input-sm" id="application-icon">
							</div>
					</div>
					<div class="form-row">
							 <div class="form-group col-md-4">
								<label for="">@lang('application.packageName')</label>
								<input type="text" class="form-control input-sm" {{ $edit=='no' ? '' : ''}} id="application-packageName" xstyle="width:70%;"  value="{{$data ? $data['packageName'] : ''}}">
							 </div>
							<div class="form-group col-md-2 offset-2">
								<label for="">@lang('application.appVersion')</label>
								<input type="text" name="version_app" {{ $edit=='no' ? '' : ''}} class="form-control input-sm" id="application-appVersion" value="{{$data? $data['appVersion'] : ''}}">
							</div>
					</div>
					<div class="form-row">
						<div class="form-group col-md-4">
							<label for="">@lang('application.companyName')</label>
							 <input type="text" class="form-control input-xs" id="application-companyName" value="{{$data? $data['companyName'] : ''}}">
						</div>
						<div class="form-group col-md-2 offset-2">
							<label for="uninstallable">Uninstallable</label>
							<div class="form-check">
								<input class="form-check-input position-static" {{$data? json_encode($data['uninstallable'])=='true' ? 'checked' : '' : ''}} name="uninstallable" type="checkbox" id="uninstallable" value="true">
							</div>
						</div>
					</div>
					<div class="form-group">
                        <label for="" class="control-label">@lang('application.description')</label>
                        <textarea style="width:83%;" rows="3"  type="text" class="form-control" id="application-description">{{$data? $data['description'] : ''}}</textarea>
                    </div>
                 	<div class="form-group">
						<label for="fr-list-dm"><a href="#" class="btn btn-link text-secondary text-left" style="margin-left:-20px;text-decoration: none;display: inline;" id="btn-modal-list-d-model">Device Model <i class="fa fa-plus-square fa-lg" style="color:#717682;margin-left:20px;"></i></a></label>
						
						<ul id="fr-list-dm" class="rounded p-2" style="border: 1px solid rgba(170, 170, 170, .3); width:33%;">
                          <?php
                           if($edit!=='no' )
                           {
							  if($data['deviceModel'])
							  {
								$gh = $data['deviceModel'];
								foreach($gh as $k)
								{
									   $nameModel = '<span class="md">'.$k['modelName'].'</span>';
									   $uid = '<span class="uid" style="display:none;">'.$k['id'].'</span>';
									   $rem = '<i class="fa fa-trash-o btn-delete-list-tg-t" style="color:#717682;margin-left:20px;"></i>'; 
									   echo '<li class="ch-li">'.$nameModel.' '.$rem.' '.$uid.'</li>';
								}
							  }
							  
                           }
                          ?>
                        </ul>
					</div> 
					
					<div class="form-group row" id="div-m-version" style="{{ $edit=='no' ? 'display:none;':'display:none;'}}">
                        <label for="" class="control-label col-md-3">Version</label>
                        <div class="col-md-8"><input readOnly type="text" class="form-control input-xs" id="version-app" value="{{$data? $data['version'] : ''}}"></div>
                    </div>
					<div class="form-group mt-5">
						<button type="button" class="btn btn-rounded btn-primary" id="btn-submit-application"> {{ $edit=='no' ? 'ADD' : 'UPDATE'}}</button>
						<a class="btn btn-rounded btn-secondary" href="{{url('/application')}}">@lang('general.cancel')</a>
						<span class="loading"><img class="hidden" width="40" src="{{asset('assets/images/icon/loader.gif')}}"/></span>
					</div>
                </div>
               
            </form>
                   
                </div>
            </div>
        </div>

    </div>
</div>

@endsection


@section('modal')
    @include('pages.application.modal-list-md')
@endsection

@section('script')
<!-- Start datatable js -->
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js"></script>
<script src="https://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.18/js/dataTables.bootstrap4.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.2.3/js/responsive.bootstrap.min.js"></script>
<script src="{{ asset('assets/js/application.js') }}"></script>

@endsection

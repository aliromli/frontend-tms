@extends('layouts.app')
@section('title', 'Application')
@section('ribbon')
@endsection

<style>
#dataTableApplication {
    overflow-x: scroll;
    max-width: 40%;
    display: block;
    white-space: nowrap;
    height: 300px;
}  
</style>
@section('content')

<div class="title-page">
    <div class="row">
            <div class="col-md-12">
                <ul class="breadcrumb mt-bread">
                    <li><a href="/application">APPLICATION</a></li>
                </ul>
            </div>
    </div> 
</div>
<div class="main-content-inner">
    <div class="row">
        <div class="col-12">
            <div class="card mb-tng">
                <div class="card-body">
                    <div class="clear_filter">Clear Search</div>
                  
					<div class="form-row row mb-5"> 
                     	<div class="col-md-12">
							<div xclass="row-btn-top" style="display:flex;gap:30px; flex-direction:row;">
								<a  href="{{url('/application-form')}}" class="btn btn-link text-secondary text-left" style="text-decoration: none;"><i class="fa fa-plus-square fa-lg" style="color:#717682;"></i> &nbsp;Add Application</a> 
							
								<span style="display:flex;gap:30px; ">
									<input type="text" id="search-app-name" class="form-control  ml-4" style="width:70%;" placeholder="App Name"/>
									
                                    <input type="text" id="search-app-sn" class="form-control  ml-4" style="width:70%;" placeholder="SN"/>
									

                                    <select class="form-control w-60" id="select-device-model" style="height:calc(2.25rem + 6px);">
                                        <option value=""> -- Select Device Model --</option>		
                                        <?php
                                         if($deviceModel)
                                         {
                                            foreach($deviceModel as $model)
                                            {
                                                echo '<option value="'.$model["id"].'">'.$model["model"].'</option>';
                                            }
                                         }
                                        ?>
                                    </select>
								  
								</span>
                                <i class="ti-search"  id="btn-search-app-name" style="margin-left:3px;color:#000 !important;" xstyle="margin-top: 9px !important;" ></i>  


							</div>
						</div> 
                    </div>
                  
                    
                    
                    <div class="data-tables">
                        <table id="dataTableApplication" width="100%">
                            <thead class="bg-light text-capitalize">
                                <tr>
                                    <th style="width:30px;">No</th>
                                    <th>@lang('application.version')</th>
                                    <th>App Name</th>
									<th>@lang('application.appVersion')</th>
                                    <th>Device Model</th>
                                    <th>@lang('application.companyName')</th>
                                    <th>Size</th>
                                    <th>@lang('general.action')</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
@endsection

@section('modal')
    @include('pages.application.modalIcon')
@endsection


@section('script')
<!-- Start datatable js -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>

 <!-- Start datatable js -->
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js"></script>
<script src="https://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.18/js/dataTables.bootstrap4.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.2.3/js/responsive.bootstrap.min.js"></script>

<script src="{{ asset('assets/js/application.js') }}"></script>

@endsection

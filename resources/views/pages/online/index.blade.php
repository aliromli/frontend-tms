@extends('layouts.app')
@section('title', 'Online')
@section('ribbon')
@endsection

@section('content')
<div class="title-page">
    <div class="row">
            <div class="col-md-12">
                <ul class="breadcrumb mt-bread">
                    <li><a href="/online">ONLINE</a></li>
                </ul>
            </div>
    </div> 
</div>
<div class="main-content-inner">
    <div class="row">
        <div class="col-12 mt-5">
            <div class="card mb-tng2">
                <div class="card-body">
					<form class="form-horizontal" id="form-online">               
						<div class="modal-body">
							<div class="form-group">
								  <h4 class="title-form">Export Online Report</h4>
							</div>
							
							<div class="form-row">
								<div class="form-group col-md-4">
									<label for="download-task-downloadTimeType" class="control-label">Select Time</label>
									<input type="datetime-local" step=1  class="form-control input-xs" id="online-select-time">
								</div>
							</div>
							<div class="form-row">
								<div class="form-group col-md-4">
									<label for="" class="control-label">Export ALL  <input class="ml-3" type="checkbox" name="export_all_on" id="export_all_on" value="true"/>
									</label>
								</div>
							</div>
							<div class="form-group">
								<label for="select-tg-on"><a  href="#" class="btn btn-link text-secondary text-left" style="margin-left:-20px;text-decoration: none;display: inline;" id="btn-add-terminal-group-on">Terminal Groups <i class="fa fa-plus-square fa-lg" style="color:#717682;margin-left:20px;"></i></a></label>
								<select multiple class="form-control" id="select-tg-on" style="width:33%;">
									
								</select>
							</div>
							<div class="form-group">
								<label for="select-t-on"><a  href="#" class="btn btn-link text-secondary text-left" style="margin-left:-20px;text-decoration: none;display: inline;" id="btn-add-terminal-list-on">Terminal List <i class="fa fa-plus-square fa-lg" style="color:#717682;margin-left:20px;"></i></a></label>
								<select multiple class="form-control" id="select-t-on" style="width:33%;">
									
								</select>
							</div>
							<div class="form-group mt-5">
								 <a class="btn btn-rounded btn-primary" href="#" id="export-online">EXPORT</a>
							</div>
						</div>
					</form>
                </div>
            </div>
        </div>

    </div>
</div>
@endsection

@section('modal')
    @include('pages.online.tg-list')
    @include('pages.online.t-list')
@endsection


@section('script')
<!-- Start datatable js -->
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js"></script>
<script src="https://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.18/js/dataTables.bootstrap4.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.2.3/js/responsive.bootstrap.min.js"></script>
<script src="{{ asset('assets/js/online.js') }}"></script>
@endsection

@extends('layouts.app')
@section('title', 'Device Model')
@section('ribbon')
@endsection
<style>
#dataTableMerchant {
    overflow-x: scroll;
    max-width: 40%;
    display: block;
    white-space: nowrap;
}
</style>
@section('content')

<div class="title-page">
    <div class="row">
            <div class="col-md-12">
                <ul class="breadcrumb mt-bread">
                    <li><a href="/device-model">DEVICE MODEL </a></li>
                </ul>
            </div>
    </div> 
</div>
<div class="main-content-inner">
    <div class="row">
        <div class="col-12">
            <div class="card mb-tng">
                <div class="card-body">  
					 <div class="clear_filter">Clear Search</div>
                     <div class="box-header no-border text-left filter row mb-5"> 
                        @if(Privilege::visible(Request::segment(1),'CREATE')=='Y')
                        <a href="{{url('/device-model-form')}}" class="btn btn-link text-secondary col-md-1 text-left" style="text-decoration: none;"><i class="fa fa-plus-square fa-lg" style="color:#717682;"></i> &nbsp;Add Model</a> 
                        @endif
                        <input type="text" id="search-device" class="form-control form-control-sm col-md-4 text-left ml-5" placeholder="Keyword Model.."/>
                        <i class="ti-search" id="btn-ti-search-model-device"></i>  
                    </div>
                  
                     <!-- <div class="form-filter">
                        <form class="form-horizontal">
                                <div class="form-group row" xstyle="margin-left:-28;">
                                    <div class="col-md-3">
                                        <input type="text" id="search-model" class="form-control input-xs" placeholder="Model...">
                                    </div>
                                    <div class="col-md-3">
                                        <input type="text" id="search-vendor-name" class="form-control input-xs" placeholder="Vendor Name...">
                                    </div>
                                    <div class="col-md-3">
                                        <input type="text" id="search-vendor-country" class="form-control input-xs" placeholder="Vendor Country...">
                                    </div>
                                    <div class="col-md-3">
                                        <button type="button" id="btn-cari-model-device" class="btn  btn-flat btn-secondary input-sm" style="xbackground-color:transparent; height:3em;" ><i class="ti-search" style="font-size:22px;"></i></button>
                                    </div>
                                </div>
                        </form>
                    </div>  -->
                    <div class="data-tables">
                        <table id="dataTableDeviceModel" width="100%">
                            <thead class="bg-light text-capitalize">
                                <tr>
                                    <th style="width:30px;">No</th>
                                    <th>@lang('device-model.version')</th>
                                    <th>@lang('device-model.model')</th>
                                    <th>@lang('device-model.vendorName')</th>
                                    <th>@lang('device-model.vendorCountry')</th>
                                    <th>@lang('general.action')</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>

@endsection


@section('script')
 <!-- Start datatable js -->
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js"></script>
<script src="https://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.18/js/dataTables.bootstrap4.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.2.3/js/responsive.bootstrap.min.js"></script>
<script src="{{ asset('assets/js/device_model.js') }}"></script>
@endsection

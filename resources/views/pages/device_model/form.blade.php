@extends('layouts.app')
@section('title', 'Device Model')
@section('ribbon')
@endsection

@section('content')

<div class="title-page">
    <div class="row">
            <div class="col-md-12">
                <ul class="breadcrumb mt-bread">
                    <li><a href="/device-model">DEVICE MODEL</a></li>
                </ul>
            </div>
    </div> 
</div>
<div class="main-content-inner">
    <div class="row">
        <div class="col-12">
            <div class="card mb-tng">
                <div class="card-body">               
                <form class="form-horizontal" id="form-device-model">
                
                <div class="modal-body">
                    <div class="form-group">
                        <h4 class="title-form">{{ $edit=='no' ? 'Add New Model' : 'Edit Model ' }}</h4>
                    </div>
                    <div class="form-group">
                        <input type="hidden" class="form-control input-xs" id="dm-id" value="{{$data ? $data[0]['id'] : ''}}">
                        <label for="" class="control-label">Model Name</label>
                        <div class=""><input type="text" class="form-control input-xss" id="dm-model" value="{{$data ? $data[0]['model'] : ''}}"></div>
                    </div>
					<div class="form-group">
                        <label for="" class="control-label">Model Information</label>
                        <div class="">
                            <input type="text" class="form-control input-xss" id="dm-modelInformation" value="{{$data? $data[0]['modelInformation'] : ''}}"></div>
                        </div>					
                    <div class="form-group">
                        <label for="" class="control-label">@lang('device-model.vendorName')</label>
                        <div class=""><input  type="text"  class="form-control input-xss" id="dm-vendorName" value="{{$data? $data[0]['vendorName'] : ''}}"></div>
                    </div>
					<div class="form-group">
                        <label for="" class="control-label">@lang('device-model.vendorCountry')</label>
                        <div class=""><input type="text"  class="form-control input-xss" id="dm-vendorCountry" value="{{$data? $data[0]['vendorCountry'] : ''}}"></div>
                    </div>
					<div class="form-group" id="div-dm-version" style="{{ $edit=='no' ? 'display:none;':''}}">
                        <label for="" class="control-label">Version</label>
                        <div class=""><input readOnly type="text" class="form-control input-xs" id="version-dm" value="{{$data? $data[0]['version'] : ''}}"></div>
                    </div>
                    <div class="form-group">
                        <label for="" class="control-label">&nbsp;</label>
                        <div class="">
                            <button type="button" class="btn btn-rounded btn-primary" id="btn-submit-dm">@lang('general.save')</button>&nbsp;&nbsp;
							<a class="btn btn-secondary btn-rounded" href="{{url('/device-model')}}">@lang('general.cancel')</a>
				        </div>
                    </div>
					
                </div>
              
            </form>
                   
                </div>
            </div>
        </div>

    </div>
</div>

@endsection


@section('script')
 <!-- Start datatable js -->
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js"></script>
<script src="https://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.18/js/dataTables.bootstrap4.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.2.3/js/responsive.bootstrap.min.js"></script>
<script src="{{ asset('assets/js/device_model.js') }}"></script>

@endsection

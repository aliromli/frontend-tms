@extends('layouts.app')
@section('title', 'Task')
@section('ribbon')
@endsection

@section('content')
<div class="title-page">
    <div class="row">
            <div class="col-md-12">
                <ul class="breadcrumb mt-bread">
                    <li><a href="/task">TASK</a></li>
                </ul>
            </div>
    </div> 
</div>
<div class="main-content-inner">
    <div class="row">
        <div class="col-12 mt-5">
            <div class="card mb-tng2">
                <div class="card-body">
					<form class="form-horizontal" id="form-task">               
						<div class="modal-body">
							<div class="form-group">
								  <h4 class="title-form">Export Task Report</h4>
							</div>
							
							<div class="form-row">
								<div class="form-group col-md-4">
									<label for="task" class="control-label">Select Task</label>
									<select name="task" id="task" class="form-control form-control-sm">
										<option>SELECT</option>
									</select>
								</div>
							</div>
							
							<div class="form-group mt-5">
								 <a class="btn btn-rounded btn-primary " href="#" id="export-task">EXPORT</a>
							</div>
						</div>
					</form>
                </div>
            </div>
        </div>

    </div>
</div>
@endsection


@section('script')
<!-- Start datatable js -->
<script src="{{ asset('assets/js/task.js') }}"></script>
@endsection

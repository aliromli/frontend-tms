@extends('layouts.app')
@section('title', 'Terminal Template')
@section('ribbon')
@endsection

@section('content')
<div class="title-page">
    <div class="row">
            <div class="col-md-12">
                <ul class="breadcrumb mt-bread">
                     <li><a href="/terminal-template">TERMINAL TEMPLATE</a></li>
                </ul>
            </div>
    </div> 
</div>
<div class="main-content-inner">
    <div class="row">
        <div class="col-12 mt-5">
            <div class="card mb-tng2">
                <div class="card-body">               
					<form class="form-horizontal" id="form-tt">
						<div class="modal-body">
							<div class="form-group">
								<h4 class="title-form">{{ $edit=='no' ? 'Add Terminal Template' : 'Edit Terminal Template' }}</h4>
								<input type="hidden" class="form-control form-control-sm input-xs" id="tt-id" value="{{$data ? $data[0]['id'] : ''}}">
							</div>
							<div class="form-row mt-4">
								<div class="form-group col-md-4">
									<label for="" class="control-label">Name</label>
									<input type="text" class="form-control form-control-sm input-xs" id="tt-name" value="{{$data ? $data[0]['name'] : ''}}">
								</div>
								<div class="form-group col-md-4">
									<label for="" class="control-label">Description</label>
									<input type="text" class="form-control form-control-sm input-xs" id="tt-description" value="{{$data ? $data[0]['description'] : ''}}">
								</div>
							</div>
							<div class="form-row">
								<div class="form-group col-md-4">
									<label for="" class="control-label"><span style="color:#717682;font-weight:700;">Report & Logging</span></label>
								</div>								
							</div>
							<div class="form-row">
								<div class="form-group col-md-4">
									<label for="tt-host_report" class="control-label">Host Report</label>
									<div class="form-check">
										<input  id="tt-host_report" name="host_report" type="checkbox"  class="form-check-input position-static" {{$data? json_encode($data[0]['hostReport'])=='true' ? 'checked' : '' : ''}} value="true">
								    </div>
								</div>	
							</div>
							<div class="form-row">
								<div class="form-group col-md-4">
									<label for="" class="control-label">Host Report URL</label>
									<input type="text" class="form-control form-control-sm input-xs" id="tt-HostReportURL" value="{{$data ? $data[0]['hostReportUrl'] : ''}}">
								</div>								
							</div>
							<div class="form-row">
								<div class="form-group col-md-4">
									<label for="" class="control-label">Host Report API Key</label>
									<input type="text" class="form-control form-control-sm input-xs" id="tt-HostReportAPIKey" value="{{$data ? $data[0]['hostReportApiKey'] : ''}}">
								</div>								
							</div>
							<div class="form-row">
								<div class="form-group col-md-4">
									<label for="" class="control-label">Host Report Timeout (numeric)</label>
									<input type="text" class="form-control form-control-sm input-xs" id="tt-HostReportTimeout" value="{{$data ? $data[0]['hostReportTimeout'] : ''}}">
								</div>								
							</div>
							<div class="form-row">
								<div class="form-group col-md-4">
									<label for="tt-host_report" class="control-label">Host Logging</label>
									<div class="form-check">
										<input  id="tt-HostLogging" name="hostLogging" type="checkbox"  class="form-check-input position-static" {{$data? json_encode($data[0]['hostLogging'])=='true' ? 'checked' : '' : ''}} value="true">
								    </div>
								</div>	
							</div>
							<div class="form-row">
								<div class="form-group col-md-4">
									<label for="" class="control-label">Host Logging URL</label>
									<input type="text" class="form-control form-control-sm input-xs" id="tt-hostLoggingUrl" value="{{$data ? $data[0]['hostLoggingUrl'] : ''}}"> 
								</div>								
							</div>
							<div class="form-row">
								<div class="form-group col-md-4">
									<label for="" class="control-label">Host Logging API Key</label>
									<input type="text" class="form-control form-control-sm input-xs" id="tt-HostLoggingAPIKey" value="{{$data ? $data[0]['hostLoggingApiKey'] : ''}}">
								</div>								
							</div>
							<div class="form-row">
								<div class="form-group col-md-4">
									<label for="" class="control-label">Host Logging Timeout (numeric)</label>
									<input type="text" class="form-control form-control-sm input-xs" id="tt-HostLoggingTimeout" value="{{$data ? $data[0]['hostLoggingTimeout'] : ''}}">
								</div>								
							</div>
							<div class="form-row">
								<div class="form-group col-md-4">
									<label for="" class="control-label">Host Logging Interval (numeric)</label>
									<input type="text" class="form-control form-control-sm input-xs" id="tt-hostLoggingInterval" value="{{$data ? $data[0]['hostLoggingInterval'] : ''}}">
								</div>								
							</div>
							<div class="form-row">
								<div class="form-group col-md-4">
									<label for="" class="control-label"><span style="color:#717682;font-weight:700;">Application Menus</span></label>
								</div>								
							</div>
							<div class="form-row">
								<div class="form-group col-md-4">
									<label for="tt-host_report" class="control-label">Sale</label>
									<div class="form-check">
										<input  id="tt-Sale" name="tt_Sale" type="checkbox"  class="form-check-input position-static" {{$data? json_encode($data[0]['featureSale'])=='true' ? 'checked' : '' : ''}} value="true">
								    </div>
								</div>	
								<div class="form-group col-md-4">
									<label for="tt-host_report" class="control-label">Sale TIP</label>
									<div class="form-check">
										<input  id="tt-SaleTIP" name="SaleTIP" type="checkbox"  class="form-check-input position-static" {{$data? json_encode($data[0]['featureSaleTip'])=='true' ? 'checked' : '' : ''}} value="true">
								    </div>
								</div>	
								<div class="form-group col-md-4">
									<label for="tt-host_report" class="control-label">Sale Redemption</label>
									<div class="form-check">
										<input  id="tt-SaleRedemption" name="SaleRedemption" type="checkbox"  class="form-check-input position-static" {{$data? json_encode($data[0]['featureSaleRedemption'])=='true' ? 'checked' : '' : ''}} value="true">
								    </div>
								</div>	
							</div>
							<div class="form-row">
								<div class="form-group col-md-4">
									<label for="tt-host_report" class="control-label">Card Verification</label>
									<div class="form-check">
										<input  id="tt-CardVerification" name="CardVerification" type="checkbox"  class="form-check-input position-static" {{$data? json_encode($data[0]['featureCardVer'])=='true' ? 'checked' : '' : ''}} value="true">
								    </div>
								</div>	
								<div class="form-group col-md-4">
									<label for="tt-host_report" class="control-label">Sale Completion</label>
									<div class="form-check">
										<input  id="tt-SaleCompletion" name="SaleCompletion" type="checkbox"  class="form-check-input position-static" {{$data? json_encode($data[0]['featureSaleCompletion'])=='true' ? 'checked' : '' : ''}} value="true">
								    </div>
								</div>	
								<div class="form-group col-md-4">
									<label for="tt-host_report" class="control-label">Installment</label>
									<div class="form-check">
										<input  id="tt-Installment" name="Installment" type="checkbox"  class="form-check-input position-static" {{$data? json_encode($data[0]['featureInstallment'])=='true' ? 'checked' : '' : ''}} value="true">
								    </div>
								</div>	
							</div>
							<div class="form-row">
								<div class="form-group col-md-4">
									<label for="tt-host_report" class="control-label">Sale Fare non Fare</label>
									<div class="form-check">
										<input  id="tt-SaleFarenonFare" name="SaleFarenonFare" type="checkbox"  class="form-check-input position-static" {{$data? json_encode($data[0]['featureSaleFareNonFare'])=='true' ? 'checked' : '' : ''}} value="true">
								    </div>
								</div>	
								<div class="form-group col-md-4">
									<label for="tt-host_report" class="control-label">Manual Key in</label>
									<div class="form-check">
										<input  id="tt-ManualKeyin" name="ManualKeyin" type="checkbox"  class="form-check-input position-static" {{$data? json_encode($data[0]['featureManualKeyIn'])=='true' ? 'checked' : '' : ''}} value="true">
								    </div>
								</div>	
								<div class="form-group col-md-4">
									<label for="tt-host_report" class="control-label">QRIS</label>
									<div class="form-check">
										<input  id="tt-QRIS" name="QRIS" type="checkbox"  class="form-check-input position-static" {{$data? json_encode($data[0]['featureQris'])=='true' ? 'checked' : '' : ''}} value="true">
								    </div>
								</div>	
							</div>
							<div class="form-row">
								<div class="form-group col-md-4">
									<label for="tt-host_report" class="control-label">Contactless</label>
									<div class="form-check">
										<input  id="tt-Contactless" name="Contactless" type="checkbox"  class="form-check-input position-static" {{$data? json_encode($data[0]['featureContactless'])=='true' ? 'checked' : '' : ''}} value="true">
								    </div>
								</div>	
								
							</div>
							<div class="form-row">
								<div class="form-group col-md-4">
									<label for="" class="control-label"><span style="color:#717682;font-weight:700;">Terminal Options</span></label>
								</div>								
							</div>
							<div class="form-row">
								<div class="form-group col-md-4">
									<label for="tt-host_report" class="control-label">Fallback Enable</label>
									<div class="form-check">
										<input  id="tt-Fallbackenable" name="Fallbackenable" type="checkbox"  class="form-check-input position-static" {{$data? json_encode($data[0]['fallbackEnabled'])=='true' ? 'checked' : '' : ''}} value="true">
								    </div>
								</div>	
								<div class="form-group col-md-4">
									<label for="tt-host_report" class="control-label">Random PIN Keypad</label>
									<div class="form-check">
										<input  id="tt-RandomPINKeypad" name="RandomPINKeypad" type="checkbox"  class="form-check-input position-static" {{$data? json_encode($data[0]['randomPinKeypad'])=='true' ? 'checked' : '' : ''}} value="true">
								    </div>
								</div>	
							</div>
							<div class="form-row">
								<div class="form-group col-md-4">
									<label for="tt-host_report" class="control-label">Beep PIN Keypad</label>
									<div class="form-check">
										<input  id="tt-BeepPINKeypad" name="BeepPINKeypad" type="checkbox"  class="form-check-input position-static" {{$data? json_encode($data[0]['beepPinKeypad'])=='true' ? 'checked' : '' : ''}} value="true">
								    </div>
								</div>	
								<div class="form-group col-md-4">
									<label for="tt-host_report" class="control-label">Auto Logon</label>
									<div class="form-check">
										<input  id="tt-AutoLogon" name="AutoLogon" type="checkbox"  class="form-check-input position-static" {{$data? json_encode($data[0]['autoLogon'])=='true' ? 'checked' : '' : ''}} value="true">
								    </div>
								</div>	
							</div>
							<div class="form-row">
								<div class="form-group col-md-4">
									<label for="" class="control-label">Next Logon (in Hour)</label>
									<input type="text" class="form-control form-control-sm input-xs" id="tt-NextLogon" name="NextLogon" value="{{$data ? $data[0]['nextLogon'] : ''}}">
								</div>
								<div class="form-group col-md-4">
									<label for="" class="control-label">Push Logon</label>
									<div class="form-check">
										<input  id="tt-PushLogon" name="PushLogon" type="checkbox"  class="form-check-input position-static" {{$data? json_encode($data[0]['pushLogon'])=='1' ? 'checked' : '' : ''}} value="true">
								    </div>
								</div>
							</div>
							<div class="form-row">
								<div class="form-group col-md-4">
									<label for="" class="control-label">QRIS Countdown (seconds)</label>
									<input type="text" class="form-control form-control-sm input-xs" id="tt-QRISCountdown" value="{{$data ? $data[0]['qrisCountDown'] : ''}}">
								</div>
								<div class="form-group col-md-4">
									<label for="" class="control-label">Reprint Online Retry (numeric)</label>
									<input type="text" class="form-control form-control-sm input-xs" id="tt-ReprintOnlineRetry" value="{{$data ? $data[0]['reprintOnlineRetry'] : ''}}">
								</div>
							</div>
							<div class="form-row">
								<div class="form-group col-md-4">
									<label for="" class="control-label">Settle Warning Trx Count (numeric)</label>
									<input type="text" class="form-control form-control-sm input-xs" id="tt-SettleWarningTrxCount" value="{{$data ? $data[0]['settlementWarningTrxCount'] : ''}}">
								</div>
								<div class="form-group col-md-4">
									<label for="" class="control-label">Settle Max Trx Count (numeric)</label>
									<input type="text" class="form-control form-control-sm input-xs" id="tt-SettleMaxTrxCount" value="{{$data ? $data[0]['settlementMaxTrxCount'] : ''}}">
								</div>
							</div>
							<div class="form-row">
								<div class="form-group col-md-4">
									<label for="" class="control-label">Merchant Password</label>
									<input type="text" class="form-control form-control-sm input-xs" id="tt-MerchantPassword" value="{{$data ? $data[0]['merchantPassword'] : ''}}">
								</div>
								<div class="form-group col-md-4">
									<label for="" class="control-label">Admin Password</label>
									<input type="text" class="form-control form-control-sm input-xs" id="tt-AdminPassword" value="{{$data ? $data[0]['adminPassword'] : ''}}">
								</div>
							</div>
							<div class="form-row">
								<div class="form-group col-md-4">
									<label for="" class="control-label">Settlement Password</label>
									<input type="text" class="form-control form-control-sm input-xs" id="tt-SettlementPassword" value="{{$data ? $data[0]['settlementPassword'] : ''}}">
								</div>
								<div class="form-group col-md-4">
									<label for="" class="control-label">Void Password</label>
									<input type="text" class="form-control form-control-sm input-xs" id="tt-VoidPassword" value="{{$data ? $data[0]['voidPassword'] : ''}}">
								</div>
							</div>
							
							<div class="form-row">
								<div class="form-group col-md-4">
									<label for="" class="control-label">Brizzi discount percentage</label>
									<input type="text" class="form-control form-control-sm input-xs" id="tt-Brizzidiscountpercentage" value="{{$data ? $data[0]['brizziDiscountPercentage'] : ''}}">
								</div>
								<div class="form-group col-md-4">
									<label for="" class="control-label">Brizzi discount amount</label>
									<input type="text" class="form-control form-control-sm input-xs" id="tt-Brizzidiscountamount" value="{{$data ? $data[0]['brizziDiscountAmount'] : ''}}">
								</div>
							</div>
							<div class="form-row">
								<div class="form-group col-md-4">
									<label for="" class="control-label">Installment 1 Options</label>
									<input type="text" class="form-control form-control-sm input-xs" id="tt-InstallmentOptions1" value="{{$data ? $data[0]['installment1Options'] : ''}}">
								</div>
								
							</div>
							<div class="form-row">
								<div class="form-group col-md-4">
									<label for="" class="control-label">Installment 2 Options</label>
									<input type="text" class="form-control form-control-sm input-xs" id="tt-InstallmentOptions2" value="{{$data ? $data[0]['installment2Options'] : ''}}">
								</div>
								
							</div>
							<div class="form-row">
								<div class="form-group col-md-4">
									<label for="" class="control-label">Installment 3 Options</label>
									<input type="text" class="form-control form-control-sm input-xs" id="tt-InstallmentOptions3" value="{{$data ? $data[0]['installment3Options'] : ''}}">
								</div>
								
							</div>
							<div class="form-row">
								<div class="form-group col-md-4">
									<label for="" class="control-label">Call Center 1</label>
									<input type="text" class="form-control form-control-sm input-xs" id="tt-callCenter1" value="{{$data ? $data[0]['callCenter1'] : ''}}">
								</div>
								
							</div>
							<div class="form-row">
								<div class="form-group col-md-4">
									<label for="" class="control-label">Call Center 2</label>
									<input type="text" class="form-control form-control-sm input-xs" id="tt-callCenter2" value="{{$data ? $data[0]['callCenter2'] : ''}}">
								</div>
								
							</div>
							<div class="form-group">
								<label for="exampleFormControlSelect-tg"><a  href="#" class="btn btn-link text-secondary text-left" style="margin-left:-20px;text-decoration: none;display: inline;" id="btn-add-acquirer-modal-tt">Add Acquirer <i class="fa fa-plus-square fa-lg" style="color:#717682;margin-left:20px;"></i></a></label>
							</div>
							<div class="form-row">
							    <div class="form-group col-md-8">
									<table class="table  table-striped" id="acquirer-tt-list">
										<thead>
											<tr>
												<th scope="col">No</th>
												<th scope="col">Name</th>
												<th scope="col">Acquirer Type</th>
												<th scope="col">Acquirer ID</th>
												<th scope="col">Description</th>
												<th scope="col">Action</th>
											</tr>
										</thead>
										<tbody>
										<?php
											if(!empty($data[0]['acquirer']))
											{
												$no = 0;
												foreach($data[0]['acquirer'] as $t)
												{
														$no = $no + 1;
														$uid = $t['id'];
														$name = $t['name'];
														$type = $t['type'];
														$acquirerId = $t['acquirerId'];
														$description = $t['description'];
														$act = '<a href="#" class="btn-delete"><img style="width:0.7rem;" class="trash" src="/assets/images/icon/trash.png" /></a>';
														$d='<tr class="tr-dt">';
																$d.='<td><span class="no">'.$no.'</span></td>';
																$d.='<td><span class="name">'.$name.'</span></td>';
																$d.='<td><span class="type">'.$type.'</span></td>';
																$d.='<td><span class="acquirerId">'.$acquirerId.'</span></td>';
																$d.='<td><span class="description">'.$description.'</span><span class="uid" style="display:none;">'.$uid.'</span></td>';
																$d.='<td>'.$act.'</td>';
														$d.='</tr>';
														echo $d;
												}
											}
										
										?>
										</tbody>
									</table>
								</div>
							</div>
							
							<div class="form-group row" id="div-dp-version" style="{{ $edit=='no' ? 'display:none;':'display:none;'}}">
								<label for="" class="control-label col-md-3">Version</label>
								<div class="col-md-8"><input type="text" class="form-control form-control-sm input-xs" readOnly id="version-tt" value="{{$data? $data[0]['version'] : ''}}"></div>
							</div>
							<div class="form-group  mt-4">
								<button type="button" class="btn btn-rounded btn-primary btn-xs" id="btn-submit-ttem"> @lang('general.save')</button>
								<a class="btn btn-rounded btn-secondary btn-xs" href="{{url('/terminal-template')}}">@lang('general.cancel')</a>
								<span class="loading"><img class="hidden" width="40" src="{{asset('assets/images/icon/loader.gif')}}"/></span>
							</div>
						</div>
						
					</form>
                 
                </div>
            </div>
        </div>

    </div>
</div>

@endsection
@section('modal')
    @include('pages.terminal_template.modal_list_acquirer')
@endsection
@section('script')
<!-- Start datatable js -->
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js"></script>
<script src="https://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.18/js/dataTables.bootstrap4.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.2.3/js/responsive.bootstrap.min.js"></script>
<script src="{{ asset('assets/js/template/terminal_template.js') }}"></script>

@endsection

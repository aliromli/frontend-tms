@extends('layouts.app')
@section('title', 'Form State')
@section('ribbon')
@endsection

<!-- Start datatable css -->
@section('content')

<div class="title-page">
    <div class="row">
            <div class="col-md-12">
                <ul class="breadcrumb mt-bread">
                     <li><a href="/state">STATE</a></li>
                </ul>
            </div>
    </div> 
</div>

<div class="main-content-inner">
    <div class="row">
        <div class="col-12">
            <div class="card mb-tng">
                <div class="card-body">               
                     <form class="form-horizontal" id="form-state">
						<div class="modal-header no-border">
							<h4 class="modal-title">{{ $edit=='no' ? 'Add State' : 'Edit State' }}</h4>
						</div>
						<div class="modal-body">
							<div class="form-group">
								<input type="hidden" class="form-control form-control-sm input-xs w-50" id="state-id" value="{{$data ? $data[0]['id'] : ''}}">
								<label for="name">@lang('state.name')</label>
								<input type="text" class="form-control form-control-sm input-xs w-50" id="state-name" value="{{$data ? $data[0]['name'] : ''}}"  placeholder="">
                                <input type="hidden" class="form-control form-control-sm input-xs w-50" id="country-opt-id" value="{{$data ? $data[0]['country']['id'] : ''}}"  placeholder="">
						    
                            </div>
							<div class="form-group">
								<label for="">@lang('state.country')</label>
								<div>
									<select class="form-control form-control-sm input-xs" id="state-country" style="width:50%;">
										<option value="">&raquo; @lang('general.select') Country</option>
										@if(!empty($country))
											@foreach($country as $c)
											<option value="{{ $c['id'] }}">{{ $c['name'] }}</option>
											@endforeach
										@endif
									</select>
								</div>
								
							</div>
							<div class="form-group" id="divct-version" style="{{ $edit=='no' ? 'display:none;':'display:none;'}}">
								<label for="" class="">Version</label>
								<input readOnly type="text" class="form-control form-control-sm input-xs" id="version-st" value="{{$data ? $data[0]['version'] : ''}}">
							</div>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-rounded btn-primary" id="btn-submit">@lang('general.save')</button>
							<a class="btn btn-secondary btn-rounded" href="{{url('/state')}}">@lang('general.cancel')</a>
						</div>
					</form>
                   
                </div>
            </div>
        </div>

    </div>
</div>

@endsection

@section('script')
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
<script src="{{ asset('assets/js/state.js') }}"></script>

@endsection

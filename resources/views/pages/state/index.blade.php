@extends('layouts.app')
@section('title', 'State')
@section('ribbon')
@endsection

@section('content')

<div class="title-page">
    <div class="row">
            <div class="col-md-12">
                <ul class="breadcrumb mt-bread">
                    <li><a href="/state">STATE</a></li>
                </ul>
            </div>
    </div> 
</div>
<div class="main-content-inner">
    <div class="row">
        <div class="col-12">
            <div class="card mb-tng">
                <div class="card-body">
                    <div class="clear_filter">Clear Search</div>
                    <div class="box-header no-border text-left filter row mb-5"> 
                        @if(Privilege::visible(Request::segment(1),'CREATE')=='Y')
                        <button class="btn btn-link text-secondary col-md-1 text-left" style="text-decoration: none;" id="btn-add-state"><i class="fa fa-plus-square fa-lg" style="color:#717682;"></i> &nbsp;Add State</button> 
                        @endif
                        <input type="text" id="search-state" class="form-control form-control-sm col-md-4 text-left ml-5" placeholder="Keyword State.."/>
                        <i class="ti-search" id="btn-ti-searc-state"></i>
                    </div>
                  
                    <div class="form-filter">
                        <form class="form-horizontal">
                           
                                <div class="form-group row" xstyle="margin-left:-28;">
                                   
                                    <div class="col-md-4"><input type="text" id="search-state" class="form-control input-xs" placeholder="State..."></div>
                                    <div class="col-md-4">
                                        <select class="form-control input-xs" id="search-state-country">
                                            <option value="">&raquo; @lang('general.select') @lang('state.name')</option>
                                            @if(!empty($country))
                                                @foreach($country as $c)
                                                <option value="{{ $c['id'] }}">{{ $c['name'] }}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                    <div class="col-md-3">
                                        <button type="button" id="btn-cari-state" class="btn  btn-flat btn-secondary input-sm" style="xbackground-color:transparent; height:3em;" ><i class="ti-search" style="font-size:22px;"></i></button>
                                    </div>
                                </div>
                        </form>
                    </div>
                    <div class="data-tables">
                        <table id="dataTableState"  xclass="text-center" width="100%">
                            <thead class="bg-light text-capitalize">
                                <tr>
                                    <th style="width:30px;">No</th>
                                    <th>@lang('state.version')</th>
                                    <th>@lang('state.name')</th>
                                    <th>@lang('state.country')</th>
                                    <th>@lang('general.action')</th>
                                </tr>
                            </thead>
                            <tbody>
                               
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>

@endsection

@section('script')
 <!-- Start datatable js -->
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js"></script>
<script src="https://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.18/js/dataTables.bootstrap4.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.2.3/js/responsive.bootstrap.min.js"></script>
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
<script src="{{ asset('assets/js/state.js') }}"></script>

@endsection

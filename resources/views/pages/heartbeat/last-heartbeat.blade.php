@extends('layouts.app')
@section('title', 'HeartBeat')
@section('ribbon')
@endsection

<style>


#dataTableLastDiagnostic{
    overflow-x: scroll;
    max-width: 40%;
    display: block;
    white-space: nowrap;
}
</style>
@section('content')

<div class="title-page">
    <div class="row">
            <div class="col-md-12">
                <ul class="breadcrumb mt-bread">
                    <li><a href="/heartbeat">HEARTBEAT</a></li>
                </ul>
            </div>
    </div> 
</div>
<div class="main-content-inner">
    <div class="row">
        <div class="col-12 mt-5">
            <div class="card mb-tng2">
                <div class="card-body">
					<form class="form-horizontal" id="form-last-heartbeat">               
						<div class="modal-body">
							<div class="form-group">
								  <h4 class="title-form">Export Last Heartbeat Report</h4>
							</div>
							<div class="form-row">
								<div class="form-group col-md-4">
									<label for="download-task-downloadTimeType" class="control-label">Select Time</label>
									<input type="date"  class="form-control form-control-sm input-xs" id="heartbeat-select-time" disabled>
								</div>
							</div>
							<div class="form-row">
								<div class="form-group col-md-4">
									<label for="" class="control-label">Export ALL <input class="ml-3" type="checkbox" name="export_all_hb" id="export_all_hb" value="true" checked/>
									</label>
								</div>
							</div>
							<div class="form-group">
								<label for="terminal-group-list-hb"><a  href="#" class="btn btn-link text-secondary text-left" style="margin-left:-20px;text-decoration: none;display: inline;" id="btn-add-terminal-group-hb">Terminal Groups <i class="fa fa-plus-square fa-lg" style="color:#717682;margin-left:20px;"></i></a></label>
								<select multiple class="form-control form-control-sm" id="terminal-group-list-hb" style="width:33%;" disabled>									
								</select>
							</div>
							<div class="form-group">
								<label for="terminal-list-hb"><a  href="#" class="btn btn-link text-secondary text-left" style="margin-left:-20px;text-decoration: none;display: inline;" id="btn-add-terminal-list-hb">Terminal List <i class="fa fa-plus-square fa-lg" style="color:#717682;margin-left:20px;"></i></a></label>
								<select multiple class="form-control form-control-sm" id="terminal-list-hb" style="width:33%;" disabled>
								</select>
							</div>
							<div class="form-group mt-5">
								 <a class="btn btn-rounded btn-primary btn-xs" href="#" id="export-heartbeat">EXPORT</a>
							</div>
						</div>
					</form>
                </div>
            </div>
        </div>

    </div>
</div>
@endsection
@section('modal')
    @include('pages.heartbeat.tg-list')
    @include('pages.heartbeat.t-list')
@endsection

@section('script')
<!-- Start datatable js -->
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js"></script>
<script src="https://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.18/js/dataTables.bootstrap4.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.2.3/js/responsive.bootstrap.min.js"></script>
<script src="{{ asset('assets/js/heartbeat.js') }}"></script>
@endsection

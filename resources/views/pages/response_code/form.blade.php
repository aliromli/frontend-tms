@extends('layouts.app')
@section('title', 'Response Code')
@section('ribbon')
@endsection

@section('content')
<div class="title-page">
    <div class="row">
            <div class="col-md-12">
                <ul class="breadcrumb mt-bread">
                     <li><a href="/response-code">RESPONSE CODES</a></li>
                </ul>
            </div>
    </div> 
</div>
<div class="main-content-inner">
    <div class="row">
        <div class="col-12 mt-5">
            <div class="card mb-tng2">
                <div class="card-body">               
					<form class="form-horizontal" id="form-rc">
						
						<div class="modal-body">
							<div class="form-group">
								<h4 class="title-form">{{ $edit=='no' ? 'Add Response Codes' : 'Edit Response Code' }}</h4>
								<input type="hidden" class="form-control input-xs" id="rc-id" value="{{$data ? $data[0]['id'] : ''}}">
							</div>
							<div class="form-row mt-4">
								<div class="form-group col-md-4">
									<label for="" class="control-label">Transaction Type</label>
									<select class="form-control form-control-sm input-xs" name="tt" id="tt">
										<?php
											
											if($edit=="yes")
											{
												foreach($type as $t){
													if($t==$data[0]['type'])
													{
														echo "<option value='".$t."' selected>$t</option>";
													}
													else
													{
														echo "<option value='".$t."'>$t</option>";
													}		
													
												}
												echo "<option value=''>-- Select --</option>";
											}	
											else
											{
												
												foreach($type as $t){
													echo "<option value='".$t."'>$t</option>";
												}
											}	
											
											
										?>
									</select>
								</div>								
							</div>
							<div class="form-row">
								<div class="form-group col-md-4">
									<label for="" class="control-label">RC</label>
									<input type="text" class="form-control form-control-sm input-xs" id="rc-RC" value="{{$data ? $data[0]['code'] : ''}}">
								</div>								
							</div>
							<div class="form-row">
								<div class="form-group col-md-6">
									<label for="" class="control-label">Message</label>
									<textarea class="form-control form-control-sm" name="description" id="description-rc">{{$data ? $data[0]['description'] : ''}}</textarea>
								</div>								
							</div>
							<div class="form-group row" id="div-dp-version" style="{{ $edit=='no' ? 'display:none;':'display:none;'}}">
								<label for="" class="control-label col-md-3">Version</label>
								<div class="col-md-8"><input type="text" class="form-control input-xs" readOnly id="version-rc" value="{{$data? $data[0]['version'] : ''}}"></div>
							</div>
							<div class="form-group  mt-4">
								<button type="button" class="btn btn-rounded btn-primary btn-xs" id="btn-submit-rc"> @lang('general.save')</button>
								<a class="btn btn-rounded btn-secondary btn-xs" href="{{url('/response-code')}}">@lang('general.cancel')</a>
								<span class="loading"><img class="hidden" width="40" src="{{asset('assets/images/icon/loader.gif')}}"/></span>
							</div>
						</div>
						
					</form>
                 
                </div>
            </div>
        </div>

    </div>
</div>

@endsection


@section('script')
 <!-- Start datatable js -->

<script src="{{ asset('assets/js/response_code.js') }}"></script>

@endsection

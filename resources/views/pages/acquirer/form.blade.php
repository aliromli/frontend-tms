@extends('layouts.app')
@section('title', 'Acquirer')
@section('ribbon')
@endsection

<!-- Start datatable css -->
@section('content')
<div class="title-page">
    <div class="row">
            <div class="col-md-12">
                <ul class="breadcrumb mt-bread">
                    <li><a href="/acquirer">ACQUIRER</a></li>
                </ul>
            </div>
    </div> 
</div>
	
<div class="main-content-inner">
    <div class="row">
        <div class="col-12 mt-5">
            <div class="card mb-tng2">
                <div class="card-body">               
                    <form class="form-horizontal" id="form-ac">
						<div class="modal-body">
							<div class="form-group">
								<h4 class="title-form">{{ $edit=='no' ? 'Add Acquirer' : 'Edit Acquirer' }}</h4>
								<input type="hidden" class="form-control form-control-sm input-xs" id="ac-id" value="{{$data ? $data[0]['id'] : ''}}">
							</div>
							<div class="form-row">
								<div class="form-group col-md-4">
									<label for="" class="control-label">Name</label>
									<input type="text" class="form-control form-control-sm input-xs" id="ac-name" value="{{$data ? $data[0]['name'] : ''}}">
								</div>
								<div class="form-group col-md-4 offset-2">
									<label for="" class="control-label">Acquirer Type</label>
									<select name="type"  id="ac-type" class="form-control form-control-sm input-xs" >
										<?php
											if($edit=='yes')
											{
												foreach($type as $t)
												{
													if($data[0]['type']==$t)
													{
														echo "<option value='".$t."' selected>".$t."</option>";
													}
													else
													{
														echo "<option value='".$t."'>".$t."</option>";
													}	
												}
												echo "<option value=''>--Pilih--</option>";	
											}
											else
											{
												echo "<option value=''>--Pilih--</option>";
												foreach($type as $t)
												{
													echo "<option value='".$t."'>".$t."</option>";
												}
											}
										?>
										
									</select>
								</div>
							</div>
							<div class="form-row">
								<div class="form-group col-md-4">
									<label for="" class="control-label">Acquirer ID</label>
									<input type="text" class="form-control form-control-sm input-xs" id="ac-acquirerId" value="{{$data ? $data[0]['acquirerId'] : ''}}">
								</div>
								<div class="form-group col-md-4 offset-2">
									<label for="" class="control-label">Batch Number</label>
									<input type="text" class="form-control form-control-sm input-xs" id="ac-batchNumber" value="{{$data? $data[0]['batchNumber'] : ''}}">
								</div>
							</div>
							<div class="form-row">
								<div class="form-group col-md-10">
									<label for="" class="control-label">Description</label>
									<textarea class="form-control form-control-sm input-xs" id="ac-description"> {{$data ? $data[0]['description'] : ''}}</textarea>
								</div>
							</div>
							<div class="form-row">
								<div class="form-group col-md-4">
									<label for="" class="control-label">Host Id</label>
									<input type="text" class="form-control form-control-sm input-xs" id="ac-hostId" value="{{$data ? $data[0]['hostId'] : ''}}">
								</div>
								<div class="form-group col-md-4 offset-2">
									<label for="" class="control-label">Settlement Host Id</label>
									<input type="text" class="form-control form-control-sm input-xs" id="ac-settlementHostId" value="{{$data? $data[0]['settlementHostId'] : ''}}">
								</div>
							</div>
							
							<div class="form-row">
								<div class="form-group col-md-4">
									<label for="" class="control-label">Host Destination Address</label>
									<input type="text" class="form-control form-control-sm input-xs" id="ac-hostDestAddr" value="{{$data ? $data[0]['hostDestAddr'] : ''}}">
								</div>
								<div class="form-group col-md-4 offset-2">
									<label for="" class="control-label">Host Destination Port</label>
									<input type="text" class="form-control form-control-sm input-xs" id="ac-hostDestPort" value="{{$data? $data[0]['hostDestPort'] : ''}}">
								</div>
							</div>
							<div class="form-row">
								<div class="form-group col-md-4">
									<label for="" class="control-label">Number of Print</label>
									<input type="text" class="form-control form-control-sm input-xs" id="ac-numberOfPrint" value="{{$data ? $data[0]['numberOfPrint'] : ''}}">
								</div>
								<div class="form-group col-md-4 offset-2">
									<label for="" class="control-label">Response Timeout</label>
									<input type="text" class="form-control form-control-sm input-xs" id="ac-respTimeout" value="{{$data? $data[0]['respTimeout'] : ''}}">
								</div>
							</div>
							
							<div class="form-row">
								<div class="form-group col-md-4">
									<label for="debitSettlement">Debit Settlement</label>
									 <div class="form-check">
											<input  id="ac-debitSettlement" name="ac-debitSettlement"  class="form-check-input position-static" <?php echo $data? json_encode($data[0]['debitSettlement'])=='true' ? 'checked' : '' : ''; ?>  type="checkbox"  value="true">
									</div>
								 </div>
								<div class="form-group col-md-2 offset-2">
										<label for="creditSettlement">Credit Settlement</label>
										<div class="form-check">
											<input class="form-check-input position-static" {{$data? json_encode($data[0]['creditSettlement'])=='true' ? 'checked' : '' : ''}} name="creditSettlement" type="checkbox" id="ac-creditSettlement" value="true">
										</div>
								</div>
								<div class="form-group col-md-2 offset-2">
										<label for="showPrintExpDate">Show Print Exp Date</label>
										<div class="form-check">
											<input class="form-check-input position-static" {{$data? json_encode($data[0]['showPrintExpDate'])=='true' ? 'checked' : '' : ''}} name="showPrintExpDate" type="checkbox" id="ac-showPrintExpDate" value="true">
										</div>
								</div>
							</div>
							<div class="form-row">
								<div class="form-group col-md-4">
									<label for="checkCardExpDate">Check Card Exp Date</label>
									 <div class="form-check">
											<input  id="ac-checkCardExpDate" name="ac-checkCardExpDate"  class="form-check-input position-static" <?php echo $data? json_encode($data[0]['checkCardExpDate'])=='true' ? 'checked' : '' : ''; ?>  type="checkbox" value="true">
									</div>
								 </div>
								<div class="form-group col-md-2 offset-2">
									<label for="tleAcquirer">TLE Acquirer</label>
									<div class="form-check">
										<input class="form-check-input position-static" <?php echo $data? json_encode($data[0]['tleAcquirer'])=='true' ? 'checked' : '' : ''; ?> name="tleAcquirer" type="checkbox" id="ac-tleAcquirer" value="true">
									</div>
								</div>
							</div>
							<div class="form-row">
								<div class="form-group col-md-4">
									<label for="" class="control-label">TLE Settings</label>
									<select name="tleSettingId"  id="ac-tleSettingId" class="form-control form-control-sm input-xs">
									<?php
										if(!empty($tleSetting))
										{
											if($edit=='yes')
											{	
												foreach($tleSetting as $tle)
												{
													if($data[0]['tleSetting']['id']==$tle['id'])
													{
														echo "<option value='".$tle['id']."' selected>".$tle['tleId']."</option>";
													}
													else
													{
														echo "<option value='".$tle['id']."'>".$tle['tleId']."</option>";
													}	
												}
												echo "<option value=''>--Pilih--</option>";
											}
											else
											{
												echo "<option value=''>--Pilih--</option>";
												foreach($tleSetting as $tle)
												{
													echo "<option value='".$tle['id']."'>".$tle['tleId']."</option>";
												}
											}	
										}	
										
									?>
									</select>
									
								</div>
							</div>
							<div class="form-row">
								<div class="form-group col-md-4">
									<label for="" class="control-label">Master Key Location</label>
									
									<input type="text" class="form-control form-control-sm input-xs" id="ac-masterKeyLocation" value="{{$data ? $data[0]['masterKeyLocation'] : ''}}">
								</div>
							</div>
							<div class="form-row">
								<div class="form-group col-md-4">
									<label for="" class="control-label">Master Key</label>
									<input type="text" class="form-control form-control-sm input-xs" id="ac-masterKey" value="{{$data ? $data[0]['masterKey'] : ''}}">
								</div>
							</div>
							<div class="form-row">
								<div class="form-group col-md-4">
									<label for="" class="control-label">Working Key</label>
									<input type="text" class="form-control form-control-sm input-xs" id="ac-workingKey" value="{{$data ? $data[0]['workingKey'] : ''}}">
								</div>
							</div>
							<div class="form-group row" id="div-dp-version" style="{{ $edit=='no' ? 'display:none;':'display:none;'}}">
								<label for="" class="control-label col-md-3">Version</label>
								<div class="col-md-8"><input type="text" class="form-control form-control-sm input-xs" readOnly id="version-ac" value="{{$data? $data[0]['version'] : ''}}"></div>
							</div>
							<div class="form-group  mt-4">
								<button type="button" class="btn btn-rounded btn-primary" id="btn-submit-ac"> @lang('general.save')</button>
								<a class="btn btn-rounded btn-secondary" href="{{url('/acquirer')}}">@lang('general.cancel')</a>
								<span class="loading"><img class="hidden" width="40" src="{{asset('assets/images/icon/loader.gif')}}"/></span>
							</div>
						</div>
					</form>
                </div>
            </div>
        </div>

    </div>
</div>

@endsection


@section('script')
 <!-- Start datatable js -->
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js"></script>
<script src="https://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.18/js/dataTables.bootstrap4.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.2.3/js/responsive.bootstrap.min.js"></script>
<script src="{{ asset('assets/js/acquirer.js') }}"></script>

@endsection

@extends('layouts.app')
@section('title', 'Acquirer')
@section('ribbon')
@endsection

<!-- Start datatable css -->
<style>
#dataTableAcquirer {
    overflow-x: scroll;
    max-width: 40%;
    display: block;
    white-space: nowrap;
}
</style>
@section('content')

<div class="title-page">
    <div class="row">
            <div class="col-md-12">
                <ul class="breadcrumb mt-bread">
                    <li><a href="/acquirer">ACQUIRER</a></li>
                </ul>
            </div>
    </div> 
</div>
<div class="main-content-inner">
    <div class="row">
        <div class="col-12 mt-5">
            <div class="card mb-tng2">
                <div class="card-body">
				   <div class="clear_filter">Clear Search</div>
                   <div class="form-row row mb-5"> 
                      	<div class="col-md-12">
							<div class="row-btn-top">
								<a  href="{{url('/acquirer-form')}}" class="btn btn-link text-secondary text-left" style="text-decoration: none;display: inline;" id="btn-add"><i class="fa fa-plus-square fa-lg" style="color:#717682;"></i> &nbsp;Add Acquirer</a> 
								<span>
									 <input type="text" id="search-acq" class="form-control form-control-sm text-left ml-4" style="width:30%;display: inline;" placeholder="Keyword Name.."/>
									 <i class="ti-search" id="btn-search-acq" ></i>  
								</span>
							</div>
						</div> 
                    </div>
                    <div class="data-tables">
                        <table id="dataTableAcquirer" width="100%">
                            <thead class="bg-light text-capitalize">
                                <tr>
                                    <th style="width:30px;">No</th>
                                    <th>Version</th>
                                    <th>Name</th>
                                    <th>Acquirer Type</th>
                                    <th>Acquirer ID</th>
                                    <th>Description</th>
                                    <th>@lang('general.action')</th>
						        </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
@endsection


@section('script')
 <!-- Start datatable js -->
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js"></script>
<script src="https://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.18/js/dataTables.bootstrap4.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.2.3/js/responsive.bootstrap.min.js"></script>
<script src="{{ asset('assets/js/acquirer.js') }}"></script>

@endsection

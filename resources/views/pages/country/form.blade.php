@extends('layouts.app')
@section('title', 'Country')
@section('ribbon')
@endsection

@section('content')

<div class="title-page">
    <div class="row">
            <div class="col-md-12">
                <ul class="breadcrumb mt-bread">
                    <li><a href="/country">COUNTRY</a></li>
                </ul>
            </div>
    </div> 
</div>
<div class="main-content-inner">
    <div class="row">
        <div class="col-12">
            <div class="card mb-tng">
                <div class="card-body" >
					<form class="form-horizontal" id="form-country">
						<div class="modal-header no-border">
							<h4 class="modal-title">{{ $edit=='no' ? 'Add Country' : 'Edit Country ' }}</h4>
						</div>
						<div class="modal-body">
							<div class="form-group">
								<input type="hidden" class="form-control form-control-sm input-xs w-50" id="country-id" value="{{$data ? $data[0]['id'] : ''}}">
								<label for="name">@lang('country.code')</label>
								<input type="text" class="form-control form-control-sm input-xs w-50" id="code" value="{{$data ? $data[0]['code'] : ''}}"  placeholder="">
							</div>
							<div class="form-group">
								<label for="name">@lang('country.name')</label>
								<input type="text" class="form-control form-control-sm input-xs w-50" id="name" value="{{$data ? $data[0]['name'] : ''}}"  placeholder="">
							</div>
							<div class="form-group" id="divct-version" style="display:none;">
								<label for="" class="">Version</label>
								<input readOnly type="text" class="form-control input-xs" id="version" value="{{$data ? $data[0]['version'] : ''}}">
							</div>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-rounded btn-primary" id="btn-submit">@lang('general.save')</button>
							<a class="btn btn-secondary btn-rounded" href="{{url('/country')}}">@lang('general.cancel')</a>
						</div>
					</form>
                   
                </div>
            </div>
        </div>

    </div>
</div>

@endsection
@section('script')
<script src="{{ asset('assets/js/country/country.js') }}"></script>
@endsection
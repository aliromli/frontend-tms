@extends('layouts.app')

@section('title', 'User Role')

@section('ribbon')
<ul class="breadcrumb">
    <li>Setting</li>
    <li>User Role</li>
</ul>
@endsection

@section('content')
<section>
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="jarviswidget jarviswidget-color-darken" id="wid-id-1" data-widget-editbutton="false">                
                <header>
                    <span class="widget-icon"> <i class="fa fa-table"></i></span>
                    <h2>On Process</h2>
                </header>
                <div>
                    <div class="widget-body no-padding">
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section>
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
            <div class="jarviswidget jarviswidget-color-darken" id="wid-id-1" data-widget-editbutton="false">                
                <header>
                    <span class="widget-icon"> <i class="fa fa-table"></i></span>
                    <h2>On Process</h2>
                </header>
                <div>
                    <div class="widget-body no-padding">
                        
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
            <div class="jarviswidget jarviswidget-color-darken" id="wid-id-1" data-widget-editbutton="false">                
                <header>
                    <span class="widget-icon"> <i class="fa fa-table"></i></span>
                    <h2>On Process</h2>
                </header>
                <div>
                    <div class="widget-body no-padding">
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@endsection
<div class="modal fade" id="modal-vendor">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="page-loader hidden">
                <div class="sk-circle">
                    <div class="sk-circle1 sk-child"></div>
                    <div class="sk-circle2 sk-child"></div>
                    <div class="sk-circle3 sk-child"></div>
                    <div class="sk-circle4 sk-child"></div>
                    <div class="sk-circle5 sk-child"></div>
                    <div class="sk-circle6 sk-child"></div>
                    <div class="sk-circle7 sk-child"></div>
                    <div class="sk-circle8 sk-child"></div>
                    <div class="sk-circle9 sk-child"></div>
                    <div class="sk-circle10 sk-child"></div>
                    <div class="sk-circle11 sk-child"></div>
                    <div class="sk-circle12 sk-child"></div>
                </div>
            </div>
            <form class="form-horizontal" id="form-vendor">
                <div class="modal-header bg-dark no-border">
                    <h4 class="modal-title">@lang('vendor.new_vendor')</h4>
                </div>
                <div class="modal-body">

                    <div class="form-group required">
                        <input type="hidden" class="form-control input-xs" id="vendor-id">
                        <label for="" class="control-label col-md-4">@lang('vendor.name')</label>
                        <div class="col-md-8"><input type="text" class="form-control input-xs" id="name"></div>
                    </div>
                    <div class="form-group required">
                        <label for="" class="control-label col-md-4">@lang('vendor.address') 1</label>
                        <div class="col-md-8"><input type="text" class="form-control input-xs" id="address1"></div>
                    </div>
                    <div class="form-group">
                        <label for="" class="control-label col-md-4">@lang('vendor.address') 2</label>
                        <div class="col-md-8"><input type="text" class="form-control input-xs" id="address2"></div>
                    </div>
                    <div class="form-group required">
                        <label for="" class="control-label col-md-4">@lang('vendor.city')</label>
                        <div class="col-md-8"><input type="text" class="form-control input-xs" id="city"></div>
                    </div>
                    <div class="form-group required">
                        <label for="" class="control-label col-md-4">@lang('vendor.zip_code')</label>
                        <div class="col-md-8"><input type="text" maxlength="5" class="form-control input-xs numeric" id="zip-code"></div>
                    </div>
                    <div class="form-group required">
                        <label for="" class="control-label col-md-4">@lang('vendor.phone')</label>
                        <div class="col-md-8"><input type="text" class="form-control input-xs" id="phone"></div>
                    </div>
                    <div class="form-group">
                        <label for="" class="control-label col-md-4">@lang('vendor.fax')</label>
                        <div class="col-md-8">
                            <input type="text" class="form-control input-xs" id="fax" />
                        </div>
                    </div>
                    <div class="form-group required">
                        <label for="" class="control-label col-md-4">@lang('vendor.email')</label>
                        <div class="col-md-8">
                            <input type="email" class="form-control input-xs" id="email" />
                        </div>
                    </div>

					<div class="form-group" id="img-view" style="display:none;">
                        <label for="" class="control-label col-md-4"></label>
                        <div class="col-md-8">
                            <img class="vendor-image"  id="img-view-logo" style="width:auto; height:90px;  display: block;  margin: 0 0;"  />
                        </div>
                    </div>

					<div class="form-group required">
                        <label for="" class="control-label col-md-4" id="label-gambar">@lang('vendor.image')</label>
                        <div class="col-md-8">
                           <input type="file" name="image" class="form-control input-xs" id="image-vendor">
                        </div>
                    </div>

					<div class="form-group" id="img-view2" style="display:none;">
                        <label for="" class="control-label col-md-4"></label>
                        <div class="col-md-8">
                            <img class="vendor-image"  id="img-view-sign" style="width:auto; height:90px;  display: block;  margin: 0 0;"  />
                        </div>
                    </div>

					<div class="form-group required">
                        <label for="" class="control-label col-md-4" id="label-gambar">@lang('vendor.sign')</label>
                        <div class="col-md-8">
                           <input type="file" name="sign" class="form-control input-xs" id="image-sign">
                        </div>
                    </div>

					<div class="form-group required">
                        <label for="" class="control-label col-md-4" id="label-gambar">@lang('vendor.doctor_name')</label>
                        <div class="col-md-8">
                           <input type="text" name="doctor_name" class="form-control input-xs" id="doctor_name">
                        </div>
                    </div>

					<div class="form-group required">
                        <label for="" class="control-label col-md-4" id="label-gambar">@lang('vendor.doctor_license')</label>
                        <div class="col-md-8">
                           <input type="text" name="doctor_license" class="form-control input-xs" id="doctor_license">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <label class="pull-left">(<span style="color: red">*</span>) @lang('general.required_field')</label>
                    <button type="button" class="btn btn-primary" id="btn-submit"><i class="fa fa-check-circle"></i> @lang('general.submit')</button>
                    <button class="btn btn-default" data-dismiss="modal">@lang('general.close')</button>
                </div>
            </form>
        </div>
    </div>
</div>

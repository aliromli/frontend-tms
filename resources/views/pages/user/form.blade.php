@extends('layouts.app')
@section('title', 'User')
@section('ribbon')
@endsection

@section('content')
<div class="title-page">
    <div class="row">
            <div class="col-md-12">
                <ul class="breadcrumb mt-bread">
                    <li><a href="/user">User</a></li>
                </ul>
            </div>
    </div> 
</div>

<div class="main-content-inner">
    <div class="row">
        <div class="col-12">
            <div class="card mb-tng">
                <div class="card-body" >
					<form class="form-horizontal" id="form-user" >
						<div class="modal-header no-border">
							<h4 class="modal-title">{{ $edit=='no' ? 'Add User' : 'Edit User ' }}</h4>
						</div>
						<div class="modal-body">
							<div class="form-group">
								<input type="hidden" class="form-control form-control-sm input-xs w-50" id="user-id" value="{{$data ? $data[0]['id'] : ''}}">
								<input type="hidden" class="form-control form-control-sm input-xs w-50" id="user-userGroup-id" value="{{$data ? $data[0]['user_group_id'] : ''}}">
								<input type="hidden" class="form-control form-control-sm input-xs w-50" id="user-tenant-id" value="{{$data ? $data[0]['tenant_id'] : ''}}">
								<label for="email">@lang('user.email')</label>
								<input type="text" class="form-control form-control-sm input-xs w-50" id="email" value="{{$data ? $data[0]['email'] : ''}}"  placeholder="">
							</div>
							<div class="form-group">
								<label for="name">@lang('user.name')</label>
								<input type="text" class="form-control form-control-sm input-xs w-50" id="name" value="{{$data ? $data[0]['name'] : ''}}"  placeholder="">
							</div>
							<div class="form-group">
								<label for="name">@lang('change-password.password')</label>
								<input type="password" class="form-control form-control-sm input-xs w-50" id="password" value=""  placeholder="">
							</div>
							<div class="form-group">
								<label for="name">@lang('change-password.confirm_password')</label>
								<input type="password" class="form-control form-control-sm input-xs w-50" id="confirm-password" value=""  placeholder="">
							</div>
							@if(session()->get('user.userGroup.name')!=="Admin Tenant")
								<div class="form-group">
									<label for="" class="">@lang('user.group')</label>
									<select class="form-control input-xs w-50" id="user-group"> 
										@if ($edit=='no')
											<option value="">&raquo; @lang('general.select') @lang('user-group.user_group')</option>
											@foreach($userGroup as $group)
											<option value="{{ $group->id }}">{{ $group->name }}</option>
											@endforeach
										@else
											@foreach($userGroup as $group)
												@if($data[0]['user_group_id']==$group->id)
													<option value="{{ $group->id }}" selected>{{ $group->name }}</option>
												@else
													<option value="{{ $group->id }}">{{ $group->name }}</option>
												@endif
											@endforeach
											<option value="">&raquo; @lang('general.select') @lang('user-group.user_group')</option>
											
										@endif
									</select>
									
								</div>
								<div class="form-group" id="u-cs">
									<label for="" class="">Tenant</label>
									
									<select class="form-control input-xs w-50" id="tenant">
										@if ($edit=='no')
											<option value="">&raquo; @lang('user.all') Tenant</option>
											@foreach($tenant as $t)
											<option value="{{ $t['id'] }}">{{ $t['name'] }}</option>
											@endforeach
										@else
											
											<option value="">&raquo; @lang('user.all') Tenant</option>
											@if(!empty($tenant))
												@foreach($tenant as $t)
													@if($data[0]['tenant_id']==$t['id'])
														<option value="{{ $t['id'] }}" selected>{{ $t['name'] }}</option>
													@else
														<option value="{{ $t['id'] }}">{{ $t['name'] }}</option>
													@endif
												@endforeach
											@endif
											
										@endif
									</select>
									
								</div>
							@endif
							<div class="form-group">
								<label for="" class="">@lang('user.active')</label>
								
                                <div class="radio radio-inline">
                                    <label><input  checked="" type="radio" name="active[]" value="Y" id="user-active-yes"> @lang('general.yes')</label>
                                </div>
                                <div class="radio radio-inline">
                                    <label><input type="radio" name="active[]" value="N" id="user-active-no"> @lang('general.no')</label>
                                </div>
							</div>
							<div class="form-group">
							   <label for="" class="">Force change password</label>
								
                                <div class="radio radio-inline">
                                    <label><input   type="radio" name="force[]" value="Y" id="force-change-password-yes"> @lang('general.yes')</label>
                                </div>
                                <div class="radio radio-inline">
                                    <label><input  checked="" type="radio" name="force[]" value="N" id="force-change-password-no"> @lang('general.no')</label>
                                </div>
								
                               
							</div>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-rounded btn-primary btn-xs" id="btn-submit">
								<span class="loading-in-button fac"><i class="fa fa-refresh fa-spin"></i> Loading</span> <span class="text-in-button fac">@lang('general.save')</span>
							</button> 
							<!-- <button type="button" class="btn btn-rounded btn-primary btn-xs " id="btn-submit" data-loading-text="<i class='fa fa-circle-o-notch fa-spin'></i> Processing">@lang('general.save')</button> -->
							<a class="btn btn-secondary btn-rounded  btn-xs" href="{{url('/user')}}">@lang('general.cancel')</a>
						</div>
					</form>
                   
                </div>
            </div>
        </div>

    </div>
</div>

@endsection
@section('script')
<script src="{{ asset('assets/js/user/form.js') }}"></script>
@endsection
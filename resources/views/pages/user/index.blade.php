@extends('layouts.app')
@section('title', 'User')
@section('ribbon')
@endsection

@section('content')
<div class="title-page">
    <div class="row">
            <div class="col-md-12">
                <ul class="breadcrumb mt-bread">
                    <li><a href="/user">USER</a></li>
                </ul>
            </div>
    </div> 
</div>
<div class="main-content-inner">
    <div class="row">
        <div class="col-12 mt-5">
            <div class="card mb-tng2">
                <div class="card-body">
                    <div class="box-header no-border text-left filter" style="margin-bottom:20px;"> 
                  	<a class="clear-search" href="#">Clear Search</a> &nbsp;
                        <a class="tugel" href="#"><i class="fa fa-chevron-down sign"></i> <span class="filter-title">Show</span><span class="filter-title hidden">Hide</span> Filter <i class="fa fa-filter"></i></a> &nbsp;
                        <a href="{{url('/user-form')}}" class="btn btn-link text-secondary text-left" style="text-decoration: none;display: inline;" ><i class="fa fa-plus-circle"></i> Add New User</a> 
                    </div>
                  
                    <div class="form-filter">
                        <form class="form-horizontal">                           
                                <div class="form-group row" xstyle="margin-left:-28;">
                                    <div class="col-md-4"><input type="text" id="search-name" class="form-control input-xs" placeholder="Name"></div>
                                </div>
                                <!-- <div class="form-group row">
                                    <div class="col-md-4">
                                        <select class="form-control input-xs" id="search-group">
                                            <option value="">&raquo; @lang('general.select') @lang('user-group.user_group')</option>
                                            @foreach($userGroup as $group)
                                            <option value="{{ $group->id }}">{{ $group->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div> -->
                                <div class="form-group row">
                                    <div class="col-md-4">
                                         <button type="button" id="btn-cari-user" class="btn btn-rounded btn-primary  btn-xs" style="height:1.5em;width:100%;color:#fff;" ><span style="color:#ffffff;">Cari</span></button>
                                    </div>
                                </div>
                                <br/>
                        </form>
                    </div>
                    <br/>
                    <div class="data-tables">
                        <table id="dataTableUser"   width="100%">
                            <thead class="bg-light text-capitalize">
                                <tr>
                                    <th style="width:30px;">No</th>
                                    <th>@lang('user.name')</th>
                                    <th>@lang('user.email')</th>
                                    <th>Email Verified</th>
                                    <th>@lang('user.group')</th>
                                    <th>Tenant</th>
                                    <th>@lang('user.active')</th>
                                    <th>Force ChangePass</th>
                                    <th>@lang('general.action')</th>
                                </tr>
                            </thead>
                            <tbody>
                               
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
@endsection


@section('script')
<!-- Start datatable js -->
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js"></script>
<script src="https://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.18/js/dataTables.bootstrap4.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.2.3/js/responsive.bootstrap.min.js"></script>
<script src="{{ asset('assets/js/user/index.js') }}"></script>
@endsection

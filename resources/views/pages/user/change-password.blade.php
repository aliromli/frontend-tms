@extends('layouts.app')
@section('title', 'User')
@section('ribbon')
@endsection


@section('content')
<div class="title-page">
    <div class="row">
            <div class="col-md-12">
                <ul class="breadcrumb mt-bread">
                    <li><a href="/user/change-password">Change Password</a></li>
                </ul>
            </div>
    </div> 
</div>



<div class="main-content-inner">
    <div class="row">
        <div class="col-12 mt-5">
            <div class="card mb-tng2">
                <div class="card-body">
						<form  enctype="multipart/form-data" action="{{ url('/user/change-password') }}" id="login-form"  class="form-horizontal"  method="post">
							@csrf
							<div class="modal-body">
								<div class="modal-header no-border" style="margin-top: -24px;">
									<i class="fa fa-lock"></i> @lang('change-password.change_password')
									
								</div>
								<div>
									@if(session('error'))
									<div class="alert alert-danger">
										<ul><li style='padding-left:10px;'>{{ session('error') }}</li></ul>
									</div>
									@endif

                                    @if(session('error2'))
									<div class="alert alert-danger">
									    <?php
                                            $ty = session('error2');
                                            echo "<ul style='padding-left:10px;'>";
                                            foreach(json_decode($ty,true)['password'] as $v)
                                            {
                                                echo "<li>".$v."</li>";
                                            }
                                            echo "</ul>";
                                        ?>
 

									</div>
									@endif

									@if(session('success'))
									<div class="alert alert-success">
									    <ul><li style='padding-left:10px;'>{{ session('success') }}</li></ul>
									</div>
									@endif
								</div>
								
								<div class="row mt-5">
									<div class="col-md-6">
										<fieldset> 
											<div class="form-group">
												<label for="name">@lang('change-password.change_password')</label>
												<input id="password" type="password"  class="form-control form-control-sm input-xs w-80 password"  name="password" id="password"  required autofocus>
                                                <div id="passwordHelpBlock" class="form-text">
                                                    Your password must be min 8 characters, contain uppercase and lowercase, contain numbers, contain special characters.
                                                </div>
                                                
                                               
                                              
											</div>
											<div class="form-group">
												<label for="email">@lang('change-password.confirm_password')</label>
												<input type="password"  class="form-control form-control-sm input-xs w-80"  name="repassword" id="repassword"  required autofocus>
											</div>
                                            <div class="form-check">
                                                    <input class="form-check-input" type="checkbox" id="disabledFieldsetCheck" onclick="myFunction()" >
                                                    <label class="form-check-label" for="disabledFieldsetCheck">
                                                    Show Password
                                                    </label>
                                            </div>
										</fieldset>
									</div>
									
								</div>
								<footer class="mt-3">
									<button type="submit" class="btn btn-primary btn-block btn-rounded">
										<i class="fa fa-check-circle"></i> @lang('general.submit')
									</button>
								</footer>
							</div>
						</form>
					
				
		
				</div>
			</div>
		</div>
	</div>
</div>

@endsection
@section('script')
<script>
   
    function myFunction() {
        var x = document.getElementById("password");
        if (x.type === "password") {
            x.type = "text";
        } else {
            x.type = "password";
        }

        var x2 = document.getElementById("repassword");
        if (x2.type === "password") {
            x2.type = "text";
        } else {
            x2.type = "password";
        }
    }
</script>
@endsection
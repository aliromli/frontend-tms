@extends('layouts.app')
@section('title', 'Issuer')
@section('ribbon')
@endsection

@section('content')
<div class="title-page">
    <div class="row">
            <div class="col-md-12">
                <ul class="breadcrumb mt-bread">
                    <li><a href="/issuer">ISSUER</a></li>
                </ul>
            </div>
    </div> 
</div>
<div class="main-content-inner">
    <div class="row">
        <div class="col-12 mt-5">
            <div class="card mb-tng2">
                <div class="card-body">               
					<form class="form-horizontal" id="form-dp">
						
						<div class="modal-body">
							<div class="form-group">
								
								<h4 class="title-form">{{ $edit=='no' ? 'Add Issuer' : 'Edit Issuer' }}</h4>
								<input type="hidden" class="form-control form-control-sm input-xs" id="is-id" value="{{$data ? $data[0]['id'] : ''}}">
							</div>
							<div class="form-row mt-5">
								<div class="form-group col-md-4">
									<label for="" class="control-label">Name</label>
									<input type="text" class="form-control form-control-sm input-xs" id="is-name" value="{{$data ? $data[0]['name'] : ''}}">
								</div>
								<div class="form-group col-md-4">
									<label for="" class="control-label">Issuer Id</label>
									<input type="text" class="form-control form-control-sm input-xs" id="is-issuerId" value="{{$data ? $data[0]['issuerId'] : ''}}">
								</div>
								<div class="form-group col-md-2 offset-2">
									<label for="" class="control-label">On Us</label>
									<div class="form-check">
											<input  id="is-onUs" name="isOnUs" type="checkbox"  class="form-check-input position-static" {{$data? json_encode($data[0]['onUs'])=='true' ? 'checked' : '' : ''}} value="true">
									</div>
								</div>
							</div>
							<div class="form-row">
								<div class="form-group col-md-4">
									<label for="" class="control-label">Acquirer</label>
									<select class="form-control form-control-sm" id="is-acquirerId" name="acquirerId">
										<?php
										if(!empty($acquirer))
										{
											if($edit=='yes')
											{	
												foreach($acquirer as $tle)
												{
													 if($data[0]['acquirer']['id']==$tle['id'])
													 {
														 echo "<option value='".$tle['id']."' selected>".$tle['name']."</option>";
													 }
													 else
													 {
														 echo "<option value='".$tle['id']."'>".$tle['name']."</option>";
													 }	
												}
												echo "<option value=''>--Pilih--</option>"; 
											}
											else
											{
												echo "<option value=''>--Pilih--</option>";
												foreach($acquirer as $tle)
												{
													echo "<option value='".$tle['id']."'>".$tle['name']."</option>";
												}
											}	
										}	
										
									?>
									</select>
								</div>								
							</div>
							<div class="form-group">
								<label for=""><a  href="#" class="btn btn-link text-secondary text-left" style="margin-left:-20px;text-decoration: none;display: inline;" id="btn-add-cards-modal">Add Cards <i class="fa fa-plus-square fa-lg" style="color:#717682;margin-left:20px;"></i></a></label>
								<table class='table border' id="table-card-issuer-form">
									<thead>
										<tr>
											<th>Card Num Range from</th>
											<th>Card Num Range end</th>
											<th>Card Num Length</th>
											<th>PAN Digit Unmasking</th>
											<th>Action</td>
										</tr>
									<thead>
									<tbody>
									<?php
										if(!empty($data))
										{
											if(count($data[0]['cards'])>0)
											{
												foreach($data[0]['cards'] as $da)
												{
													$act = '<a href="#" class="btn-delete"><img class="trash" src="/assets/images/icon/trash.png" /></a>';
													$d='<tr class="tr-dt">';
													$d.='<td><span class="binRangeStart">'.$da["binRangeStart"].'</span></td>';
													$d.='<td><span class="binRangeEnd">'.$da["binRangeEnd"].'</span></td>';
													$d.='<td><span class="cardNumLength">'.$da["cardNumLength"].'</span></td>';
													$d.='<td><span class="panDigitUnmasking">'.$da["panDigitUnmasking"].'</span><span class="uid" style="display:none;">'.$da["id"].'</span></td>';
													$d.='<td>'.$act.'</td>';
													$d.='</tr>';
													echo $d;
												}
											}
										}	
									?>
									</tbody>
								</table>
							</div>
							<div class="form-group row" id="div-dp-version" style="{{ $edit=='no' ? 'display:none;':'display:none;'}}">
								<label for="" class="control-label col-md-3">Version</label>
								<div class="col-md-8"><input type="text" class="form-control form-control-sm input-xs" readOnly id="version-is" value="{{$data? $data[0]['version'] : ''}}"></div>
							</div>
							<div class="form-group  mt-4">
								<button type="button" class="btn btn-rounded btn-primary" id="btn-submit-is"> @lang('general.save')</button>
								<a class="btn btn-rounded btn-secondary" href="{{url('/issuer')}}">@lang('general.cancel')</a>
								<span class="loading"><img class="hidden" width="40" src="{{asset('assets/images/icon/loader.gif')}}"/></span>
							</div>
						</div>
						
					</form>
                 
                </div>
            </div>
        </div>

    </div>
</div>

@endsection

@section('modal')
    @include('pages.issuer.list_modal_cards')
@endsection

@section('script')
 <!-- Start datatable js -->
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js"></script>
<script src="https://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.18/js/dataTables.bootstrap4.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.2.3/js/responsive.bootstrap.min.js"></script>
<script src="{{ asset('assets/js/issuer.js') }}"></script>

@endsection

<div class="modal fade" id="modal-menu-item">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="page-loader hidden">
                <div class="sk-circle">
                    <div class="sk-circle1 sk-child"></div>
                    <div class="sk-circle2 sk-child"></div>
                    <div class="sk-circle3 sk-child"></div>
                    <div class="sk-circle4 sk-child"></div>
                    <div class="sk-circle5 sk-child"></div>
                    <div class="sk-circle6 sk-child"></div>
                    <div class="sk-circle7 sk-child"></div>
                    <div class="sk-circle8 sk-child"></div>
                    <div class="sk-circle9 sk-child"></div>
                    <div class="sk-circle10 sk-child"></div>
                    <div class="sk-circle11 sk-child"></div>
                    <div class="sk-circle12 sk-child"></div>
                </div>
            </div>
            <form class="form-horizontal" id="form-menu">
                <div class="modal-header no-border">
                    <h4 class="title-form">New Menu Item</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group row" style="margin-left:0;">
                        <label for="" class="control-label col-md-4">@lang('menu.name')</label>
                        <input type="hidden" id="menu-item-id">
                        <input type="hidden" id="squence">
                        <div class="col-md-8"><input type="text" class="form-control input-xs" id="menu-name"></div>
                    </div>
                    <div class="form-group row" style="margin-left:0;">
                        <label for="" class="control-label col-md-4">@lang('menu.tooltip')</label>
                        <div class="col-md-8"><input type="text" class="form-control input-xs" id="tooltip"></div>
                    </div>
                    <!-- <div class="form-group row" style="margin-left:0;">
                        <label for="" class="control-label col-md-4">@lang('menu.description')</label>
                        <div class="col-md-8"><input type="text" class="form-control input-xs" id="description"></div>
                    </div> -->
                    <div class="form-group row" style="margin-left:0;">
                        <label for="" class="control-label col-md-4">@lang('menu.icon') (HTML)</label>
                        <div class="col-md-8"><input type="text" class="form-control input-xs" id="icon"></div>
                    </div>
                    <div class="form-group row" style="margin-left:0;">
                        <label for="" class="control-label col-md-4">@lang('menu.url')</label>
                        <div class="col-md-8"><input type="text" class="form-control input-xs" id="url"></div>
                    </div>
                    <div class="form-group">
                        <div class="row" style="margin-left:0;">

                            <label for="" class="control-label col-md-2">@lang('general.action')</label> 
                            <div class="col-md-4">
                                <!-- <div class="checkbox"><label for="action-view"><input id="action-read" type="checkbox" value="READ">@lang('general.view')</label></div> -->
                                <!-- <div class="checkbox"><label for="action-add"><input id="action-create" type="checkbox" value="CREATE">@lang('general.add')</label></div> -->
                                <!-- <div class="checkbox"><label for="action-edit"><input id="action-update" type="checkbox" value="UPDATE">@lang('general.edit')</label></div> -->
                                <!-- <div class="checkbox"><label for="action-delete"><input id="action-delete" type="checkbox" value="DELETE">@lang('general.delete')</label></div> -->
                                <div class="form-check">
                                    <input type="checkbox" class="form-check-input" id="action-read" value="READ">
                                    <label class="form-check-label" for="exampleCheck1">@lang('general.view')</label>
                                </div>
                                <div class="form-check">
                                    <input type="checkbox" class="form-check-input" id="action-create" value="CREATE">
                                    <label class="form-check-label" for="exampleCheck1">@lang('general.add')</label>
                                </div>
                                <div class="form-check">
                                    <input type="checkbox" class="form-check-input" id="action-update" value="UPDATE">
                                    <label class="form-check-label" for="exampleCheck1">@lang('general.edit')</label>
                                </div>
                                <div class="form-check">
                                    <input type="checkbox" class="form-check-input" id="action-delete" value="DELETE">
                                    <label class="form-check-label" for="exampleCheck1">@lang('general.delete')</label>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <!-- <div class="checkbox"><label for="action-import"><input id="action-import" type="checkbox" value="IMPORT">@lang('general.import')</label></div> -->
                                <!-- <div class="checkbox"><label for="action-export"><input id="action-export" type="checkbox" value="EXPORT">@lang('general.export')</label></div> -->
                                <!-- <div class="checkbox"><label for="action-print"><input id="action-print" type="checkbox" value="PRINT">@lang('general.print')</label></div> -->
                                <div class="form-check">
                                    <input type="checkbox" class="form-check-input" id="action-import" value="IMPORT">
                                    <label class="form-check-label" for="exampleCheck1">@lang('general.import')</label>
                                </div>
                                <div class="form-check">
                                    <input type="checkbox" class="form-check-input" id="action-export" value="EXPORT">
                                    <label class="form-check-label" for="exampleCheck1">@lang('general.export')</label>
                                </div>
                                <div class="form-check">
                                    <input type="checkbox" class="form-check-input" id="action-print"  value="PRINT">
                                    <label class="form-check-label" for="exampleCheck1">@lang('general.print')</label>
                                </div>
                            </div>
                        
                        </div>
                       
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-flat btn-xs btn-primary" id="btn-submit-item"><i class="fa fa-check-circle"></i> @lang('general.save')</button>
                    <button class="btn btn-flat btn-xs  btn-default" data-dismiss="modal">@lang('general.close')</button>
                </div>
            </form>
        </div>
    </div>
</div>

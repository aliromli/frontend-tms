@extends('layouts.app')
@section('title', 'Menu Builder')
@section('ribbon')
@endsection


@section('content')
<!--
<link rel="stylesheet" type="text/css" href="{{ asset('assets/css/plugins/smartadmin-production.min.css')}}"> 
<link rel="stylesheet" type="text/css" href="{{ asset('assets/css/plugins/theme-material.css')}}">  
-->
<link rel="stylesheet" type="text/css" href="{{ asset('assets/css/menuModul.css')}}">  
<div class="title-page">
    <div class="row">
            <div class="col-md-12">
                <ul class="breadcrumb mt-bread">
                     <li><a href="/menu">Menu Builder</li>
                </ul>
            </div>
    </div> 
</div>
<div class="main-content-inner">
    <div class="row">
        <div class="col-12 mt-5">
            <div class="card mb-tng2">
                <div class="card-body">
                    <!-- <h4 class="header-title">Menu Builder</h4> -->
					<div class="box-header no-border text-right mb-3">
                                 <a href="#" class="btn btn-link text-secondary text-left"  style="text-decoration: none;display: inline;" id="btn-add-menu-item" >Add Menu <i class="fa fa-plus-circle" style="color:#717682;"></i> </a> &nbsp;
                                 <a  href="#" class="btn btn-flat btn-xs btn-default" type="button" id="btn-submit-struk">@lang('general.update') Structure</a>
                    </div>
					<div class="data-tables" style="border: 0px !important;">
                        <div class="box bg-none no-border">
                            <!-- Menu list update -->
                            <input id="menu-list" type="hidden">

                            <div class="box-body">
                                <div class="dd" id="nestable">
                                    <ol class="dd-list">
                                        @each('pages.menu.menu', $menus, 'menu')
                                    </ol>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('modal')
    @include('pages.menu.new-item');
@endsection

@section('script')
<script src="{{ asset('assets/js/libs/jquery-ui.min.js') }}"></script>
<script src="{{ asset('assets/js/plugin/jquery-nestable/jquery.nestable.min.js') }}"></script>
<script src="{{ asset('assets/js/scriptsMenu.js') }}"></script>
@endsection


@section('css')
<!-- <link rel="stylesheet" type="text/css" media="screen" href="{{ asset('css/bootstrap-datepicker.css') }}"> -->
<!-- <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/jquery-ui.css') }}"> -->
<style>
    .dd {
        margin-bottom: 15px;
    }
    .builder-menu,
    .builder-footer {
        position: relative;
        overflow: auto;
        margin-left: -15px;
        margin-right: -15px;
    }
    .builder-menu {
        border-bottom: 1px solid #ddd;
        padding: 0 15px;
        margin-bottom: 10px;
    }
    .builder-footer {
        border-top: 1px solid #ddd;
        padding: 15px;
        margin-top: 10px;
    }
    .builder-menu .title {
        font-size: 13px;
        font-weight: 700;
    }
    .builder-menu >button {
        position: absolute;
        display: inline;
        top: 0;
        right: 15px;
    }
</style>
@endsection



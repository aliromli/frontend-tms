@extends('layouts.app')

@section('ribbon')
<ol class="breadcrumb">
    <li>@lang('tools.tools')</li>
    <li>@lang('tools.restore_database')</li>
</ol>
@endsection

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="panel">
            <div class="panel-heading">
                <strong><i class="fa fa-th-large"></i> @lang('tools.restore_database')</strong>
            </div>
            <div class="panel-body">
                <div class="form-horizontal" id="form-backup">
                    <div class="form-group">
                        <label for="" class="control-label col-md-2">@lang('tools.select_file')</label>
                        <div class="col-md-4">
                            <input accept=".sql" type="file" id="file" class="form-control input-xs">
                            <span class="help-block">@lang('tools.file_info')</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="panel-footer">
                <button type="button" class="btn btn-primary" id="btn-submit"><i class="fa fa-check-circle"></i> @lang('general.submit')</button>
            </div>
        </div>
    </div>
</div>

@endsection

@section('script')
<script>
    $(document).ready(function(){
        $('.backup-type').click(function(){
            if($(this).val() == 2) {
                $('#table-container').removeClass('hidden');
            } else {
                $('#table-container').addClass('hidden');
            }
        })
    });
</script>
@endsection

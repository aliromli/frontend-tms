@extends('layouts.app')
@section('title', 'Terminal')
@section('ribbon')
@endsection

@section('content')
<div class="title-page">
    <div class="row">
            <div class="col-md-12">
                <ul class="breadcrumb mt-bread">
                    <li><a href="/terminal">TERMINALS</a></li>
                </ul>
            </div>
    </div> 
</div>
<div class="main-content-inner">
	<div class="row">
        <div class="col-12 mt-5">
            <div class="card mb-tng2">
                <div class="card-body">
                <form class="form-horizontal" id="form-terminal">
                   
                    <div class="modal-body">
						<div class="form-group">
							 <h4 class="title-form">{{ $edit=='no' ? 'Add  Terminal2' : 'Edit  Terminal ' }}</h4>
						</div>
						<div class="form-group">
                            <input type="hidden" class="form-control form-control-sm input-xs" id="terminal-id2" value="{{$data ? $data[0]['id'] : ''}}">
                            <label for="" class="control-label">@lang('terminal.sn')</label>
                            <input type="text" class="form-control form-control-sm input-xs" id="terminal-sn2" value="{{$data ? $data[0]['sn'] : ''}}" style="width:50%;">
							
                        </div>
						<div class="form-row">
							 <div class="form-group col-md-3">
								<label for="">Device Model</label>
								<select class="form-control form-control-sm input-xs" id="terminal-device-model2" style="width:70%;">
                                   @if($edit=="ya")
                                            @if(!empty($devicemodel))
												
												@foreach($devicemodel as $c)
													
													@if($data[0]['model']['id'] == $c['id'])
														
														<option value="{{ $c['id'] }}" selected>{{ $c['model'] }}</option>
													@else
														
														<option value="{{ $c['id'] }}" >{{ $c['model'] }}</option>
													@endif
												@endforeach
											@endif
                                            <option value="">&raquo; @lang('general.select') @lang('terminal.devicemodel')</option>
                                    @else
                                            <option value="">&raquo; @lang('general.select') @lang('terminal.devicemodel')</option>
                                            @if(!empty($devicemodel))
											
												@foreach($devicemodel as $c)
													<option value="{{ $c['id'] }}" >{{ $c['model'] }}</option>
												@endforeach
											@endif
                                    @endif
                                </select>
							 </div>
							<div class="form-group col-md-2 offset-1">
								<label for="">Terminal Profile</label>
								<select class="form-control form-control-sm input-xs" id="terminal-profile2">
                                  @if($edit=="ya")
										@if(!empty($deviceprofile))
											
											@foreach($deviceprofile as $c)
												
												@if($data[0]['profile']['id'] == $c['id'])
													
													<option value="{{ $c['id'] }}" selected>{{ $c['name'] }}</option>
												@else
													
													<option value="{{ $c['id'] }}" >{{ $c['name'] }}</option>
												@endif
											@endforeach
										@endif
                                        <option value="">&raquo; @lang('general.select') @lang('terminal.deviceprofile')</option>
                                    @else
                                        <option value="">&raquo; @lang('general.select') @lang('terminal.deviceprofile')</option>
                                        @if(!empty($deviceprofile))
										
											@foreach($deviceprofile as $c)
												 <option value="{{ $c['id'] }}" >{{ $c['name'] }}</option>
											@endforeach
										@endif
                                    @endif
                                </select>
							</div>
							<div class="form-group col-md-2 offset-1">
								<label for="">Merchant</label>
								<select class="form-control form-control-sm input-xs" id="terminal-merchant2">
                                    
									
									 @if($edit=="ya")
                                        @if(!empty($merchant))
											@foreach($merchant as $c) 
												@if($data[0]['merchant']['id'] == $c['id'])
													<option value="{{ $c['id'] }}" selected>{{ $c['name'] }}</option>
												@else
													<option value="{{ $c['id'] }}" >{{ $c['name'] }}</option>
												@endif
											@endforeach
										@endif
                                        <option value="">&raquo; @lang('general.select') Merchant</option>
                             
                                    @else
                                        <option value="">&raquo; @lang('general.select') Merchant</option>
                                        @if(!empty($merchant))
											@foreach($merchant as $c)
												<option value="{{ $c['id'] }}" >{{ $c['name'] }}</option>
											@endforeach
										@endif
                                    @endif

                                </select>
							</div>
						</div>
						<?php
                          //  echo '<pre>';
                            //    var_dump($data[0]['terminalGroups']);
                            //echo '</pre>';
                          //  <li class="ch-li"><span class="nm">'+mergedArr[i].name+'</span> <i class="fa fa-trash-o btn-delete-list-tg-t" style="color:#717682;margin-left:20px;"></i> <span class="uid" style="display:none;">'+mergedArr[i].uid+'</span></li>

                        ?>
						<div class="form-group">
							<label for="list-group-tg-t"><a  href="#" class="btn btn-link text-secondary text-left" style="margin-left:-20px;text-decoration: none;display: inline;" id="add-tg-to-list-t">Terminals Group <i class="fa fa-plus-square fa-lg" style="color:#717682;margin-left:20px;"></i></a></label>
							<ul id="list-group-tg-t"  class="rounded p-2" style="border: 1px solid rgba(170, 170, 170, .3); width:33%;">
                               @if($edit=="ya")
                                   @foreach($data[0]['terminalGroups'] as $b)
                                             <li class="ch-li"><span class="nm">{{$b['name']}}</span> <i class="fa fa-trash-o btn-delete-list-tg-t" style="color:#717682;margin-left:20px;"></i> <span class="uid" style="display:none;">{{$b['id']}}</span></li>

                                   @endforeach
                               @endif
                            <ul> 
                            <!-- <textarea id="list-group-tg-t2"  class="rounded p-2" style="border: 1px solid rgba(170, 170, 170, .3); width:33%;">
                            </textarea> -->
                            <!-- <select multiple class="form-control form-control-sm" id="list-group-tg-t" style="width:50%;">
							</select> -->
					    </div>
						<!--
                        
                        
                        <div class="form-group row">
                            <label for="" class="control-label col-md-3">Terminal Group</label>
                            <div class="cardx col-md-8">
                                <div class="xcard-body">
                                    <button type="button" class="btn btn-flat btn-success btn-xs" id="add-tg-to-list-t"> Click List Terminal Group</button>
                                    <table class="table mt-2" id="list-group-tg-txxx">
                                        <thead>
                                            <tr>
                                                <th>Name</th>
                                                <th>Description</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody> 
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div> -->
                        <div class="form-group row" xid="div-t-version" style="display:none;">
                            <label for="" class="control-label col-md-3">Version</label>
                            <div class="col-md-8"><input readOnly type="text" class="form-control form-control-sm input-xs" id="version-terminal2" value="{{$data? $data[0]['version'] : ''}}"></div>
                        </div>
						<div class="form-row mt-5">
							<button type="button" class="btn btn-rounded btn-primary" id="btn-submit-terminal2">ADD2</button>&nbsp;&nbsp;
							<a class="btn btn-rounded btn-secondary" href="{{url('/terminal')}}">@lang('general.cancel')</a>
						</div>
                    </div>
                   
                </form>
                   
                </div>
            </div>
        </div>

    </div>
</div>

@endsection


@section('modal')
    @include('pages.terminal.tg-download-task')
@endsection

@section('script')
 <!-- Start datatable js -->
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js"></script>
<script src="https://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.18/js/dataTables.bootstrap4.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.2.3/js/responsive.bootstrap.min.js"></script>
<script src="{{ asset('assets/js/terminal.js') }}"></script>

@endsection

@extends('layouts.app')
@section('title', 'Terminal Group')
@section('ribbon')
@endsection


@section('content')
<div class="title-page">
    <div class="row">
            <div class="col-md-12">
                <ul class="breadcrumb mt-bread">
                    <li><a href="/terminal-group">TERMINALS</a></li>
                </ul>
            </div>
    </div> 
</div>
<div class="main-content-inner">
    <div class="row">
        <div class="col-12">
            <div class="card mb-tng">
                <div class="card-body">
                <form class="form-horizontal" id="form-t">
                   
                    <div class="modal-body">
						<div class="form-group">
							 <h4 class="modal-title">Import</h4>
						</div>
                        <div class="form-group row" xid="div-gt-version" style="display:none;">
                            <label for="" class="control-label col-md-3">Version</label>
                            <div class="col-md-8"><input type="text" readonly class="form-control input-xs" id="version-terminal-g" value=""></div>
                        </div>
						<div class="row mt-3">
							<div class="col-md-12">
								<div class="data-tables">
									<input type="hidden" id="userName-import-view" value="{{$user_name}}"/>
									<table id="dataTableListTerminal_Result" width="100%">
										<thead class="bg-light text-capitalize">
											<tr>
												<th style="width:40px;" align="center">No</th>
												<th>SN</th>
												<th>Model</th>
												<th>Profile</th>
												<th>Merchant</th>
												<th>Group</th>
												<th>Note</th>
											</tr>
										</thead>
										<tbody>
											<?php
												if($data_temp->count()>0)
												{
													$no=1;
													foreach($data_temp as $d){
													$gn = explode(",",$d->groupName);
													$v = "";
													for($i=0;$i<count($gn);$i++)
													{
														$v.="<span class='badge badge-success p-1 m-1'>".$gn[$i]."</span>";
													}	
													$tr = "<tr>";
													$tr.="<td align='center'>".$no."</td>";
													$tr.= "<td><span class='sn'>".$d->sn."</span></td>";
													$tr.= "<td>".$d->modelName."<span class='model_id' style='display:none;'>".$d->model_id."</span></td>";
													$tr.= "<td>".$d->profileName."<span class='profile_id' style='display:none;'>".$d->profile_id."</span></td>";
													$tr.= "<td>".$d->merchantName."<span class='merchant_id' style='display:none;'>".$d->merchant_id."</span></td>";
													$tr.= "<td>".$v."<span class='group_id' style='display:none;'>".$d->group_id."</span></td>";
													$tr.= "<td>".$d->note."</td>";
													$tr.= "</tr>";
													echo $tr;
													$no++;
													}
												}	
												
											?>
										</tbody>
									</table>
								</div>
							</div>
							<div class="col-md-12">
								<div class="mt-3" style="color:#717682;">{{$data_temp->count()}} terminal to be imported</div>
							</div>
						</div>
						<div class="form-row mt-5">
							<button type="button" class="btn btn-rounded btn-primary" id="btn-submit-import-tt">IMPORT</button>&nbsp;&nbsp;
							<span class="loading"><img class="hidden" width="40" src="{{asset('assets/images/icon/loader.gif')}}"/></span>
						</div>
						
                    </div>
                    
                </form>
                   
                </div>
            </div>
        </div>

    </div>
</div>

@endsection


@section('script')
<!-- Start datatable js -->
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js"></script>
<script src="https://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.18/js/dataTables.bootstrap4.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.2.3/js/responsive.bootstrap.min.js"></script>
<script src="{{ asset('assets/js/terminal.js') }}"></script>

@endsection

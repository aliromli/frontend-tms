@extends('layouts.app')
@section('title', 'Terminal')
@section('ribbon')
@endsection

<style>
#dataTableTerminal {
    overflow-x: scroll;
    max-width: 40%;
    display: block;
    white-space: nowrap;
}  
</style>
@section('content')

<div class="title-page">
    <div class="row">
            <div class="col-md-12">
                <ul class="breadcrumb mt-bread">
                  
                    <li><a href="/terminal">TERMINALS</a></li>
                    
                </ul>
            </div>
    </div> 
</div>
<div class="main-content-inner">
    <div class="row">
        <div class="col-12 mt-5">
            <div class="card mb-tng2">
                <div class="card-body">
                    <div class="clear_filter">Clear Search</div>
                    <div class="form-row row mb-5"> 
						<input type="hidden" id="userName-terminal" value="{{session()->get('user.name')}}">
                       	<div class="col-md-12">
							<div class="row-btn-top">
                                @if(Privilege::visible(Request::segment(1),'CREATE')=='Y')
								<a  href="{{url('/terminal-form')}}" class="btn btn-link text-secondary text-left" style="text-decoration: none;display: inline;" id="btn-add-terminal"><i class="fa fa-plus-square fa-lg" style="color:#717682;"></i> &nbsp;Add Terminal</a> 
								@endif
                                <button type="button" class="btn btn-outline-secondary btn-xs" style="margin-left: 40px;display: inline;" id="btn-import-terminal">import</button>
								<span>
									 <input type="text" id="ter-sn-search" class="form-control form-control-sm text-left ml-4" style="width:30%;display: inline;" placeholder="SN.."/>
									 <i class="ti-search" id="btn-cari-terminal" ></i>  
								</span>
							</div>
						</div> 
                    </div>
                  
                    <!-- <div class="form-filter">
                        <form class="form-horizontal">
                           
                                <div class="form-group row" xstyle="margin-left:-28;">
                                
                                    <div class="col-md-4 mt-3">
                                        <input type="hidden"  id="ter-deviceModel-id-search">
                                        <input type="text" id="ter-deviceModel-search" class="form-control input-xs" placeholder="Model">
                                        
                                    </div>
                                    <div class="col-md-4 mt-3">
                                        <input type="hidden"  id="ter-merchant-id-search">
                                        <input type="text" id="ter-merchant-search" class="form-control input-xs" placeholder="Merchant">
                                    </div>
                                    <div class="col-md-4 mt-3">
                                        <input type="hidden"  id="ter-profile-id-search">
                                        <input type="text" id="ter-profile-search" class="form-control input-xs" placeholder="Profile">
                                    </div>
                                    <div class="col-md-4 mt-3">
                                         <input type="text" id="ter-sn-search" class="form-control input-xs" placeholder="SN">
                                  
                                    </div>
                                    <div class="col-md-3 mt-3">
                                        <button type="button" id="btn-cari-terminal" class="btn  btn-flat btn-secondary input-sm" style="xbackground-color:transparent; height:3em;" ><i class="ti-search" style="font-size:22px;"></i></button>
                                    </div>
                                </div> 
                        </form>
                    </div> -->
					<div class="form-row row mb-5"> 
						<div class="col-md-12">
							<div class="data-tables">
								<table id="dataTableTerminal" width="100%">
									<thead class="bg-light text-capitalize">
										<tr>
											<th style="width:30px;">No</th>
											<th>@lang('terminal.version')</th>
											<th>SN</th>
											<th>Model</th>
											<th>Merchant</th>
											<th>Profile</th>
											<th>@lang('general.action')</th>
										</tr>
									</thead>
									<tbody>
									   
									</tbody>
								</table>
							</div>
						</div>
					</div>
                </div>
            </div>
        </div>

    </div>
</div>

@endsection


@section('modal')
    @include('pages.terminal.modal_terminal')
@endsection


@section('script')
 <!-- Start datatable js -->

<!--<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
-->
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js"></script>
<script src="https://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.18/js/dataTables.bootstrap4.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.2.3/js/responsive.bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/xlsx/0.7.7/xlsx.core.min.js"></script>  
<script src="https://cdnjs.cloudflare.com/ajax/libs/xls/0.7.4-a/xls.core.min.js"></script> 
<script src="{{ asset('assets/js/terminal.js') }}"></script>
@endsection


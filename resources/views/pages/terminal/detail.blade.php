@extends('layouts.app')
@section('title', 'Terminal')
@section('ribbon')
@endsection


@section('content')
<div class="title-page">
    <div class="row">
            <div class="col-md-12">
                <ul class="breadcrumb mt-bread">
                    <li><a href="/terminal">TERMINALS</a></li>
                </ul>
            </div>
    </div> 
</div>
<div class="main-content-inner">
    <div class="row">
        <div class="col-12">
            <div class="card mb-tng">
                <div class="card-body">
                <form class="form-horizontal" id="form-tg">
                   
                    <div class="">
						<!--<div class="form-group">
							 <h4 class="modal-title">Detail Terminal</h4>
						</div>-->
						
						<div class="form-group">
							 <input type="hidden" id="id-terminal" value="{{$data? $data[0]['id'] : ""}}">
							 <input type="hidden" id="version-terminal" value="{{$data? $data[0]['version'] : ""}}">
							 <h6 class="">{{$data? $data[0]['sn'] : ""}}</h6>
						</div>
						<div class="form-row">
							 <div class="form-group col-md-3">
								<label for="">Device Model</label>
								<div>{{$data? $data[0]['model']['model'] : ""}}</div>
							 </div>
							<div class="form-group col-md-3">
								<label for="">Profile</label>
								<div>{{$data? $data[0]['profile']['name'] : ""}}</div>
							</div>
							<div class="form-group col-md-3">
								<label for="">Online Status</label>
								<div>Online/Offline/Disconnected</div>
							</div>
						</div>
						<div class="form-row">
							 <div class="form-group col-md-3">
								<label for="">Terminal Status</label>
								<div id="status-terminal">{{$data? $data[0]['locked']? "Locked" : "Unlocked"  : ""}}</div>
							 </div> 
							<div class="form-group col-md-3">
								<label for="">Last Heartbeat</label>
								<div>{{$dataHB? $dataHB[0]['updateTime'] : ""  }}</div>  
							</div>
							<div class="form-group col-md-3">
								<label for="">Last Diagnostic</label><!-- 25 Desember 2023 18:30:22 WIB -->
								<div>{{$dataDN? $dataDN[0]['updateTime'] : ""  }}</div>
							</div>
						</div>
						<div class="form-group">
							 <div class="mt-4" style="border-bottom:2px solid #579974;"></div>
						</div>
						<div class="form-group">
							 <h6 class="">Status</h6>
						</div>
						<div class="form-row"> 
							 <div class="form-group col-md-3">
								<label for="">Battery Percentage</label> 
								<div>{{$dataHB? $dataHB[0]['batteryPercentage'] : ""}}</div>
							 </div>
							<div class="form-group col-md-3">
								<label for="">Battery Temp</label>
								<div>{{$dataHB? $dataHB[0]['batteryTemp']." C" : ""}}</div>
							</div>
							<div class="form-group col-md-3">
								<label for="">&nbsp;</label>
								<div></div>
							</div>
						</div>
						<div class="form-row">
							 <div class="form-group col-md-3"> 
								<label for="">Cell Name</label>
								<div>{{$dataHB? $dataHB[0]['cellName'] : ""}}</div>
							 </div>
							<div class="form-group col-md-3">
								<label for="">Cell Type</label>
								<div>{{$dataHB? $dataHB[0]['cellType'] : ""}}</div>
							</div>
							<div class="form-group col-md-3">
								<label for="">Cell Strength</label>
								<div>{{$dataHB? $dataHB[0]['cellStrength']." dbm" : ""}}</div>
							</div>
						</div>
						<div class="form-group">
							 <div class="mt-4" style="border-bottom:2px solid #579974;"></div>
						</div>
						<div class="form-group">
							 <h6 class="">Action (advance only)</h6>
						</div>
						<div class="form-row">
							 <div class="form-group col-md-3">
								<div class="text-center"><a href="#" id="ForceHeartbeat" style="color:#717682;">
									<img src="{{asset('assets/images/icon/heart_beat.png')}}"/>
									<div class="mt-1">Force Heartbeat</div></a>
								</div>
								
							 </div>
							<div class="form-group col-md-3"><a href="#" id="ForceDiagnostic" style="color:#717682;">
								<div class="text-center">
									<img src="{{asset('assets/images/icon/diagnostic.png')}}"/>
									<div class="mt-1">Force Diagnostic</div></a>
								</div>
								
							</div>
							<div class="form-group col-md-3">
								<div class="text-center"><a href="#" id="btn-restart" style="color:#717682;">
									<img src="{{asset('assets/images/icon/rebort.png')}}"/>
									<div class="mt-1">Rebort</div></a>
								</div>
								
							</div>
							<div class="form-group col-md-3">
								<div class="text-center"><a href="#" id="lockUnlock" style="color:#717682;">
									<img src="{{asset('assets/images/icon/lock.png')}}"/>
									<div class="mt-1">Lock Terminal</div></a>
								</div>
								
							</div>
						</div>
						<div class="form-group">
							 <div class="mt-4" style="border-bottom:2px solid #579974;"></div>
						</div>
						<div class="form-group">
							 <h6 class="">Diagnostic</h6> 
						</div>
						
						<div class="form-row">
							 <div class="form-group col-md-3">
								<label for="">Total Memory</label>
								<div>{{$dataDN? $dataDN[0]['totalMemory']." Mb" : ""}}</div>
							 </div>
							<div class="form-group col-md-3">
								<label for="">Available Memory</label> 
								<div>{{$dataDN? $dataDN[0]['availableMemory']." Mb" : ""}}</div>
							</div>
							<div class="form-group col-md-3">
								<label for="">Flash Memory</label>
								<div>{{$dataDN? $dataDN[0]['totalFlashMemory']." Gb" : ""}}</div> 
							</div>
							<div class="form-group col-md-3">
								<label for="">Available Flash Memory</label>
								<div>{{$dataDN? $dataDN[0]['availableFlashMemory']." Mb" : ""}}</div>
							</div>
						</div>
						<div class="form-row">
							 <div class="form-group col-md-3">
								<label for="">Total Mobile Data</label> 
								<div>{{$dataDN? $dataDN[0]['totalMobileData']." Mb" : ""}}</div>
							 </div>
							<div class="form-group col-md-3">
								<label for="">Switching Times</label>
								<div>{{$dataDN? $dataDN[0]['switchingTimes']."" : ""}}</div>
							</div>
							<div class="form-group col-md-3">
								<label for="">Current Boot Times</label> 
								<div>{{$dataDN? $dataDN[0]['currentBootTime']."" : ""}}</div>
							</div>
							<div class="form-group col-md-3">
								<label for="">Total Boot Times</label>
								<div>{{$dataDN? $dataDN[0]['totalBootTime']."" : ""}}</div>
							</div>
						</div>
						<div class="form-row">
							 <div class="form-group col-md-3">
								<label for="">Total Length Printed</label>
								<div>{{$dataDN? $dataDN[0]['totalLengthPrinted']." mm" : ""}}</div>
							 </div>
							<div class="form-group col-md-3">
								<label for="">Swiping Card Times</label>
								<div>{{$dataDN? $dataDN[0]['swipingCardTimes']."" : ""}}</div>
							</div>
							<div class="form-group col-md-3">
								<label for="">DIP Inserting Times</label>
								<div>{{$dataDN? $dataDN[0]['dipInsertingTimes']."" : ""}}</div>
							</div>
							<div class="form-group col-md-3">
								<label for="">NFC Reading Times</label>
								<div>{{$dataDN? $dataDN[0]['nfcCardReadingTimes']."" : ""}}</div>
							</div>
						</div>
						<div class="form-row">
							 <div class="form-group col-md-3">
								<label for="">Latitude</label> 
								<div id="lat">{{$dataHB? $dataHB[0]['latitude']."" : ""}}</div>
							 </div>
							<div class="form-group col-md-3">
								<label for="">Longitude</label>
								<div id="lon">{{$dataHB? $dataHB[0]['longitude']."" : ""}}</div>
							</div>
						</div>
						<div class="form-row">
							 <div class="form-group col-md-9">
								<label for="">Installed Applications</label>
								<div>
										<table class="table" style="border:1px solid #D9D9D9;">
										  <thead>
											<tr>
											  <th scope="col">No</th>
											  <th scope="col">App Name</th>
											  <th scope="col">Package Name</th>
											  <th scope="col">Version</th>
											</tr>
										  </thead>
										  <tbody class="table-group-divider">
											<?php
												if(!empty($dataDN))
												{
													$no = 1;
													foreach($dataDN[0]['installedApps'] as $d)
													{
														echo "<tr><th scope='row'>".$no."</th><td>".$d['appName']."</td><td>".$d['packageName']."</td><td>".$d['appVersion']."</td></tr>";
														$no++;
													}
												}
											?>
										  </tbody>
										</table>
								</div>
							 </div>
						</div>
						<div class="form-group">
							 <div class="mt-4" style="border-bottom:2px solid #579974;"></div>
						</div>
						<div class="form-group">
							 <h6 class="">Map</h6>
						</div>
						<div class="form-group col-md-9">
							 <div class="mt-4 text-left">
								<!--<img style="margin-left:-12px;" src="{{asset('assets/images/icon/map.png')}}"/>-->
								<div id="googleMap" style="width:100%;height:400px;"></div>
							 </div>
						</div>
                    </div>
                    
                </form>
                   
                </div>
            </div>
        </div>

    </div>
</div>

@endsection

@section('script')
<!-- Start datatable js -->
<script>

	function myMap() {
		//-0.42228698730469
		//116.98654174805
		var la = "<?php echo $dataHB? $dataHB[0]['latitude']."" : ""; ?>";
		var lo = "<?php echo $dataHB? $dataHB[0]['longitude']."" : ""; ?>";
		console.log(la,"==",lo);
		var mapProp= {
		//center:new google.maps.LatLng(-0.42228698730469,116.98654174805),
		center:new google.maps.LatLng(la,lo),
		zoom:5,
		};
		var map = new google.maps.Map(document.getElementById("googleMap"),mapProp);
	}
	
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDIkU0HBh8-K1ocFIis-28xLNLBEmnNvwY&callback=myMap"></script>

<!-- <script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?sensor=false"></script> -->

<script src="{{ asset('assets/js/terminal.js') }}"></script>
	

@endsection

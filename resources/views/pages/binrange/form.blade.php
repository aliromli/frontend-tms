@extends('layouts.app')
@section('title', 'BIN RANGE')
@section('ribbon')
@endsection

@section('content')
<div class="title-page">
    <div class="row">
            <div class="col-md-12">
                <ul class="breadcrumb mt-bread">
                     <li><a href="/binrange">BIN RANGE</a></li>
                </ul>
            </div>
    </div> 
</div>
<div class="main-content-inner">
    <div class="row">
        <div class="col-12 mt-5">
            <div class="card mb-tng2">
                <div class="card-body">               
					<form class="form-horizontal" id="form-br">
						
						<div class="modal-body">
							<div class="form-group">
								<h4 class="title-form">{{ $edit=='no' ? 'Add BIN Range' : 'Edit BIN Range' }}</h4>
								<input type="hidden" class="form-control form-control-sm input-xs" id="br-id" value="{{$data ? $data[0]['id'] : ''}}">
							</div>
							<div class="form-row">
								<div class="form-group col-md-4">
									<label for="" class="control-label">Name</label>
									<input type="text" class="form-control form-control-sm input-xs" id="br-name" value="{{$data ? $data[0]['name'] : ''}}">
								</div>								
							</div>
							<div class="form-row">
								<div class="form-group col-md-4">
									<label for="" class="control-label">Card Num Range from</label>
									<input type="text" class="form-control form-control-sm input-xs" id="br-Card_Num_Range_from" value="{{$data ? $data[0]['binRangeStart'] : ''}}">
								</div>								
							</div>
							<div class="form-row">
								<div class="form-group col-md-4">
									<label for="" class="control-label">Card Num Range end</label>
									<input type="text" class="form-control form-control-sm input-xs" id="br-Card_Num_Range_end" value="{{$data ? $data[0]['binRangeEnd'] : ''}}">
								</div>								
							</div>
							<div class="form-row">
								<div class="form-group col-md-4">
									<label for="" class="control-label">PAN Digit unmasking</label>
									<div class="form-check">
										<input  id="br-PAN-Digit-unmasking" name="PAN_Digit_unmasking" type="checkbox"  class="form-check-input position-static" {{$data? json_encode($data[0]['panDigitUnmasking'])=='true' ? 'checked' : '' : ''}} value="true">
									</div>								
								</div>								
							</div>
							<div class="form-row">
								<div class="form-group col-md-4">
									<label for="" class="control-label">Card Num Length</label>
									<input type="text" class="form-control form-control-sm input-xs" id="br-Card_Num_Length" value="{{$data ? $data[0]['cardNumLength'] : ''}}">
								</div>
								<div class="form-group col-md-4">
									<label for="" class="control-label">PIN Length</label>
									<input type="text" class="form-control form-control-sm input-xs" id="br-PIN_Length" value="{{$data ? $data[0]['pinLength'] : ''}}">
								</div>
								
							</div>
							<div class="form-row">
								<div class="form-group col-md-4">
									<label for="" class="control-label">PIN Prompt</label>
									<div class="form-check">
											<input  id="br-PIN_Prompt" name="PIN_Prompt" type="checkbox"  class="form-check-input position-static" {{$data? json_encode($data[0]['pinPrompt'])=='true' ? 'checked' : '' : ''}} value="true">
									</div>
								</div>
								<div class="form-group col-md-4">
									<label for="" class="control-label">Print Cardholder Copy</label>
									<div class="form-check">
											<input  id="br-Print_Cardholder_Copy" name="Print_Cardholder_Copy" type="checkbox"  class="form-check-input position-static" {{$data? json_encode($data[0]['printCardholderCopy'])=='true' ? 'checked' : '' : ''}} value="true">
									</div>
								</div>
								
							</div>
							<div class="form-row">
								<div class="form-group col-md-4">
									<label for="" class="control-label">Print Merchant Copy</label>
									<div class="form-check">
											<input  id="br-Print_Merchant_Copy" name="Print_Merchant_Copy" type="checkbox"  class="form-check-input position-static" {{$data? json_encode($data[0]['printMerchantCopy'])=='true' ? 'checked' : '' : ''}} value="true">
									</div>
								</div>
								<div class="form-group col-md-4">
									<label for="" class="control-label">Print Bank Copy</label>
									<div class="form-check">
											<input  id="br-Print_Bank_Copy" name="Print_Bank_Copy" type="checkbox"  class="form-check-input position-static" {{$data? json_encode($data[0]['printBankCopy'])=='true' ? 'checked' : '' : ''}} value="true">
									</div>
								</div>
								
							</div>
							
							<div class="form-group row" id="div-dp-version" style="{{ $edit=='no' ? 'display:none;':'display:none;'}}">
								<label for="" class="control-label col-md-3">Version</label>
								<div class="col-md-8"><input type="text" class="form-control form-control-sm input-xs" readOnly id="version-br" value="{{$data? $data[0]['version'] : ''}}"></div>
							</div>
							<div class="form-group  mt-4">
								<button type="button" class="btn btn-rounded btn-primary" id="btn-submit-br"> @lang('general.save')</button>
								<a class="btn btn-rounded btn-secondary" href="{{url('/binrange')}}">@lang('general.cancel')</a>
								<span class="loading"><img class="hidden" width="40" src="{{asset('assets/images/icon/loader.gif')}}"/></span>
							</div>
						</div>
					</form>
                </div>
            </div>
        </div>

    </div>
</div>
@endsection
@section('script')
<!-- Start datatable js -->
<script src="{{ asset('assets/js/binrange.js') }}"></script>
@endsection

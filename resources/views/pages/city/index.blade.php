@extends('layouts.app')
@section('title', 'City')
@section('ribbon')
@endsection
@section('content')

<div class="title-page">
    <div class="row">
            <div class="col-md-12">
                <ul class="breadcrumb mt-bread">
                     <li><a href="/city">CITY</a></li>
                </ul>
            </div>
    </div> 
</div>
<div class="main-content-inner">
<div class="row">
        <div class="col-12">
            <div class="card mb-tng">
                <div class="card-body">
                    <div class="clear_filter">Clear Search</div>
                    <div class="box-header no-border text-left filter row mb-5"> 
                        <!-- <a class="btn btn-flat btn-secondary clear-search" href="#">Clear Search</a> &nbsp; -->
                        <!-- <a class="btn btn-flat btn-secondary tugel" href="#"><i class="fa fa-chevron-down sign"></i> <span class="filter-title">Show</span><span class="filter-title hidden">Hide</span> Filter <i class="fa fa-filter"></i></a> &nbsp; -->
                        <button class="btn btn-link text-secondary col-md-1 text-left" style="text-decoration: none;" id="btn-add-city"><i class="fa fa-plus-square fa-lg" style="color:#717682;"></i> &nbsp;Add City</button> 
                        <input type="text" id="search-city" class="form-control form-control-sm col-md-4 text-left ml-4" placeholder="Keyword City.."/>
                        <i class="ti-search" id="btn-ti-search-city"></i>
                    </div>
                  
                 
                    <div class="data-tables">
                        <table id="dataTableCity" class="dt-table" width="100%">
                            <thead class="bg-light text-capitalize">
                                <tr>
                                    <th style="width:30px;">No</th>
                                    <th>@lang('city.name')</th>
                                    <th>@lang('city.state')</th>
                                    <th>@lang('general.action')</th>
                                </tr>
                            </thead>
                            <tbody>
                               
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>

@endsection


@section('script')
 <!-- Start datatable js -->
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js"></script>
<script src="https://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.18/js/dataTables.bootstrap4.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.2.3/js/responsive.bootstrap.min.js"></script>
<script src="{{ asset('assets/js/city/index.js') }}"></script>


@endsection

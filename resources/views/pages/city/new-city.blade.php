<div class="modal fade" id="modal-city">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="page-loader hidden">
                <div class="sk-circle">
                    <div class="sk-circle1 sk-child"></div>
                    <div class="sk-circle2 sk-child"></div>
                    <div class="sk-circle3 sk-child"></div>
                    <div class="sk-circle4 sk-child"></div>
                    <div class="sk-circle5 sk-child"></div>
                    <div class="sk-circle6 sk-child"></div>
                    <div class="sk-circle7 sk-child"></div>
                    <div class="sk-circle8 sk-child"></div>
                    <div class="sk-circle9 sk-child"></div>
                    <div class="sk-circle10 sk-child"></div>
                    <div class="sk-circle11 sk-child"></div>
                    <div class="sk-circle12 sk-child"></div>
                </div>
            </div>
            <form class="form-horizontal" id="form-city">
                <div class="modal-header bg-dark no-border">
                    <h4 class="modal-title">New City</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group row">
                        <input type="hidden" class="form-control input-xs" id="city-id">
                        <label for="" class="control-label col-md-4">@lang('city.name')</label>
                        <div class="col-md-8"><input type="text" class="form-control input-xs" id="city-name"></div>
                    </div>
                    <div class="form-group row">
                        <label for="" class="control-label col-md-4">@lang('city.state')</label>
                        <div class="col-md-8">
                            <select class="form-control input-xs" id="city-state">
                                <option value="">&raquo; @lang('general.select') @lang('city.state')</option>
                                @foreach($state as $c)
                                <option value="{{ $c['id'] }}">{{ $c['name'] }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group row" id="divcity-version">
                        <label for="" class="control-label col-md-4">Version</label>
                        <div class="col-md-8"><input type="text" class="form-control input-xs" id="version-city"></div>
                    </div>
                 
					
                    
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-flat btn-primary" id="btn-submit"><i class="fa fa-check-circle"></i> @lang('general.submit')</button>
                    <button class="btn btn-flat btn-default" data-dismiss="modal">@lang('general.close')</button>
                </div>
            </form>
        </div>
    </div>
</div>

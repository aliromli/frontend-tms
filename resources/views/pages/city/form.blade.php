@extends('layouts.app')
@section('title', 'Form City')
@section('ribbon')
@endsection

<!-- Start datatable css -->
@section('content')

<div class="title-page">
    <div class="row">
            <div class="col-md-12">
                <ul class="breadcrumb mt-bread">
					<li><a href="/city">CITY</a></li>
                </ul>
            </div>
    </div> 
</div>
<div class="main-content-inner">
    <div class="row">
        <div class="col-12">
            <div class="card mb-tng">
                <div class="card-body">               
                    <form class="form-horizontal" id="form-ct">
						<div class="modal-header no-border">
							<h4 class="modal-title">{{ $edit=='no' ? 'Add City' : 'Edit City ' }}</h4>
						</div>
						<div class="modal-body">
							<div class="form-group">
								<input type="hidden" class="form-control form-control-sm input-xs" id="city-id" value="{{$data ? $data[0]['id'] : ''}}">
								<label for="name">Name</label>
								<input type="text" class="form-control form-control-sm input-xs w-50" id="city-name" value="{{$data ? $data[0]['name'] : ''}}" aria-describedby="nameHelp" placeholder="">
								<input type="hidden" class="form-control form-control-sm input-xs w-50" id="city-state-opt-id" value="{{$data ? $data[0]['state']['id'] : ''}}"  placeholder="">
						    
							</div>
							<div class="form-group">
								<label for="city-state">@lang('city.state')</label>
								<div>
									<select class="form-control form-control-sm input-xs w-50" id="city-state">
											<option value="">&raquo; @lang('general.select') @lang('city.state')</option>
											@if(!empty($state))
												@foreach($state as $c)
												<option value="{{ $c['id'] }}">{{ $c['name'] }}</option>
												@endforeach
											@endif
									</select>
								</div>
							</div>
							<div class="form-group" id="div-ct-version" style="{{ $edit=='no' ? 'display:none;':'display:none;'}}">
								<label for="" class="">Version</label>
								<input readOnly type="text" class="form-control form-control-sm input-xs" id="version-city" value="{{$data ? $data[0]['version'] : ''}}">
							</div>
							
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-primary btn-rounded" id="btn-submit-cd"></i> @lang('general.save')</button>
							<a class="btn btn-secondary btn-rounded" href="{{url('/city')}}">@lang('general.cancel')</a>
						</div>
					</form>
                   
                </div>
            </div>
        </div>

    </div>
</div>

@endsection


@section('script')
  
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
<script src="{{ asset('assets/js/city/city.js') }}"></script>

@endsection

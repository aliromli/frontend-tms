@extends('layouts.app')
@section('title', 'Form City')
@section('ribbon')
@endsection

<!-- Start datatable css -->
@section('content')

<div class="title-page">
    <div class="row">
            <div class="col-md-12">
                <ul class="breadcrumb mt-bread">
                    <li><a href="/district">DISTRICT</a></li>
                </ul>
            </div>
    </div> 
</div>
<div class="main-content-inner">
    <div class="row">
        <div class="col-12">
            <div class="card mb-tng">
                <div class="card-body">               
                    <form class="form-horizontal" id="form-dis">
						<div class="modal-header no-border">
							<h4 class="modal-title">{{ $edit=='no' ? 'Add District' : 'Edit District ' }}</h4>
						</div>
						<div class="modal-body">
							<div class="form-group">
								<input type="hidden" class="form-control form-control-sm input-xs w-50" id="district-id" value="{{$data ? $data[0]['id'] : ''}}">
								<label for="name">Name</label>
								<input type="text" class="form-control form-control-sm input-xs w-50" id="district-name" value="{{$data ? $data[0]['name'] : ''}}" aria-describedby="nameHelp" placeholder="">
								<input type="hidden" class="form-control form-control-sm input-xs w-50" id="city-district-opt-id" value="{{$data ? $data[0]['city']['id'] : ''}}"  placeholder="">

							</div>
							<div class="form-group">
                            
                                <input type="hidden" class="form-control form-control-sm input-xs" id="district-city-opt" value="{{$data ? $data[0]['city']['id'] : ''}}" >
								<label for="district-city">@lang('district.city')</label>
								<div>
									<select class="form-control form-control-sm input-xs w-50" id="district-city">
											<option value="">&raquo; @lang('general.select') @lang('district.city')</option>
											@if(!empty($city))
												@foreach($city as $c)
												<option value="{{ $c['id'] }}">{{ $c['name'] }}</option>
												@endforeach
											@endif
									</select>
								</div>
							</div>
                            
                            <div class="form-group" id="divdis-version" style="{{ $edit=='no' ? 'display:none;':'display:none;'}}">
								<label for="" class="">Version</label>
								<input readOnly type="text" class="form-control form-control-sm input-xs" id="version-dis" value="{{$data ? $data[0]['version'] : ''}}">
							</div>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-primary btn-rounded" id="btn-submit"></i> @lang('general.save')</button>
							<a class="btn btn-secondary btn-rounded" href="{{url('/district')}}">@lang('general.cancel')</a>
						</div>
					</form>
                   
                </div>
            </div>
        </div>

    </div>
</div>

@endsection


@section('script')
 <!-- Start datatable js -->
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
<script src="{{ asset('assets/js/district/district.js') }}"></script>

@endsection

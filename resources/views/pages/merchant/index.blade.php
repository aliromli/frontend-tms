@extends('layouts.app')
@section('title', 'Merchant')
@section('ribbon')
@endsection

<style>


#dataTableMerchantxx{
    overflow-x: scroll;
    max-width: 40%;
    display: block;
    white-space: nowrap;
}
</style>
@section('content')
<div class="title-page">
    <div class="row">
            <div class="col-md-12">
                <ul class="breadcrumb mt-bread">
                      <li><a href="/marchent">MARCHANT</a></li>
                </ul>
            </div>
    </div> 
</div>
<div class="main-content-inner">
    <div class="row">
        <div class="col-12">
            <div class="card mb-tng">
                <div class="card-body">
					<div class="clear_filter">Clear Search</div>
                    <div class="box-header no-border text-left filter row mb-5"> 
                        @if(Privilege::visible(Request::segment(1),'CREATE')=='Y')
						<a href="{{url('/merchant-form')}}" class="btn btn-link text-secondary col-md-1 text-left" style="text-decoration: none;"><i class="fa fa-plus-square fa-lg" style="color:#717682;"></i> &nbsp;Add Merchant</a> 
                        @endif
                        <input type="text" id="search-merchant" class="form-control form-control-sm col-md-4 text-left ml-5" placeholder="Keyword Merchant.."/>
                        <i class="ti-search" id="btn-ti-search-merchant" ></i>
                    </div>
                  
				    
                    <div class="form-filter">
                        <form class="form-horizontal">
                           
                                <div class="form-group row" xstyle="margin-left:-28;">
                                   
                                    <div class="col-md-4">
                                        <input type="text" id="search-merchant" class="form-control input-xs" placeholder="Merchant ...">
                                    </div>
                                    
                                    <div class="col-md-3">
                                        <button type="button" id="btn-cari-merchant" class="btn  btn-flat btn-secondary btn-cari">Cari</button>
                                    </div> 
                                </div>
                        </form>
                    </div> 
                    <div class="data-tables">
                        <table id="dataTableMerchant" width="100%">
                            <thead class="bg-light text-capitalize">
                                <tr>
                                    <th style="width:30px;">No</th>
                                    <th>@lang('merchant.version')</th>
                                    <th>@lang('merchant.name')</th>
                                    <th>@lang('merchant.company')</th>
                                    <th>@lang('merchant.district')</th>
                                    <th>@lang('merchant.city')</th>
                                    <th>@lang('merchant.state')</th>
                                    <th>@lang('merchant.address')</th>
                                    <th>@lang('merchant.zipcode')</th>
                                    <th>@lang('merchant.merchantType')</th>
                                    <th>@lang('general.action')</th>
                                </tr>
                            </thead>
                            <tbody>
                               
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>

@endsection


@section('script')
 <!-- Start datatable js -->
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js"></script>
<script src="https://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.18/js/dataTables.bootstrap4.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.2.3/js/responsive.bootstrap.min.js"></script>
<script src="{{ asset('assets/js/merchant/index.js') }}"></script>

@endsection

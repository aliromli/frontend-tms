@extends('layouts.app')
@section('title', 'Merchant')
@section('ribbon')
@endsection

<!-- Start datatable css -->

@section('content')

<div class="title-page">
    <div class="row">
            <div class="col-md-12">
                <ul class="breadcrumb mt-bread">
                    <li><a href="/marchent">MARCHANT</a></li>
                </ul>
            </div>
    </div> 
</div>
<div class="main-content-inner">
    <div class="row">
        <div class="col-12">
            <div class="card mb-tng">
                <div class="card-body">
                    <form class="form-horizontal" id="form-merchant">
                        <div class="modal-header no-border">
                            <h4 class="modal-title">{{ $edit=='no' ? 'Add Merchant' : 'Edit Merchant' }}</h4>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <input type="hidden" class="form-control form-control-sm input-xs" id="merchant-id" value="{{$data ? $data['id'] : ''}}">
                               
                                <label for="">@lang('merchant.name')</label>
                                <input type="text" class="form-control form-control-sm input-xs w-50" id="merchant-name" value="{{$data ? $data['name'] : ''}}">
                            </div>
                            <div class="form-group">
                                <label for="" class="">@lang('merchant.company')</label>
                                <input type="text" class="form-control form-control-sm input-xs  w-50" id="merchant-company" value="{{$data? $data['companyName'] : ''}}">
                            </div>
                            <div class="form-group">
                                <label for="" class="">@lang('merchant.address')</label>
                                <input type="text" class="form-control form-control-sm input-xs w-50" id="merchant-address" value="{{$data? $data['address'] : ''}}">
                            </div>
                            <div class="form-group">
                                <label for="" class="">@lang('merchant.district')</label>
								<input type="hidden" class="form-control form-control-sm input-xs" id="disId-opt" value="{{$data ? $data['district']['id'] : ''}}"> 
                                <div>
                                    <select class="form-control form-control-sm w-50" id="merchant-district">
											<option value="">&raquo; @lang('general.select') @lang('merchant.district')</option>
                                            @if(!empty($district))
                                                @foreach($district as $d)
                                                        <option value="{{ $d['id'] }}" >{{ $d['name'] }}</option>
                                                @endforeach
                                            @endif
                                    </select>
                                </div>
                            </div>
                               
                            <div class="form-group">
                                <label for="" class="">@lang('merchant.zipcode')</label>
                                <input type="text" class="form-control form-control-sm input-xs w-50" id="merchant-zipcode" value="{{$data? $data['zipcode'] : ''}}">
                            </div>
                            <div class="form-group">
                                <label for="" class="">@lang('merchant.merchantType')</label>
								 <input type="hidden" class="form-control form-control-sm input-xs" id="mtId-opt" value="{{$data ? $data['merchantType']['id'] : ''}}">
                                <div class="">
                                    <select class="form-control form-control-sm w-50" id="merchant-merchantType">
                                            <option value="">&raquo; @lang('general.select') @lang('merchant.name')</option>
                                            @if(!empty($merchantType))
                                                @foreach($merchantType as $c)
                                                        <option value="{{ $c['id'] }}" >{{ $c['name'] }}</option>
                                                @endforeach
                                            @endif
                                          
                                    </select>
                                </div>
                            </div>
                           
                            <div class="form-group row" id="div-m-version" style="{{ $edit=='no' ? 'display:none;':'display:none;'}}">
                                <label for="" class="control-label col-md-3">Version</label>
                                <div class="col-md-8"><input readOnly type="text" class="form-control form-control-sm input-xs" id="version-m" value="{{$data? $data['version'] : ''}}"></div>
                            </div>
                            
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-rounded btn-primary" id="btn-submit-merchant"> @lang('general.save')</button>
                            <a class="btn btn-rounded btn-secondary" href="{{url('/merchant')}}">@lang('general.cancel')</a>
                        </div>
                    </form>
                   
                </div>
            </div>
        </div>

    </div>
</div>

@endsection


@section('script')

<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
<script src="{{ asset('assets/js/merchant/merchant.js') }}"></script>

@endsection

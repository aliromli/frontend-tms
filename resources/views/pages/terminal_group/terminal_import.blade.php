<div class="modal fade" id="modal-list-terminal-import">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="page-loader hidden">
                <div class="sk-circle">
                    <div class="sk-circle1 sk-child"></div>
                    <div class="sk-circle2 sk-child"></div>
                    <div class="sk-circle3 sk-child"></div>
                    <div class="sk-circle4 sk-child"></div>
                    <div class="sk-circle5 sk-child"></div>
                    <div class="sk-circle6 sk-child"></div>
                    <div class="sk-circle7 sk-child"></div>
                    <div class="sk-circle8 sk-child"></div>
                    <div class="sk-circle9 sk-child"></div>
                    <div class="sk-circle10 sk-child"></div>
                    <div class="sk-circle11 sk-child"></div>
                    <div class="sk-circle12 sk-child"></div>
                </div> 
            </div>
            <form class="form-horizontal" id="form-list-terminal-g2">
                <div class="modal-header">
                    <h4 class="modal-title">List Terminal</h4>
                </div>
                <div class="modal-body">
                    <div class="input-form">
                        <div class="form-group">
                            <div class="alert alert-info"><i class="fa fa-info-circle"></i> Sebelum melakukan impor data, pastikan template excel yang digunakan sudah benar. Silakan unduh template di sini. <a href="{{ asset('template/template_list_terminal.xlsx') }}"><strong>Unduh template</strong></a>.</div>
						</div>
                        <div class="form-group">
                            <label for="" class="control-label">Upload file</label>
                            <input accept=".xlsx" id="excelfile" type="file" name="file" class="form-control input-xs">
                        </div>
                    </div>
					<div class="form-group text-right mt-5">
						<button type="button" class="btn btn-primary btn-rounded btn-xs" id="btn-select-to-list-import"><i class="fa fa-check-circle"></i> Add</button>
						<button class="btn btn-default btn-rounded btn-xs" data-dismiss="modal">@lang('general.close')</button>
						<span class="loading"><img class="hidden" width="40" src="{{asset('assets/images/icon/loader.gif')}}"/><span>
					</div>
                </div>
            </form>
        </div>
    </div>
</div>

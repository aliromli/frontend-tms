@extends('layouts.app')
@section('title', 'Terminal Group')
@section('ribbon')
@endsection

<!-- Start datatable css -->
<!--
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.18/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/responsive/2.2.3/css/responsive.bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/responsive/2.2.3/css/responsive.jqueryui.min.css">
<link rel="stylesheet" type="text/css" href="{{ asset('assets/css/plugins/smartadmin-production.min.css')}}"> 
<link rel="stylesheet" type="text/css" href="{{ asset('assets/css/plugins/theme-material.css')}}">  
<style>


</style>-->

@section('content')
<div class="title-page">
    <div class="row">
            <div class="col-md-12">
                <ul class="breadcrumb mt-bread">
                    <li><a href="/terminal-group">TERMINAL GROUP</a></li>
                </ul>
            </div>
    </div> 
</div>
<div class="main-content-inner">
    <div class="row">
        <div class="col-12">
            <div class="card mb-tng">
                <div class="card-body">
                <form class="form-horizontal" id="form-tg">
                   
                    <div class="modal-body">
						<div class="form-group">
							 <h4 class="modal-title">Import</h4>
						</div>
                        <div class="form-group row" xid="div-gt-version" style="display:none;">
                            <label for="" class="control-label col-md-3">Version</label>
                            <div class="col-md-8">
								<input type="text" readonly class="form-control input-xs" id="version-terminal-gim" value="{{$data? $data[0]['version'] : ''}}">
								<input type="hidden" readonly class="form-control input-xs" id="idg" value="{{$data? $data[0]['id'] : ''}}">
							</div>
                        </div>
						<div class="row mt-3">
							<div class="col-md-12">
								<div class="data-tables">
									<table id="dataTableListSN_TG_Result" width="100%">
										<thead class="bg-light text-capitalize">
											<tr>
												<th style="width:30px;">No</th>
												<th>SN</th>
												<th>Result</th>
											</tr>
										</thead>
										<tbody>
											
											<?php
												if($data_temp->count()>0)
												{
													$no=1;
													foreach($data_temp as $d){
													 
													 echo "<tr><td>".$no."</td><td><span class='sn'>".$d->sn."</span><span class='t_id' style='display:none;'>".$d->terminal_id."</span></td><td>".$d->result."</td></tr>";
													
													 $no++;
													}
												}	
												
											?>
										</tbody>
									</table>
								</div>
							</div>
							<div class="col-md-12">
								<div class="mt-3" style="color:#717682;">{{$found}} found from {{$data_temp->count()}} terminal</div>
							</div>
						</div>
						<div class="form-row mt-5">
							<button type="button" class="btn btn-rounded btn-primary" id="btn-submit-import-result">IMPORT</button>&nbsp;&nbsp;
							<span class="loading"><img class="hidden" width="40" src="{{asset('assets/images/icon/loader.gif')}}"/></span>
						</div>
				    </div>
                </form>
                   
                </div>
            </div>
        </div>

    </div>
</div>

@endsection


@section('modal')
    @include('pages.terminal_group.terminal_list_form')
@endsection

@section('script')
  <!-- Start datatable js -->
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js"></script>
<script src="https://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.18/js/dataTables.bootstrap4.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.2.3/js/responsive.bootstrap.min.js"></script>
<script src="{{ asset('assets/js/terminal_group.js') }}"></script>

@endsection

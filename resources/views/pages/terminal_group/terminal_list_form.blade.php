<div class="modal fade" id="modal-list-terminal-form"> 
    <div class="modal-dialog  modal-lg">
        <div class="modal-content">
            <div class="page-loader hidden">
                <div class="sk-circle">
                    <div class="sk-circle1 sk-child"></div>
                    <div class="sk-circle2 sk-child"></div>
                    <div class="sk-circle3 sk-child"></div>
                    <div class="sk-circle4 sk-child"></div>
                    <div class="sk-circle5 sk-child"></div>
                    <div class="sk-circle6 sk-child"></div>
                    <div class="sk-circle7 sk-child"></div>
                    <div class="sk-circle8 sk-child"></div>
                    <div class="sk-circle9 sk-child"></div>
                    <div class="sk-circle10 sk-child"></div>
                    <div class="sk-circle11 sk-child"></div>
                    <div class="sk-circle12 sk-child"></div>
                </div> 
            </div>
            <form class="form-horizontal" id="form-list-terminal-g2">
                <div class="modal-header">
                    <h4 class="modal-title">List Terminal</h4>
                </div>
                <div class="modal-body">
					<!--<div class="form-row">
						<input type="text" class="form-control form-control-sm input-xs" id="search-sn">
					</div>-->
					<div class="input-group mb-3">
					  <input type="text" class="form-control form-control-sm input-xs" id="search-sn" placeholder="SN .." aria-label="SN" aria-describedby="basic-addon2">
					  <div class="input-group-append">
						 <button class="btn  btn-outline-secondary btn-xs" type="button" id="btn-search-sn">Search</button>
						 <button class="btn btn-outline-secondary btn-xs" type="button" id="btn-clear-se">Clear</button>
					  </div>
					</div>
                    <div class="form-group">
						<div class="data-tables">
							<table id="dataTableTerminalListGForm" width="100%">
								<thead class="bg-light text-capitalize">
									<tr>
										<th style="width:30px;">No</th>								   
										<th>SN</th>
										<th>ModelName</th>
										<th>Merchant</th>
										<th>@lang('general.action')</th>
									</tr>
								</thead>
								<tbody>
								</tbody>
							</table>
						</div>
					</div>
					 <div class="form-group text-right mt-5">
						<button type="button" class="btn btn-primary btn-rounded btn-xs" id="btn-select-to-list-tg"><i class="fa fa-check-circle"></i> Add</button>
						<button class="btn btn-default btn-rounded btn-xs" data-dismiss="modal">@lang('general.close')</button>
						<span class="loading"><img class="hidden" width="40" src="{{asset('assets/images/icon/loader.gif')}}"/></span>
					</div>
                </div>
            </form>
        </div>
    </div>
</div>

@extends('layouts.app')
@section('title', 'Terminal Group')
@section('ribbon')
@endsection


@section('content')
<div class="title-page">
    <div class="row">
            <div class="col-md-12">
                <ul class="breadcrumb mt-bread">
                    <li><a href="/terminal-group">TERMINAL GROUP</a></li>
                </ul>
            </div>
    </div> 
</div>
<div class="main-content-inner">
    <div class="row">
        <div class="col-12">
            <div class="card mb-tng">
                <div class="card-body">
                <form class="form-horizontal" id="form-tg">
                   
                    <div class="modal-body">
						<div class="form-group">
							 <h4 class="modal-title">{{ $edit=='no' ? 'Add  Group' : 'Edit  Group ' }}</h4>
						</div>
                        <div class="form-group">
							
                            <input type="hidden" class="form-control input-xs" id="terminal-g-id" value="{{$data ? $data[0]['id'] : ''}}">
                            <label for="" class="control-label">Group Name</label>
                            <input type="text" class="form-control input-xs" id="terminal-g-name" value="{{$data ? $data[0]['name'] : ''}}" style="width:50%;">
                        </div>
                        <div class="form-group">
                            <label for="" class="control-label">Group Description</label>
                            <input type="text" class="form-control input-xs" id="terminal-g-description" value="{{$data ? $data[0]['description'] : ''}}" style="width:50%;">
                        </div>
                        <!--<div class="form-group row">
                            <label for="" class="control-label col-md-3">Terminal</label>
                            <div class="cardx col-md-8">
                                <div class="xcard-body">
                                    <button type="button" class="btn btn-flat btn-primary" id="add-terminal-to-list"> Click List Terminal</button>
                                    <table class="table mt-2" id="table-form-terminal-group">
                                        <thead>
                                            <tr>
                                                <th>SN</th>
                                                <th>Model</th>
                                                <th>Merchant</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody> 
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>-->
                        
                        <div class="form-group row" xid="div-gt-version" style="<?php echo ($edit=='no') ? 'display:none;':'display:none;'; ?>">
                            <label for="" class="control-label col-md-3">Version</label>
                            <div class="col-md-8"><input type="text" readonly class="form-control input-xs" id="version-terminal-g" value="{{$data? $data[0]['version'] : ''}}"></div>
                        </div>
						<div class="form-row mt-5">
							<button type="button" class="btn btn-rounded btn-primary" id="btn-submit-terminal-group">@lang('general.save')</button>&nbsp;&nbsp;
							<a class="btn btn-rounded btn-secondary" href="{{url('/terminal-group')}}">@lang('general.cancel')</a>
						</div>
                    </div>
                    
                </form>
                   
                </div>
            </div>
        </div>

    </div>
</div>

@endsection


@section('modal')
    @include('pages.terminal_group.terminal_list_form')
@endsection

@section('script')
 <!-- Start datatable js -->
 
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js"></script>
<script src="https://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.18/js/dataTables.bootstrap4.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.2.3/js/responsive.bootstrap.min.js"></script>
<script src="{{ asset('assets/js/terminal_group.js') }}"></script>

@endsection

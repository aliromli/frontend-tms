@extends('layouts.app')
@section('title', 'Terminal Group')
@section('ribbon')
@endsection


@section('content')
<div class="title-page">
    <div class="row">
            <div class="col-md-12">
                <ul class="breadcrumb mt-bread">
                    <li><a href="/terminal-group">TERMINAL GROUP</a></li>
                </ul>
            </div>
    </div> 
</div>
<div class="main-content-inner">
    <div class="row">
        <div class="col-12">
            <div class="card mb-tng">
                <div class="card-body">
                <form class="form-horizontal" id="form-tg">
                  <div class="modal-body"> 
						<div class="form-group">
							 <h4 class="modal-title">Detail Group</h4>
						</div>
                        <div class="form-group row">
                             <div id="terminal_g_detail_id" style="display:none;"><?php echo $id;?></div>
                             <label for="" class="control-label col-md-12">Group Name : <span id="terminal-g-detail-gn"></span></label>
                        </div>
                        <div class="form-group row" style="margin-top:-15px">
                            <label for="" class="control-label col-md-12">Group Description : <span id="terminal-g-detail-gd"></span></label>
                        </div>
						<div class="form-row row">
							<div class="col-md-12">
								<button type="button" id="add-terminal-to-list-import" class="btn btn-outline-secondary btn-xs">import</button>
								<button type="button" id="add-terminal-to-list" class="btn btn-outline-secondary btn-xs" style="margin-left:20px;">add..</button>
							</div>
						</div>
                     
						<div class="row mt-3">
							<div class="col-md-10">
								<div class="data-tables">
									<table class='table' id="table_form_terminal_group_sn_terminal" width="100%">
										<thead class="bg-light text-capitalize">
											<tr>
												<th style="width:30px;">No</th>
												<th>Version</th>
												<th>SN</th>
												<th>TID</th>
												<th align="center">@lang('general.action')</th>
											</tr>
										</thead>
										<tbody>
										</tbody>
									</table>
								</div>
							</div>
						</div>
						 <div class="form-group row"  style="display:none;">
                            <label for="" class="control-label col-md-3">Version</label>
                            <div class="col-md-8"><input type="text" readonly class="form-control input-xs" id="version-terminal-g-detail" ></div>
                        </div>
						
                    </div>
                    
                </form>
                   
                </div>
            </div>
        </div>

    </div>
</div>

@endsection


@section('modal')
    @include('pages.terminal_group.terminal_list_form')
    @include('pages.terminal_group.terminal_import')
@endsection

@section('script')
 <!-- Start datatable js -->
 <!-- Start datatable js -->
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js"></script>
<script src="https://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.18/js/dataTables.bootstrap4.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.2.3/js/responsive.bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/xlsx/0.7.7/xlsx.core.min.js"></script>  
<script src="https://cdnjs.cloudflare.com/ajax/libs/xls/0.7.4-a/xls.core.min.js"></script> 
<script src="{{ asset('assets/js/terminal_group.js') }}"></script>

@endsection

@extends('layouts.app')
@section('title', 'MerchantType')
@section('ribbon')
@endsection

<!-- Start datatable css -->

@section('content')

<div class="title-page">
    <div class="row">
            <div class="col-md-12">
                <ul class="breadcrumb mt-bread">
                    <li><a href="/marchenttype">MARCHENT TYPE</a></li>
                </ul>
            </div>
    </div> 
</div>
<div class="main-content-inner">
    <div class="row">
        <div class="col-12">
            <div class="card mb-tng">
                <div class="card-body">
                     <div class="clear_filter">Clear Search</div>
                     <div class="box-header no-border text-left filter row mb-5"> 
                        @if(Privilege::visible(Request::segment(1),'CREATE')=='Y')
						<button class="btn btn-link text-secondary col-md-1 text-left mr-5" style="text-decoration: none;" id="btn-add-merchantType"><i class="fa fa-plus-square fa-lg" style="color:#717682;"></i> &nbsp;Add Merchant Type</button> 
                        @endif
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="text" id="search-merchantType" class="form-control form-control-sm col-md-4 text-left" placeholder="Keyword Merchant Type.."/>
                        <i class="ti-search" id="btn-ti-search-merchant-type"></i>
                    </div>
                    
                    <div class="data-tables">
                        <table id="dataTablemerchantType"  xclass="text-center" width="100%">
                            <thead class="bg-light text-capitalize">
                                <tr>
                                    <th style="width:30px;">No</th>
                                    <th>@lang('merchant-type.version')</th>
                                    <th>@lang('merchant-type.name')</th>
                                    <th>@lang('merchant-type.description')</th>
                                    <th>@lang('general.action')</th>
                                </tr>
                            </thead>
                            <tbody>
                               
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>

@endsection

@section('modal')
    @include('pages.merchant_type.new-merchantType')
   
@endsection

@section('script')
 <!-- Start datatable js -->
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js"></script>
<script src="https://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.18/js/dataTables.bootstrap4.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.2.3/js/responsive.bootstrap.min.js"></script>

<script src="{{ asset('assets/js/merchantType.js') }}"></script>

@endsection

@extends('layouts.app')
@section('title', 'Form MerchantType')
@section('ribbon')
@endsection

<!-- Start datatable css -->
@section('content')

<div class="title-page">
    <div class="row">
            <div class="col-md-12">
                <ul class="breadcrumb mt-bread">
                    <li><a href="/marchenttype">MARCHENT TYPE</a></li>
				</ul>
            </div>
    </div> 
</div>

<div class="main-content-inner">
    <div class="row">
        <div class="col-12">
            <div class="card mb-tng">
                <div class="card-body">               
                     <form class="form-horizontal" id="form-MerchantType">
						<div class="modal-header no-border">
							<h4 class="modal-title">{{ $edit=='no' ? 'Add Merchant Type' : 'Edit Merchant Type ' }}</h4>
						</div>
						<div class="modal-body">
							<div class="form-group">
								<input type="hidden" class="form-control form-control-sm input-xs w-50" id="merchantType-id" value="{{$data ? $data[0]['id'] : ''}}">
								<label for="name">@lang('merchant-type.name')</label>
								<input type="text" class="form-control form-control-sm input-xs  w-50" id="merchantType-name" value="{{$data ? $data[0]['name'] : ''}}"  placeholder="">
							</div>
							<div class="form-group">
								<label for="name">Description</label>
								<textarea class="form-control form-control-sm input-xs w-50" id="merchantType-description" rows="3">{{$data ? $data[0]['description'] : ''}}</textarea>
							</div>
							<div class="form-group" id="divmerType-version" style="{{ $edit=='no' ? 'display:none;':'display:none;'}}">
								<label for="" class="">Version</label>
								<input readOnly type="text" class="form-control form-control-sm input-xs" id="version-mt" value="{{$data ? $data[0]['version'] : ''}}">
							</div>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-rounded btn-primary" id="btn-submit">@lang('general.save')</button>
							<a class="btn btn-secondary btn-rounded" href="{{url('/merchanttype')}}">@lang('general.cancel')</a>
						</div>
					</form>
                   
                </div>
            </div>
        </div>

    </div>
</div>

@endsection
@section('script')
<script src="{{ asset('assets/js/merchantType.js') }}"></script>

@endsection

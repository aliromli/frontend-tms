@extends('layouts.app')
@section('title', 'Delete Task')
@section('ribbon')
@endsection


@section('content')

<div class="title-page">
    <div class="row">
            <div class="col-md-12">
                <ul class="breadcrumb mt-bread">
					<li><a href="/delete-task">DELETE TASK</a></li>
                </ul>
            </div>
    </div> 
</div>
<div class="main-content-inner">
    <div class="row">
        <div class="col-12 mt-5">
            <div class="card mb-tng2">
                <div class="card-body">
					<form class="form-horizontal" id="form-delete-task">
						
						<div class="modal-body">
							<div class="form-group">
							  <h4 class="title-form">{{ $edit=='no' ? 'New  Delete Task' : 'Edit  Delete Task ' }}</h4>
                              <input type="hidden" class="form-control input-xs" id="delete-task-id" value="{{$data ? $data[0]['id'] : ''}}">
							</div>
							 <div class="form-group mt-3">
								<label for="" class="control-label">Name</label>
								<input type="text" style="width:33%;" class="form-control input-xs" id="delete-task-name" value="{{$data ? $data[0]['name'] : ''}}">
							</div>
							<!--
							<div class="form-group">
								<label for="" class="control-label">Add Application</label>
								<input type="text" class="form-control input-xs"  style="width:33%;"/>
							</div> 
							-->
							
							<div class="form-row">
								<div class="form-group col-md-4">
									<label for="" class="control-label">Delete Time</label>
									<input type="datetime-local" step=1 value="{{$data? $data[0]['deleteTime'] : ''}}" class="form-control input-xs" id="delete-task-time">
								</div>
							</div>
							<div class="form-group">
								<label for="app-dlt"><a  href="#" class="btn btn-link text-secondary text-left" style="margin-left:-20px;text-decoration: none;display: inline;" id="add-app-to-list">Applications <i class="fa fa-plus-square fa-lg" style="color:#717682;margin-left:20px;"></i></a></label>
								<table class="table mt-2 table-striped" id="list-app-del-t">
									<thead  class="thead-light">
										<tr>
											<th>No</th>
											<th>App Version</th>
											<th>Name</th>
											<th>Package Name</th>
											<th>Action</th>
										</tr>
									</thead>
									<tbody> 
									</tbody>
								</table>
							</div>
							<div class="form-group">
								<label for="exampleFormControlSelect-tg-d"><a  href="#" class="btn btn-link text-secondary text-left" style="margin-left:-20px;text-decoration: none;display: inline;" id="btn-add-terminal-group-del">Terminal Groups <i class="fa fa-plus-square fa-lg" style="color:#717682;margin-left:20px;"></i></a></label>
								<select multiple class="form-control" id="select-tg-delt" style="width:33%;">
									
								</select>
							</div>
							<div class="form-group">
								<label for="exampleFormControlSelect-t-d"><a  href="#" class="btn btn-link text-secondary text-left" style="margin-left:-20px;text-decoration: none;display: inline;" id="btn-add-terminal-list-del">Terminal List <i class="fa fa-plus-square fa-lg" style="color:#717682;margin-left:20px;"></i></a></label>
								<select multiple class="form-control" id="select-t-delt" style="width:33%;">
									
								</select>
							</div>
							
							<div class="form-group row" id="div-version-dtask" style="{{ $edit=='no' ? 'display:none;':'display:none;'}}">
								<label for="" class="control-label col-md-3">Version</label>
								<div class="col-md-8">
					<input readOnly type="text" class="form-control input-xs" id="version-app-dtask" value="{{$data? $data[0]['version'] : ''}}"/>
								</div>
							</div>
							<div class="form-group mt-5">
								<button type="button" class="btn btn-rounded btn-primary" id="btn-submit-delete-task">@lang('general.save')</button>&nbsp;&nbsp;
								<a class="btn btn-rounded btn-secondary" href="{{url('/delete-task')}}">@lang('general.cancel')</a>
							</div>
						</div>
						
					</form>
                </div>
            </div>
        </div>

    </div>
</div>

@endsection

@section('modal')
    @include('pages.delete_task.terminal_list_modal')
    @include('pages.delete_task.app_form')
@endsection

@section('script')
 <!-- Start datatable js -->
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js"></script>
<script src="https://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.18/js/dataTables.bootstrap4.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.2.3/js/responsive.bootstrap.min.js"></script>
<script src="{{ asset('assets/js/delete_task.js') }}"></script>
@endsection

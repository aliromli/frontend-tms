<div class="modal fade" id="modal-app-form">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="page-loader hidden">
                <div class="sk-circle">
                    <div class="sk-circle1 sk-child"></div>
                    <div class="sk-circle2 sk-child"></div>
                    <div class="sk-circle3 sk-child"></div>
                    <div class="sk-circle4 sk-child"></div>
                    <div class="sk-circle5 sk-child"></div>
                    <div class="sk-circle6 sk-child"></div>
                    <div class="sk-circle7 sk-child"></div>
                    <div class="sk-circle8 sk-child"></div>
                    <div class="sk-circle9 sk-child"></div>
                    <div class="sk-circle10 sk-child"></div>
                    <div class="sk-circle11 sk-child"></div>
                    <div class="sk-circle12 sk-child"></div>
                </div>
            </div> 
            <form class="form-horizontal" id="form-app">
                <div class="modal-header bg-dark no-border">
                    <h4 class="modal-title">Add Application</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group row">
                        <input type="hidden" class="form-control input-xs" id="app-id">
                        <label for="" class="control-label col-md-4">App Name</label>
                        <div class="col-md-8"><input type="text" class="form-control input-xs" id="app-name"></div>
                    </div>
                    <div class="form-group row">
                        <label for="" class="control-label col-md-4">Package Name</label>
                        <div class="col-md-8"><input type="text" class="form-control input-xs" id="app-package-name"></div>
                    </div>
                    <div class="form-group row" id="div-m-version">
                        <label for="" class="control-label col-md-4">App Version</label>
                        <div class="col-md-8"><input type="text" class="form-control input-xs" id="app-version-app"></div>
                    </div>                    
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-flat btn-primary btn-xs" id="btn-submit-modal-app"><i class="fa fa-check-circle"></i> Add</button>
                    <button class="btn btn-flat btn-default btn-xs" data-dismiss="modal"> Close</button>
                </div>
            </form>
        </div>
    </div>
</div>

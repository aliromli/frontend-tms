@extends('layouts.app')
@section('title', 'Delete Task')
@section('ribbon')
@endsection

<style>
#dataTableApp1 {
    overflow-x: scroll;
    max-width: 40%;
    display: block;
    white-space: nowrap;
}
#dataTableApp2 {
    overflow-x: scroll;
    max-width: 40%;
    display: block;
    white-space: nowrap;
}
</style>
@section('content')

<div class="title-page">
    <div class="row">
            <div class="col-md-12">
                <ul class="breadcrumb mt-bread">
                    <li><a href="/delete-task">DELETE TASK</a></li>
                </ul>
            </div>
    </div> 
</div>
<div class="main-content-inner">
    <div class="row">
        <div class="col-12 mt-5">
            <div class="card mb-tng2">
                <div class="card-body">
                <form class="form-horizontal" id="form-download-task">
               
                <div class="modal-body">
                    <div class="form-group">
					      <h4 class="title-form">{{$data ? $data[0]['name'] : ''}}</h4>
                           <input type="hidden" class="form-control input-xs" id="download-task-id" value="{{$data ? $data[0]['id'] : ''}}">
                   </div>
                    <div class="form-group mt-3">
                        <label for="" class="control-label">Application</label>
                        <table style="width:100%;border:0.5px solid #D9D9D9;">
                            <!--<tr>
                                <td style="width:33.3%;padding:10px;">FMS 123</td>
                                <td style="width:33.3%;padding:10px;">com.briit.brimobile</td>
                                <td style="width:33.3%;padding:10px;">fms_1.0.1</td>
                            </tr>-->
							<?php
								if($data)
								{
									foreach($data[0]['applications'] as $g)
									{
											echo '<tr>';
											echo	'<td style="width:33.3%;padding:10px;">'.$g['name'].'</td>';
											echo	'<td style="width:33.3%;padding:10px;">'.$g['packageName'].'</td>';
											echo	'<td style="width:33.3%;padding:10px;">'.$g['appVersion'].'</td>';
											echo '</tr>';
									}
									
								}	
							?>
							
                        </table>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-4">
                            <label for="" class="control-label">Delete Time</label>
                            <!--<div>5 Desember 2023 18:00:00 WIB</div>-->
							<div>{{$data ? $data[0]['deleteTime']:''}}</div>
                        </div>
                       
					</div>
                    <div class="form-row">
                        <div class="form-group col-md-4">
                            <label for="" class="control-label">Task Report <button type="button" class="btn btn-outline-secondary btn-xs" id="btn-export-delt" style="margin-left: 40px;display: inline;">Export</button>
							</label>
                        </div>
					</div>
					<div class="form-row row mb-5"> 
						<div class="col-md-12">
							<div class="data-tables">
								<table id="dataTableViewDeleteTask" width="100%">
									<thead class="bg-light text-capitalize">
										<tr>
											<th style="width:30px;">No</th>
											<th>SN</th>
											<th>Group</th>
											<th>Status</th>
										</tr>
									</thead>
									<tbody>
									   
									</tbody>
								</table>
							</div>
						</div>
					</div>
                    <div class="form-group mt-5">
                         <a class="btn btn-rounded btn-primary" href="{{url('/delete-task')}}">@lang('general.cancel')</a>
                    </div>
                </div>
               
            </form>
                   
                </div>
            </div>
        </div>

    </div>
</div>
@endsection


@section('script')

<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js"></script>
<script src="https://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.18/js/dataTables.bootstrap4.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.2.3/js/responsive.bootstrap.min.js"></script> 
<script src="{{ asset('assets/js/delete_task.js') }}"></script>
 @endsection

@extends('layouts.app')
@section('title', 'TleSetting')
@section('ribbon')
@endsection

@section('content')

<div class="title-page">
    <div class="row">
            <div class="col-md-12">
                <ul class="breadcrumb mt-bread">
                    <li><a href="/tlesetting">TLE</a></li>
                </ul>
            </div>
    </div> 
</div>
<div class="main-content-inner">
    <div class="row">
        <div class="col-12 mt-5">
            <div class="card mb-tng2">
                <div class="card-body">               
                    <form class="form-horizontal" id="form-dp">
						
						<div class="form-group">
								<h4 class="modal-title">{{ $edit=='no' ? 'Add TLE' : 'Edit TLE' }}</h4>
								<input type="hidden" class="form-control input-xs" id="tl-id" value="{{$data ? $data[0]['id'] : ''}}">
						</div>
						<div class="modal-body">
							<div class="form-row">
								<div class="form-group col-md-4">
									<label for="" class="control-label">TLE Eft Sec</label>
									<input type="text" class="form-control input-xs" id="tl-tleEftSec" value="{{$data ? $data[0]['tleEftSec'] : ''}}">
								</div>
								<div class="form-group col-md-4 offset-2">
									<label for="" class="control-label">TLE ID</label>
									<input type="text" class="form-control input-xs" id="tl-tleId" value="{{$data? $data[0]['tleId'] : ''}}">
								</div>
							</div>
							<div class="form-row">
								<div class="form-group col-md-4">
									<label for="" class="control-label">Acquirer ID</label>
									<input type="text" class="form-control input-xs" id="tl-acquirerId" value="{{$data ? $data[0]['acquirerId'] : ''}}">
								</div>
								<div class="form-group col-md-4 offset-2">
									<label for="" class="control-label">LTMK AID</label>
									<input type="text" class="form-control input-xs" id="tl-ltmkAid" value="{{$data? $data[0]['ltmkAid'] : ''}}">
								</div>
							</div>
							<div class="form-row">
								<div class="form-group col-md-4">
									<label for="" class="control-label">Vendor ID</label>
									<input type="text" class="form-control input-xs" id="tl-vendorId" value="{{$data ? $data[0]['vendorId'] : ''}}">
								</div>
								<div class="form-group col-md-4 offset-2">
									<label for="" class="control-label">TLE Ver</label>
									<input type="text" class="form-control input-xs" id="tl-tleVer" value="{{$data? $data[0]['tleVer'] : ''}}">
								</div>
							</div>
							<div class="form-row">
								<div class="form-group col-md-4">
									<label for="" class="control-label">KMS Secure NII</label>
									<input type="text" class="form-control input-xs" id="tl-kmsSecureNii" value="{{$data ? $data[0]['kmsSecureNii'] : ''}}">
								</div>
								<div class="form-group col-md-4 offset-2">
									<label for="" class="control-label">EDC Secure NII</label>
									<input type="text" class="form-control input-xs" id="tl-edcSecureNii" value="{{$data? $data[0]['edcSecureNii'] : ''}}">
								</div>
							</div>
							<div class="form-row">
								<div class="form-group col-md-4">
									<label for="" class="control-label">CAPK Exponent</label>
									<input type="text" class="form-control input-xs" id="tl-capkExponent" value="{{$data ? $data[0]['capkExponent'] : ''}}">
								</div>
								<div class="form-group col-md-4 offset-2">
									<label for="" class="control-label">CAPK Length</label>
									<input type="text" class="form-control input-xs" id="tl-capkLength" value="{{$data? $data[0]['capkLength'] : ''}}">
								</div>
							</div>
							<div class="form-row">
								<div class="form-group col-md-10">
									<label for="" class="control-label">CAPK Value</label>
									<textarea class="form-control" rows="4" id="tl=capkValue">Belum ada colom DB</textarea>
								</div>
								
							</div>
							<div class="form-row">
								<div class="form-group col-md-4">
									<label for="" class="control-label">AID Length</label>
									<input type="text" class="form-control input-xs" id="tl-aidLength" value="{{$data ? $data[0]['aidLength'] : ''}}">
								</div>
								
							</div>
							<div class="form-row">
								<div class="form-group col-md-10">
									<label for="" class="control-label">AID Value</label>
									<textarea class="form-control" rows="4" id="tl=aidValue">Belum ada colom DB</textarea>
								</div>
								
							</div>
							<div class="form-row">
								<div class="form-group col-md-4">
									<label for="" class="control-label">Field Encrypted 1</label>
									<input type="text" class="form-control input-xs" id="tl-encryptedField1" value="{{$data ? $data[0]['encryptedField1'] : ''}}">
								</div>
								<div class="form-group col-md-4 offset-2">
									<label for="" class="control-label">Field Encrypted 2</label>
									<input type="text" class="form-control input-xs" id="tl-encryptedField2" value="{{$data? $data[0]['encryptedField2'] : ''}}">
								</div>
							</div>
							<div class="form-row">
								<div class="form-group col-md-4">
									<label for="" class="control-label">Field Encrypted 3</label>
									<input type="text" class="form-control input-xs" id="tl-encryptedField3" value="{{$data ? $data[0]['encryptedField3'] : ''}}">
								</div>
								<div class="form-group col-md-4 offset-2">
									<label for="" class="control-label">Field Encrypted 4</label>
									<input type="text" class="form-control input-xs" id="tl-encryptedField4" value="{{$data? $data[0]['encryptedField4'] : ''}}">
								</div>
							</div>
							<div class="form-row">
								<div class="form-group col-md-4">
									<label for="" class="control-label">Field Encrypted 5</label>
									<input type="text" class="form-control input-xs" id="tl-encryptedField5" value="{{$data ? $data[0]['encryptedField5'] : ''}}">
								</div>
								<div class="form-group col-md-4 offset-2">
									<label for="" class="control-label">Field Encrypted 6</label>
									<input type="text" class="form-control input-xs" id="tl-encryptedField6" value="{{$data? $data[0]['encryptedField6'] : ''}}">
								</div>
							</div>
							<div class="form-row">
								<div class="form-group col-md-4">
									<label for="" class="control-label">Field Encrypted 7</label>
									<input type="text" class="form-control input-xs" id="tl-encryptedField7" value="{{$data ? $data[0]['encryptedField7'] : ''}}">
								</div>
								<div class="form-group col-md-4 offset-2">
									<label for="" class="control-label">Field Encrypted 8</label>
									<input type="text" class="form-control input-xs" id="tl-encryptedField8" value="{{$data? $data[0]['encryptedField8'] : ''}}">
								</div>
							</div>
							<div class="form-row">
								<div class="form-group col-md-4">
									<label for="" class="control-label">Field Encrypted 9</label>
									<input type="text" class="form-control input-xs" id="tl-encryptedField9" value="{{$data ? $data[0]['encryptedField9'] : ''}}">
								</div>
								<div class="form-group col-md-4 offset-2">
									<label for="" class="control-label">Field Encrypted 10</label>
									<input type="text" class="form-control input-xs" id="tl-encryptedField10" value="{{$data? $data[0]['encryptedField10'] : ''}}">
								</div>
							</div>
							<div class="form-group row" id="div-dp-version" style="{{ $edit=='no' ? 'display:none;':'display:none;'}}">
								<label for="" class="control-label col-md-3">Version</label>
								<div class="col-md-8"><input type="text" class="form-control input-xs" readOnly id="version-tl" value="{{$data? $data[0]['version'] : ''}}"></div>
							</div>
							<div class="form-group mt-4">
								<button type="button" class="btn btn-rounded btn-primary" id="btn-submit-tl">@lang('general.save')</button>
								<a class="btn btn-rounded btn-secondary" href="{{url('/tleSetting')}}">@lang('general.cancel')</a>
								<span class="loading"><img class="hidden" width="40" src="{{asset('assets/images/icon/loader.gif')}}"/></span>
							</div>
						</div>
					</form>
                </div>
            </div>
        </div>

    </div>
</div>

@endsection


@section('script')
 <!-- Start datatable js -->
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js"></script>
<script src="https://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.18/js/dataTables.bootstrap4.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.2.3/js/responsive.bootstrap.min.js"></script>
<script src="{{ asset('assets/js/tleSetting.js') }}"></script>

@endsection

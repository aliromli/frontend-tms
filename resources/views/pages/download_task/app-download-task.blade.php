<div class="modal fade" id="modal-list-app-form">
    <div class="modal-dialog  modal-lg" >
        <div class="modal-content">
            <div class="page-loader hidden">
                <div class="sk-circle">
                    <div class="sk-circle1 sk-child"></div>
                    <div class="sk-circle2 sk-child"></div>
                    <div class="sk-circle3 sk-child"></div>
                    <div class="sk-circle4 sk-child"></div>
                    <div class="sk-circle5 sk-child"></div>
                    <div class="sk-circle6 sk-child"></div>
                    <div class="sk-circle7 sk-child"></div>
                    <div class="sk-circle8 sk-child"></div>
                    <div class="sk-circle9 sk-child"></div>
                    <div class="sk-circle10 sk-child"></div>
                    <div class="sk-circle11 sk-child"></div>
                    <div class="sk-circle12 sk-child"></div>
                </div> 
            </div>
            <form class="form-horizontal" id="form-list-app-g">
                <div class="modal-header no-border">
                    <h4 class="modal-title">List Application</h4>
                </div>
                <div class="modal-body">
					<div class="input-group mb-3">
					  <input type="text" class="form-control form-control-sm input-xs" id="search-name-app-dlt" placeholder="Name .." aria-label="Name" aria-describedby="basic-addon2">
					  <div class="input-group-append">
						 <button class="btn  btn-outline-secondary btn-xs" type="button" id="btn-search-name-app-dlt">Search</button>
						 <button class="btn btn-outline-secondary btn-xs" type="button" id="btn-clear-name-app-dlt">Clear</button>
					  </div>
					</div> 
                    
                    <div class="data-tables">
                        <table id="dataTableAppListGForm" width="100%">
                            <thead class="bg-light text-capitalize">
                                <tr>
                                    <th style="width:30px;">No</th>
                                    <th>App Version</th>
                                    <th>Name</th>
                                    <th>Device Model</th>
                                    <th>Package Name</th>
                                    <th>@lang('general.action')</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                    
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary btn-rounded btn-flat" id="btn-app-to-list"><i class="fa fa-check-circle"></i> Add</button>
                    <button class="btn btn-default btn-rounded btn-flat" data-dismiss="modal">@lang('general.close')</button>
                </div>
            </form>
        </div>
    </div>
</div>

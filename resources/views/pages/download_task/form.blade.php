@extends('layouts.app')
@section('title', 'Download Task')
@section('ribbon')
@endsection

<style>
#dataTableApp1 {
    overflow-x: scroll;
    max-width: 40%;
    display: block;
    white-space: nowrap;
}
#dataTableApp2 {
    overflow-x: scroll;
    max-width: 40%;
    display: block;
    white-space: nowrap;
}
.group-icon {
    position: relative;
    color: #aaa;
    font-size: 16px;
    width: 100%;
    margin-bottom: 10px;
}

.group-icon {display: inline-block;}

.group-icon input { text-indent: 1px;}
.group-icon .fa { 
  position: absolute;
  top: 8px;
  left: 10px;
}
.group-icon .fa {left: auto; right: 10px;}

</style>
@section('content')

<div class="title-page">
    <div class="row">
            <div class="col-md-12">
                <ul class="breadcrumb mt-bread">
                    <li><a href="/download-task">DOWNLOAD TASK</a></li>
                </ul>
            </div>
    </div> 
</div>
<div class="main-content-inner">
    <div class="row">
        <div class="col-12 mt-5">
            <div class="card mb-tng2">
                <div class="card-body">
                <form class="form-horizontal" id="form-download-task">
               
                <div class="modal-body">
                   
                    <div class="form-group">
							  <h4 class="title-form">{{ $edit=='no' ? 'New  Download Task' : 'Edit  Download Task ' }}</h4>
                              <input type="hidden" class="form-control input-xs" id="download-task-id" value="{{$data ? $data[0]['id'] : ''}}">
                   </div>
                    <div class="form-group mt-3">
                        <label for="" class="control-label">@lang('download-task.name')</label>
                        <input type="text" style="width:33%;" class="form-control form-control-sm" id="download-task-name" value="{{$data ? $data[0]['name'] : ''}}">
                    </div>
					 <div class="form-row">
                        <div class="form-group col-md-4">
                            <label for="" class="control-label">Publish Time Type</label>
                            <select id="download-task-publishTimeType" class="form-control form-control-sm">
								<option value=""> Select</option>
								<option value="1">Immediate</option>
								<option value="2">Date Time</option>
							</select>
                        </div>
                        <div class="form-group col-md-4 offset-2">
                            <label for="download-task-publishTime" class="control-label">Publish Time</label> 
                            <div class="group-icon"> 
                                 <span class="fa fa-calendar"></span>
                                 <input placeholder="" type="text" class="form-control form-control-sm"  id="download-task-publishTime" value="{{$data? $data[0]['publishTime'] : ''}}">
                            </div>

						</div>
					</div> 
                    <div class="form-row">
                        <div class="form-group col-md-4">
                            <label for="" class="control-label">@lang('download-task.downloadTimeType')</label>
							<select id="download-task-downloadTimeType" class="form-control form-control-sm">
								<option value=""> Select</option>
								<option value="1">Next Contact</option>
								<option value="2">Datetime</option>
							</select>
                        </div>
                        <div class="form-group col-md-4 offset-2">
                            <label for="" class="control-label">Download Time</label> 
                            <div class="group-icon"> 
                                 <span class="fa fa-calendar"></span>
                                 <input placeholder="" type="text" class="form-control form-control-sm"  id="download-task-downloadTime" value="{{$data? $data[0]['downloadTime'] : ''}}">
                            </div>
                        </div>
					</div>
                    <div class="form-row">
                        <div class="form-group col-md-4">
                            <label for="" class="control-label">@lang('download-task.installation_time_type')</label>
							<select id="download-task-installation_time_type" class="form-control form-control-sm">
								<option value=""> Select</option>
								<option value="1">Immediate</option>
								<option value="2">Datetime</option>
							</select>
						</div>
                        <div class="form-group col-md-4 offset-2">
                            <label for="download-task-installation_time" class="control-label">@lang('download-task.installation_time')</label>
                            <div class="group-icon"> 
                                 <span class="fa fa-calendar"></span>
                                 <input placeholder="" type="text" class="form-control form-control-sm"  id="download-task-installation_time" value="{{$data? $data[0]['installationTime'] : ''}}">
                            </div>
                        </div>
					</div>
                    <div class="form-row">
                        <div class="form-group col-md-4">
                            <label for="" class="control-label">Installation Notification</label> 
                            <select id="download-task-Installation-Notification" class="form-control form-control-sm">
								<option value=""> Select</option> 
								<option value="1">Silent</option>
								<option value="2">Need Confirmation</option>
							</select>
                        </div>
                        
					</div>
					<div class="form-group">
                        <label for="app-dlt"><a  href="#" class="btn btn-link text-secondary text-left" style="margin-left:-20px;text-decoration: none;display: inline;" id="add-app-to-list">Applications <i class="fa fa-plus-square fa-lg" style="color:#717682;margin-left:20px;"></i></a></label>
                        <table class="table mt-2 table-striped" id="list-group-app">
							<thead  class="thead-light">
								<tr>
									<th>No</th>
									<th>App Version</th>
									<th>Name</th>
									<th>Package Name</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody> 
							</tbody>
                        </table>
				    </div>
                    <div class="form-group">
                        <label for="exampleFormControlSelect-tg">
                            <a  href="#" class="btn btn-link text-secondary text-left" style="margin-left:-20px;text-decoration: none;display: inline;" id="btn-add-terminal-group-dlt">Terminal Groups <i class="fa fa-plus-square fa-lg" style="color:#717682;margin-left:20px;"></i></a>
                        </label>
                        <ul id="exampleFormControlSelect-tg" class="rounded p-2" style="border: 1px solid rgba(170, 170, 170, .3); width:33%;">
                        </ul>
                        <!-- <select multiple class="form-control" id="exampleFormControlSelect-tg" style="width:33%;">
                        </select> --> 
				    </div>
                    <div class="form-group">
                        <label for="terminal-list-sn-dlt"><a  href="#" class="btn btn-link text-secondary text-left" style="margin-left:-20px;text-decoration: none;display: inline;" id="btn-add-terminal-list-dlt">Terminal List <i class="fa fa-plus-square fa-lg" style="color:#717682;margin-left:20px;"></i></a></label>
                        <ul id="terminal-list-sn-dlt" class="rounded p-2" style="border: 1px solid rgba(170, 170, 170, .3); width:33%;">
                        </ul>
                        <!-- <select multiple class="form-control" id="terminal-list-sn-dlt" style="width:33%;"> -->
                        <!-- </select> -->
				    </div>
                  
					<div class="form-group row" id="div-dt-version" style="{{ $edit=='no' ? 'display:none;':'display:none;'}}">
                        <label for="" class="control-label col-md-3">Version</label>
                        <div class="col-md-8"><input readOnly type="text" class="form-control input-xs" id="version-dt" value="{{$data? $data[0]['version'] : ''}}"></div>
                    </div>
                    <div class="form-group mt-5">
                        <button type="button" class="btn btn-rounded btn-primary" id="btn-submit-dt">@lang('general.save')</button>&nbsp;&nbsp;
                        <a class="btn btn-rounded btn-secondary" href="{{url('/download-task')}}">@lang('general.cancel')</a>
						<span class="loading"><img class="hidden" width="40" src="{{asset('assets/images/icon/loader.gif')}}"/></span>
                    </div>
                </div>
               
            </form>
                   
                </div>
            </div>
        </div>

    </div>
</div>
<link rel="stylesheet" href="{{ asset('assets/js/plugin/datetimepicker-master/jquery.datetimepicker.css') }}">

@endsection


@section('modal')
    @include('pages.download_task.app-download-task') 
    @include('pages.download_task.tg-download-task')
    @include('pages.download_task.t-download-task')
@endsection

@section('script')
  
<!-- Start datatable js -->
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js"></script>
<script src="https://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.18/js/dataTables.bootstrap4.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.2.3/js/responsive.bootstrap.min.js"></script>

<script src="{{ asset('assets/js/download_task.js') }}"></script>
<script src="{{ asset('assets/js/plugin/datetimepicker-master/build/jquery.datetimepicker.full.js') }}"></script>
<script src="{{ asset('assets/js/plugin/datetimepicker-master/build/jquery.datetimepicker.full.min.js') }}"></script>
<script src="{{ asset('assets/js/plugin/datetimepicker-master/build/jquery.datetimepicker.min.js') }}"></script>

<script>
     $( function() {
        $('#download-task-publishTime').datetimepicker({
            // datepicker:true,
            format:'Y-m-d H:i:s',
            step : 5,
            formatTime:'H:i:s',
            formatDate:'Y-m-d',
        }); 
        $('#download-task-installation_time').datetimepicker({
            // datepicker:true,
            format:'Y-m-d H:i:s',
            step : 5,
            formatTime:'H:i:s',
            formatDate:'Y-m-d',
        }); 
        $('#download-task-downloadTime').datetimepicker({
            // datepicker:true,
            format:'Y-m-d H:i:s',
            step : 5,
            formatTime:'H:i:s',
            formatDate:'Y-m-d',
        }); 
    });

    // $('input.timepicker').timepicker({
    //     change: function(time) {
    //         var element = $(this), text;
    //         var timepicker = element.timepicker();
    //         text = 'Selected time is: ' + timepicker.format(time);
    //         element.siblings('span.help-line').text(text);
    //     }
    // });


</script>
 @endsection

@extends('layouts.app')
@section('title', 'Download Task')
@section('ribbon')
@endsection

<style>


#dataTableDownloadTask {
    overflow-x: scroll;
    max-width: 40%;
    display: block;
    white-space: nowrap;
}
</style>
@section('content')

<div class="title-page">
    <div class="row">
            <div class="col-md-12">
                <ul class="breadcrumb mt-bread">
                     <li><a href="/download-task">DOWNLOAD TASK</a></li>
                </ul>
            </div>
    </div> 
</div>
<div class="main-content-inner">
    <div class="row">
        <div class="col-12">
            <div class="card mb-tng">
                <div class="card-body">
                    <div class="clear_filter">Clear Search</div>
                    <div class="form-row row mb-5"> 
                      	<div class="col-md-12">
							<div class="row-btn-top">
                                @if(Privilege::visible(Request::segment(1),'CREATE')=='Y')
								<a  href="{{url('/download-task-form')}}" class="btn btn-link text-secondary text-left" style="text-decoration: none;display: inline;" id="btn-add-terminal"><i class="fa fa-plus-square fa-lg" style="color:#717682;"></i> &nbsp;Add Download Task</a> 
								@endif
                                <span>
									 <input type="text" id="search-name-dnt" class="form-control form-control-sm text-left ml-4" style="width:30%;display: inline;" placeholder="Keyword Name.."/>
									 <i class="ti-search" id="btn-ti-search-dt"></i>   
								</span>
							</div>
						</div> 
                    </div>
                   
                    <div class="data-tables">
                        <table id="dataTableDownloadTask" width="100%">
                            <thead class="bg-light text-capitalize">
                                <tr>
                                    <th style="width:30px;">No</th>
                                    <th>@lang('download-task.version')</th>
                                    <th>@lang('download-task.name')</th>
                                    <th>Task Time</th>
                                    <th>Status</th>
                                    <th>@lang('general.action')</th>
                                </tr>
                            </thead>
                            <tbody>
                        
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>

@endsection


@section('script')

<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>

<!-- Start datatable js -->
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js"></script>
<script src="https://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.18/js/dataTables.bootstrap4.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.2.3/js/responsive.bootstrap.min.js"></script>

<script src="{{ asset('assets/js/download_task.js') }}"></script>

@endsection

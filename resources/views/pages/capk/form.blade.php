@extends('layouts.app')
@section('title', 'Capk')
@section('ribbon')
@endsection

@section('content')
<div class="title-page">
    <div class="row">
            <div class="col-md-12">
                <ul class="breadcrumb mt-bread">
                    <li><a href="/capk">CAPK</a></li>
                </ul>
            </div>
    </div> 
</div>
<div class="main-content-inner">
    <div class="row">
        <div class="col-12 mt-5">
            <div class="card mb-tng2">
                <div class="card-body">               
                    <form class="form-horizontal" id="form-cp">
						
						<div class="modal-body">
							<div class="form-group">
								<h4 class="title-form">{{ $edit=='no' ? 'Add Capk' : 'Edit Capk ' }}</h4>
								<input type="hidden" class="form-control input-xs" id="capk-id" value="{{$data ? $data[0]['id'] : ''}}">
							</div>
							<div class="form-row">
								<div class="form-group col-md-4">
									<label for="" class="control-label">Name</label>
									<input type="text" class="form-control input-xs" id="capk-name" value="{{$data ? $data[0]['name'] : ''}}">
								</div>
							</div>
							<div class="form-row">
								<div class="form-group col-md-4">
									<label for="" class="control-label">CAPK IDX</label>
									<input type="text" class="form-control input-xs" id="capk-idx" value="{{$data ? $data[0]['idx'] : ''}}">
								</div>
								<div class="form-group col-md-4 offset-2">
									<label for="" class="control-label">RID</label>
									<input type="text" class="form-control input-xs" id="capk-rid" value="{{$data? $data[0]['rid'] : ''}}">
								</div>
							</div>
							<div class="form-row">
								<div class="form-group col-md-4">
									<label for="" class="control-label">Exponent</label>
									<input type="text" class="form-control input-xs" id="capk-exponent" value="{{$data ? $data[0]['exponent'] : ''}}">
								</div>
								<div class="form-group col-md-4 offset-2">
									<label for="" class="control-label">Algo</label>
									<input type="text" class="form-control input-xs" id="capk-algo" value="{{$data? $data[0]['algo'] : ''}}">
								</div>
							</div>
							<div class="form-row">
								<div class="form-group col-md-6">
									<label for="" class="control-label">Modulus</label>
									<textarea  rows="5" class="form-control" id="capk-modulus">{{$data ? $data[0]['modulus'] : ''}}</textarea>
								</div>
								
							</div>
							<div class="form-row">
								<div class="form-group col-md-4">
									<label for="" class="control-label">Hash</label>
									<input type="text" class="form-control input-xs" id="capk-hash" value="{{$data ? $data[0]['hash'] : ''}}">
								</div>
							</div>
							<div class="form-row">
								<div class="form-group col-md-4">
									<label for="" class="control-label">Expiry Date</label>
									<input type="datetime-local" class="form-control input-xs" id="capk-expiryDate" value="{{$data ? $data[0]['expiryDate'] : ''}}">
								</div>
								<div class="form-group col-md-4 offset-2">
									<label for="" class="control-label">Remark</label>
									<input type="text" class="form-control input-xs" id="capk-remark" value="{{$data? $data[0]['remark'] : ''}}">
								</div>
							</div>
							<div class="form-group row" id="div-dp-version" style="{{ $edit=='no' ? 'display:none;':'display:none;'}}">
								<label for="" class="control-label col-md-3">Version</label>
								<div class="col-md-8"><input type="text" class="form-control input-xs" readOnly id="version-capk" value="{{$data? $data[0]['version'] : ''}}"></div>
							</div>
							<div class="form-group mt-4">
								<button type="button" class="btn btn-rounded btn-primary" id="btn-submit-capk">@lang('general.save')</button>&nbsp;&nbsp;
								<a class="btn btn-rounded btn-secondary" href="{{url('/capk')}}">@lang('general.cancel')</a>
								<span class="loading"><img class="hidden" width="40" src="{{asset('assets/images/icon/loader.gif')}}"/></span>
							</div>
						</div>
				    </form>
                </div>
            </div>
        </div>

    </div>
</div>

@endsection

@section('script')
<!-- Start datatable js -->
<script src="{{ asset('assets/js/capk.js') }}"></script>
@endsection

@extends('layouts.app')
@section('title', 'Device Profile')
@section('ribbon')
@endsection

@section('content')

<div class="title-page">
    <div class="row">
            <div class="col-md-12">
                <ul class="breadcrumb mt-bread">
                   <li><a href="/device-profile">DEVICE PROFILES</a></li>
                </ul>
            </div>
    </div> 
</div>
<div class="main-content-inner">
    <div class="row">
        <div class="col-12">
            <div class="card mb-tng">
                <div class="card-body">               
                    <form class="form-horizontal" id="form-dp">
                <div xclass="modal-header no-border">
                    <h4 class="modal-title">DETAIL PROFILE</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <input type="hidden" class="form-control input-xs" id="dp-id" value="{{$data ? $data[0]['id'] : ''}}">
                        <label for="" class="control-label">Profile Name</label>
                        <div class=""><input style="width:49%;" type="text" class="form-control input-xs" id="dp-name" value="{{$data ? $data[0]['name'] : ''}}"></div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-3">
                             <label for="dp-heartbeatInterval">Heartbeat Interval (minutes)</label>
                             <input type="text" class="form-control" id="dp-heartbeatInterval" value="{{$data ? $data[0]['heartbeatInterval'] : ''}}" style="width:70%;">
                        </div>
                        <div class="form-group col-md-2 offset-1">
                            <label for="dp-diagnosticInterval">Diagnostic Interval (day)</label>
                            <input type="text" class="form-control" id="dp-diagnosticInterval" value="{{$data ? $data[0]['diagnosticInterval'] : ''}}">
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-4">
                             <label for="maskHomeButton">Mask Home Button</label>
                             <div class="form-check">
                                <input class="form-check-input position-static" name="maskHomeButton" type="checkbox" id="maskHomeButton" value="true" {{$data[0]["maskHomeButton"] ? "checked" : ""}}>
                            </div>
                        </div>
                        <div class="form-group col-md-4">
                            <label for="maskStatusBar">Mask Status Bar</label>
                            <div class="form-check">
                                <input class="form-check-input position-static" name="maskStatusBar" type="checkbox" id="maskStatusBar" value="true" {{$data[0]["maskStatusBar"] ? "checked" : ""}}>
                            </div>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-4">
                             <label for="dp-movingThreshold">Moving Threshold</label>
                             <input class="form-control" type="text" name="movingThreshold" id="dp-movingThreshold" value="{{$data ? $data[0]['movingThreshold'] : ''}}">
                        </div>
                        <div class="form-group col-md-4">
                            <label for="dp-adminPassword">Admin Password</label>
                            <input class="form-control" type="text" name="adminPassword" id="dp-adminPassword" value="{{$data ? $data[0]['adminPassword'] : ''}}">
                        </div>
                        <div class="form-group col-md-2">
                            <label for="dp-frontApp">Front App</label>
                            <input class="form-control" type="text" name="frontApp" id="dp-frontApp" value="{{$data ? $data[0]['frontApp'] : ''}}">
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-4">
                             <label for="relocationAlert">Relocation Alert</label>
                             <div class="form-check">
                                <input class="form-check-input position-static" type="checkbox" name="relocationAlert" id="relocationAlert" value="true" {{$data[0]["relocationAlert"] ? "checked" : ""}}>
                            </div>
                        </div>
                        <div class="form-group col-md-4">
                            <label for="scheduleReboot">Schedule Reboot</label>
                            <div class="form-check">
                                <input class="form-check-input position-static" type="checkbox" name="scheduleReboot" id="scheduleReboot" value="true" {{$data[0]["scheduleReboot"] ? "checked" : ""}}>
                            </div>
                        </div>
                        <div class="form-group col-md-2">
                            <label for="dp-scheduleRebootTime"></label>
                            <input class="form-control input-xs" type="time" step=1 value="{{$data[0]['scheduleRebootTime']}}" id="dp-scheduleRebootTime">
                        </div>
                    </div>
                    
                    <div class="form-row">
                        <div class="form-group col-md-4">
                             <label for="default">Set Default</label>
                             <div class="form-check">
                                <input class="form-check-input position-static" type="checkbox" id="default" value="true" name="default" {{$data[0]["default"] ? "checked" : ""}}>
                            </div>
                        </div>
                        <div class="form-group col-md-4">
                        </div>
                        <div class="form-group col-md-2">
                        </div>
                    </div>
                    <div class="form-group" id="div-dp-version" style="{{ $edit=='no' ? 'display:none;':'display:none;'}}">
                        <label for="" class="control-label">Version</label>
                        <div class=""><input readOnly type="text" class="form-control input-xs" id="version-dp" value="{{$data? $data[0]['version'] : ''}}"></div>
                    </div>
                    <div class="form-row mt-5">
                         <a class="btn btn-rounded btn-primary" href="{{url('/device-profile')}}">@lang('general.cancel')</a>
                     </div>
					
				 </div>
               
            </form>
                   
                </div>
            </div>
        </div>

    </div>
</div>

@endsection


@section('script')
 <script src="{{ asset('assets/js/device_profile.js') }}"></script>

@endsection

@extends('layouts.app')
@section('title', 'Acquirer')
@section('ribbon')
@endsection

<!-- Start datatable css -->
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.18/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/responsive/2.2.3/css/responsive.bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/responsive/2.2.3/css/responsive.jqueryui.min.css">
<link rel="stylesheet" type="text/css" href="{{ asset('assets/css/plugins/smartadmin-production.min.css')}}"> 
<link rel="stylesheet" type="text/css" href="{{ asset('assets/css/plugins/theme-material.css')}}">  
<style>


</style>
@section('content')

<div class="title-page">
    <div class="row">
            <div class="col-md-12">
                <ul class="breadcrumb mt-bread">
                    <li><a href="/">Home</a></li>
                    <li><a href="/card">Card</a></li>
                    
                </ul>
            </div>
    </div> 
</div>

<div class="main-content-inner">
    <div class="row">
        <div class="col-12 mt-5">
            <div class="card">
                <div class="card-body">               
                    <form class="form-horizontal" id="form-dp">
                <div class="modal-header bg-dark no-border">
                    <h4 class="modal-title">{{ $edit=='no' ? 'Add Card' : 'Edit Card ' }}</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group row">
                        <input type="hidden" class="form-control input-xs" id="cd-id" value="{{$data ? $data[0]['id'] : ''}}">
                        <label for="" class="control-label col-md-3">Name</label>
                        <div class="col-md-8"><input type="text" class="form-control input-xs" id="cd-name" value="{{$data ? $data[0]['name'] : ''}}"></div>
                    </div>
					<div class="form-group row">
                        <label for="" class="control-label col-md-3">BinRangeStart</label>
                        <div class="col-md-8"><input type="text" class="form-control input-xs" id="cd-binRangeStart" value="{{$data? $data[0]['binRangeStart'] : ''}}"></div>
                    </div>		
					<div class="form-group row">
                        <label for="" class="control-label col-md-3">BinRangeEnd</label>
                        <div class="col-md-8"><input type="text" class="form-control input-xs" id="cd-binRangeEnd" value="{{$data? $data[0]['binRangeEnd'] : ''}}"></div>
                    </div>	
                    <div class="form-group row">
                        <label for="" class="control-label col-md-3">CardNumLength</label>
                        <div class="col-md-8"><input type="number" class="form-control input-xs" id="cd-cardNumLength" value="{{$data? $data[0]['cardNumLength'] : ''}}"></div>
                    </div>
                    
					<div class="form-group row">
                        <label for="" class="control-label col-md-3">PanDigitUnmasking</label>
                        <div class="col-md-8">
                            <div class="custom-control custom-radio custom-control-inline">
                                <input type="radio" {{$data? $data[0]['panDigitUnmasking']=='true' ? 'checked' : '' : ''}}  name="panDigitUnmasking[]" value="true" id="cd-panDigitUnmasking-true"  class="custom-control-input">
                                <label class="custom-control-label" for="cd-panDigitUnmasking-true">TRUE</label>
                            </div>
                            <div class="custom-control custom-radio custom-control-inline">
                                <input type="radio" {{$data? $data[0]['panDigitUnmasking']=='false' ? 'checked' : '' : ''}} name="panDigitUnmasking[]" value="false"  id="cd-panDigitUnmasking-false"  class="custom-control-input">
                                <label class="custom-control-label" for="cd-panDigitUnmasking-false">FALSE</label>
                            </div>
                        </div>
                    </div> 
					
					<div class="form-group row">
                        <label for="" class="control-label col-md-3">printCardholderCopy</label>
                        <div class="col-md-8">
                            <div class="custom-control custom-radio custom-control-inline">
                                <input type="radio" {{$data? json_encode($data[0]['printCardholderCopy'])=='true' ? 'checked' : '' : ''}}  name="printCardholderCopy[]" value="true" id="cd-printCardholderCopy-true"  class="custom-control-input">
                                <label class="custom-control-label" for="cd-printCardholderCopy-true">TRUE</label>
                            </div>
                            <div class="custom-control custom-radio custom-control-inline">
                                <input type="radio" {{$data? json_encode($data[0]['printCardholderCopy'])=='false' ? 'checked' : '' : ''}} name="printCardholderCopy[]" value="false"  id="cd-printCardholderCopy-false"  class="custom-control-input">
                                <label class="custom-control-label" for="cd-printCardholderCopy-false">FALSE</label>
                            </div>
                        </div>
                    </div>
					
					<div class="form-group row">
                        <label for="" class="control-label col-md-3">PrintMerchantCopy</label>
                        <div class="col-md-8">
                            <div class="custom-control custom-radio custom-control-inline">
                                <input type="radio" {{$data? json_encode($data[0]['printMerchantCopy'])=='true' ? 'checked' : '' : ''}}  name="printMerchantCopy[]" value="true" id="cd-printMerchantCopy-true"  class="custom-control-input">
                                <label class="custom-control-label" for="cd-printMerchantCopy-true">TRUE</label>
                            </div>
                            <div class="custom-control custom-radio custom-control-inline">
                                <input type="radio" {{$data? json_encode($data[0]['printMerchantCopy'])=='false' ? 'checked' : '' : ''}} name="printMerchantCopy[]" value="false"  id="cd-printMerchantCopy-false"  class="custom-control-input">
                                <label class="custom-control-label" for="cd-printMerchantCopy-false">FALSE</label>
                            </div>
                        </div>
                    </div>
					
					<div class="form-group row">
                        <label for="" class="control-label col-md-3">printBankCopy</label>
                        <div class="col-md-8">
                            <div class="custom-control custom-radio custom-control-inline">
                                <input type="radio" {{$data? json_encode($data[0]['printBankCopy'])=='true' ? 'checked' : '' : ''}}  name="printBankCopy[]" value="true" id="cd-printBankCopy-true"  class="custom-control-input">
                                <label class="custom-control-label" for="cd-printBankCopy-true">TRUE</label>
                            </div>
                            <div class="custom-control custom-radio custom-control-inline">
                                <input type="radio" {{$data? json_encode($data[0]['printBankCopy'])=='false' ? 'checked' : '' : ''}} name="printBankCopy[]" value="false"  id="cd-printBankCopy-false"  class="custom-control-input">
                                <label class="custom-control-label" for="cd-printBankCopy-false">FALSE</label>
                            </div>
                        </div>
                    </div>
					
					<div class="form-group row">
                        <label for="" class="control-label col-md-3">PinPrompt</label>
                        <div class="col-md-8">
                            <div class="custom-control custom-radio custom-control-inline">
                                <input type="radio" {{$data? json_encode($data[0]['pinPrompt'])=='true' ? 'checked' : '' : ''}}  name="pinPrompt[]" value="true" id="cd-pinPrompt-true"  class="custom-control-input">
                                <label class="custom-control-label" for="cd-pinPrompt-true">TRUE</label>
                            </div>
                            <div class="custom-control custom-radio custom-control-inline">
                                <input type="radio" {{$data? json_encode($data[0]['pinPrompt'])=='false' ? 'checked' : '' : ''}} name="pinPrompt[]" value="false"  id="cd-pinPrompt-false"  class="custom-control-input">
                                <label class="custom-control-label" for="cd-pinPrompt-false">FALSE</label>
                            </div>
                        </div>
                    </div>
				
				     <div class="form-group row">
                        <label for="" class="control-label col-md-3">PinLength</label>
                        <div class="col-md-8"><input type="number" class="form-control input-xs" id="cd-pinLength" value="{{$data? $data[0]['pinLength'] : ''}}"></div>
                    </div>
                   
                   
					
					<div class="form-group row" id="div-dp-version" style="{{ $edit=='no' ? 'display:none;':''}}">
                        <label for="" class="control-label col-md-3">Version</label>
                        <div class="col-md-8"><input type="text" class="form-control input-xs" readOnly id="version-cd" value="{{$data? $data[0]['version'] : ''}}"></div>
                    </div>
					
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-flat btn-primary" id="btn-submit-cd"><i class="fa fa-check-circle"></i> @lang('general.submit')</button>
                    <a class="btn btn-flat btn-secondary" href="{{url('/card')}}">@lang('general.back')</a>
                </div>
            </form>
                   
                </div>
            </div>
        </div>

    </div>
</div>

@endsection


@section('script')
 <!-- Start datatable js -->
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js"></script>
<script src="https://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.18/js/dataTables.bootstrap4.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.2.3/js/responsive.bootstrap.min.js"></script>
<script src="{{ asset('assets/js/card.js') }}"></script>

@endsection

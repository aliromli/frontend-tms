@extends('layouts.app')
@section('title', 'TerminalExt')
@section('ribbon')
@endsection


@section('content')
<div class="title-page">
    <div class="row">
            <div class="col-md-12">
                <ul class="breadcrumb mt-bread">
                    <li><a href="/terminalExt">TERMINAL EXTENDED</a></li>
                </ul>
            </div>
    </div> 
</div>
<div class="main-content-inner">
    <div class="row">
        <div class="col-12">
            <div class="card mb-tng">
                <div class="card-body">
                <form class="form-horizontal" id="form-te">
                   
                    <div class="modal-body">
						<div class="form-group">
							 <h4 class="modal-title">Import</h4>
						</div>
                        <div class="form-group row" xid="div-gt-version" style="display:none;">
                            <label for="" class="control-label col-md-3">Version</label>
                            <div class="col-md-8">
								<input type="text" readonly class="form-control input-xs" id="version-terminal-ext" value="">
							</div>
                        </div>
						<div class="row mt-3">
							<div class="col-md-12">
								<div class="data-tables">
									<input type="hidden" id="userName-import-view" value="{{$user_name}}"/>
									<table id="dataTableListTerminal_Result_Ext_Temp" width="100%">
										<thead class="bg-light text-capitalize">
											<tr>
												<th style="width:30px;">No</th>
												<th>Terminal ID</th>
												<th>Merchant ID</th>
												<th>Merchant Name 1</th>
												<th>Merchant Name 2</th>
												<th>Merchant Name 3</th>
												<th>Template Name</th>
												<th>Status</th>
											</tr>
										</thead>
										<tbody>
											<?php
												if($data_temp->count()>0)
												{
													$no=1;
													foreach($data_temp as $d){
													
														
														$sty = "style='color:#EE6F6F;'";	
														if($d->status=='Pass')
														{
															$sty = "style='color:#5A84E9;'";
														}	
														$tr = "<tr>";
														$tr.="<td>".$no."</td>";
														$tr.= "<td><span class='tid'>".$d->terminal_id."</span></td>";
														$tr.= "<td><span class='mid'>".$d->merchant_id."</span></td>";
														$tr.= "<td><span class='mn1'>".$d->merchant_name1."</span></td>";
														$tr.= "<td><span class='mn2'>".$d->merchant_name2."</span></td>";
														$tr.= "<td><span class='mn3'>".$d->merchant_name3."</span></td>";
														$tr.= "<td><span class='temp_name'>".$d->template_name."</span><span style='display:none;' class='template_id'>".$d->template_id."</span></td>";
														$tr.= "<td><span class='status' $sty>".$d->status."</span></td>";
														$tr.= "</tr>";
														echo $tr;
													$no++;
													}
												}	
												
											?>
										</tbody>
									</table>
								</div>
							</div>
							<div class="col-md-12">
								<div class="mt-3" style="color:#717682;">{{$canimp->count()}} terminal extended to be imported from {{$data_temp->count()}}</div>
							</div>
						</div>
						<div class="form-row mt-5">
							<button type="button" class="btn btn-rounded btn-primary btn-xs" id="btn-submit-import-te">IMPORT</button>&nbsp;&nbsp;
							<span class="loading"><img class="hidden" width="40" src="{{asset('assets/images/icon/loader.gif')}}"/></span>
						</div>
						
                    </div>
                    
                </form>
                   
                </div>
            </div>
        </div>

    </div>
</div>

@endsection


@section('script')
<!-- Start datatable js -->
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js"></script>
<script src="https://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.18/js/dataTables.bootstrap4.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.2.3/js/responsive.bootstrap.min.js"></script>
<script src="{{ asset('assets/js/terminal_ext/index.js') }}"></script>

@endsection

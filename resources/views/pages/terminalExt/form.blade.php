@extends('layouts.app')
@section('title', 'TerminalExt')
@section('ribbon')
@endsection

@section('content')

<div class="title-page">
    <div class="row">
            <div class="col-md-12">
                <ul class="breadcrumb mt-bread">
                    <li><a href="/terminalExt">TERMINAL EXTENDED</a></li>
                </ul>
            </div>
    </div> 
</div>

<div class="main-content-inner">
    <div class="row">
        <div class="col-12 mt-5">
            <div class="card mb-tng2">
                <div class="card-body">               
                    <form class="form-horizontal" id="form-te">
						
						<div class="modal-body">
							<div class="form-group">
								<h4 class="title-form">{{ $edit=='no' ? 'Add Terminal Extended' : 'Edit Extended' }}</h4>
								<input type="hidden" class="form-control form-control-sm input-xs" id="te-id" value="{{$data ? $data[0]['id'] : ''}}">
							</div>
							<div class="form-row">
								<div class="form-group col-md-2">
									<label for="" class="control-label">Terminal ID</label>
									<input type="text" class="form-control form-control-sm input-xs" id="te-tid" value="{{$data ? $data[0]['tid'] : ''}}">
								</div>
								<div class="form-group col-md-3 offset-1">
									<label for="" class="control-label">Merchant ID</label>
									<input type="text" class="form-control form-control-sm input-xs" id="te-mid" value="{{$data? $data[0]['mid'] : ''}}">
								</div>
							</div>								
							<div class="form-group">
								<label for="" class="control-label">Merchant Name 1</label>
								<input type="text" class="form-control form-control-sm input-xs" id="te-merchantName1" style="width:50%;" value="{{$data? $data[0]['merchantName1'] : ''}}">
							</div>	
							<div class="form-group">
								<label for="" class="control-label">Merchant Name 2</label>
								<input type="text" class="form-control form-control-sm input-xs" id="te-merchantName2" style="width:50%;" value="{{$data? $data[0]['merchantName2'] : ''}}">
							</div>	
							<div class="form-group">
								<label for="" class="control-label">Merchant Name 3</label>
								<input type="text" class="form-control form-control-sm input-xs" id="te-merchantName3" style="width:50%;" value="{{$data? $data[0]['merchantName3'] : ''}}">
							</div>	
							<div class="form-group">
								<label for="" class="control-label">Select Terminal Template</label>
								<select class="form-control form-control-sm" style="width:50%;" name="select_terminal_tempalte" id="select_terminal_tempalte">
									
									<?php
										if(!empty($template))
										{
											if($edit=="yes")
											{
												foreach($template as $t)
												{
													if($t['id']==$data[0]['template']['id'])
													{
														echo "<option value='".$t['id']."' selected>".$t['name']."</option>";
													}
													else
													{
														echo "<option value='".$t['id']."'>".$t['name']."</option>";
													}		
													
												}
												echo "<option value=''>--Select Template--</option>";
											}
											else
											{
												echo "<option value=''>--Select Template--</option>";
												foreach($template as $t)
												{
													echo "<option value='".$t['id']."'>".$t['name']."</option>";
												}
												
											}
										}
									?>
								</select>
							</div>	
							<div class="form-group row" id="div-dp-version" style="{{ $edit=='no' ? 'display:none;':'display:none;'}}">
								<label for="" class="control-label col-md-3">Version</label>
								<div class="col-md-8"><input type="text" class="form-control form-control-sm input-xs" readOnly id="version-te" value="{{$data? $data[0]['version'] : ''}}"></div>
							</div>
							<div class="form-group mt-4">
								<button type="button" class="btn btn-rounded btn-primary btn-xs" id="btn-submit-te">@lang('general.save')</button>
								<a class="btn btn-rounded btn-secondary btn-xs ml-sm-3" href="{{url('/terminalExt')}}">@lang('general.cancel')</a>
								<span class="loading"><img class="hidden" width="40" src="{{asset('assets/images/icon/loader.gif')}}"/></span>
							</div>
							
						</div>
						
					</form>
                   
                </div>
            </div>
        </div>

    </div>
</div>
@endsection


@section('script')

<script src="{{ asset('assets/js/terminal_ext/terminalExt.js') }}"></script>

@endsection

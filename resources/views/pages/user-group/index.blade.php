@extends('layouts.app')
@section('title', 'User Group')
@section('ribbon')
@endsection


<!-- Start datatable css -->
@section('content')

<div class="title-page">
    <div class="row">
            <div class="col-md-12">
                <ul class="breadcrumb mt-bread">
                    <li><a href="/user-group">USER GROUP</a></li>
                </ul>
            </div>
    </div> 
</div>
<div class="main-content-inner">
    <div class="row">
        <div class="col-12 mt-5">
            <div class="card mb-tng2">
                <div class="card-body">
                    <div class="box-header no-border text-left filter" style="margin-bottom:20px;"> 
                      <a class="btn btn-link text-secondary text-left" id="btn-add-user-group"><i class="fa fa-plus-circle" style="color:#717682;"></i> Add User Group</a> 
                    </div>
                    <div class="form-filter">
                        <form class="form-horizontal">
                            <div class="col-md-7">
                                <div class="form-group row" style="margin-left:-28;">
                                   
                                    <div class="col-md-4"><input type="text" id="search-user-group" class="form-control input-xs" placeholder="Search..."></div>
                                    <button type="button" id="btn-cari-usergroup" class="btn btn-xs btn-flat btn-light col-md-1" xstyle="background-color:transparent" ><i class="ti-search" style="font-size:23px;"></i></button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="data-tables">
                        <table id="dataTableUserGroup"  width="100%">
                            <thead class="bg-light text-capitalize">
                                <tr>
                                    <th style="width:50px">No</th>
                                    <th>ID</th>
                                    <th>@lang('user-group.group_name')</th>
                                    <th>@lang('user-group.description')</th>
                                    <th style="width: 340px">@lang('general.action')</th>
                                </tr>                                
                            </thead>
                            <tbody>                               
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>

@endsection


@section('modal')
    @include('pages.user-group.new-user-group')
    @include('pages.user-group.privileges')
@endsection

@section('script')
<!-- <script src="{{ asset('js/plugin/bootstrap-datepicker/bootstrap-datepicker.js') }}"></script> -->
 <!-- Start datatable js -->
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js"></script>
<script src="https://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.18/js/dataTables.bootstrap4.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.2.3/js/responsive.bootstrap.min.js"></script> 
<script src="{{ asset('assets/js/scriptsUserGroup.js') }}"></script>

@endsection

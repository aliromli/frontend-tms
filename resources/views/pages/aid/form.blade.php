@extends('layouts.app')
@section('title', 'Aid')
@section('ribbon')
@endsection

@section('content')
<div class="title-page">
    <div class="row">
            <div class="col-md-12">
                <ul class="breadcrumb mt-bread">
                    <li><a href="/aid">AID</a></li>
                </ul>
            </div>
    </div> 
</div>
<div class="main-content-inner">
    <div class="row">
        <div class="col-12 mt-5">
            <div class="card mb-tng2">
                <div class="card-body">               
                    <form class="form-horizontal" id="form-aid">
						<div class="modal-body">
						    <div class="form-group">  
								<h4 class="title-form">{{ $edit=='no' ? 'Add AID' : 'Edit AID ' }}</h4>
								<input type="hidden" class="form-control input-xs" id="aid-id" value="{{$data ? $data[0]['id'] : ''}}">
							</div>
							<div class="form-row">
								<div class="form-group col-md-4">
									<label for="" class="control-label">Name</label>
									<input type="text" class="form-control input-xs" id="aid-name" value="{{$data ? $data[0]['name'] : ''}}">
								</div>
								<div class="form-group col-md-4 offset-2">
									<label for="" class="control-label">Txn Type</label>
									<input type="text" class="form-control input-xs" id="aid-txnType" value="{{$data? $data[0]['txnType'] : ''}}">
								</div>
							</div>
							<div class="form-row">
								<div class="form-group col-md-4">
									<label for="" class="control-label">AID</label>
									<input type="text" class="form-control input-xs" id="aid-aid" value="{{$data ? $data[0]['aid'] : ''}}">
								</div>
								<div class="form-group col-md-4 offset-2">
									<label for="" class="control-label">Aid version</label>
									<input type="text" class="form-control input-xs" id="aid-aidVersion" value="{{$data? $data[0]['aidVersion'] : ''}}">
								</div>
							</div>
							<div class="form-row">
								<div class="form-group col-md-4">
									<label for="" class="control-label">Tac Default</label>
									<input type="text" class="form-control input-xs" id="aid-tacDefault" value="{{$data ? $data[0]['tacDefault'] : ''}}">
								</div>
								<div class="form-group col-md-4 offset-2">
									<label for="" class="control-label">Tac Denial</label>
									<input type="text" class="form-control input-xs" id="aid-tacDenial" value="{{$data? $data[0]['tacDenial'] : ''}}">
								</div>
							</div>
							<div class="form-row">
								<div class="form-group col-md-4">
									<label for="" class="control-label">Tac Online</label>
									<input type="text" class="form-control input-xs" id="aid-tacOnline" value="{{$data ? $data[0]['tacOnline'] : ''}}">
								</div>
								<div class="form-group col-md-4 offset-2">
									<label for="" class="control-label">Threshold</label>
									<input type="text" class="form-control input-xs" id="aid-threshold" value="{{$data? $data[0]['threshold'] : ''}}">
								</div>
							</div>
							<div class="form-row">
								<div class="form-group col-md-4">
									<label for="" class="control-label">Target Percentage</label>
									<input type="text" class="form-control input-xs" id="aid-targetPercentage" value="{{$data ? $data[0]['targetPercentage'] : ''}}">
								</div>
								<div class="form-group col-md-4 offset-2">
									<label for="" class="control-label">Max Target Percentage</label>
									<input type="text" class="form-control input-xs" id="aid-maxTargetPercentage" value="{{$data? $data[0]['maxTargetPercentage'] : ''}}">
								</div>
							</div>
							<div class="form-row">
								<div class="form-group col-md-4">
									<label for="" class="control-label">DDOL</label>
									<input type="text" class="form-control input-xs" id="aid-ddol" value="{{$data ? $data[0]['ddol'] : ''}}">
								</div>
								<div class="form-group col-md-4 offset-2">
									<label for="" class="control-label">TDOL</label>
									<input type="text" class="form-control input-xs" id="aid-tdol" value="{{$data? $data[0]['tdol'] : ''}}">
								</div>
							</div>
							<div class="form-row">
								<div class="form-group col-md-4">
									<label for="" class="control-label">Floor Limit</label>
									<input type="text" class="form-control input-xs" id="aid-floorLimit" value="{{$data ? $data[0]['floorLimit'] : ''}}">
								</div>
								<div class="form-group col-md-4 offset-2">
									<label for="" class="control-label">App Select</label>
									<input type="text" class="form-control input-xs" id="aid-appSelect" value="{{$data? $data[0]['appSelect'] : ''}}">
								</div>
							</div>
							<div class="form-row">
								<div class="form-group col-md-4">
									<label for="" class="control-label">AID Priority</label>
									<input type="text" class="form-control input-xs" id="aid-aidPriority" value="{{$data ? $data[0]['aidPriority'] : ''}}">
								</div>
								<div class="form-group col-md-4 offset-2">
									<label for="" class="control-label">Trx Type 9C</label>
									<input type="text" class="form-control input-xs" id="aid-trxType9C" value="{{$data? $data[0]['trxType9C'] : ''}}">
								</div>
							</div>
							<div class="form-row">
								<div class="form-group col-md-4">
									<label for="" class="control-label">Category Code</label>
									<input type="text" class="form-control input-xs" id="aid-categoryCode" value="{{$data ? $data[0]['categoryCode'] : ''}}">
								</div>
								<div class="form-group col-md-4 offset-2">
									<label for="" class="control-label">CL Kernel To Use</label>
									<input type="text" class="form-control input-xs" id="aid-clKernelToUse" value="{{$data? $data[0]['clKernelToUse'] : ''}}">
								</div>
							</div>
							<div class="form-row">
								<div class="form-group col-md-4">
									<label for="" class="control-label">CL Options</label>
									<input type="text" class="form-control input-xs" id="aid-clOptions" value="{{$data ? $data[0]['clOptions'] : ''}}">
								</div>
								<div class="form-group col-md-4 offset-2">
									<label for="" class="control-label">CL TrxLimit</label>
									<input type="text" class="form-control input-xs" id="aid-clTrxLimit" value="{{$data? $data[0]['clTrxLimit'] : ''}}">
								</div>
							</div>
							<div class="form-row">
								<div class="form-group col-md-4">
									<label for="" class="control-label">CL Options</label>
									<input type="text" class="form-control input-xs" id="aid-clOptions" value="{{$data ? $data[0]['clOptions'] : ''}}">
								</div>
								<div class="form-group col-md-4 offset-2">
									<label for="" class="control-label">CL TrxLimit</label>
									<input type="text" class="form-control input-xs" id="aid-clTrxLimit" value="{{$data? $data[0]['clTrxLimit'] : ''}}">
								</div>
							</div>
							<div class="form-row">
								<div class="form-group col-md-4">
									<label for="" class="control-label">CL CVM Limit</label>
									<input type="text" class="form-control input-xs" id="aid-clCVMLimit" value="{{$data ? $data[0]['clCVMLimit'] : ''}}">
								</div>
								<div class="form-group col-md-4 offset-2">
									<label for="" class="control-label">CL Floor Limit</label>
									<input type="text" class="form-control input-xs" id="aid-clFloorLimit" value="{{$data? $data[0]['clFloorLimit'] : ''}}">
								</div>
							</div>
							<div class="form-row">
								<div class="form-group col-md-4">
									<label for="" class="control-label">EMV Config Terminal Capabilities</label>
									<input type="text" class="form-control input-xs" id="aid-emvConfTerminalCapability" value="{{$data ? $data[0]['emvConfTerminalCapability'] : ''}}">
								</div>
								<div class="form-group col-md-4 offset-2">
									<label for="" class="control-label">Additional Terminal Capabilities</label>
									<input type="text" class="form-control input-xs" id="aid-additionalTerminalCapability" value="{{$data? $data[0]['additionalTerminalCapability'] : ''}}">
								</div>
							</div>
							<div class="form-row">
								<div class="form-group col-md-4">
									<label for="" class="control-label">Data TTQ</label>
									<input type="text" class="form-control input-xs" id="aid-dataTtq" value="{{$data ? $data[0]['dataTtq'] : ''}}">
								</div>
								<div class="form-group col-md-4 offset-2">
									<label for="" class="control-label">Remark</label>
									<input type="text" class="form-control input-xs" id="aid-remark" value="{{$data? $data[0]['remark'] : ''}}">
								</div>
							</div>
							
							<div class="form-group row" id="div-dp-version" style="{{ $edit=='no' ? 'display:none;':'display:none;'}}">
								<label for="" class="control-label col-md-3">Version</label>
								<div class="col-md-8"><input type="text" class="form-control input-xs" readOnly id="version-aid" value="{{$data? $data[0]['version'] : ''}}"></div>
							</div>
							<div class="form-group mt-5">
								<button type="button" class="btn btn-rounded btn-primary" id="btn-submit-aid">@lang('general.save')</button>&nbsp;&nbsp;
								<a class="btn btn-rounded btn-secondary" href="{{url('/aid')}}">@lang('general.cancel')</a>
								<span class="loading"><img class="hidden" width="40" src="{{asset('assets/images/icon/loader.gif')}}"/></span>
							</div>
						</div>
					</form>
                   
                </div>
            </div>
        </div>

    </div>
</div>

@endsection


@section('script')
<script src="{{ asset('assets/js/aid.js') }}"></script>
@endsection

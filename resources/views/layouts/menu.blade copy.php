@if ($menu->children->count() == 0)
    <li class="{{ url($menu->action_url) == url()->current()?'active':'' }}">
        <a href="{{($menu->action_url == '#')?'#':url($menu->action_url)}}" title="">{!!$menu->icon!!} <span class="menu-item-parent">{{strtolower($menu->name)}}</span></a>
    </li>
@else
    <li class="top-menu-invisible">
        <a  href="{{($menu->action_url == '#')?'#':url($menu->action_url)}}" title="">{!!$menu->icon!!} <span class="menu-item-parent">{{strtolower($menu->name)}}</span></a>
        <ul>
            @foreach($menu->children as $menu)
                @if(in_array(collect($menu->actions)->firstWhere('action_type', 'READ')['id'], collect(Auth::user()->userGroup->menuActions)->pluck('id')->toArray()))
                    @include('layouts.menu', $menu)
                @endif
            @endforeach
        </ul>
    </li>
@endif

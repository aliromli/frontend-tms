<!DOCTYPE html>
<html lang="en-us">
    <head>
        <meta charset="utf-8">
        <title> Tiara | @yield('title')</title>
        <meta name="description" content="">
        <meta name="author" content="NSDTI - NSS, Telkomsel">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
        <link rel="icon" href="/img/favicon/wrn-abu2.png" type="image/x-icon">
        
        <!-- LOAD GLOBAL CSS -->
        <link rel="stylesheet" type="text/css" media="screen" href="{{ asset('css/bootstrap.min.css') }}">
        <link rel="stylesheet" type="text/css" media="screen" href="{{ asset('css/font-awesome.min.css') }}">
        <link rel="stylesheet" type="text/css" media="screen" href="{{ asset('css/smartadmin-production-plugins.min.css') }}">
        <link rel="stylesheet" type="text/css" media="screen" href="{{ asset('css/smartadmin-production.min.css') }}">
        <link rel="stylesheet" type="text/css" media="screen" href="{{ asset('css/smartadmin-skins.min.css') }}">
        <link rel="stylesheet" type="text/css" media="screen" href="{{ asset('css/jquery-ui.css') }}">
        <link rel="stylesheet" type="text/css" media="screen" href="{{ asset('css/circle.css') }}">
        
        @yield('css')
        
        <link rel="stylesheet" type="text/css" media="screen" href="{{ asset('css/app.css') }}">
        <link rel="stylesheet" type="text/css" media="screen" href="{{ asset('css/themes/theme-material.css') }}">
        <!-- END GLOBAL CSS -->
        
    </head>
    <body class="desktop-detected pace-done smart-style-6 smart-style-detail-6 fixed-header fixed-navigation">
        <div class="page-loader hidden">
            <div class="sk-circle">
                <div class="sk-circle1 sk-child"></div>
                <div class="sk-circle2 sk-child"></div>
                <div class="sk-circle3 sk-child"></div>
                <div class="sk-circle4 sk-child"></div>
                <div class="sk-circle5 sk-child"></div>
                <div class="sk-circle6 sk-child"></div>
                <div class="sk-circle7 sk-child"></div>
                <div class="sk-circle8 sk-child"></div>
                <div class="sk-circle9 sk-child"></div>
                <div class="sk-circle10 sk-child"></div>
                <div class="sk-circle11 sk-child"></div>
                <div class="sk-circle12 sk-child"></div>
            </div>
        </div>
        
        @yield('content')
        
        <!-- GLOBAL SCRIPT -->
        <script>
            // This is global base url
            // don't change this if you
            // don't underderstand
            // what this global variable
            // functionality
            baseUrl = "{{ URL::to('/') }}";
        </script>
        <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
        <script src="{{ asset('js/libs/jquery-3.2.1.min.js') }}"></script>
        <script data-pace-options='{ "restartOnRequestAfter": true }' src="{{ asset('js/plugin/pace/pace.min.js') }}"></script>        
        <script src="{{ asset('js/app.config.js') }}"></script>
        <script src="{{ asset('js/bootstrap/bootstrap.min.js') }}"></script>
        <script src="{{ asset('js/notification/SmartNotification.min.js') }}"></script>
        <script src="{{ asset('js/plugin/datatables/jquery.dataTables.min.js') }}"></script>
        <script src="{{ asset('js/plugin/datatables/dataTables.colVis.min.js') }}"></script>
        <script src="{{ asset('js/plugin/datatables/dataTables.tableTools.min.js') }}"></script>
        <script src="{{ asset('js/plugin/datatables/dataTables.bootstrap.min.js') }}"></script>
        <script src="{{ asset('js/plugin/datatable-responsive/datatables.responsive.min.js') }}"></script>
        <script src="{{ asset('js/app.min.js') }}"></script>
        <!-- END GLOBAL SCRIPT -->
        
        @yield('script')
    </body>
</html>
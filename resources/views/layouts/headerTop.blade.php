<nav class="navbar navbar-expand-lg navbar-light" style="background:#fff;position:fixed;width:100%;z-index:999;border-bottom: 0.5px solid #D9D9D9;">
  <a class="navbar-brand" href="/">
    <img src="{{asset('assets/images/icon/logo-header.png')}}" width="30" height="30" alt="">
    <span class="text-success h6" >UTMS Dashboard</span>
  </a>
  
  <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <ul class="navbar-nav mr-auto">
        <li class="nav-item active">
          <a class="nav-link" href="#"> <span class="sr-only">(current)</span></a>
        </li>
        
      </ul>
      <ul class="navbar-nav">
          <li class="nav-item dropdown">
              <a class="nav-link  xdropdown-toggle" style="padding:0;" href="#" id="navbarDropdown2" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <div style="
                              background-color:#fff;
                              width:57px;
                              height:57px;
                              position:absolute;
                              top:-28px;
                              right: -14px;
                              z-index:23;
                              overflow: clip;
                      ">
                      @if(session()->get('user.avatar') == null)
                      <img  class="avatar" src="{{ asset('assets/images/author/user-avatar-icon.jpg') }}" alt="avatar">
                      @else
                      <img  class="avatar" src="{{ asset('storage/user') . '/' . session()->get('user.avatar') }}" alt="avatar">
                      @endif
                  </div>
              </a>
              <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown2" style="margin-top: 30px;"> 
                <a class="dropdown-item" href="{{ url('/user/profile') }}">You Are : {{session()->get('user.name')}} <br> Sign As {{session()->get('user.userGroup.name')}}</a>
                <a class="dropdown-item" href="{{ url('/user/change-password') }}">Change Password</a>
                <a class="dropdown-item" onclick="event.preventDefault(); document.getElementById('logout-form').submit();" href="{{ url('/logout') }}" >
                      Signout
                </a>
                <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                    @csrf
                </form>
              </div>
          </li>
        </ul>
    </div>
</nav>
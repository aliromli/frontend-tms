@extends('auth.app')

@section('title', 'Reset Password')

@section('content')
<!-- preloader area end -->
    <!-- login area start -->
    <div class="login-area login-s2">
        <div class="container">
            <div class="login-box ptb--100">
			    <form action="{{ route('password.email') }}" id="login-form" class="smart-form client-form" method="post"> @csrf
                    <div class="login-form-head mb-3">
                        <p>Reset Password</p>
						
                    </div>
					<div class="form-group">
                        <label for="staticEmail" class="">Email </label>
                        <input type="text"  class="form-control form-control-sm @error('email') is-invalid @enderror" name="email" value="{{ $email ?? old('email') }}" required autofocus id="staticEmail">
                    </div>
                    <div class="form-group">
						<button id="form_submit"  type="submit" class="btn-submit" style="width:100% !important;">Send Password Reset Link</button>
                    </div>
					<div class="form-group">
						<a class="text-success float-right small" href="{{ url('/login') }}">Remember Password ?</button>
                    </div> 
                    
                </form>
            </div>
        </div>
        
        <footer class="footer-login" >@ 2023 Axel Handal Trimpmitrasetia</footer>
    </div>
    
    <!-- login area end -->
@endsection

@extends('auth.app')

@section('title', 'Login')

@section('content')
<!-- preloader area end -->
    <!-- login area start -->
    <div class="login-area login-s2">
        <div class="container">
            <div class="login-box ptb--100">
			    <form action="{{ route('login') }}" id="login-form" class="smart-form client-form" method="post"> @csrf
                    <div class="login-form-head mb-3">
                        <p>Login to UTMS Dashboard</p>
					
						@if (session('success_otp'))
							<div class="alert alert-succcess alert-dismissible fade show" role="alert">
								{{session('success_otp')}} 
								<button type="button" class="close" data-dismiss="alert" aria-label="Close">
									<span class="fa fa-times"></span>
								</button>
							</div>
						@endif
						
						@if ($errors->has('email'))
							<div class="alert alert-danger alert-dismissible fade show" role="alert">
								{{ $errors->first('email') }}
								<button type="button" class="close" data-dismiss="alert" aria-label="Close">
									<span class="fa fa-times"></span>
								</button>
							</div>
						@endif
						
						@if ($errors->has('password'))
							<div class="alert alert-danger alert-dismissible fade show" role="alert">
								{{ $errors->first('password') }}
								<button type="button" class="close" data-dismiss="alert" aria-label="Close">
									<span class="fa fa-times"></span>
								</button>
							</div>
						@endif
                    </div>
                    <div class="form-group row">
                        <label for="staticEmail" class="col-sm-4 col-form-label">Username</label>
                        <div class="col-sm-8">
                            <input type="text" name="email" class="form-control-sm {{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus id="staticEmail">
                            
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputPassword" class="col-sm-4 col-form-label">Password</label>
                        <div class="col-sm-8">
                            <input type="password" class="form-control-sm  {{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required id="inputPassword" placeholder="">
                        </div>
                    </div>
                    <div class="form-group row">
                        
                        <div class="col-sm-7">
                            <div class="form-check">
                                <input class="form-check-input" type="checkbox" id="gridCheck1">
                                <label class="form-check-label small" for="gridCheck1">
                                Remember me
                                </label>
                            </div>
                        </div>
                        <div class="col-sm-5" class="">
							@if (Route::has('password.request'))
								<a class="text-success float-right small" href="{{ route('password.request') }}">
									Forget Password
								</a>
							@endif
						</div>
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-7">
                        </div>
                        <div class="col-sm-5"><button id="form_submit"  type="submit" class="float-right btn-submit">LOGIN</button></div>
                    </div>
                    
                </form>
            </div>
        </div>
        
        <footer class="footer-login" >@ 2023 Axel Handal Trimpmitrasetia</footer>
    </div>
    
    <!-- login area end -->
@endsection

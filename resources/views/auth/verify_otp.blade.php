@extends('auth.app')

@section('title', 'Verifikasi Email')

@section('content')
<!-- preloader area end -->
    <!-- login area start -->
    <div class="login-area login-s2">
        <div class="container">
            <div class="login-box ptb--100">
			    <form action="{{ route('otp.check') }}" id="otp-form" class="smart-form client-form" method="post"> @csrf
                    <div class="login-form-head mb-3">
                        <p>Verifikasi Email</p>						
						
                    </div>
					@if (session('error'))
						<div class="alert alert-danger" role="alert"> {{session('error')}} 
						</div>
                    @endif
					<div class="form-group">
                        <label for="staticEmail" class="">Email </label>
                        <input type="text"  class="form-control form-control-sm @error('email') is-invalid @enderror" name="email" value="{{$email}}" required autofocus id="staticEmail" disabled>
                        <input type="hidden" class="" name="emailAddress" value="{{$email}}" id="emailAddress">
                    </div>
					<div class="form-group">
                        <label for="otp" class="">Kode OTP </label>
                        <input type="text"  class="form-control" name="otp" value="" required autofocus id="otp">
                    </div>
                    <div class="form-group">
						<button id="form_submit"  type="submit" class="btn-submit btn btn-primary" style="width:100% !important;">Verify</button>
                    </div>
					
					
					@if (session('success'))
						<!--<div class="alert alert-success" role="alert"> {{session('success')}}</div>
						<div class="form-group">
							<a class="text-success float-right small" href="{{ url('/login') }}">Please Login</button>
						</div> -->
                    @endif
					
                    
                </form>
            </div>
        </div>
        
        <footer class="footer-login" >@ 2023 Axel Handal Trimpmitrasetia</footer>
    </div>
    
    <!-- login area end -->
@endsection

@extends('auth.app')

@section('title', 'Verifikasi Email')

@section('content')
<!-- preloader area end -->
    <!-- login area start -->
    <div class="login-area login-s2">
        <div class="container">
            <div class="login-box ptb--100">
			    <form action="{{ route('otp.check') }}" id="otp-form" class="smart-form client-form" method="post"> @csrf
                    <div class="login-form-head mb-">
                        <p>Verifikasi Email</p>
						@if (session('success'))
							<div class="alert alert-success" role="alert"> {{session('success')}} </div>
							<br/>
							<p>
								<a class="text-success large" href="{{ url('/login') }}">Please Login</button>
							</p> 
						@endif
						
						
                    </div>
					
					
					
                    
                </form>
            </div>
        </div>
        
        <footer class="footer-login" >@ 2023 Axel Handal Trimpmitrasetia</footer>
    </div>
    
    <!-- login area end -->
@endsection

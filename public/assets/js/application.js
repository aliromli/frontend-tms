(function ($) {
    "use strict";

    $("#dataTableApplication").wrap('<div class="dataTables_scroll" />');
    var dataTableApplication = null;

    if ($("#dataTableApplication").length) {
        // You can use 'alert' for alert message
        // or throw to 'throw' javascript error
        // or none to 'ignore' and hide error
        // or you own function
        // please read https://datatables.net/reference/event/error
        // for more information
        $.fn.dataTable.ext.errMode = "none";

        dataTableApplication = $("#dataTableApplication").DataTable({
            processing: true,
            serverSide: true,
            //dom : '',
            dom: "lrtip",
            searching: false,
            //"scrollY": "200px",
            // "scrollCollapse": true,
            //"sScrollX": '100%',
            //"sScrollXInner": "110%",
            pageLength: 10,
            lengthMenu: [
                [10, 25, 50, 100],
                [10, 25, 50, 100],
            ],
            pagingType: "full_numbers",
            ajax: {
                url: baseUrl + "/application-datatables",
                type: "GET",
                data: function (d) {
                    //d.deviceModelId = $('#search-model-app-id').val(); //search-model-app-id
                    d.sn = $("#search-app-sn").val();
                    d.name = $("#search-app-name").val();
                    d.deviceModelId = $("#select-device-model").val();
                },
            },
            language: {},
            //rowId: 'TRANSPORT_ID',
            columns: [
                {
                    data: "DT_RowIndex",
                    sortable: false,
                    searchable: false,
                    render: function (data, type, row, meta) {
                        return meta.row + meta.settings._iDisplayStart + 1;
                    },
                },
                { data: "version", name: "version", visible: false },
                { data: "name", name: "name" },
                { data: "appVersion", name: "appVersion" },
                { data: "deviceModel", name: "deviceModel" },
                { data: "companyName", name: "companyName" },
                { data: "fileSize", name: "fileSize" },
                { data: "id", sortable: false, searchable: false },
            ],
            columnDefs: [
                {
                    targets: 0,
                    className: "text-center",
                },
                {
                    targets: 1,
                    className: "mleft",
                    render: function (d, data, row) {
                        return `<span>` + d + `</span>`;
                    },
                },
                {
                    targets: 3,
                    className: "mleft",
                    render: function (d, data, row) {
                        return `<span>` + d + `</span>`;
                    },
                },
                {
                    targets: 4,
                    className: "mleft",
                    render: function (d, data, row) {
                        return `<span>` + d + `</span>`;
                    },
                },
                {
                    targets: 5,
                    className: "mleft",
                    render: function (d, data, row) {
                        return `<span>` + d + `</span>`;
                    },
                },
                {
                    targets: 6,
                    render: function (d, data, row) {
                        return bytesToSize(d);
                    },
                },
                {
                    targets: 7,
                    className: "text-center padd-row-table",
                    render: function (d, data, row) {
                        let v = row.version;
                        //let id = d;
                        return (
                            `<span>
					 <a href="application/` +
                            d +
                            `" data-id="` +
                            d +
                            `"><img class="pencil" src="` +
                            baseUrl +
                            `/assets/images/icon/pencil.png" /></a>&nbsp;
                     <a class="btn-delete" data-id="` +
                            d +
                            `" data-version="` +
                            v +
                            `"><img class="trash" src="` +
                            baseUrl +
                            `/assets/images/icon/trash.png" /></a>
                   
					</span>
                    `
                        );
                        //return `
                        //<a data-toggle="tooltip" data-placement="top" title="Get APK" href="application-get-apk/`+id+`" class="btn btn-flat btn-success btn-xs btn-edit" data-id="`+d+`"><i class="ti-android"></i></a>&nbsp;
                        //<a data-toggle="tooltip" data-placement="top" title="Edit" href="application/`+id+`" class="btn btn-flat btn-warning btn-xs btn-edit" data-id="`+d+`"><i class="fa fa-pencil"></i></a>&nbsp;
                        //<button data-toggle="tooltip" data-placement="top" title="Delete" type="button" class="btn btn-flat btn-danger btn-xs btn-delete" data-id="`+d+`" data-version="`+v+`"><i class="fa fa-trash"></i></button>
                        //`;
                    },
                },
            ],
        });
    }

    $("#btn-search-app-name").click(function () {
        dataTableApplication.draw(true);
    });

    $("body").on("click", "#show-icon", function () {
        //jQuery.noConflict();
        $("#modal-show-icon").modal("show");
        //$('#modal-show-icon .modal-title').html('View Icon');
        //alert("ok");
    });

    // clear
    $(".clear_filter").click(function () {
        $("#search-app-name").val("");
        $("#search-app-sn").val("");
        $("#select-device-model").val("");
        dataTableApplication.draw(true);
    });

    // Add New  or update
    $("#btn-submit-application").click(function () {
        // Update when city id has value
        var url = baseUrl + "/application/update";
        var action = "application";
        if (!$("#application-id").val()) {
            url = baseUrl + "/application/save";
            action = "save";
        }

        if ($("#application-id").val()) {
            if (!$("#version-app").val()) {
                $.smallBox({
                    //height: 50,
                    title: "Error",
                    content: "Application can't be empty",
                    color: "#dc3912",
                    sound_file: "smallbox",
                    timeout: 3000,
                });
                $("#version-app").focus();
                return;
            }
        }

        // Has error
        // var hasError = false;
        // Check requirement input
        if (!$("#application-name").val()) {
            $.smallBox({
                //height: 50,
                title: "Error",
                content: "Application Name can't be empty",
                color: "#dc3912",
                sound_file: "smallbox",
                timeout: 3000,
            });
            $("#application-name").focus();
            return;
        }

        if (!$("#application-packageName").val()) {
            $.smallBox({
                //height: 50,
                title: "Error",
                content: "Package Name can't be empty",
                color: "#dc3912",
                sound_file: "smallbox",
                timeout: 3000,
            });
            $("#application-packageName").focus();
            return;
        }
        if (!$("#application-description").val()) {
            $.smallBox({
                //height: 50,
                title: "Error",
                content: "Description can't be empty",
                color: "#dc3912",
                sound_file: "smallbox",
                timeout: 3000,
            });
            $("#application-description").focus();
            return;
        }

        if (!$("#application-appVersion").val()) {
            $.smallBox({
                //height: 50,
                title: "Error",
                content: "AppVersion can't be empty",
                color: "#dc3912",
                sound_file: "smallbox",
                timeout: 3000,
            });
            $("#application-appVersion").focus();
            return;
        }

        /* if(!$('input[name="uninstallable"]:checked').val()) {
            $.smallBox({
                //height: 50,
                title : "Error",
                content : 'Uninstallable can\'t be empty',
                color : "#dc3912",
                sound_file: "smallbox",
                timeout: 3000
            });
            $('input[name="uninstallable"]').focus();
            return;
        }*/

        if (!$("#application-companyName").val()) {
            $.smallBox({
                //height: 50,
                title: "Error",
                content: "Company Name can't be empty",
                color: "#dc3912",
                sound_file: "smallbox",
                timeout: 3000,
            });
            $("#application-companyName").focus();
            return;
        }

        /*if(!$('#application-deviceModelId').val()) {
            $.smallBox({
                //height: 50,
                title : "Error",
                content : 'Device Model  can\'t be empty',
                color : "#dc3912",
                sound_file: "smallbox",
                timeout: 3000
            });
            $('#application-deviceModelId').focus();
            return;
        }*/

        var arlist = [];
        $("#fr-list-dm>li").each(function () {
            //let b = $(this).find('.md').text();
            let a = $(this).find(".uid").text();
            //var ob = new Object();
            //ob.nameModel = b;
            //ob.uid = a;
            //arlist.push(ob);
            arlist.push(a);
        });

        // Show loder
        $(".page-loader").removeClass("hidden");
        $(".loading>img").removeClass("hidden");
        var form = new FormData();
        var apk = $("#application-apk")[0].files[0];
        var icon = $("#application-icon")[0].files[0];
        form.append("apk", apk);
        form.append("icon", icon);
        form.append("id", $("#application-id").val());
        form.append("packageName", $("#application-packageName").val());
        form.append("name", $("#application-name").val());
        form.append("appVersion", $("#application-appVersion").val());
        form.append("description", $("#application-description").val());
        form.append("companyName", $("#application-companyName").val());
        //form.append('deviceModelId',$('#application-deviceModelId').val());
        form.append("deviceModelId", arlist.join(","));
        //form.append('uninstallable',$('input[name="uninstallable"]:checked').val());
        form.append(
            "uninstallable",
            $('input[name="uninstallable"]').is(":checked") ? "true" : "false"
        );
        form.append("version", $("#version-app").val());

        // Send data
        $.ajax({
            url: url,
            type: "POST",
            contentType: false,
            processData: false,
            cache: false,
            headers: {
                "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
            },

            data: form,
            success: function (resp) {
                if (resp.responseCode == "0000") {
                    //sukses
                    // Send success message
                    $.smallBox({
                        height: 50,
                        title: resp.responseStatus,
                        content: resp.responseMessage,
                        color: "#109618",
                        sound_file: "voice_on",
                        timeout: 3000,
                        //icon : "fa fa-bell swing animated"
                    });
                    window.location.replace(baseUrl + "/application");
                    // if($('#application-id').val()=="") {
                    //     $('#application-packageName').val("");
                    //     $("#application-name").val("");
                    //     $("#application-appVersion'").val("");
                    //     $("#application-description").val("");
                    //     $("#application-companyName").val("");
                    //     $("#application-deviceModelId").val("");
                    //     $("#application-deviceModel-search").val("");
                    //     $("#application-deviceModelId").val("");
                    //     $('#version-app').val("");
                    //     $('input[name="uninstallable"]').removeAttr("checked");
                    // }
                } else if (
                    resp.responseCode == "3333" ||
                    resp.responseCode == "0400"
                ) {
                    //exception and not found

                    $.smallBox({
                        height: 50,
                        title: resp.responseStatus,
                        content: resp.responseMessage,
                        color: "#dc3912",
                        sound_file: "voice_on",
                        timeout: 3000,
                        //icon : "fa fa-bell swing animated"
                    });
                } else if (resp.responseCode == "5555") {
                    //validaator
                    var data = Object.values(resp.responseMessage);
                    var ln = data.length;
                    var w = 3000 * ln;
                    data.map(function (d) {
                        $.smallBox({
                            height: 50,
                            title: resp.responseStatus,
                            content: d,
                            color: "#dc3912",
                            sound_file: "smallbox",
                            timeout: w,
                            //icon : "fa fa-bell swing animated"
                        });
                    });
                }
                // Hide loder
                $(".page-loader").addClass("hidden");
                $(".loading>img").addClass("hidden");
            },
            error: function (xhr, ajaxOptions, thrownError) {
                $.smallBox({
                    title: "Error",
                    content: xhr.statusText,
                    color: "#dc3912",
                    timeout: 3000,
                    //icon : "fa fa-bell swing animated"
                });
                // Hide loder
                $(".page-loader").addClass("hidden");
                $(".loading>img").addClass("hidden");
            },
        });
    });

    /* list modal device model */
    $("body").on("click", "#btn-modal-list-d-model", function () {
        $("#modal-list-md-md").modal("show");
        $("#modal-list-md-md .title-form").html("Model Device List");
        //$('#modal-user input').val('');
        //$('#modal-user input[type=text],#modal-user input[type=hidden],#modal-user input[type=password],#modal-user input[type=email],#modal-user input[type=number]').val('').removeAttr('readonly');
    });

    /* list device model */
    $("#dataTableApplication-list-device-model").wrap(
        '<div class="dataTables_scroll" />'
    );
    var dataTableApplication_list_device_model = null;
    if ($("#dataTableApplication-list-device-model").length) {
        // You can use 'alert' for alert message
        // or throw to 'throw' javascript error
        // or none to 'ignore' and hide error
        // or you own function
        // please read https://datatables.net/reference/event/error
        // for more information
        $.fn.dataTable.ext.errMode = "none";

        dataTableApplication_list_device_model = $(
            "#dataTableApplication-list-device-model"
        ).DataTable({
            processing: true,
            serverSide: true,
            //dom : '',
            dom: "tip",
            searching: false,

            pageLength: 10,
            lengthMenu: [
                [10, 25, 50, 100],
                [10, 25, 50, 100],
            ],
            pagingType: "full_numbers",
            ajax: {
                url: baseUrl + "/device-model-datatables",
                type: "GET",
                data: function (d) {
                    d.model = $("#search-model").val();
                },
            },
            language: {},
            //rowId: 'TRANSPORT_ID',
            columns: [
                {
                    data: "DT_RowIndex",
                    sortable: false,
                    searchable: false,
                    render: function (data, type, row, meta) {
                        return meta.row + meta.settings._iDisplayStart + 1;
                    },
                },
                { data: "version", name: "version", visible: false },
                { data: "model", name: "model" },
                { data: "vendorName", name: "vendorName" },
                { data: "vendorCountry", name: "vendorCountry" },
                { data: "id", sortable: false, searchable: false },
            ],
            columnDefs: [
                {
                    targets: 0,
                    className: "text-center",
                    render: function (d, data, row) {
                        return `<span>` + d + `</span>`;
                    },
                },
                {
                    targets: 2,
                    className: "mleft",
                    render: function (d, data, row) {
                        return `<span>` + d + `</span>`;
                    },
                },
                {
                    targets: 3,
                    className: "mleft",
                    render: function (d, data, row) {
                        return `<span>` + d + `</span>`;
                    },
                },
                {
                    targets: 4,
                    className: "mleft",
                    render: function (d, data, row) {
                        return `<span>` + d + `</span>`;
                    },
                },
                {
                    targets: 5,
                    className: "text-center padd-row-table",
                    render: function (d, data, row) {
                        let id = d;
                        let v = row.version;

                        return (
                            `<span>
                     <input 
                     type="checkbox" 
                     name="dmApp[]" 
                     value="true"  
                     data-id="` +
                            d +
                            `" 
                     data-name="` +
                            row.model +
                            `"                               
                     
                     /></span>
                   `
                        );
                    },
                },
            ],
        });
    }

    $("body").on("click", "#btn-search-model", function () {
        dataTableApplication_list_device_model.draw(true);
    });
    $("body").on("click", "#btn-clear-model", function () {
        $("#search-model").val("");
        dataTableApplication_list_device_model.draw(true);
    });

    $("body").on("click", "#btn-add-md-to-list", function () {
        var arlist = [];
        $("#fr-list-dm>li").each(function () {
            let b = $(this).find(".md").text();
            let a = $(this).find(".uid").text();

            var ob = new Object();
            ob.nameModel = b;
            ob.uid = a;
            arlist.push(ob);
        });

        var arlist2 = [];
        var datac = $('[name="dmApp[]"]');

        $.each(datac, function () {
            var $this = $(this);

            // check if the checkbox is checked
            if ($this.is(":checked")) {
                var ob = new Object();
                let find = 0;
                if (arlist.length > 0) {
                    for (var i = 0; i < arlist.length; i++) {
                        if (arlist[i].uid == $this.data("id")) {
                            find = 1;
                            break;
                        }
                    }
                }

                if (find == 0) {
                    ob.nameModel = $this.data("name");
                    ob.uid = $this.data("id");
                    arlist2.push(ob);
                }
            }
        });

        let arr = [...arlist, ...arlist2];
        //console.log('1',arr);
        let mergedArr = [...new Set(arr)];
        //console.log('2',mergedArr);

        var d = "";
        for (var i = 0; i < mergedArr.length; i++) {
            let nameModel =
                '<span class="md">' + mergedArr[i].nameModel + "</span>";
            let uid =
                '<span class="uid" style="display:none;">' +
                mergedArr[i].uid +
                "</span>";
            let rem =
                '<i class="fa fa-trash-o btn-delete-list-tg-t" style="color:#717682;margin-left:20px;"></i>';
            //d+=`<option value="`+uid+`">`+nameModel+` `+rem+`</option>`;
            d +=
                '<li class="ch-li">' +
                nameModel +
                " " +
                rem +
                " " +
                uid +
                "</li>";
        }

        $("#fr-list-dm").find("li").remove();
        $("#fr-list-dm").html(d);
        $("#modal-list-md-md").modal("hide");
    });

    $("body").on("click", ".btn-delete-list-tg-t", function () {
        $(this).parent().remove();
    });
    $("#dataTableApplication").on("click", ".btn-delete", function () {
        let text;
        if (confirm("Are you sure to remove!") == true) {
            // Send data
            $.ajax({
                url: baseUrl + "/application/delete",
                type: "POST",
                headers: {
                    "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr(
                        "content"
                    ),
                },
                data: {
                    id: $(this).data("id"),
                    version: $(this).data("version"),
                },
                success: function (resp) {
                    if (resp.responseCode === 200) {
                        // Reload datatable
                        dataTableApplication.ajax.reload();

                        // Send success message
                        $.smallBox({
                            height: 50,
                            title: "Success",
                            content: resp.responseMessage,
                            color: "#109618",
                            sound_file: "voice_on",
                            timeout: 3000,
                            //icon : "fa fa-bell swing animated"
                        });
                    } else {
                        $.smallBox({
                            height: 50,
                            title: "Error",
                            content: resp.responseMessage,
                            color: "#dc3912",
                            sound_file: "smallbox",
                            timeout: 3000,
                            //icon : "fa fa-bell swing animated"
                        });
                    }
                    // Hide loder
                    $(".page-loader").addClass("hidden");
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    $.smallBox({
                        title: "Error",
                        content: xhr.statusText,
                        color: "#dc3912",
                        timeout: 3000,
                        //icon : "fa fa-bell swing animated"
                    });
                    // Hide loder
                    $(".page-loader").addClass("hidden");
                },
            });
        }
    });
    /*
    //----------  autocomplite devicemodel ----  
    //var path = "{{ route('autocomplete') }}";
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    $( "#application-deviceModel-search" ).autocomplete({
        delay: 100,
        // classes: {
        //     "ui-autocomplete": "highlight"
        //   },
        source: function( request, response ) {
        // Fetch data
        $.ajax({
            //url:"{{route('application-auto)}}",
            url:  baseUrl + '/application-auto',
            type: 'post',
            dataType: "json",
            data: {
            _token: CSRF_TOKEN,
            search: request.term
            },
            success: function( data ) {
            response( data );
            }
        });
        },
        minLength: 3,
        select: function (event, ui) {
            // Set selection
            $('#application-deviceModel-search').val(ui.item.label); // display the selected text
            $('#application-deviceModelId').val(ui.item.value); // save selected id to input
            return false;
        },
   
    });

    //autocomplete serach
   $( "#search-model-app" ).autocomplete({
        delay: 100,
        // classes: {
        //     "ui-autocomplete": "highlight"
        //   },
        source: function( request, response ) {
        // Fetch data
        $.ajax({
            //url:"{{route('application-auto)}}",
            url:  baseUrl + '/application-auto',
            type: 'post',
            dataType: "json",
            data: {
            _token: CSRF_TOKEN,
            search: request.term
            },
            success: function( data ) {
            response( data );
            }
        });
        },
        minLength: 3,
        select: function (event, ui) {
            // Set selection
            $('#search-model-app').val(ui.item.label); // display the selected text
            $('#search-model-app-id').val(ui.item.value); // save selected id to input
            return false;
        },
   
    });
    */

    function bytesToSize(bytes, seperator = "") {
        const sizes = ["Bytes", "KB", "MB", "GB", "TB"];
        if (bytes == 0) return "n/a";
        const i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)), 10);
        if (i === 0) return `${bytes}${seperator}${sizes[i]}`;
        return `${(bytes / 1024 ** i).toFixed(1)}${seperator}${sizes[i]}`;
    }
})(jQuery);

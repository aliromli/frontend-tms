(function($) {
    "use strict";
	
    // Terminal group modal tabel
    $('#dataTableTgListGFormDiag').wrap('<div class="dataTables_scroll" />');
    var dataTableTgListGFormDiag = null;
    if ($('#dataTableTgListGFormDiag').length) {
        // You can use 'alert' for alert message
        // or throw to 'throw' javascript error
        // or none to 'ignore' and hide error
        // or you own function
        // please read https://datatables.net/reference/event/error
        // for more information
        $.fn.dataTable.ext.errMode = 'none';
        dataTableTgListGFormDiag =  $('#dataTableTgListGFormDiag').DataTable({
        processing: true,
        serverSide: true,
        //dom : '',
        dom: 'lrtip',
        "searching": false,
        pageLength: 10,
        lengthMenu: [[10, 25, 50, 100], [10, 25, 50, 100]],
        pagingType: 'full_numbers',
        ajax: {
            url:   baseUrl+"/terminal-group-datatables",
            type: 'GET',
            data:  function(d){
                d.name = $('#search-name-g-diag').val();
            }
        },
        language: {
         
        },
        //rowId: 'TRANSPORT_ID',
        columns: [
            {data: "DT_RowIndex", 
               sortable: false, 
               searchable: false,
               "render": function (data, type, row, meta) {      
                          return meta.row + meta.settings._iDisplayStart + 1;     
               }  
            },
            {data: "version", name: "version", visible:false},
            {data: "name", name: "name"},
            {data : "description", name : "description"},
            {data: "id", sortable: false, searchable: false, }
        ],
        columnDefs:[
			{
				"targets": 0,
				"className": "text-center",
			},
			{
					"targets": 2,
					"className": "mleft",
					 render: function(d,data,row) {
						 return `<span>`+d+`</span>`
					 }
			},
			{
					"targets": 3,
					"className": "mleft",
					 render: function(d,data,row) {
						 return `<span>`+d+`</span>`
					 }
			},
	        {
                targets: 4,
				"className": "text-center",
                render: function(d,data,row) {
                    let id = d;
                    let v = row.version;
                   
                    return `
                    <input 
                    type="checkbox" 
                    name="chtg_diag[]" 
                    value="true"  
                    data-id="`+d+`" 
                    data-name="`+row.name+`" 
                    data-description="`+row.description+`"                      
                    data-version="`+v+`"
                    />
                  `;
                }
            }
        ]
        });
      
    }
	
	// terminal modal tabel
    $('#dataTableTerListGFormDiag').wrap('<div class="dataTables_scroll" />');
    var dataTableTerListGFormDiag = null;
    if ($('#dataTableTerListGFormDiag').length) {
        // You can use 'alert' for alert message
			// or throw to 'throw' javascript error
			// or none to 'ignore' and hide error
			// or you own function
			// please read https://datatables.net/reference/event/error
			// for more information
			$.fn.dataTable.ext.errMode = 'none';
			
			dataTableTerListGFormDiag =  $('#dataTableTerListGFormDiag').DataTable({
			processing: true,
			serverSide: true,
			//dom: 'lrtip',
			dom: 'tip',
			"searching": false,
			
			pageLength: 10,
			lengthMenu: [[10, 25, 50, 100], [10, 25, 50, 100]],
			pagingType: 'full_numbers',
			ajax: { 
				url:   baseUrl+"/terminal-datatables", 
				type: 'GET',
				data:  function(d){

					d.sn = $('#search-sn-diag').val();
					
				}
			},
			language: {
			 
			},
			//rowId: 'TRANSPORT_ID',
			columns: [
				{data: "DT_RowIndex", 
				   sortable: false, 
				   searchable: false,
				   "render": function (data, type, row, meta) {      
							  return meta.row + meta.settings._iDisplayStart + 1;     
				   }  
				},
				{data: "sn", name: "sn"},
				//{data: "terminalId", name: "terminalId",visible:false},
				{data: "id", sortable: false, searchable: false, }
			],
			columnDefs:[
				{
					"targets": 0,
					"className": "text-center",
				},
				//{
				//	"targets": 1,
				//	"className": "mleft",
				//	 render: function(d,data,row) {
				//		 return `<span>`+d+`</span>`
				//	 }
				//},
				{
					targets: 2,
					"className": "text-center",
					render: function(d,data,row) {
						let id = d;
						//let idT = row.id;
					 	return `<span>
							<input 
							type="checkbox" 
							name="chter_diag[]" 
							value="true"  
							data-id="`+d+`" 
							data-sn="`+row.sn+`"
							
							/></span>
							`;
					   
					}
				}
			]
		});
	
	}
	
    
    $('#btn-search-name-diag').click(function() {
        dataTableTgListGFormDiag.draw(true);
    });
	$('#btn-clear-name-diag').click(function() {
		$('#search-name-g-diag').val('');
        dataTableTgListGFormDiag.draw(true);
    });
	
	$('body').on('click', '#btn-add-terminal-group-diag', function() {
		
		$('#modal-list-tg-diag').modal('show');
        $('#modal-list-tg-diag .modal-title').html('List Terminal Group');
		dataTableTgListGFormDiag.draw(true);
		
	});
	//=== =====
	 $('#btn-search-sn-diag').click(function() {
        dataTableTerListGFormDiag.draw(true);
    });
	$('#btn-clear-sn-diag').click(function() {
		$('#search-sn-diag').val('');
        dataTableTerListGFormDiag.draw(true);
    });
	
	$('body').on('click', '#btn-add-terminal-list-diag', function() {
		
		$('#modal-list-t-diag').modal('show');
        $('#modal-list-t-diag .modal-title').html('List Terminal');
		dataTableTerListGFormDiag.draw(true);
	});
	
	
	
	
	$('body').on('click', '#btn-tg-to-list-diag', function() {
		var arlist = [];
        $('#terminal-group-list-diag option').each(function(index,element){
           
            var ob = new Object();
			ob.name = element.text;
			ob.uid = element.value;
			arlist.push(ob);
        });
        var arlist2 = [];
        var datac = $('[name="chtg_diag[]"]');
        // var html = "";
        $.each(datac, function() {
            var $this = $(this);

            // check if the checkbox is checked
            if($this.is(":checked")) {
                var ob = new Object();
                let find = 0;
                if(arlist.length > 0)
                {
                    for(var i=0; i<arlist.length; i++)
                    {
                        if(arlist[i].uid == $this.data("id"))
                        {
                            find = 1;
                            break;
                        }
                    }
                }
                
                if(find == 0) 
                {
                    ob.name = $this.data("name");
                    ob.uid = $this.data("id");
                    arlist2.push(ob);
                }
                
            }
        });	

        let arr = [...arlist, ...arlist2];
        //console.log('1',arr);
        let mergedArr = [...new Set(arr)];
        //console.log('2',mergedArr);

        var d = "";		
		for(var i=0; i<mergedArr.length; i++)
		{
		    let name = mergedArr[i].name;
			//let description = mergedArr[i].description;
            let uid = mergedArr[i].uid;
			//let act = `<button type="button" class="btn btn-flat btn-danger btn-xs btn-delete-list-tg-t"><i class="fa fa-remove"></i></button>`;
			d+=`<option value="`+uid+`">`+name+`</option>`;
		} 
        $('#terminal-group-list-diag option').remove();  
        $('#terminal-group-list-diag').html(d);       
		$('#modal-list-tg-diag').modal('hide');
		
	});
	
	$('body').on('click', '#btn-t-to-list-diag', function() {
		
		var arlist = [];
        $('#terminal-list-diag option').each(function(index,element){
           
            var ob = new Object();
			ob.sn = element.text;
			ob.uid = element.value;
			arlist.push(ob);
        });
        var arlist2 = [];
        var datac = $('[name="chter_diag[]"]');
       // var html = "";
      
        $.each(datac, function() {
            var $this = $(this);

            // check if the checkbox is checked
            if($this.is(":checked")) {
                var ob = new Object();
                let find = 0;
                if(arlist.length > 0)
                {
                    for(var i=0; i<arlist.length; i++)
                    {
                        if(arlist[i].uid == $this.data("id"))
                        {
                            find = 1;
                            break;
                        }
                    }
                }
                
                if(find == 0) 
                {
                    ob.sn = $this.data("sn");
                    ob.uid = $this.data("id");
                    arlist2.push(ob);
                }
                
            }
        });	

        let arr = [...arlist, ...arlist2];
        //console.log('1',arr);
        let mergedArr = [...new Set(arr)];
        //console.log('2',mergedArr);

        var d = "";		
		for(var i=0; i<mergedArr.length; i++)
		{
		    let sn = mergedArr[i].sn;
			//let description = mergedArr[i].description;
            let uid = mergedArr[i].uid;
			//let act = `<button type="button" class="btn btn-flat btn-danger btn-xs btn-delete-list-tg-t"><i class="fa fa-remove"></i></button>`;
			d+=`<option value="`+uid+`">`+sn+`</option>`;
		} 
        $('#terminal-list-diag option').remove();  
        $('#terminal-list-diag').html(d);       
		$('#modal-list-t-diag').modal('hide');
		
	});
	
	$('body').on('change', '#export_all_diag', function() {
		if(this.checked) {
		    // checkbox is checked
			$('#diagnostic-select-time').attr('disabled',true);
			$('#diagnostic-select-time').val('');
			$('#terminal-group-list-diag').html('');
			$('#terminal-group-list-diag').attr('disabled',true);
			$('#terminal-list-diag').html('');
			$('#terminal-list-diag').attr('disabled',true);
		}
		else
		{
			$('#diagnostic-select-time').attr('disabled',false);
			$('#terminal-list-diag').attr('disabled',false);
			$('#terminal-group-list-diag').attr('disabled',false);
			
		}
	});
	
	
	$('body').on('click', '#export-diagnostic', function() {
		
		var arlist = [];
        $('#terminal-list-diag option').each(function(index,element){
           
            //var ob = new Object();
			
			//ob.uid = element.value;
			//arlist.push(ob);
			arlist.push(element.value);
        });
		
		var arlistG = [];
        $('#terminal-group-list-diag option').each(function(index,element){
           
            //var ob = new Object();
			//ob.name = element.text;
			//ob.uid = element.value;
			arlistG.push(element.value);
			
        });
		
		
		var data = new FormData();
		
		var cek = $('input[name="export_all_diag"]:checked').val()  ===undefined ?  'false' : 'true';
		data.append('terminals',JSON.stringify(arlist));
		data.append('terminalGroups',JSON.stringify(arlistG));
		data.append('date',$('#diagnostic-select-time').val());
		data.append('export_cek',cek);
		$.ajax({
            url: baseUrl+"/diagnostic/export", 
            type: 'POST',
			contentType: false,
			processData: false,
			cache: false,
			xhrFields: {
                responseType: 'blob'
            },
			headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
			data: data,
			success: function (data, textStatus, xhr) {
                // check for a filename
                var filename = "";
                var disposition = xhr.getResponseHeader('Content-Disposition');
                if (disposition && disposition.indexOf('attachment') !== -1) {
                    var filenameRegex = /filename[^;=\n]*=((['"]).*?\2|[^;\n]*)/;
                    var matches = filenameRegex.exec(disposition);
                    if (matches != null && matches[1]) filename = matches[1].replace(/['"]/g, '');
                    var a = document.createElement('a');
                    var url = window.URL.createObjectURL(data);
                    a.href = url;
                    a.download = filename;
                    document.body.append(a);
                    a.click();
                    a.remove();
                    window.URL.revokeObjectURL(url);
                }
                else {
                    alert("Error");
                }
               
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {

            },

        });
		
	});
  
})(jQuery);
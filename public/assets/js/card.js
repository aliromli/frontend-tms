(function($) {
    "use strict";
    $('#dataTableCard').wrap('<div class="dataTables_scroll" />');
    var dataTableCard = null;
    if ($('#dataTableCard').length) {
        // You can use 'alert' for alert message
        // or throw to 'throw' javascript error
        // or none to 'ignore' and hide error
        // or you own function
        // please read https://datatables.net/reference/event/error
        // for more information
        $.fn.dataTable.ext.errMode = 'none';
        
        dataTableCard =  $('#dataTableCard').DataTable({
        processing: true,
        serverSide: true,
        //dom : '',
        dom: 'lrtip',
        "searching": false,
        //"scrollX": true,
        //"sScrollX": '100%',
        //"sScrollXInner": "110%",
        pageLength: 10,
        lengthMenu: [[10, 25, 50, 100], [10, 25, 50, 100]],
        pagingType: 'full_numbers',
        ajax: {
            url:   baseUrl+"/card-datatable",
            type: 'GET',
            data:  function(d){
                d.name= $('#search-name').val();
                
            }
        },
        language: {
            
        },
        //rowId: 'TRANSPORT_ID',
        columns: [
            {data: "DT_RowIndex", 
               sortable: false, 
               searchable: false,
               "render": function (data, type, row, meta) {      
                          return meta.row + meta.settings._iDisplayStart + 1;     
               }  
            },
            {data: "version", name: "version"},
            {data: "name", name: "name"},
            {data: "binRangeStart", name: "binRangeStart"},
            {data: "binRangeEnd", name: "binRangeEnd"}, 
            {data: "cardNumLength", name: "cardNumLength"},   
            {data: "id", sortable: false, searchable: false, class: "action"}
        ],
		
        columnDefs:[
	        {
                targets: 6,
                render: function(d,data,row) {
                 
                    let v = row.version;
                    let id = d;
                    return `
                    <a href="card/`+id+`" class="btn btn-flat btn-warning btn-xs btn-edit" data-id="`+d+`"><i class="fa fa-pencil"></i></a>&nbsp;
                    <button type="button" class="btn btn-flat btn-danger btn-xs btn-delete" data-id="`+d+`" data-version="`+v+`"><i class="fa fa-trash"></i></button>
                    `;
                }
            }
        ]
        });
      
    }

    
    $('#btn-cari-cd').click(function() {
        dataTableCard.draw(true);
    });

      /* filter tugel */
     // Toggle filter
     $('.filter .tugel').click(function() {
        $(this).toggleClass('open');
        $('.filter-title').toggleClass('hidden');
        $('.form-filter form').toggleClass('open-filter');
        
    });

    // clear
    $('.clear-search').click(function() {
        $('#search-name').val('');
       
        dataTableCard.draw(true);
    });

    // Add New  or update
    $('#btn-submit-cd').click(function(){

        // Update when city id has value
        var url = baseUrl + '/card/update';
        var action = "update";
        if(!$('#cd-id').val()) {
            url = baseUrl + '/card/save';
            action = "save";
        }

        if($('#cd-id').val()) {
            if(!$('#version-cd').val()) {
                $.smallBox({
                    //height: 50,
                    title : "Error",
                    content : 'Version can\'t be empty',
                    color : "#dc3912",
                    sound_file: "smallbox",
                    timeout: 3000
                });
                $('#version-cd').focus();
                return;
            }
        }

        // Has error
        // var hasError = false;
        // Check requirement input
        if(!$('#cd-name').val()) {
            $.smallBox({
                //height: 50,
                title : "Error",
                content : 'Name  can\'t be empty',
                color : "#dc3912",
                sound_file: "smallbox",
                timeout: 3000
            });
            $('#cd-name').focus();
            return;
        }

        if(!$('#cd-binRangeStart').val()) {
            $.smallBox({
                //height: 50,
                title : "Error",
                content : 'BinRangeStart can\'t be empty',
                color : "#dc3912",
                sound_file: "smallbox",
                timeout: 3000
            });
            $('#cd-binRangeStart').focus();
            return;
        }
		
		
		 if(!$('#cd-binRangeEnd').val()) {
            $.smallBox({
                //height: 50,
                title : "Error",
                content : 'BinRangeEnd can\'t be empty',
                color : "#dc3912",
                sound_file: "smallbox",
                timeout: 3000
            });
            $('#cd-binRangeEnd').focus();
            return;
        }
		
		if(!$('#cd-cardNumLength').val()) {
            $.smallBox({
                //height: 50,
                title : "Error",
                content : 'CardNumLength can\'t be empty',
                color : "#dc3912",
                sound_file: "smallbox",
                timeout: 3000
            });
            $('#cd-cardNumLength').focus();
            return;
        }
		
		if(!$('input[name="panDigitUnmasking[]"]:checked').val())
		{
			 $.smallBox({
                //height: 50,
                title : "Error",
                content : 'PanDigitUnmasking can\'t be empty',
                color : "#dc3912",
                sound_file: "smallbox",
                timeout: 3000
            });
            $('#cd-panDigitUnmasking').focus();
            return;
		}
	
		// Show loder
        $('.page-loader').removeClass('hidden');

        // Send data
        $.ajax({
            url: url,
            type: 'POST',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: {
                'id': $('#cd-id').val(),
                'name' : $('#cd-name').val(),
                'binRangeStart' : $('#cd-binRangeStart').val(),
                'binRangeEnd' : $('#cd-binRangeEnd').val(),
                'cardNumLength' : $('#cd-cardNumLength').val(),
                'panDigitUnmasking' : $('input[name="panDigitUnmasking[]"]:checked').val(),
                'printCardholderCopy' : $('input[name="printCardholderCopy[]"]:checked').val(),
                'printMerchantCopy' : $('input[name="printMerchantCopy[]"]:checked').val(),
                'printBankCopy' : $('input[name="printBankCopy[]"]:checked').val(),
                'pinPrompt' : $('input[name="pinPrompt[]"]:checked').val(),
				 'pinLength' : $('#cd-pinLength').val(),
                'version': $('#version-cd').val(),
                
            },
		
            success: function(resp) {
				
				
                if(resp.responseCode == '0000') { //sukses
                   
                    // Send success message
                    $.smallBox({
                        height: 50,
                        title : resp.responseStatus, 
                        content : resp.responseMessage,
                        color : "#109618",
                        sound_file: "voice_on",
                        timeout: 3000
                        //icon : "fa fa-bell swing animated"
                    });

                    if($('#cd-id').val()=="") {
                       
						$('#cd-name').val('');
                        $('#cd-binRangeStart').val('');
                        $('#cd-binRangeEnd').val('');
                        $('#cd-cardNumLength').val('');
                        $('#cd-pinLength').val('');
					
						$("input:radio").removeAttr("checked");
						
                    }


 
                } else if(resp.responseCode == '3333' || resp.responseCode == '0400'){ //exception and not found
					
					 $.smallBox({
                        height: 50,
                        title : resp.responseStatus,
                        content : resp.responseMessage,
                        color : "#dc3912",
                        sound_file: "voice_on",
                        timeout: 3000
                        //icon : "fa fa-bell swing animated"
                    });
                }
				else if(resp.responseCode == '5555')//validaator
				{
					
						 var data = Object.values(resp.responseMessage);
						 var ln = data.length;
						 var w  = 3000 * ln; 
						 data.map(function(d){
							 
								$.smallBox({
										height: 50,
										title : resp.responseStatus,
										content : d,
										color : "#dc3912",
										sound_file: "smallbox",
										timeout: w
										//icon : "fa fa-bell swing animated"
									});
						});
					
				}	
                // Hide loder
                $('.page-loader').addClass('hidden');
            },
            error: function(xhr, ajaxOptions, thrownError) {
                $.smallBox({
                    title : "Error",
                    content : xhr.statusText,
                    color : "#dc3912",
                    timeout: 3000
                    //icon : "fa fa-bell swing animated"
                });
                // Hide loder
                $('.page-loader').addClass('hidden');
            }
        });

    });

    // Edit 
    $('#dataTableCard').on('click', '.btn-delete', function() {

        let text;
        if (confirm("Are you sure to remove!") == true) {
            
             // Send data
            $.ajax({
                url:  baseUrl + '/card/delete',
                type: 'POST',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data: {
                    'id': $(this).data('id'),
                    'version': $(this).data('version')
                },
                success: function(resp) {
                    if(resp.responseCode === 200) {
                        // Reload datatable
                        dataTableCard.ajax.reload();
                        
                        // Send success message
                        $.smallBox({
                            height: 50,
                            title : "Success",
                            content : resp.responseMessage,
                            color : "#109618",
                            sound_file: "voice_on",
                            timeout: 3000
                            //icon : "fa fa-bell swing animated"
                        });
                    } else {
                        $.smallBox({
                            height: 50,
                            title : "Error",
                            content : resp.responseMessage,
                            color : "#dc3912",
                            sound_file: "smallbox",
                            timeout: 3000
                            //icon : "fa fa-bell swing animated"
                        });
                    }
                    // Hide loder
                    $('.page-loader').addClass('hidden');
                },
                error: function(xhr, ajaxOptions, thrownError) {
                    $.smallBox({
                        title : "Error",
                        content : xhr.statusText,
                        color : "#dc3912",
                        timeout: 3000
                        //icon : "fa fa-bell swing animated"
                    });
                    // Hide loder
                    $('.page-loader').addClass('hidden');
                }
            });

       
        } 

    });



})(jQuery);
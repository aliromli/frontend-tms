(function($) {
    "use strict";

    $('#dataTableState').wrap('<div class="dataTables_scroll" />');
    var dataTableState = null;
    if ($('#dataTableState').length) {
        // You can use 'alert' for alert message
        // or throw to 'throw' javascript error
        // or none to 'ignore' and hide error
        // or you own function
        // please read https://datatables.net/reference/event/error
        // for more information
        $.fn.dataTable.ext.errMode = 'none';
        
        dataTableState =  $('#dataTableState').DataTable({
        processing: true,
        serverSide: true,
        //dom : '',
        dom: 'lrtip',
        "searching": false,
        //"lengthChange": false,
        pageLength: 10,
        lengthMenu: [[10, 25, 50, 100], [10, 25, 50, 100]],
        pagingType: 'full_numbers',
        ajax: {
            url:   baseUrl+"/state-datatables",
            type: 'GET',
            data:  function(d){
                d.state = $('#search-state').val(); 
                d.country = $('#search-state-country').val();
                
            }
        },
        language: {
            //processing: '<div style="display: none"></div>',
            // info: 'Menampilkan _START_ - _END_ dari _TOTAL_ ',
            //search: '<span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>',
            // zeroRecords: 'Tidak ada data yang cocok dengan kriteria pencarian',
            // emptyTable: 'Data tidak tersedia',
            // paginate: {
            //     first: '&laquo;',
            //     last: '&raquo;',
            //     next: '&rsaquo;',
            //     previous: '&lsaquo;'
            // },
            //lengthMenu: "Baris per halaman: _MENU_ "
        },
        //rowId: 'TRANSPORT_ID',
        columns: [
            {data: "DT_RowIndex", 
               sortable: false, 
               searchable: false,
               "render": function (data, type, row, meta) {      
                          return meta.row + meta.settings._iDisplayStart + 1;     
               }  
            },
            {data: "version", name: "version", visible:false},
            {data: "name", name: "name"},
            {data: "country.name", name: "country.name"},
           
            {data: "id", sortable: false, searchable: false, }
        ],
        columnDefs:[
            {
                "targets": 0, // your case first column
                "className": "text-center",
            },
            {
                "targets": 1, 
                "className": "text-center mleft",
                render: function(d,data,row) {
                    return '<span>'+d+'</span'
                }
            },
            {
                "targets": 2, 
                "className": "mleft",
                render: function(d,data,row) {
                    return '<span>'+d+'</span'
                }
            },
            {
                "targets": 3, 
                "className": "mleft",
                render: function(d,data,row) {
                    return '<span>'+d+'</span'
                }
            },
	        {
                targets: 4,
                "className": "text-center padd-row-table",
                render: function(d,data,row) {
                 
                    let v = row.version;
                  
                    return `<span>
                    <a href="state/`+d+`"  class="" data-id="`+d+`"><img class="pencil" src="`+baseUrl+`/assets/images/icon/pencil.png" /></a>&nbsp;
                    <a  class="btn-delete" data-id="`+d+`" data-version="`+v+`"><img class="trash" src="`+baseUrl+`/assets/images/icon/trash.png" /></a>
                    </span>`;
                    
                }
            }
        ]
        });
      
    }
   
    $('#state-country').select2();
    //$('#state-country').val().;
    var setCon = $('#country-opt-id').val();
    if(setCon != '')
    {
        $("#state-country").select2().val(setCon).trigger("change");
    }
    
    
    $('#btn-ti-searc-state').click(function() {
        dataTableState.draw(true);
    });

    // clear
    $('.clear_filter').click(function() {
       
        $('#search-state').val('');
        dataTableState.draw(true);
    });

    // Add 
    $('body').on('click', '#btn-add-state', function(){
        window.location.href = baseUrl + "/state-form";
    });

      /* filter tugel */
     // Toggle filter
     $('.filter .tugel').click(function() {
        $(this).toggleClass('open');
        $('.filter-title').toggleClass('hidden');
        $('.form-filter form').toggleClass('open-filter');
        
    });

    // clear
    $('.clear-search').click(function() {
        $('#search-state').val('');
        $('#search-state-country').val('');
        dataTableState.draw(true);
    });

    // Add New State or update
    $('#btn-submit').click(function(){

        // Update when user id has value
        var url = baseUrl + '/state/update';
        var action = "state";
        if(!$('#state-id').val()) {
            url = baseUrl + '/state/save';
            action = "save";
        }

        if($('#state-id').val()) {
            if(!$('#version-st').val()) {
                $.smallBox({
                    //height: 50,
                    title : "Error",
                    content : 'Version can\'t be empty',
                    color : "#dc3912",
                    sound_file: "smallbox",
                    timeout: 3000
                });
                $('#version-st').focus();
                return;
            }
        }

        // Has error
        // var hasError = false;
        // Check requirement input
        if(!$('#state-name').val()) {
            $.smallBox({
                //height: 50,
                title : "Error",
                content : 'State can\'t be empty',
                color : "#dc3912",
                sound_file: "smallbox",
                timeout: 3000
            });
            $('#state-name').focus();
            return;
        }
        if(!$('#state-country').val()) {
            $.smallBox({
                //height: 50,
                title : "Error",
                content : 'Country can\'t be empty',
                color : "#dc3912",
                sound_file: "smallbox",
                timeout: 3000
            });
            $('#state-country').focus();
            return;
        }

        // Show loder
        $('.page-loader').removeClass('hidden');

        // Send data
        $.ajax({
            url: url,
            type: 'POST',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: {
                'id': $('#state-id').val(),
                'name': $('#state-name').val(),
                'country': $('#state-country').val(),
                'version' : $('#version-st').val()
               
            },
            success: function(resp) {
               
                if(resp.responseCode == '0000') { //sukses
                    // Send success message
                    $.smallBox({
                        height: 50,
                        title : resp.responseStatus, 
                        content : resp.responseMessage,
                        color : "#109618",
                        sound_file: "voice_on",
                        timeout: 3000
                        //icon : "fa fa-bell swing animated"
                    });
                    // if($('#state-id').val()=="") {
                    //  	$('#state-country').val('');
                    //     $('#state-name').val('');                      
                    // }
                    window.location.href= baseUrl + "/state";
				
 
                } else if(resp.responseCode == '3333' || resp.responseCode == '0400'){ //exception and not found
					
					 $.smallBox({
                        height: 50,
                        title : resp.responseStatus,
                        content : resp.responseMessage,
                        color : "#dc3912",
                        sound_file: "voice_on",
                        timeout: 3000
                        //icon : "fa fa-bell swing animated"
                    });
                }
				else if(resp.responseCode == '5555')//validaator
				{
					
						 var data = Object.values(resp.responseMessage);
						 var ln = data.length;
						 var w  = 3000 * ln; 
						 data.map(function(d){
							 
								$.smallBox({
										height: 50,
										title : resp.responseStatus,
										content : d,
										color : "#dc3912",
										sound_file: "smallbox",
										timeout: w
										//icon : "fa fa-bell swing animated"
									});
						});
					
				}	
                // Hide loder
                $('.page-loader').addClass('hidden');
            },
            error: function(xhr, ajaxOptions, thrownError) {
                $.smallBox({
                    title : "Error",
                    content : xhr.statusText,
                    color : "#dc3912",
                    timeout: 3000
                    //icon : "fa fa-bell swing animated"
                });
                // Hide loder
                $('.page-loader').addClass('hidden');
            }
        });

    });

    // Edit state
    $('#dataTableState').on('click', '.btn-edit', function() {
        $('#modal-state').modal('show');
        $('#modal-state .modal-title').html('Edit State');
        //$('#email').attr('readonly','readonly');
        $('#divst-version').show();
       
        // Hide loder
        $('.page-loader').removeClass('hidden');

        // Get data
        // Send data
        $.ajax({
            url: baseUrl + '/state/' + $(this).data('id'),
            type: 'GET',
            success: function(resp) {
                if(resp.responseCode === 200) {
                    //console.log(resp.responseMessage[0].country.id);
                    $('#state-id').val(resp.responseMessage[0].id);
                    $('#state-name').val(resp.responseMessage[0].name);
                    $('#state-country').val(resp.responseMessage[0].country.id).trigger('change');
                    $('#state-country').focus();
                    $('#version-st').val(resp.responseMessage[0].version);

                }
                else
                {
                    $.smallBox({
                        height: 50,
                        title : "Error",
                        content : resp.responseMessage,
                        color : "#dc3912",
                        sound_file: "smallbox",
                        timeout: 3000
                        //icon : "fa fa-bell swing animated"
                    });
                }
               
                // Hide loder
                $('.page-loader').addClass('hidden');
            },
            error: function(xhr, ajaxOptions, thrownError) {
                $.smallBox({
                    title : "Error",
                    content : xhr.statusText,
                    color : "#dc3912",
                    timeout: 3000
                    //icon : "fa fa-bell swing animated"
                });
                // Hide loder
                $('.page-loader').addClass('hidden');
            }
        });
    });

    $('#dataTableState').on('click', '.btn-delete', function() {

        let text;
        if (confirm("Are you sure to remove!") == true) {
            
             // Send data
            $.ajax({
                url:  baseUrl + '/state/delete',
                type: 'POST',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data: {
                    'id': $(this).data('id'),
                    'version': $(this).data('version')
                },
                success: function(resp) {
                    if(resp.responseCode === 200) {
                        // Reload datatable
                        dataTableState.ajax.reload();
                        
                        // Send success message
                        $.smallBox({
                            height: 50,
                            title : "Success",
                            content : resp.responseMessage,
                            color : "#109618",
                            sound_file: "voice_on",
                            timeout: 3000
                            //icon : "fa fa-bell swing animated"
                        });
                    } else {
                        $.smallBox({
                            height: 50,
                            title : "Error",
                            content : resp.responseMessage,
                            color : "#dc3912",
                            sound_file: "smallbox",
                            timeout: 3000
                            //icon : "fa fa-bell swing animated"
                        });
                    }
                    // Hide loder
                    $('.page-loader').addClass('hidden');
                },
                error: function(xhr, ajaxOptions, thrownError) {
                    $.smallBox({
                        title : "Error",
                        content : xhr.statusText,
                        color : "#dc3912",
                        timeout: 3000
                        //icon : "fa fa-bell swing animated"
                    });
                    // Hide loder
                    $('.page-loader').addClass('hidden');
                }
            });

       
        } 

    });


    


})(jQuery);
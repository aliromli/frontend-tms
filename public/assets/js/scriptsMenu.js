(function($) {
    "use strict";
      
    // Activate Nestable
    $('#nestable').nestable({
        group : 1,
        maxDepth: 100,
        noDragClass:'btn-container'
    }).on('change', function() {
        $('#menu-list').val(JSON.stringify($('#nestable').nestable('serialize')));
    });

     // Add new item
     $('body').on('click', '#btn-add-menu-item', function(){
        $('#modal-menu-item').modal('show');
        $('#modal-menu-item .modal-title').html('New Menu Item');
        $('#modal-menu-item input[type=text]').val('');
        $('#menu-item-id').val('');
       
    });

    // Edit menu item
    $('#nestable').on('click', '.btn-edit', function() {
        $('#modal-menu-item').modal('show');
        $('#modal-menu-item .modal-title').html("New Menu Item");

        // Hide loder
        $('.page-loader').removeClass('hidden');

        // Get data
        // Send data
        $.ajax({
            url: baseUrl + '/menu/detail/' + $(this).data('id'),
            type: 'GET',
            success: function(resp) {
                $('#menu-item-id').val(resp.id);
                $('#menu-name').val(resp.name);
                $('#tooltip').val(resp.tooltip);
                $('#description').val(resp.description);
                $('#icon').val(resp.icon);
                $('#url').val(resp.action_url);
                $('#squence').val(resp.sequence);

                $('.checkbox label input').prop('checked', false);

                $.each(resp.actions, function(i, o) {
                    $('#action-'+(o.action_type).toLowerCase()).prop('checked', true);
                });

                // Hide loder
                $('.page-loader').addClass('hidden');
            },
            error: function(xhr, ajaxOptions, thrownError) {
                $.smallBox({
                    title : "Error",
                    content : xhr.statusText,
                    color : "#dc3912",
                    sound_file: "smallbox",
                    timeout: 3000
                    //icon : "fa fa-bell swing animated"
                });
                // Hide loder
                $('.page-loader').addClass('hidden');
            }
        });
    });

    // Delete menu item
    $('#nestable').on('click', '.btn-delete', function() {

        var id = $(this).data('id');

        if(!confirm('Are you sure want to delete this menu?')) {
            return;
        }

        // Get data
        // Send data
        $.ajax({
            url: baseUrl + '/menu/delete/' + $(this).data('id'),
            type: 'GET',
            success: function(resp) {
                if(resp.responseCode === 200) {
                    // Remove from menu
                    $('#nestable .dd-item[data-id="'+id+'"]').remove();
                    // Send success message
                    $.smallBox({
                        height: 50,
                        title : "Success",
                        content : resp.responseMessage,
                        color : "#109618",
                        sound_file: "voice_on",
                        timeout: 3000,
                        //icon : "fa fa-bell swing animated"
                    });
                } else {
                    $.smallBox({
                        title : "Error",
                        content : xhr.statusText,
                        color : "#dc3912",
                        sound_file: "smallbox",
                        timeout: 3000,
                        //icon : "fa fa-bell swing animated"
                    });
                }

            },
            error: function(xhr, ajaxOptions, thrownError) {
                $.smallBox({
                    title : "Error",
                    content : xhr.statusText,
                    color : "#dc3912",
                    timeout: 3000
                    //icon : "fa fa-bell swing animated"
                });
            }
        });
    });

  
    // Submit menu item
    $('#btn-submit-item').click(function(){

        // Update when menu item id has value
        var url = baseUrl + '/menu/update';
        var squence = $('#squence').val();
        var action = "update";
        if(!$('#menu-item-id').val()) {
            url = baseUrl + '/menu/save';
            squence = $('#total-child').html();
            action = "save";
        }

        // Check requirement input
        if(!$('#menu-name').val()) {
            $.smallBox({
                //height: 50,
                title : "Error",
                content : 'Name can\'t be empty',
                color : "#dc3912",
                sound_file: "smallbox",
                timeout: 3000
                //icon : "fa fa-bell swing animated"
            });

            return;
        }

        if(!$('#url').val()) {
            $.smallBox({
                //height: 50,
                title : "Error",
                content : 'URL can\'t be empty',
                color : "#dc3912",
                sound_file: "smallbox",
                timeout: 3000
                //icon : "fa fa-bell swing animated"
            });
            return;
        }

        // Show loder
        $('.page-loader').removeClass('hidden');
         
        var actions = [];
        $('.form-check input:checked').each(function(i,o) {
            actions.push($(o).val());
        });

        if(actions.length == 0) {
            $.smallBox({
                //height: 50,
                title : "Error",
                content : 'Actions not checked',
                color : "#dc3912",
                sound_file: "smallbox",
                timeout: 3000
                //icon : "fa fa-bell swing animated"
            });
            return;
        }



        // Send data
        $.ajax({
            url: url,
            type: 'POST',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: {
                'menuId': $('#menu-item-id').val(),
                'menuName': $('#menu-name').val(),
                'tooltip': $('#tooltip').val(),
                'description': $('#description').val(),
                'icon': $('#icon').val(),
                'url': $('#url').val(),
                'squence': squence,
                'actions': actions
            },
            success: function(resp) {
                if(resp.responseCode === 200) {
                    // Check menu icon
                    var icon = (resp.result.icon === null)?'':resp.result.icon;

                    // IF save added new item
                    if(action == "save") {
                        // Reload item
                        var item = `<li class="dd-item" data-id="`+resp.result.id+`">
                                        <div class="dd-handle">
                                            `+icon+` `+resp.result.name+`

                                            <!-- Button -->
                                            <div class="btn-container">
                                                <button class="btn btn-warning btn-xs btn-edit" data-id="`+resp.result.id+`"><i class="fa fa-fw fa-pencil"></i> Edit</button>
                                                <button class="btn btn-danger btn-xs btn-delete" data-id="`+resp.result.id+`"><i class="fa fa-fw fa-trash"></i> Delete</button>
                                            </div>
                                            <!-- /Button -->
                                        </div>
                                    </li>`;
                        // Append new item
                        $('ol.dd-list').append(item);


                        // Update total child
                        $('#total-child').html(parseInt($('#total-child').html()) + 1);
                    } else {
                        // Update item instead of add item
                        var updateItem = `<div class="dd-handle">
                                            `+icon+` `+resp.result.name+`

                                            <!-- Button -->
                                            <div class="btn-container">
                                                <button class="btn btn-warning btn-xs btn-edit" data-id="`+resp.result.id+`"><i class="fa fa-fw fa-pencil"></i> Edit</button>
                                                <button class="btn btn-danger btn-xs btn-delete" data-id="`+resp.result.id+`"><i class="fa fa-fw fa-trash"></i> Delete</button>
                                            </div>
                                            <!-- /Button -->
                                        </div>`;
                        $('#nestable .dd-item[data-id="'+resp.result.id+'"]').html(updateItem);
                    }

                    // Reset Form
                    $('#modal-menu-item input[type=text]').val('');
                    // Close modal
                    $('#modal-menu-item').modal('hide');
                    // Send success message
                    $.smallBox({
                        height: 50,
                        title : "Success",
                        content : resp.responseMessage,
                        color : "#109618",
                        sound_file: "voice_on",
                        timeout: 3000,
                        //icon : "fa fa-bell swing animated"
                    });
                    window.location.href=baseUrl+"/menu"
                } else {
                    $.smallBox({
                        height: 50,
                        title : "Error",
                        content : resp.responseMessage,
                        color : "#dc3912",
                        sound_file: "smallbox",
                        timeout: 3000,
                        //icon : "fa fa-bell swing animated"
                    });
                }
                // Hide loder
                $('.page-loader').addClass('hidden');
            },
            error: function(xhr, ajaxOptions, thrownError) {
                $.smallBox({
                    title : "Error",
                    content : xhr.statusText,
                    color : "#dc3912",
                    sound_file: "smallbox",
                    timeout: 3000,
                    //icon : "fa fa-bell swing animated"
                });
                // Hide loder
                $('.page-loader').addClass('hidden');
            }
        });
    });

    // Submit menu structure
    $('#btn-submit-struk').click(function(){

        if(!$('#menu-list').val()) {
            $.smallBox({
                //height: 50,
                title : "Error",
                content : 'You don\'t change menu structure',
                color : "#dc3912",
                sound_file: "smallbox",
                timeout: 3000,
                //icon : "fa fa-bell swing animated"
            });
            return;
        }

        // Show loder
        $('.page-loader').removeClass('hidden');

        // Send data
        $.ajax({
            url: baseUrl + '/menu/build',
            type: 'POST',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: {
                'menuList': $('#menu-list').val()
            },
            success: function(resp) {
                if(resp.responseCode === 200) {

                    $('#menu-list').val('');

                    // Send success message
                    $.smallBox({
                        height: 50,
                        title : "Success",
                        content : resp.responseMessage,
                        color : "#109618",
                        sound_file: "voice_on",
                        timeout: 3000
                        //icon : "fa fa-bell swing animated"
                    });
                } else {
                    $.smallBox({
                        height: 50,
                        title : "Error",
                        content : resp.responseMessage,
                        color : "#dc3912",
                        sound_file: "smallbox",
                        timeout: 3000,
                        //icon : "fa fa-bell swing animated"
                    });
                }
                // Hide loder
                $('.page-loader').addClass('hidden');
            },
            error: function(xhr, ajaxOptions, thrownError) {
                $.smallBox({
                    title : "Error",
                    content : xhr.statusText,
                    color : "#dc3912",
                    sound_file: "smallbox",
                    timeout: 3000,
                    //icon : "fa fa-bell swing animated"
                });
                // Hide loder
                $('.page-loader').addClass('hidden');
            }
        });
    });


})(jQuery);
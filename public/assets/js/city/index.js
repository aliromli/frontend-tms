(function($) {
    "use strict";

    $('#dataTableCity').wrap('<div class="dataTables_scroll" />');
    var dataTableCity = null;
    if ($('#dataTableCity').length) {
        // You can use 'alert' for alert message
        // or throw to 'throw' javascript error
        // or none to 'ignore' and hide error
        // or you own function
        // please read https://datatables.net/reference/event/error
        // for more information
        $.fn.dataTable.ext.errMode = 'none';
        
        dataTableCity =  $('#dataTableCity').DataTable({
        processing: true,
        serverSide: true,
        //dom : '',
        dom: 'lrtip',
        "searching": false,
        "lengthChange": true,
        pageLength: 10,
        lengthMenu: [[10, 25, 50, 100], [10, 25, 50, 100]],
        pagingType: 'full_numbers',
        ajax: {
            url:   baseUrl+"/city-datatables",
            type: 'GET',
            data:  function(d){
                d.city = $('#search-city').val();
                d.state = $('#search-city-state').val();
                
            }
        },
        language: {
            
            //processing: '<div style="display: none"></div>',
            // info: 'Menampilkan _START_ - _END_ dari _TOTAL_ ',
            //search: '<span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>',
            // zeroRecords: 'Tidak ada data yang cocok dengan kriteria pencarian',
            // emptyTable: 'Data tidak tersedia',
            // paginate: {
            //     first: '&laquo;',
            //     last: '&raquo;',
            //     next: '&rsaquo;',
            //     previous: '&lsaquo;'
            // },
            //lengthMenu: "Baris per halaman: _MENU_ "
        },
        //rowId: 'TRANSPORT_ID',
        columns: [
            {data: "DT_RowIndex", 
               sortable: false, 
               searchable: false,
               "render": function (data, type, row, meta) {      
                          return meta.row + meta.settings._iDisplayStart + 1;     
               }  
            },
           
            {data: "name", name: "name"},
            {data: "state.name", name: "state.name"},
            {data: "id", sortable: false, searchable: false, }
        ],
        columnDefs:[
            {
                "targets": 0, // your case first column
                "className": "text-center",
            },
            {
                "targets": 1, 
                "className": "mleft",
                render : function(d){
                    return '<span>'+d+'</span>';
                }
            },
	        {
                targets: 3,
                "className": "text-center padd-row-table",
                render: function(d,data,row) {
                 
                    let v = row.version;
                    return `
                    <span>
                    <a href="city/`+d+`" data-id="`+d+`"><img class="pencil" src="`+baseUrl+`/assets/images/icon/pencil.png" /></a>&nbsp;
                    <a class="btn-delete" data-id="`+d+`" data-version="`+v+`"><img class="trash" src="`+baseUrl+`/assets/images/icon/trash.png" /></a>
                    </span>
                    `;
                }
            }
        ]
        });
      
    }

    
    $('#btn-ti-search-city').click(function() {
        dataTableCity.draw(true);
    });

    // clear
    $('.clear_filter').click(function() {
        $('#search-city').val('');
        dataTableCity.draw(true);
    });

    // Add 
    $('body').on('click', '#btn-add-city', function(){
          window.location.href = baseUrl + "/city-form";
    });

      /* filter tugel */
     // Toggle filter
     $('.filter .tugel').click(function() {
        $(this).toggleClass('open');
        $('.filter-title').toggleClass('hidden');
        $('.form-filter form').toggleClass('open-filter');
        
    });

    // clear
    $('.clear-search').click(function() {
        $('#search-city').val('');
        $('#search-city-state').val('');
        dataTableCity.draw(true);
    });

    

    // Edit city
    $('#dataTableCity').on('click', '.btn-edit', function() {
        $('#modal-city').modal('show');
        $('#modal-city .modal-title').html('Edit City');
        //$('#email').attr('readonly','readonly');
        $('#divcity-version').show();
       
        // Hide loder
        $('.page-loader').removeClass('hidden');

        // Get data
        // Send data
        $.ajax({
            url: baseUrl + '/city/' + $(this).data('id'),
            type: 'GET',
            success: function(resp) {
                if(resp.responseCode === 200) {
                    //console.log(resp.responseMessage[0].country.id);
                    $('#city-id').val(resp.responseMessage[0].id);
                    $('#city-name').val(resp.responseMessage[0].name);
                    $('#city-state').val(resp.responseMessage[0].state.id).trigger('change');
                    $('#vity-state').focus();
                    $('#version-city').val(resp.responseMessage[0].version);

                }
                else
                {
                    $.smallBox({
                        height: 50,
                        title : "Error",
                        content : resp.responseMessage,
                        color : "#dc3912",
                        sound_file: "smallbox",
                        timeout: 3000
                        //icon : "fa fa-bell swing animated"
                    });
                }
               
                // Hide loder
                $('.page-loader').addClass('hidden');
            },
            error: function(xhr, ajaxOptions, thrownError) {
                $.smallBox({
                    title : "Error",
                    content : xhr.statusText,
                    color : "#dc3912",
                    timeout: 3000
                    //icon : "fa fa-bell swing animated"
                });
                // Hide loder
                $('.page-loader').addClass('hidden');
            }
        });
    });

    $('#dataTableCity').on('click', '.btn-delete', function() {

        let text;
        if (confirm("Are you sure to remove!") == true) {
            
             // Send data
            $.ajax({
                url:  baseUrl + '/city/delete',
                type: 'POST',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data: {
                    'id': $(this).data('id'),
                    'version': $(this).data('version')
                },
                success: function(resp) {
                    if(resp.responseCode === 200) {
                        // Reload datatable
                        dataTableCity.ajax.reload();
                        
                        // Send success message
                        $.smallBox({
                            height: 50,
                            title : "Success",
                            content : resp.responseMessage,
                            color : "#109618",
                            sound_file: "voice_on",
                            timeout: 3000
                            //icon : "fa fa-bell swing animated"
                        });
                    } else {
                        $.smallBox({
                            height: 50,
                            title : "Error",
                            content : resp.responseMessage,
                            color : "#dc3912",
                            sound_file: "smallbox",
                            timeout: 3000
                            //icon : "fa fa-bell swing animated"
                        });
                    }
                    // Hide loder
                    $('.page-loader').addClass('hidden');
                },
                error: function(xhr, ajaxOptions, thrownError) {
                    $.smallBox({
                        title : "Error",
                        content : xhr.statusText,
                        color : "#dc3912",
                        timeout: 3000
                        //icon : "fa fa-bell swing animated"
                    });
                    // Hide loder
                    $('.page-loader').addClass('hidden');
                }
            });

       
        } 

    });


    


})(jQuery);
(function($) {
    "use strict";

    $('#dataTableDeviceModel').wrap('<div class="dataTables_scroll" />');
    var dataTableDeviceModel = null;
    if ($('#dataTableDeviceModel').length) {
        // You can use 'alert' for alert message
        // or throw to 'throw' javascript error
        // or none to 'ignore' and hide error
        // or you own function
        // please read https://datatables.net/reference/event/error
        // for more information
        $.fn.dataTable.ext.errMode = 'none';
        
        dataTableDeviceModel =  $('#dataTableDeviceModel').DataTable({
        processing: true,
        serverSide: true,
        //dom : '',
        dom: 'lrtip',
        "searching": false,
        
        pageLength: 10,
        lengthMenu: [[10, 25, 50, 100], [10, 25, 50, 100]],
        pagingType: 'full_numbers',
        ajax: {
            url:   baseUrl+"/device-model-datatables",
            type: 'GET',
            data:  function(d){

                d.model = $('#search-device').val();
                d.vendorName = $('#search-vendor-name').val();
                d.vendorCountry = $('#search-vendor-country').val();
                
            }
        },
        language: {
          
        },
        //rowId: 'TRANSPORT_ID',
        columns: [
            {data: "DT_RowIndex", 
               sortable: false, 
               searchable: false,
               "render": function (data, type, row, meta) {      
                          return meta.row + meta.settings._iDisplayStart + 1;     
               }  
            },
            {data: "version", name: "version", visible:false},
            {data: "model", name: "model"},
            {data: "vendorName", name: "vendorName"},
            {data: "vendorCountry", name: "vendorCountry"},
            {data: "id", sortable: false, searchable: false, }
        ],
        columnDefs:[
            {
                "targets": 0, // your case first column
                "className": "text-center",
            },
            {
                "targets": 1,
                "className": "mleft",
                render: function(d,data,row) {
                    return `<span>`+d+`</span>`;
                }

            },
            {
                "targets": 2,
                "className": "mleft",
                render: function(d,data,row) {
                    return `<span>`+d+`</span>`;
                }

            },
            {
                "targets": 3,
                "className": "mleft",
                render: function(d,data,row) {
                    return `<span>`+d+`</span>`;
                }

            },
            {
                "targets": 4,
                "className": "mleft",
                render: function(d,data,row) {
                    return `<span>`+d+`</span>`;
                }

            },
          
	        {
                targets: 5,
                "className": "text-center padd-row-table",
                render: function(d,data,row) {
                 
                    let v = row.version;
                   
                    return `
                    <span>
                    <a href="device-model/`+d+`" data-id="`+d+`"><img class="pencil" src="`+baseUrl+`/assets/images/icon/pencil.png" /></a>&nbsp;
                    <a class="btn-delete" data-id="`+d+`" data-version="`+v+`"><img class="trash" src="`+baseUrl+`/assets/images/icon/trash.png" /></a>
                    </span>
                    `;
                }
            }
        ]
        });
      
    }
   
    
    $('#btn-ti-search-model-device').click(function() {
        dataTableDeviceModel.draw(true);
    });

    

      /* filter tugel */
     // Toggle filter
    //  $('.filter .tugel').click(function() {
    //     $(this).toggleClass('open');
    //     $('.filter-title').toggleClass('hidden');
    //     $('.form-filter form').toggleClass('open-filter');
        
    // });

    // clear
    // $('.clear-search').click(function() {
    //     $('#search-model').val('');
    //     $('#search-vendor-name').val('');
    //     $('#search-vendor-country').val('');
    //     dataTableDeviceModel.draw(true);
    // });

    // clear
    $('.clear_filter').click(function() {
        $('#search-device').val('');
        dataTableDeviceModel.draw(true);
    });

    // Add New or update
    $('#btn-submit-dm').click(function(){

        // Update when city id has value
        var url = baseUrl + '/device-model/update';
        var action = "device-model";
        if(!$('#dm-id').val()) {
            url = baseUrl + '/device-model/save';
            action = "save";
        }

        if($('#dm-id').val()) {
            if(!$('#version-dm').val()) {
                $.smallBox({
                    //height: 50,
                    title : "Error",
                    content : 'Merchant can\'t be empty',
                    color : "#dc3912",
                    sound_file: "smallbox",
                    timeout: 3000
                });
                $('#version-dm').focus();
                return;
            }
        }

        // Has error
        // var hasError = false;
        // Check requirement input
        if(!$('#dm-modelInformation').val()) {
            $.smallBox({
                //height: 50,
                title : "Error",
                content : 'Model Information Name can\'t be empty',
                color : "#dc3912",
                sound_file: "smallbox",
                timeout: 3000
            });
            $('#dm-modelInformation').focus();
            return;
        }

        if(!$('#dm-model').val()) {
            $.smallBox({
                //height: 50,
                title : "Error",
                content : 'Model can\'t be empty',
                color : "#dc3912",
                sound_file: "smallbox",
                timeout: 3000
            });
            $('#dm-model').focus();
            return;
        }
        if(!$('#dm-vendorName').val()) {
            $.smallBox({
                //height: 50,
                title : "Error",
                content : 'Vendor name can\'t be empty',
                color : "#dc3912",
                sound_file: "smallbox",
                timeout: 3000
            });
            $('#dm-vendorName').focus();
            return;
        }

        if(!$('#dm-vendorCountry').val()) {
            $.smallBox({
                //height: 50,
                title : "Error",
                content : 'Vendor Country can\'t be empty',
                color : "#dc3912",
                sound_file: "smallbox",
                timeout: 3000
            });
            $('#dm-vendorCountry').focus();
            return;
        }
        // Show loder
        $('.page-loader').removeClass('hidden');
        // Send data
        $.ajax({
            url: url,
            type: 'POST',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: {
                'id': $('#dm-id').val(),
                'modelInformation': $('#dm-modelInformation').val(),
                'model': $('#dm-model').val(),
                'vendorName' : $('#dm-vendorName').val(),
                'vendorCountry' : $('#dm-vendorCountry').val(),
                'version' : $('#version-dm').val()

            },
            success: function(resp) {
                
                if(resp.responseCode == '0000') { //sukses
                    // Send success message
                    $.smallBox({
                        height: 50,
                        title : resp.responseStatus, 
                        content : resp.responseMessage,
                        color : "#109618",
                        sound_file: "voice_on",
                        timeout: 3000
                        //icon : "fa fa-bell swing animated"
                    });
                        //  if($('#dm-id').val()=="") {
                       
                        //     $('#dm-modelInformation').val("");
                        //     $("#dm-model").val("");
                        //     $("#dm-vendorName").val("");
                        //     $("#dm-vendorCountry").val("");
                        //     $("#version-dm").val("");
                        // }
                        // else
                        //{
                            window.location.href=baseUrl + "/device-model";
                        //}
				
 
                } else if(resp.responseCode == '3333' || resp.responseCode == '0400'){ //exception and not found
					
					 $.smallBox({
                        height: 50,
                        title : resp.responseStatus,
                        content : resp.responseMessage,
                        color : "#dc3912",
                        sound_file: "voice_on",
                        timeout: 3000
                        //icon : "fa fa-bell swing animated"
                    });
                }
				else if(resp.responseCode == '5555')//validaator
				{
					
						 var data = Object.values(resp.responseMessage);
						 var ln = data.length;
						 var w  = 3000 * ln; 
						 data.map(function(d){
							 
								$.smallBox({
										height: 50,
										title : resp.responseStatus,
										content : d,
										color : "#dc3912",
										sound_file: "smallbox",
										timeout: w
										//icon : "fa fa-bell swing animated"
									});
						});
					
				}	
                // Hide loder
                $('.page-loader').addClass('hidden');
            },
            error: function(xhr, ajaxOptions, thrownError) {
                $.smallBox({
                    title : "Error",
                    content : xhr.statusText,
                    color : "#dc3912",
                    timeout: 3000
                    //icon : "fa fa-bell swing animated"
                });
                // Hide loder
                $('.page-loader').addClass('hidden');
            }
        });

    });

    // Edit 
    //$('#dataTableMerchant').on('click', '.btn-edit', function() {
        //$('#modal-city').modal('show');
        //$('#modal-city .modal-title').html('Edit City');
        //$('#email').attr('readonly','readonly');
        //$('#div-m-version').show();
       
        // Hide loder
        // $('.page-loader').removeClass('hidden');

        // // Get data
        // // Send data
        // $.ajax({
        //     url: baseUrl + '/merchant/' + $(this).data('id'),
        //     type: 'GET',
        //     success: function(resp) {
        //         if(resp.responseCode === 200) {
        //             //console.log(resp.responseMessage[0].country.id);

        //             $('#merchant-id').val(resp.responseMessage[0].id);
        //             $('#merchant-name').val(resp.responseMessage[0].name);
        //             $('#merchant-company').val(resp.responseMessage[0].company);
        //             $('#merchant-address').val(resp.responseMessage[0].address);
        //             $('#merchant-district').val(resp.responseMessage[0].state.id).trigger('change');
        //             $('#merchant-district').focus();
        //             $('#merchant-merchantType').val(resp.responseMessage[0].state.id).trigger('change');
        //             $('#merchant-merchantType').focus();
        //             $('#version-zipcode').val(resp.responseMessage[0].zipcode);

        //         }
        //         else
        //         {
        //             $.smallBox({
        //                 height: 50,
        //                 title : "Error",
        //                 content : resp.responseMessage,
        //                 color : "#dc3912",
        //                 sound_file: "smallbox",
        //                 timeout: 3000
        //                 //icon : "fa fa-bell swing animated"
        //             });
        //         }
               
        //         // Hide loder
        //         $('.page-loader').addClass('hidden');
        //     },
        //     error: function(xhr, ajaxOptions, thrownError) {
        //         $.smallBox({
        //             title : "Error",
        //             content : xhr.statusText,
        //             color : "#dc3912",
        //             timeout: 3000
        //             //icon : "fa fa-bell swing animated"
        //         });
        //         // Hide loder
        //         $('.page-loader').addClass('hidden');
        //     }
        // });
   // });

    $('#dataTableDeviceModel').on('click', '.btn-delete', function() {

        let text;
        if (confirm("Are you sure to remove!") == true) {
            
             // Send data
            $.ajax({
                url:  baseUrl + '/device-model/delete',
                type: 'POST',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data: {
                    'id': $(this).data('id'),
                    'version': $(this).data('version')
                },
                success: function(resp) {
                    if(resp.responseCode === 200) {
                        // Reload datatable
                        dataTableDeviceModel.ajax.reload();
                        
                        // Send success message
                        $.smallBox({
                            height: 50,
                            title : "Success",
                            content : resp.responseMessage,
                            color : "#109618",
                            sound_file: "voice_on",
                            timeout: 3000
                            //icon : "fa fa-bell swing animated"
                        });
                    } else {
                        $.smallBox({
                            height: 50,
                            title : "Error",
                            content : resp.responseMessage,
                            color : "#dc3912",
                            sound_file: "smallbox",
                            timeout: 3000
                            //icon : "fa fa-bell swing animated"
                        });
                    }
                    // Hide loder
                    $('.page-loader').addClass('hidden');
                },
                error: function(xhr, ajaxOptions, thrownError) {
                    $.smallBox({
                        title : "Error",
                        content : xhr.statusText,
                        color : "#dc3912",
                        timeout: 3000
                        //icon : "fa fa-bell swing animated"
                    });
                    // Hide loder
                    $('.page-loader').addClass('hidden');
                }
            });

       
        } 

    });


    


})(jQuery);
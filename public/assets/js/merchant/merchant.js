(function($) {
    "use strict";

   
	
	//var setCon = $('#merchant-id').val();
	var disId = $('#disId-opt').val();
	var mtId = $('#mtId-opt').val();
	$('#merchant-district').select2();
	$('#merchant-merchantType').select2();
    if(disId != '')
    {
        $("#merchant-district").select2().val(disId).trigger("change");
       
    }
	if(mtId != '')
	{
		 $("#merchant-merchantType").select2().val(mtId).trigger("change");
	}
	
	

    // Add New City or update
    $('#btn-submit-merchant').click(function(){

        // Update when city id has value
        var url = baseUrl + '/merchant/update';
        var action = "merchant";
        if(!$('#merchant-id').val()) {
            url = baseUrl + '/merchant/save';
            action = "save";
        }

        if($('#merchant-id').val()) {
            if(!$('#version-m').val()) {
                $.smallBox({
                    //height: 50,
                    title : "Error",
                    content : 'Merchant can\'t be empty',
                    color : "#dc3912",
                    sound_file: "smallbox",
                    timeout: 3000
                });
                $('#version-m').focus();
                return;
            }
        }

        // Has error
        // var hasError = false;
        // Check requirement input
        if(!$('#merchant-name').val()) {
            $.smallBox({
                //height: 50,
                title : "Error",
                content : 'Marchant Name can\'t be empty',
                color : "#dc3912",
                sound_file: "smallbox",
                timeout: 3000
            });
            $('#merchant-company').focus();
            return;
        }

        if(!$('#merchant-company').val()) {
            $.smallBox({
                //height: 50,
                title : "Error",
                content : 'District can\'t be empty',
                color : "#dc3912",
                sound_file: "smallbox",
                timeout: 3000
            });
            $('#merchant-company').focus();
            return;
        }
        if(!$('#merchant-address').val()) {
            $.smallBox({
                //height: 50,
                title : "Error",
                content : 'District can\'t be empty',
                color : "#dc3912",
                sound_file: "smallbox",
                timeout: 3000
            });
            $('#merchant-address').focus();
            return;
        }

        if(!$('#merchant-district').val()) {
            $.smallBox({
                //height: 50,
                title : "Error",
                content : 'District can\'t be empty',
                color : "#dc3912",
                sound_file: "smallbox",
                timeout: 3000
            });
            $('#merchant-district').focus();
            return;
        }
        if(!$('#merchant-zipcode').val()) {
            $.smallBox({
                //height: 50,
                title : "Error",
                content : 'Zipcode can\'t be empty',
                color : "#dc3912",
                sound_file: "smallbox",
                timeout: 3000
            });
            $('#merchant-zipcode').focus();
            return;
        }

        if(!$('#merchant-merchantType').val()) {
            $.smallBox({
                //height: 50,
                title : "Error",
                content : 'Merchant Type can\'t be empty',
                color : "#dc3912",
                sound_file: "smallbox",
                timeout: 3000
            });
            $('#merchant-merchantType').focus();
            return;
        }


        
        // Show loder
        $('.page-loader').removeClass('hidden');

        // Send data
        $.ajax({
            url: url,
            type: 'POST',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: {
                'id': $('#merchant-id').val(),
                'name': $('#merchant-name').val(),
                'companyName': $('#merchant-company').val(),
                'address' : $('#merchant-address').val(),
                'districtId' : $('#merchant-district').val(),
                'zipcode' : $('#merchant-zipcode').val(),
                'merchantTypeId' : $('#merchant-merchantType').val(),
                'version': $('#version-m').val(),

            },
            success: function(resp) {
               
				if(resp.responseCode == '0000') { //sukses
						// Send success message
						$.smallBox({
							height: 50,
							title : resp.responseStatus, 
							content : resp.responseMessage,
							color : "#109618",
							sound_file: "voice_on",
							timeout: 3000
							//icon : "fa fa-bell swing animated"
						});
						// if($('#merchant-id').val()=="") {
						// 	$('#merchant-name').val('');                      
						// 	$('#merchant-company').val('');                      
						// 	$('#merchant-address').val('');                      
						// 	$('#merchant-district').val('');                      
						// 	$('#merchant-zipcode').val('');                      
						// 	$('#merchant-merchantType').val('');                      
						// }
						// else
						//{
							window.location.href= baseUrl+"/merchant";
                            //window.history.back();
                            
						//}
					
	 
					} else if(resp.responseCode == '3333' || resp.responseCode == '0400'){ //exception and not found
						
						 $.smallBox({
							height: 50,
							title : resp.responseStatus,
							content : resp.responseMessage,
							color : "#dc3912",
							sound_file: "voice_on",
							timeout: 3000
							//icon : "fa fa-bell swing animated"
						});
					}
					else if(resp.responseCode == '5555')//validaator
					{
						
							 var data = Object.values(resp.responseMessage);
							 var ln = data.length;
							 var w  = 3000 * ln; 
							 data.map(function(d){
								 
									$.smallBox({
											height: 50,
											title : resp.responseStatus,
											content : d,
											color : "#dc3912",
											sound_file: "smallbox",
											timeout: w
											//icon : "fa fa-bell swing animated"
										});
							});
						
					}	
                // Hide loder
                $('.page-loader').addClass('hidden');
            },
            error: function(xhr, ajaxOptions, thrownError) {
                $.smallBox({
                    title : "Error",
                    content : xhr.statusText,
                    color : "#dc3912",
                    timeout: 3000
                    //icon : "fa fa-bell swing animated"
                });
                // Hide loder
                $('.page-loader').addClass('hidden');
            }
        });

    });


})(jQuery);
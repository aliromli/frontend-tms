(function($) {
    "use strict";

    $('#dataTablemerchantType').wrap('<div class="dataTables_scroll" />');
    var dataTablemerchantType = null;
    if ($('#dataTablemerchantType').length) {
        // You can use 'alert' for alert message
        // or throw to 'throw' javascript error
        // or none to 'ignore' and hide error
        // or you own function
        // please read https://datatables.net/reference/event/error
        // for more information
        $.fn.dataTable.ext.errMode = 'none';
       
        dataTablemerchantType =  $('#dataTablemerchantType').DataTable({
        processing: true,
        serverSide: true,
        dom: 'lrtip',
        "lengthChange": false,
        pageLength: 15,
        lengthMenu: [[10, 25, 50, 100], [10, 25, 50, 100]],
        pagingType: 'full_numbers',
        ajax: {
            url:   baseUrl+"/merchanttype-datatables",
            type: 'GET',
            data:  function(d){
                d.name = $('#search-merchantType').val();
               
            }
        },
        language: {
            //processing: '<div style="display: none"></div>',
            // info: 'Menampilkan _START_ - _END_ dari _TOTAL_ ',
            //search: '<span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>',
            // zeroRecords: 'Tidak ada data yang cocok dengan kriteria pencarian',
            // emptyTable: 'Data tidak tersedia',
            // paginate: {
            //     first: '&laquo;',
            //     last: '&raquo;',
            //     next: '&rsaquo;',
            //     previous: '&lsaquo;'
            // },
            //lengthMenu: "Baris per halaman: _MENU_ "
        },
        //rowId: 'TRANSPORT_ID',
        columns: [
            {data: "DT_RowIndex", 
               sortable: false, 
               searchable: false,
               "render": function (data, type, row, meta) {      
                          return meta.row + meta.settings._iDisplayStart + 1;     
               }  
            },
            {data: "version", name: "version", visible:false},
            {data: "name", name: "name"},
            {data: "description", name: "description"},
            {data: "id", sortable: false, searchable: false, }
        ],
        columnDefs:[
			{
                "targets": 0, 
                "className": "text-center",
            },{
                "targets": 1, 
                "className": "text-center",
            },{
			 
                "targets": 2, //
                "className": "mleft",
                render : function(d,data,row){
                    return '<span>'+d+'</span>';
                }
            },{
			 
                "targets": 3, //
                "className": "mleft",
                render : function(d,data,row){
                    return '<span>'+d+'</span>';
                }
            },{
                targets: 4,
                "className": "text-center padd-row-table",
                render: function(d,data,row) {
                 
                    let v = row.version;

                    return `<span>
                    <a href="merchanttype/`+d+`"  class="" data-id="`+d+`"><img class="pencil" src="`+baseUrl+`/assets/images/icon/pencil.png" /></a>&nbsp;
                    <a  class="btn-delete" data-id="`+d+`" data-version="`+v+`"><img class="trash" src="`+baseUrl+`/assets/images/icon/trash.png" /></a>
                    </span>`;
                  
                }
            }
        ]
        });
      
    }

    
    $('#btn-ti-search-merchant-type').click(function() {
        dataTablemerchantType.draw(true);
    });

    // Add 
    $('body').on('click', '#btn-add-merchantType', function(){
     	window.location.href = baseUrl + "/merchanttype-form";
    });

      /* filter tugel */
     // Toggle filter
     $('.filter .tugel').click(function() {
        $(this).toggleClass('open');
        $('.filter-title').toggleClass('hidden');
        $('.form-filter form').toggleClass('open-filter');
        
    });

    // clear
    $('.clear_filter').click(function() {
        $('#search-merchantType').val(''); 
       
        dataTablemerchantType.draw(true);
    });

    

     // Add New Or Update Country
     $('#btn-submit').click(function(){

        // Update when user id has value
        var url = baseUrl + '/merchanttype/update';
        var action = "update";
        if(!$('#merchantType-id').val()) {
            url = baseUrl + '/merchanttype/save';
            action = "save";
        }

        if($('#merchantType-id').val()) {
            if(!$('#version-mt').val()) {
                $.smallBox({
                    //height: 50,
                    title : "Error",
                    content : 'Version can\'t be empty',
                    color : "#dc3912",
                    sound_file: "smallbox",
                    timeout: 3000
                });
                $('#version-mt').focus();
                return;
            }
        }

        // Has error
        // var hasError = false;
        // Check requirement input
        if(!$('#merchantType-name').val()) {
            $.smallBox({
                //height: 50,
                title : "Error",
                content : 'MerchantType can\'t be empty',
                color : "#dc3912",
                sound_file: "smallbox",
                timeout: 3000
            });
            $('#merchantType-name').focus();
            return;
        }
        if(!$('#merchantType-description').val()) {
            $.smallBox({
                //height: 50,
                title : "Error",
                content : 'Description can\'t be empty',
                color : "#dc3912",
                sound_file: "smallbox",
                timeout: 3000
            });
            $('#merchantType-description').focus();
            return;
        }

       

        // Show loder
        $('.page-loader').removeClass('hidden');

        // Send data
        $.ajax({
            url: url,
            type: 'POST',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: {
                'id': $('#merchantType-id').val(),
                'description': $('#merchantType-description').val(),
                'name': $('#merchantType-name').val(),
                'version' : $('#version-mt').val()
               
            },
            success: function(resp) {
               
				if(resp.responseCode == '0000') { //sukses
						// Send success message
						$.smallBox({
							height: 50,
							title : resp.responseStatus, 
							content : resp.responseMessage,
							color : "#109618",
							sound_file: "voice_on",
							timeout: 3000
							//icon : "fa fa-bell swing animated"
						});
						//if($('#merchantType-id').val()=="") {
						//	$('#merchantType-name').val('');                      
						//	$('#merchantType-description').val('');                      
						//}
						//else
						//{
							window.location.href= baseUrl+"/merchanttype";
						//}
					
	 
					} else if(resp.responseCode == '3333' || resp.responseCode == '0400'){ //exception and not found
						
						 $.smallBox({
							height: 50,
							title : resp.responseStatus,
							content : resp.responseMessage,
							color : "#dc3912",
							sound_file: "voice_on",
							timeout: 3000
							//icon : "fa fa-bell swing animated"
						});
					}
					else if(resp.responseCode == '5555')//validaator
					{
						
							 var data = Object.values(resp.responseMessage);
							 var ln = data.length;
							 var w  = 3000 * ln; 
							 data.map(function(d){
								 
									$.smallBox({
											height: 50,
											title : resp.responseStatus,
											content : d,
											color : "#dc3912",
											sound_file: "smallbox",
											timeout: w
											//icon : "fa fa-bell swing animated"
										});
							});
						
					}	
                // Hide loder
                $('.page-loader').addClass('hidden');
            },
            error: function(xhr, ajaxOptions, thrownError) {
                $.smallBox({
                    title : "Error",
                    content : xhr.statusText,
                    color : "#dc3912",
                    timeout: 3000
                    //icon : "fa fa-bell swing animated"
                });
                // Hide loder
                $('.page-loader').addClass('hidden');
            }
        });

    });
	
    // Edit merchantType
    $('#dataTablemerchantType').on('click', '.btn-edit', function() {
        $('#modal-merchantType').modal('show');
        $('#modal-merchantType .modal-title').html('Edit MerchantType');
        //$('#merchantType-name').attr('readonly','readonly');
        $('#divmerType-version').show();
       
        // Hide loder
        $('.page-loader').removeClass('hidden');

        // Get data
        // Send data
        $.ajax({
            url: baseUrl + '/merchanttype/' + $(this).data('id'),
            type: 'GET',
            success: function(resp) {
                if(resp.responseCode === 200) {
                   
                    $('#merchantType-id').val(resp.responseMessage[0].id);
                    $('#merchantType-description').val(resp.responseMessage[0].description);
                    $('#merchantType-name').val(resp.responseMessage[0].name);
                    $('#version-mt').val(resp.responseMessage[0].version);

                }
                else
                {
                    $.smallBox({
                        height: 50,
                        title : "Error",
                        content : resp.responseMessage,
                        color : "#dc3912",
                        sound_file: "smallbox",
                        timeout: 3000
                        //icon : "fa fa-bell swing animated"
                    });
                }
               
                // Hide loder
                $('.page-loader').addClass('hidden');
            },
            error: function(xhr, ajaxOptions, thrownError) {
                $.smallBox({
                    title : "Error",
                    content : xhr.statusText,
                    color : "#dc3912",
                    timeout: 3000
                    //icon : "fa fa-bell swing animated"
                });
                // Hide loder
                $('.page-loader').addClass('hidden');
            }
        });
    });

    $('#dataTablemerchantType').on('click', '.btn-delete', function() {

        let text;
        if (confirm("Are you sure to remove!") == true) {
            
             // Send data
            $.ajax({
                url:  baseUrl + '/merchanttype/delete',
                type: 'POST',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data: {
                    'id': $(this).data('id'),
                    'version': $(this).data('version')
                },
                success: function(resp) {
					
					
                    if(resp.responseCode === 200) {
                        // Reload datatable
                        dataTablemerchantType.ajax.reload();
                        
                        // Send success message
                        $.smallBox({
                            height: 50,
                            title : "Success",
                            content : resp.responseMessage,
                            color : "#109618",
                            sound_file: "voice_on",
                            timeout: 3000
                            //icon : "fa fa-bell swing animated"
                        });
                    } else {
                        $.smallBox({
                            height: 50,
                            title : "Error",
                            content : resp.responseMessage,
                            color : "#dc3912",
                            sound_file: "smallbox",
                            timeout: 3000
                            //icon : "fa fa-bell swing animated"
                        });
                    }
                    // Hide loder
                    $('.page-loader').addClass('hidden');
                },
                error: function(xhr, ajaxOptions, thrownError) {
                    $.smallBox({
                        title : "Error",
                        content : xhr.statusText,
                        color : "#dc3912",
                        timeout: 3000
                        //icon : "fa fa-bell swing animated"
                    });
                    // Hide loder
                    $('.page-loader').addClass('hidden');
                }
            });

       
        } 

    });
    


})(jQuery);
(function($) {
    "use strict";
    $('#dataTableRC').wrap('<div class="dataTables_scroll" />');
    var dataTableRC = null;
    if ($('#dataTableRC').length) {
        // You can use 'alert' for alert message
        // or throw to 'throw' javascript error
        // or none to 'ignore' and hide error
        // or you own function
        // please read https://datatables.net/reference/event/error
        // for more information
        $.fn.dataTable.ext.errMode = 'none';
        
        dataTableRC =  $('#dataTableRC').DataTable({
        processing: true,
        serverSide: true,
        //dom : '',
        dom: 'lrtip',
        "searching": false,
        //"scrollX": true,
        //"sScrollX": '100%',
        //"sScrollXInner": "110%",
        pageLength: 10,
        lengthMenu: [[10, 25, 50, 100], [10, 25, 50, 100]],
        pagingType: 'full_numbers',
        ajax: {
            url:   baseUrl+"/response-code-datatable",
            type: 'GET',
            data:  function(d){
                d.code= $('#search-rc-name').val();
                
            }
        },
        language: {
            
        },
        //rowId: 'TRANSPORT_ID',
        columns: [
            {data: "DT_RowIndex", 
               sortable: false, 
               searchable: false,
               "render": function (data, type, row, meta) {      
                          return meta.row + meta.settings._iDisplayStart + 1;     
               }  
            },
            {data: "version", name: "version", visible: false},
            {data: "type", name: "type"},
            {data: "code", name: "code"}, 
            {data: "description", name: "description"}, 
            {data: "id", sortable: false, searchable: false, }
        ],
		
        columnDefs:[
			{
                "targets": 0, // your case first column
                "className": "text-center",
            },
			{
                "targets": 2, 
                "className": "mleft",
				 render: function(d,data,row) {
					 return `<span>`+d+`</span>`;
				 }
            },
			{
                "targets": 4, 
                "className": "mleft",
				 render: function(d,data,row) {
					 return `<span>`+d+`</span>`;
				 }
            },
			
	        {
                targets: 5,
				"className": "text-center padd-row-table",
                render: function(d,data,row) {
                 
                    let v = row.version;
                    let id = d;
					return `
						<span>
							<a href="response-code/`+d+`" data-id="`+d+`"><img class="pencil" src="`+baseUrl+`/assets/images/icon/pencil.png" /></a>&nbsp;
							<a class="btn-delete" data-id="`+d+`" data-version="`+v+`"><img class="trash" src="`+baseUrl+`/assets/images/icon/trash.png" /></a>
						</span>
                    `;
                   
                }
            }
        ]
        });
      
    }
	
	
    $('#btn-search-rc-name').click(function() {
        dataTableRC.draw(true);
    });

      /* filter tugel */
     // Toggle filter
    //  $('.filter .tugel').click(function() {
    //     $(this).toggleClass('open');
    //     $('.filter-title').toggleClass('hidden');
    //     $('.form-filter form').toggleClass('open-filter');
        
    // });

    // clear
    $('.clear_filter').click(function() {
        $('#search-rc-name').val('');
       
        dataTableRC.draw(true);
    });

    // Add New  or update
    $('#btn-submit-rc').click(function(){

        // Update when city id has value
        var url = baseUrl + '/response-code/update';
        var action = "update";
        if(!$('#rc-id').val()) {
            url = baseUrl + '/response-code/save';
            action = "save";
        }

        if($('#rc-id').val()) {
            if(!$('#version-rc').val()) {
                $.smallBox({
                    //height: 50,
                    title : "Error",
                    content : 'Version can\'t be empty',
                    color : "#dc3912",
                    sound_file: "smallbox",
                    timeout: 3000
                });
                $('#version-rc').focus();
                return;
            }
        }
   
        // Has error
        // var hasError = false;
        // Check requirement input
        if(!$('#tt').val()) {
            $.smallBox({
                //height: 50,
                title : "Error",
                content : 'Type can\'t be empty',
                color : "#dc3912",
                sound_file: "smallbox",
                timeout: 3000
            });
            $('#tt').focus();
            return;
        }

        if(!$('#rc-RC').val()) {
            $.smallBox({
                //height: 50,
                title : "Error",
                content : 'Response Code can\'t be empty',
                color : "#dc3912",
                sound_file: "smallbox",
                timeout: 3000
            });
            $('#rc-RC').focus();
            return;
        }

        if(!$('#description-rc').val()) {
            $.smallBox({
                //height: 50,
                title : "Error",
                content : 'Message can\'t be empty',
                color : "#dc3912",
                sound_file: "smallbox",
                timeout: 3000
            });
            $('#description-rc').focus();
            return;
        }
		
		// Show loder
        $('.page-loader').removeClass('hidden');
        $('.loading>img').removeClass('hidden');
        // Send data
        $.ajax({
            url: url,
            type: 'POST',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: {
                'id': $('#rc-id').val(),
                'code' : $('#rc-RC').val(),
                'type' : $('#tt').val(),
                'version': $('#version-rc').val(),
                'description': $('#description-rc').val(),
            },
		    success: function(resp) {
		        if(resp.responseCode == '0000') { //sukses
                    // Send success message
                    $.smallBox({
                        height: 50,
                        title : resp.responseStatus, 
                        content : resp.responseMessage,
                        color : "#109618",
                        sound_file: "voice_on",
                        timeout: 3000
                        //icon : "fa fa-bell swing animated"
                    });
					window.location.replace(baseUrl +"/response-code");

                    // if($('#rc-id').val()=="") {
                       
					// 	$('#is-name').val('');
                    //     $('#is-issuerId').val('');
					// 	$('#is-acquirerId').val('');
					// 	$("input:radio").removeAttr("checked");
						
                    // }

 
                } else if(resp.responseCode == '3333' || resp.responseCode == '0400'){ //exception and not found
					
					 $.smallBox({
                        height: 50,
                        title : resp.responseStatus,
                        content : resp.responseMessage,
                        color : "#dc3912",
                        sound_file: "voice_on",
                        timeout: 3000
                        //icon : "fa fa-bell swing animated"
                    });
                }
				else if(resp.responseCode == '5555')//validaator
				{
					
						 var data = Object.values(resp.responseMessage);
						 var ln = data.length;
						 var w  = 3000 * ln; 
						 data.map(function(d){
							 
								$.smallBox({
										height: 50,
										title : resp.responseStatus,
										content : d,
										color : "#dc3912",
										sound_file: "smallbox",
										timeout: w
										//icon : "fa fa-bell swing animated"
									});
						});
					
				}	
                // Hide loder
                $('.page-loader').addClass('hidden');
                $('.loading>img').addClass('hidden');
            },
            error: function(xhr, ajaxOptions, thrownError) {
                $.smallBox({
                    title : "Error",
                    content : xhr.statusText,
                    color : "#dc3912",
                    timeout: 3000
                    //icon : "fa fa-bell swing animated"
                });
                // Hide loder
                $('.page-loader').addClass('hidden');
                $('.loading>img').addClass('hidden');
            }
        });

    });

    // Edit 
    $('#dataTableRC').on('click', '.btn-delete', function() {

        let text;
        if (confirm("Are you sure to remove!") == true) {
            
             // Send data
            $.ajax({
                url:  baseUrl + '/response-code/delete',
                type: 'POST',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data: {
                    'id': $(this).data('id'),
                    'version': $(this).data('version')
                },
                success: function(resp) {
                    if(resp.responseCode === 200) {
                        // Reload datatable
                        dataTableRC.ajax.reload();
                        
                        // Send success message
                        $.smallBox({
                            height: 50,
                            title : "Success",
                            content : resp.responseMessage,
                            color : "#109618",
                            sound_file: "voice_on",
                            timeout: 3000
                            //icon : "fa fa-bell swing animated"
                        });
                    } else {
                        $.smallBox({
                            height: 50,
                            title : "Error",
                            content : resp.responseMessage,
                            color : "#dc3912",
                            sound_file: "smallbox",
                            timeout: 3000
                            //icon : "fa fa-bell swing animated"
                        });
                    }
                    // Hide loder
                    $('.page-loader').addClass('hidden');
                },
                error: function(xhr, ajaxOptions, thrownError) {
                    $.smallBox({
                        title : "Error",
                        content : xhr.statusText,
                        color : "#dc3912",
                        timeout: 3000
                        //icon : "fa fa-bell swing animated"
                    });
                    // Hide loder
                    $('.page-loader').addClass('hidden');
                }
            });

       
        } 

    });


 

})(jQuery);
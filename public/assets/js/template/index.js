(function($) {
    "use strict";
    $('#dataTableTT').wrap('<div class="dataTables_scroll" />');
    var dataTableTT = null;
    if ($('#dataTableTT').length) {
        // You can use 'alert' for alert message
        // or throw to 'throw' javascript error
        // or none to 'ignore' and hide error
        // or you own function
        // please read https://datatables.net/reference/event/error
        // for more information
        $.fn.dataTable.ext.errMode = 'none';
        
        dataTableTT =  $('#dataTableTT').DataTable({
        processing: true,
        serverSide: true,
        //dom : '',
        dom: 'lrtip',
        "searching": false,
        //"scrollX": true,
        //"sScrollX": '100%',
        //"sScrollXInner": "110%",
        pageLength: 10,
        lengthMenu: [[10, 25, 50, 100], [10, 25, 50, 100]],
        pagingType: 'full_numbers',
        ajax: {
            url:   baseUrl+"/terminal-template-datatable",
            type: 'GET',
            data:  function(d){
                d.name= $('#search-name-tt').val();
                
            }
        },
        language: {
            
        },
        //rowId: 'TRANSPORT_ID',
        columns: [
            {data: "DT_RowIndex", 
               sortable: false, 
               searchable: false,
               "render": function (data, type, row, meta) {      
                          return meta.row + meta.settings._iDisplayStart + 1;     
               }  
            },
            {data: "version", name: "version", visible: false},
            {data: "name", name: "name"},
            {data: "description", name: "description"}, 
            {data: "id", sortable: false, searchable: false, }
        ],
		
        columnDefs:[
			{
                "targets": 0, // your case first column
                "className": "text-center",
            },
			{
                "targets": 2, 
                "className": "mleft",
				 render: function(d,data,row) {
					 return `<span>`+d+`</span>`;
				 }
            },
			{
                "targets": 3, 
                "className": "mleft",
				 render: function(d,data,row) {
					 return `<span>`+d+`</span>`;
				 }
            },
			
	        {
                targets: 4,
				"className": "text-center padd-row-table",
                render: function(d,data,row) {
                 
                    let v = row.version;
                    let id = d;
					return `
						<span>
							<a href="terminal-template/`+d+`" data-id="`+d+`"><img class="pencil" src="`+baseUrl+`/assets/images/icon/pencil.png" /></a>&nbsp;
							<a class="btn-delete" data-id="`+d+`" data-version="`+v+`"><img class="trash" src="`+baseUrl+`/assets/images/icon/trash.png" /></a>
						</span>
                    `;
                   
                }
            }
        ]
        });
      
    }
	
	
    $('#btn-search-name-tt').click(function() {
        dataTableTT.draw(true);
    });

      /* filter tugel */
     // Toggle filter
    //  $('.filter .tugel').click(function() {
    //     $(this).toggleClass('open');
    //     $('.filter-title').toggleClass('hidden');
    //     $('.form-filter form').toggleClass('open-filter');
        
    // });

    // clear
    $('.clear_filter').click(function() {
        $('#search-name-tt').val('');
       
        dataTableTT.draw(true);
    });

   
    // Edit 
    $('#dataTableTT').on('click', '.btn-delete', function() {

        let text;
        if (confirm("Are you sure to remove!") == true) {
            
             // Send data
            $.ajax({
                url:  baseUrl + '/terminal-template/delete',
                type: 'POST',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data: {
                    'id': $(this).data('id'),
                    'version': $(this).data('version')
                },
                success: function(resp) {
                    if(resp.responseCode === 200) {
                        // Reload datatable
                        dataTableTT.ajax.reload();
                        
                        // Send success message
                        $.smallBox({
                            height: 50,
                            title : "Success",
                            content : resp.responseMessage,
                            color : "#109618",
                            sound_file: "voice_on",
                            timeout: 3000
                            //icon : "fa fa-bell swing animated"
                        });
                    } else {
                        $.smallBox({
                            height: 50,
                            title : "Error",
                            content : resp.responseMessage,
                            color : "#dc3912",
                            sound_file: "smallbox",
                            timeout: 3000
                            //icon : "fa fa-bell swing animated"
                        });
                    }
                    // Hide loder
                    $('.page-loader').addClass('hidden');
                },
                error: function(xhr, ajaxOptions, thrownError) {
                    $.smallBox({
                        title : "Error",
                        content : xhr.statusText,
                        color : "#dc3912",
                        timeout: 3000
                        //icon : "fa fa-bell swing animated"
                    });
                    // Hide loder
                    $('.page-loader').addClass('hidden');
                    $('.loading>img').addClass('hidden');
                }
            });

       
        } 

    });

   
    
    
})(jQuery);
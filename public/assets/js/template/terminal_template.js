(function($) {
    "use strict";
   
	
    // Add New  or update
    $('#btn-submit-ttem').click(function(){

        // Update when city id has value
        var url = baseUrl + '/terminal-template/update';
        var action = "update";
        if(!$('#tt-id').val()) {
            url = baseUrl + '/terminal-template/save';
            action = "save";
        }

        if($('#tt-id').val()) {
            if(!$('#version-tt').val()) {
                $.smallBox({
                    //height: 50,
                    title : "Error",
                    content : 'Version can\'t be empty',
                    color : "#dc3912",
                    sound_file: "smallbox",
                    timeout: 3000
                });
                $('#version-tt').focus();
                return;
            }
        }
   
        // Has error
        // var hasError = false;
        // Check requirement input
        if(!$('#tt-name').val()) {
            $.smallBox({
                //height: 50,
                title : "Error",
                content : 'Name  can\'t be empty',
                color : "#dc3912",
                sound_file: "smallbox",
                timeout: 3000
            });
            $('#tt-name').focus();
            return;
        }
        
       
        var ar = [];
		$("#acquirer-tt-list>tbody>tr").each(function(){
			let id = $(this).find('.uid').text();
			ar.push(id);
        }); 
		
		//console.log($('input[name="hostLogging"]:checked').val());
        //console.log($('input[name="host_report"]:checked'));

		// Show loder
        $('.page-loader').removeClass('hidden');
        $('.loading>img').removeClass('hidden');
        // Send data
        
        $.ajax({
            url: url,
            type: 'POST',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: {
                'id': $('#tt-id').val(),
                'name' : $('#tt-name').val(),
                'description' : $('#tt-description').val(), 
                'hostReport' :  $('input[name="host_report"]:checked').val() ===undefined ? 'false' : 'true',
                'hostReportUrl' :  $('#tt-HostReportURL').val(),
                'hostReportApiKey' :  $('#tt-HostReportAPIKey').val(),
                'hostReportTimeout' :  $('#tt-HostReportTimeout').val(),
                'hostLogging' :   $('input[name="hostLogging"]:checked').val() ===undefined ?  'false' : 'true',
                'hostLoggingUrl' :  $('#tt-hostLoggingUrl').val(),
                'hostLoggingApiKey' :  $('#tt-HostLoggingAPIKey').val(),
                'hostLoggingTimeout' :  $('#tt-HostLoggingTimeout').val(),
                'hostLoggingInterval' :  $('#tt-hostLoggingInterval').val(),
                'featureSale' :  $('input[name="tt_Sale"]:checked').val() ===undefined ?  'false' : 'true',
                'featureSaleTip' :  $('input[name="SaleTIP"]:checked').val() ===undefined ? 'false' : 'true',
                'featureSaleRedemption' :  $('input[name="SaleRedemption"]:checked').val()  ===undefined ?  'false' : 'true',
                'featureCardVer' :  $('input[name="CardVerification"]:checked').val()  ===undefined ?  'false' : 'true',
                'featureSaleCompletion' :  $('input[name="SaleCompletion"]:checked').val()  ===undefined ?  'false' : 'true',
                'featureInstallment' :  $('input[name="Installment"]:checked').val()  ===undefined ?  'false' : 'true',
                'featureSaleFareNonFare' :  $('input[name="SaleFarenonFare"]:checked').val()  ===undefined ?  'false' : 'true',
                'featureManualKeyIn' :  $('input[name="ManualKeyin"]:checked').val()  ===undefined ? 'false' : 'true',
                'featureQris' :  $('input[name="QRIS"]:checked').val() ===undefined ?  'false' : 'true',
                'featureContactless' :  $('input[name="Contactless"]:checked').val()  ===undefined ? 'false' : 'true',
                'fallbackEnabled' :  $('input[name="Fallbackenable"]:checked').val() ===undefined ?  'false' : 'true',
                'randomPinKeypad' :  $('input[name="RandomPINKeypad"]:checked').val() ===undefined ?  'false' : 'true',
                'beepPinKeypad' :  $('input[name="BeepPINKeypad"]:checked').val()  ===undefined ?  'false' : 'true',
                'autoLogon' :  $('input[name="AutoLogon"]:checked').val()  ===undefined ?  'false' : 'true',
                'nextLogon' :  $('#tt-NextLogon').val(),
                'pushLogon' :  $('input[name="PushLogon"]:checked').val() ===undefined ?  'false' : 'true', //$('#tt-PushLogon').val(),
                'qrisCountDown' :  $('#tt-QRISCountdown').val(),
                'reprintOnlineRetry' :  $('#tt-ReprintOnlineRetry').val(),
                'settlementWarningTrxCount' :  $('#tt-SettleWarningTrxCount').val(),
                'settlementMaxTrxCount' :  $('#tt-SettleMaxTrxCount').val(),
                'merchantPassword' :  $('#tt-MerchantPassword').val(),
                'adminPassword' :  $('#tt-AdminPassword').val(),
                'settlementPassword' :  $('#tt-SettlementPassword').val(),
                'voidPassword' :  $('#tt-VoidPassword').val(),
                'brizziDiscountPercentage' :  $('#tt-Brizzidiscountpercentage').val(),
                'brizziDiscountAmount' :  $('#tt-Brizzidiscountamount').val(),
                'installment1Options' :  $('#tt-InstallmentOptions1').val(),
                'installment2Options' :  $('#tt-InstallmentOptions2').val(),
                'installment3Options' :  $('#tt-InstallmentOptions3').val(),
                'callCenter1' :  $('#tt-callCenter1').val(),
                'callCenter2' :  $('#tt-callCenter2').val(),
                'version': $('#version-tt').val(),
                'acquirerIds': JSON.stringify(ar),
                
            },
		    success: function(resp) {
		        if(resp.responseCode == '0000') { //sukses
                    // Send success message
                    $.smallBox({
                        height: 50,
                        title : resp.responseStatus, 
                        content : resp.responseMessage,
                        color : "#109618",
                        sound_file: "voice_on",
                        timeout: 3000
                        //icon : "fa fa-bell swing animated"
                    });
					window.location.replace(baseUrl +"/terminal-template");

                    // if($('#is-id').val()=="") {
                       
					// 	$('#is-name').val('');
                    //     $('#is-issuerId').val('');
					// 	$('#is-acquirerId').val('');
					// 	$("input:radio").removeAttr("checked");
						
                    // }

 
                } else if(resp.responseCode == '3333' || resp.responseCode == '0400'){ //exception and not found
					
					 $.smallBox({
                        height: 50,
                        title : resp.responseStatus,
                        content : resp.responseMessage,
                        color : "#dc3912",
                        sound_file: "voice_on",
                        timeout: 3000
                        //icon : "fa fa-bell swing animated"
                    });
                }
				else if(resp.responseCode == '5555')//validaator
				{
					
						 var data = Object.values(resp.responseMessage);
						 var ln = data.length;
						 var w  = 3000 * ln; 
						 data.map(function(d){
							 
								$.smallBox({
										height: 50,
										title : resp.responseStatus,
										content : d,
										color : "#dc3912",
										sound_file: "smallbox",
										timeout: w
										//icon : "fa fa-bell swing animated"
									});
						});
					
				}	
                // Hide loder
                $('.page-loader').addClass('hidden');
                $('.loading>img').addClass('hidden');
            },
            error: function(xhr, ajaxOptions, thrownError) {
                $.smallBox({
                    title : "Error",
                    content : xhr.statusText,
                    color : "#dc3912",
                    timeout: 3000
                    //icon : "fa fa-bell swing animated"
                });
                // Hide loder
                $('.page-loader').addClass('hidden');
                $('.loading>img').addClass('hidden');
            }
        });

    });

   
   
    /* list modal acquirer */
	 $('#dataTableListAcquirer_md').wrap('<div class="dataTables_scroll" />');
     var dataTableListAcquirer_md = null;
     if ($('#dataTableListAcquirer_md').length) {
         // You can use 'alert' for alert message
         // or throw to 'throw' javascript error
         // or none to 'ignore' and hide error
         // or you own function
         // please read https://datatables.net/reference/event/error
         // for more information
         $.fn.dataTable.ext.errMode = 'none';
         
         dataTableListAcquirer_md =  $('#dataTableListAcquirer_md').DataTable({
         processing: true,
         serverSide: true,
         //dom : '',
         dom: 'tip',
         "searching": false,
         
         pageLength: 10,
         lengthMenu: [[10, 25, 50, 100], [10, 25, 50, 100]],
         pagingType: 'full_numbers',
         ajax: {
             url:   baseUrl+"/acquirer-datatable",
             type: 'GET',
             data:  function(d){
                 d.name = $('#search-name-ac-list-modal').val();
             }
         },
         language: {
          
         },
         //rowId: 'TRANSPORT_ID',
         columns: [
             {data: "DT_RowIndex", 
               sortable: false, 
               searchable: false,
               "render": function (data, type, row, meta) {      
                          return meta.row + meta.settings._iDisplayStart + 1;     
               }  
            },
            {data: "version", name: "version", visible: false},
            {data: "name", name: "name"},
            {data: "type", name: "type"},
            {data: "acquirerId", name: "acquirerId"},
            {data: "description", name: "description"},
            {data: "id", sortable: false, searchable: false, }
         ],
         columnDefs:[
			  {
                targets: 0,
                "className": "text-center",
                render: function(d,data,row) {
                    return `<span>`+d+`</span>`;
                }
              },	
			  {
                targets: 2,
                "className": "mleft",
                render: function(d,data,row) {
                    return `<span>`+d+`</span>`;
                }
             },
			 {
                targets: 3,
                "className": "mleft",
                render: function(d,data,row) {
                    return `<span>`+d+`</span>`;
                }
             },
             {
                targets: 4,
                "className": "mleft",
                render: function(d,data,row) {
                    return `<span>`+d+`</span>`;
                }
             },
             {
                targets: 5,
                "className": "mleft",
                render: function(d,data,row) {
                    return `<span>`+d+`</span>`;
                }
             },
			 {
                 targets: 6,
				 "className": "text-center padd-row-table",
                 render: function(d,data,row) {
                     let id = d;
                     let v = row.version;
                    
                     return `<span>
                     <input 
                     type="checkbox" 
                     name="ch_ac[]" 
                     value="true"  
                     data-id="`+d+`" 
                     data-name="`+row.name+`"     
                     data-version="`+row.version+`" 
                     data-acquirerid="`+row.acquirerId+`" 
                     data-description="`+row.description+`" 
                     data-type="`+row.type+`" 
                     /></span>
                   `;
                 }
             }
         ]
         });
       
     }

     
    $('#btn-search-ac-list-modal').click(function(){
        dataTableListAcquirer_md.draw(true);
    }); 

    $('#btn-clear-ac-list-modal').click(function(){
        $("#search-name-ac-list-modal").val('');
        dataTableListAcquirer_md.draw(true);
    }); 
     
     
    //btn-add-acquirer-modal-tt
    $('#btn-add-acquirer-modal-tt').click(function(){
        //
        $('#modal-list-acquirer').modal('show'); 
        $('#modal-list-acquirer .title-form').html('Acquirer List');
        dataTableListAcquirer_md.draw(true);
        
    });

    $('#btn-to-list-acq').click(function(){

        var arlist = [];
        $("#acquirer-tt-list>tbody>tr").each(function(){

            let description = $(this).find('.description').text();
            let acquirerId = $(this).find('.acquirerId').text();
            let name = $(this).find('.name').text();
            let type = $(this).find('.type').text();
            let id = $(this).find('.uid').text();

			var ob = new Object();
			ob.description = description;
			ob.acquirerId = acquirerId;
            ob.name = name;
            ob.type = type;
			ob.uid = id;
			arlist.push(ob);
        }); 
       
        var arlist2 = [];
        var datac = $('[name="ch_ac[]"]');
       // var html = "";
      
        $.each(datac, function() {
            var $this = $(this);
			//console.log($this.data("binrangeend"));
            // check if the checkbox is checked
            if($this.is(":checked")) {
                var ob = new Object();
                let find = 0;
                if(arlist.length > 0)
                {
                    for(var i=0; i<arlist.length; i++)
                    {
                        if(arlist[i].uid == $this.data("id"))
                        {
                            find = 1;
                            break;
                        }
                    }
                }
                
                if(find == 0) 
                {
                    ob.description = $this.data("description");
                    ob.acquirerId = $this.data("acquirerid");
                    ob.name = $this.data("name");
                    ob.type = $this.data("type");
                    ob.uid = $this.data("id");
                    arlist2.push(ob);
                }
                
            }
        });	

        let arr = [...arlist, ...arlist2];
        //console.log('1',arlist2);
        let mergedArr = [...new Set(arr)];
        //console.log('2',mergedArr);

        var d = "";		
		for(var i=0; i<mergedArr.length; i++)
		{
		    let no = i+1;
			let description = mergedArr[i].description
            let acquirerId = mergedArr[i].acquirerId;
			let name = mergedArr[i].name;
			let type = mergedArr[i].type;
            let uid = mergedArr[i].uid;
			let act = `<a href="#" class="btn-delete"><img style="width:0.7rem;" class="trash" src="`+baseUrl+`/assets/images/icon/trash.png" /></a>`;
			d+=`<tr class="tr-dt">`+
                    `<td><span class="no">`+no+`</span></td>`+
				    `<td><span class="name">`+name+`</span></td>`+
                    `<td><span class="type">`+type+`</span></td>`+
					`<td><span class="acquirerId">`+acquirerId+`</span></td>`+
					`<td><span class="description">`+description+`</span><span class="uid" style="display:none;">`+uid+`</span></td>`+
					`<td>`+act+`</td>`+
				`</tr>`;
		}

        $('#acquirer-tt-list>tbody').find('tr').remove();  
        $('#acquirer-tt-list>tbody').html(d);       
        $('#modal-list-acquirer').modal('hide');

    });

    $('#acquirer-tt-list').on('click', '.btn-delete', function() {
        $(this).parent().parent().remove();
    });
    
    $("#tt-HostReportTimeout").ForceNumericOnly();
    $("#tt-hostLoggingInterval").ForceNumericOnly();
    $("#tt-HostLoggingTimeout").ForceNumericOnly();
    $("#tt-NextLogon").ForceNumericOnly();
    $("#tt-QRISCountdown").ForceNumericOnly();
    $("#tt-ReprintOnlineRetry").ForceNumericOnly();
    $("#tt-SettleWarningTrxCount").ForceNumericOnly();
    $("#tt-SettleMaxTrxCount").ForceNumericOnly();


    
    
})(jQuery);
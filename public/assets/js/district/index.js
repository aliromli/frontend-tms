(function($) {
    "use strict";

    $('#dataTableDistrict').wrap('<div class="dataTables_scroll" />');
    var dataTableDistrict = null;
    if ($('#dataTableDistrict').length) {
        // You can use 'alert' for alert message
        // or throw to 'throw' javascript error
        // or none to 'ignore' and hide error
        // or you own function
        // please read https://datatables.net/reference/event/error
        // for more information
        $.fn.dataTable.ext.errMode = 'none';
        
        dataTableDistrict =  $('#dataTableDistrict').DataTable({
        processing: true,
        serverSide: true,
        //dom : '',
        dom: 'lrtip',
        "searching": false,
        //"lengthChange": false,
        pageLength: 10,
        lengthMenu: [[10, 25, 50, 100], [10, 25, 50, 100]],
        pagingType: 'full_numbers',
        ajax: {
            url:   baseUrl+"/district-datatables",
            type: 'GET',
            data:  function(d){
                d.district = $('#search-district').val();
                d.city = $('#search-district-city').val(); 
                
            }
        },
        language: {
            //processing: '<div style="display: none"></div>',
            // info: 'Menampilkan _START_ - _END_ dari _TOTAL_ ',
            //search: '<span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>',
            // zeroRecords: 'Tidak ada data yang cocok dengan kriteria pencarian',
            // emptyTable: 'Data tidak tersedia',
            // paginate: {
            //     first: '&laquo;',
            //     last: '&raquo;',
            //     next: '&rsaquo;',
            //     previous: '&lsaquo;'
            // },
            //lengthMenu: "Baris per halaman: _MENU_ "
        },
        //rowId: 'TRANSPORT_ID',
        columns: [
            {data: "DT_RowIndex", 
               sortable: false, 
               searchable: false,
               "render": function (data, type, row, meta) {      
                          return meta.row + meta.settings._iDisplayStart + 1;     
               }  
            },
            {data: "version", name: "version", visible:false},
            {data: "name", name: "name"},
            {data: "city.name", name: "city.name"},
           
            {data: "id", sortable: false, searchable: false, }
        ],
        columnDefs:[
            {
                "targets": 0, 
                "className": "text-center",
            },
            {
                "targets": 1, 
                "className": "text-center",
            },
            {
                "targets": 2, //
                "className": "mleft",
                render : function(d,data,row){
                    return '<span>'+d+'</span>';
                }
            },
            {
                "targets": 3, //
                "className": "mleft",
                render : function(d,data,row){
                    return '<span>'+d+'</span>';
                }
            },
	        {
                targets: 4,
                "className": "text-center padd-row-table",
                render: function(d,data,row) {
                 
                    let v = row.version;

                    return `<span>
                    <a href="district/`+d+`"  class="" data-id="`+d+`"><img class="pencil" src="`+baseUrl+`/assets/images/icon/pencil.png" /></a>&nbsp;
                    <a  class="btn-delete" data-id="`+d+`" data-version="`+v+`"><img class="trash" src="`+baseUrl+`/assets/images/icon/trash.png" /></a>
                    </span>`;
                  
                }
            }
        ]
        });
      
    }

  

    
    $('#btn-ti-district-city').click(function() {
        dataTableDistrict.draw(true);
    });

    // Add 
    $('body').on('click', '#btn-add-district', function(){
         window.location.href = baseUrl + "/district-form";

    });

      /* filter tugel */
     // Toggle filter
     $('.filter .tugel').click(function() {
        $(this).toggleClass('open');
        $('.filter-title').toggleClass('hidden');
        $('.form-filter form').toggleClass('open-filter');
        
    });

    // clear
    $('.clear_filter').click(function() {
        $('#search-district').val('');
        $('#search-district-city').val('');
        dataTableDistrict.draw(true);
    });

    // Edit state
    $('#dataTableDistrict').on('click', '.btn-edit', function() {
        $('#modal-district').modal('show');
        $('#modal-district .modal-title').html('Edit District');
        //$('#email').attr('readonly','readonly');
        $('#divdis-version').show();
       
        // Hide loder
        $('.page-loader').removeClass('hidden');

        // Get data
        // Send data
        $.ajax({
            url: baseUrl + '/district/' + $(this).data('id'),
            type: 'GET',
            success: function(resp) {
                if(resp.responseCode === 200) {
                    //console.log(resp.responseMessage[0].country.id);
                    $('#district-id').val(resp.responseMessage[0].id);
                    $('#district-name').val(resp.responseMessage[0].name);
                    $('#district-city').val(resp.responseMessage[0].city.id).trigger('change');
                    $('#district-city').focus();
                    $('#version-dis').val(resp.responseMessage[0].version);

                }
                else
                {
                    $.smallBox({
                        height: 50,
                        title : "Error",
                        content : resp.responseMessage,
                        color : "#dc3912",
                        sound_file: "smallbox",
                        timeout: 3000
                        //icon : "fa fa-bell swing animated"
                    });
                }
               
                // Hide loder
                $('.page-loader').addClass('hidden');
            },
            error: function(xhr, ajaxOptions, thrownError) {
                $.smallBox({
                    title : "Error",
                    content : xhr.statusText,
                    color : "#dc3912",
                    timeout: 3000
                    //icon : "fa fa-bell swing animated"
                });
                // Hide loder
                $('.page-loader').addClass('hidden');
            }
        });
    });

    $('#dataTableDistrict').on('click', '.btn-delete', function() {

        let text;
        if (confirm("Are you sure to remove!") == true) {
            
             // Send data
            $.ajax({
                url:  baseUrl + '/district/delete',
                type: 'POST',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data: {
                    'id': $(this).data('id'),
                    'version': $(this).data('version')
                },
                success: function(resp) {
                    if(resp.responseCode === 200) {
                        // Reload datatable
                        dataTableDistrict.ajax.reload();
                        
                        // Send success message
                        $.smallBox({
                            height: 50,
                            title : "Success",
                            content : resp.responseMessage,
                            color : "#109618",
                            sound_file: "voice_on",
                            timeout: 3000
                            //icon : "fa fa-bell swing animated"
                        });
                    } else {
                        $.smallBox({
                            height: 50,
                            title : "Error",
                            content : resp.responseMessage,
                            color : "#dc3912",
                            sound_file: "smallbox",
                            timeout: 3000
                            //icon : "fa fa-bell swing animated"
                        });
                    }
                    // Hide loder
                    $('.page-loader').addClass('hidden');
                },
                error: function(xhr, ajaxOptions, thrownError) {
                    $.smallBox({
                        title : "Error",
                        content : xhr.statusText,
                        color : "#dc3912",
                        timeout: 3000
                        //icon : "fa fa-bell swing animated"
                    });
                    // Hide loder
                    $('.page-loader').addClass('hidden');
                }
            });

       
        } 

    });


    


})(jQuery);
(function($) {
    "use strict";
    $('#dataTableIs').wrap('<div class="dataTables_scroll" />');
    var dataTableIs = null;
    if ($('#dataTableIs').length) {
        // You can use 'alert' for alert message
        // or throw to 'throw' javascript error
        // or none to 'ignore' and hide error
        // or you own function
        // please read https://datatables.net/reference/event/error
        // for more information
        $.fn.dataTable.ext.errMode = 'none';
        
        dataTableIs =  $('#dataTableIs').DataTable({
        processing: true,
        serverSide: true,
        //dom : '',
        dom: 'lrtip',
        "searching": false,
        //"scrollX": true,
        //"sScrollX": '100%',
        //"sScrollXInner": "110%",
        pageLength: 10,
        lengthMenu: [[10, 25, 50, 100], [10, 25, 50, 100]],
        pagingType: 'full_numbers',
        ajax: {
            url:   baseUrl+"/issuer-datatable",
            type: 'GET',
            data:  function(d){
                d.name= $('#search-isu').val();
                
            }
        },
        language: {
            
        },
        //rowId: 'TRANSPORT_ID',
        columns: [
            {data: "DT_RowIndex", 
               sortable: false, 
               searchable: false,
               "render": function (data, type, row, meta) {      
                          return meta.row + meta.settings._iDisplayStart + 1;     
               }  
            },
            {data: "version", name: "version", visible: false},
            {data: "name", name: "name"},
            {data: "issuerId", name: "issuerId"}, 
            {data: "acquirer", name: "acquirer"},  
            {data: "acquirerId", name: "acquirerId"}, 
            {data: "onUs", name: "onUs"},   
            {data: "id", sortable: false, searchable: false, }
        ],
		
        columnDefs:[
			{
                "targets": 0, // your case first column
                "className": "text-center",
            },
			{
                "targets": 2, 
                "className": "mleft",
				 render: function(d,data,row) {
					 return `<span>`+d+`</span>`;
				 }
            },
			{
                "targets": 3, 
                "className": "mleft",
				 render: function(d,data,row) {
					 return `<span>`+d+`</span>`;
				 }
            },
			// {
                // "targets": 5, 
                // "className": "mleft",
				 // render: function(d,data,row) {
					 // return `<span>`+d+`</span>`;
				 // }
            // },
			{
                "targets": 6, 
                "className": "mleft",
				 render: function(d,data,row) {
					 return `<span>`+d+`</span>`;
				 }
            },
	        {
                targets: 7,
				"className": "text-center padd-row-table",
                render: function(d,data,row) {
                 
                    let v = row.version;
                    let id = d;
					return `
						<span>
							<a href="issuer/`+d+`" data-id="`+d+`"><img class="pencil" src="`+baseUrl+`/assets/images/icon/pencil.png" /></a>&nbsp;
							<a class="btn-delete" data-id="`+d+`" data-version="`+v+`"><img class="trash" src="`+baseUrl+`/assets/images/icon/trash.png" /></a>
						</span>
                    `;
                   
                }
            }
        ]
        });
      
    }
	
	
    $('#btn-search-isu').click(function() {
        dataTableIs.draw(true);
    });

      /* filter tugel */
     // Toggle filter
    //  $('.filter .tugel').click(function() {
    //     $(this).toggleClass('open');
    //     $('.filter-title').toggleClass('hidden');
    //     $('.form-filter form').toggleClass('open-filter');
        
    // });

    // clear
    $('.clear_filter').click(function() {
        $('#search-isu').val('');
       
        dataTableIs.draw(true);
    });

    // Add New  or update
    $('#btn-submit-is').click(function(){

        // Update when city id has value
        var url = baseUrl + '/issuer/update';
        var action = "update";
        if(!$('#is-id').val()) {
            url = baseUrl + '/issuer/save';
            action = "save";
        }

        if($('#is-id').val()) {
            if(!$('#version-is').val()) {
                $.smallBox({
                    //height: 50,
                    title : "Error",
                    content : 'Version can\'t be empty',
                    color : "#dc3912",
                    sound_file: "smallbox",
                    timeout: 3000
                });
                $('#version-is').focus();
                return;
            }
        }
   
        // Has error
        // var hasError = false;
        // Check requirement input
        if(!$('#is-name').val()) {
            $.smallBox({
                //height: 50,
                title : "Error",
                content : 'Name  can\'t be empty',
                color : "#dc3912",
                sound_file: "smallbox",
                timeout: 3000
            });
            $('#is-name').focus();
            return;
        }

        if(!$('#is-issuerId').val()) {
            $.smallBox({
                //height: 50,
                title : "Error",
                content : 'IssuerId can\'t be empty',
                color : "#dc3912",
                sound_file: "smallbox",
                timeout: 3000
            });
            $('#is-issuerId').focus();
            return;
        }
		var arcard = [];
		$("#table-card-issuer-form>tbody>tr").each(function(){
			let id = $(this).find('.uid').text();
			arcard.push(id);
        }); 
		
		// Show loder
        $('.page-loader').removeClass('hidden');
		$('.loading>img').removeClass('hidden');
        // Send data
        $.ajax({
            url: url,
            type: 'POST',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: {
                'id': $('#is-id').val(),
                'name' : $('#is-name').val(),
                'issuerId' : $('#is-issuerId').val(),
                'version': $('#version-is').val(),
                'acquirerId': $('#is-acquirerId').val(),
                'onUs': $('input[name="isOnUs"]:checked').val(),
                'cards': JSON.stringify(arcard),
                
            },
		    success: function(resp) {
		        if(resp.responseCode == '0000') { //sukses
                    // Send success message
                    $.smallBox({
                        height: 50,
                        title : resp.responseStatus, 
                        content : resp.responseMessage,
                        color : "#109618",
                        sound_file: "voice_on",
                        timeout: 3000
                        //icon : "fa fa-bell swing animated"
                    });
					window.location.replace(baseUrl +"/issuer");

                    // if($('#is-id').val()=="") {
                       
						// $('#is-name').val('');
                        // $('#is-issuerId').val('');
						// $('#is-acquirerId').val('');
						// $("input:radio").removeAttr("checked");
						
                    // }

 
                } else if(resp.responseCode == '3333' || resp.responseCode == '0400'){ //exception and not found
					
					 $.smallBox({
                        height: 50,
                        title : resp.responseStatus,
                        content : resp.responseMessage,
                        color : "#dc3912",
                        sound_file: "voice_on",
                        timeout: 3000
                        //icon : "fa fa-bell swing animated"
                    });
                }
				else if(resp.responseCode == '5555')//validaator
				{
					
						 var data = Object.values(resp.responseMessage);
						 var ln = data.length;
						 var w  = 3000 * ln; 
						 data.map(function(d){
							 
								$.smallBox({
										height: 50,
										title : resp.responseStatus,
										content : d,
										color : "#dc3912",
										sound_file: "smallbox",
										timeout: w
										//icon : "fa fa-bell swing animated"
									});
						});
					
				}	
                // Hide loder
                $('.page-loader').addClass('hidden');
				$('.loading>img').addClass('hidden');
            },
            error: function(xhr, ajaxOptions, thrownError) {
                $.smallBox({
                    title : "Error",
                    content : xhr.statusText,
                    color : "#dc3912",
                    timeout: 3000
                    //icon : "fa fa-bell swing animated"
                });
                // Hide loder
                $('.page-loader').addClass('hidden');
				$('.loading>img').addClass('hidden');
            }
        });

    });

    // Edit 
    $('#dataTableIs').on('click', '.btn-delete', function() {

        let text;
        if (confirm("Are you sure to remove!") == true) {
            
             // Send data
            $.ajax({
                url:  baseUrl + '/issuer/delete',
                type: 'POST',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data: {
                    'id': $(this).data('id'),
                    'version': $(this).data('version')
                },
                success: function(resp) {
                    if(resp.responseCode === 200) {
                        // Reload datatable
                        dataTableIs.ajax.reload();
                        
                        // Send success message
                        $.smallBox({
                            height: 50,
                            title : "Success",
                            content : resp.responseMessage,
                            color : "#109618",
                            sound_file: "voice_on",
                            timeout: 3000
                            //icon : "fa fa-bell swing animated"
                        });
                    } else {
                        $.smallBox({
                            height: 50,
                            title : "Error",
                            content : resp.responseMessage,
                            color : "#dc3912",
                            sound_file: "smallbox",
                            timeout: 3000
                            //icon : "fa fa-bell swing animated"
                        });
                    }
                    // Hide loder
                    $('.page-loader').addClass('hidden');
                },
                error: function(xhr, ajaxOptions, thrownError) {
                    $.smallBox({
                        title : "Error",
                        content : xhr.statusText,
                        color : "#dc3912",
                        timeout: 3000
                        //icon : "fa fa-bell swing animated"
                    });
                    // Hide loder
                    $('.page-loader').addClass('hidden');
                }
            });

       
        } 

    });

    /* modal cards */
	
	
	$('#dataTableCardsListModal').wrap('<div class="dataTables_scroll" />');
    var dataTableCardsListModal = null;
    if ($('#dataTableCardsListModal').length) {
        // You can use 'alert' for alert message
        // or throw to 'throw' javascript error
        // or none to 'ignore' and hide error
        // or you own function
        // please read https://datatables.net/reference/event/error
        // for more information
        $.fn.dataTable.ext.errMode = 'none';
        
        dataTableCardsListModal =  $('#dataTableCardsListModal').DataTable({
        processing: true,
        serverSide: true,
        //dom : '',
        dom: 'lrtip',
        "searching": false,
        //"scrollX": true,
        //"sScrollX": '100%',
        //"sScrollXInner": "110%",
        pageLength: 10,
        lengthMenu: [[10, 25, 50, 100], [10, 25, 50, 100]],
        pagingType: 'full_numbers',
        ajax: {
            url:   baseUrl+"/card-datatable",
            type: 'GET',
            data:  function(d){
                d.name= $('#search-name-cards-list-modal').val();
                
            }
        },
        language: {
            
        },
        //rowId: 'TRANSPORT_ID',
        columns: [
            {data: "DT_RowIndex", 
               sortable: false, 
               searchable: false,
               "render": function (data, type, row, meta) {      
                          return meta.row + meta.settings._iDisplayStart + 1;     
               }  
            },
            {data: "version", name: "version", visible: false},
            {data: "name", name: "name"},
            {data: "binRangeStart", name: "binRangeStart"}, 
            {data: "binRangeEnd", name: "binRangeEnd"}, 
            {data: "cardNumLength", name: "cardNumLength"}, 
            {data: "panDigitUnmasking", name: "panDigitUnmasking"},   
            {data: "id", sortable: false, searchable: false, }
        ],
		
        columnDefs:[
			{
                "targets": 0, // your case first column
                "className": "text-center",
            },
			{
                "targets": 2, 
                "className": "mleft",
				 render: function(d,data,row) {
					 return `<span>`+d+`</span>`;
				 }
            },
			{
                "targets": 3, 
                "className": "mleft",
				 render: function(d,data,row) {
					 return `<span>`+d+`</span>`;
				 }
            },
			{
                "targets": 4, 
                "className": "mleft",
				 render: function(d,data,row) {
					 return `<span>`+d+`</span>`;
				 }
            },
			{
                "targets": 5, 
                "className": "mleft",
				 render: function(d,data,row) {
					 return `<span>`+d+`</span>`;
				 }
            },
			{
                "targets": 6, 
                "className": "text-center",
				 
            },
	        {
                targets: 7,
				"className": "text-center padd-row-table",
				render: function(d,data,row) {
                    let id = d;
                    let v = row.version;
					//console.log(row.binRangeStart);
					//console.log(row.panDigitUnmasking);
                    return `
                      <span><input type="checkbox" 
                      name="ch_card[]" value="true"
                      data-id="`+d+`" 
                      data-version="`+row.version+`"
                      data-name="`+row.name+`"
                      data-binrangestart="`+row.binRangeStart+`"
                      data-cardnumlength="`+row.cardNumLength+`"
                      data-pandigitunmasking="`+row.panDigitUnmasking+`"
                      data-binrangeend="`+row.binRangeEnd+`"/></span>
                    `;
                   
                }
            }
        ]
        });
      
    }
	
	$('body').on('click', '#btn-add-cards-modal', function() {
		$('#modal-list-cards').modal('show');
        $('#modal-list-cards .modal-title').html('List Cards');
        dataTableCardsListModal.draw(true);
	});
	
	
	$('body').on('click', '#btn-search-cards-list-modal', function() {
		dataTableCardsListModal.draw(true);
	});
	
	$('body').on('click', '#btn-clear-cards-list-modal', function() {
		$('#search-name-cards-list-modal').val('');
        dataTableCardsListModal.draw(true);
	});
	
	$('body').on('click', '#btn-to-temp-cards-modal', function() {
		 var arlist = [];
        $("#table-card-issuer-form>tbody>tr").each(function(){

            let binRangeStart = $(this).find('.binRangeStart').text();
            let binRangeEnd = $(this).find('.binRangeEnd').text();
            let cardNumLength = $(this).find('.cardNumLength').text();
            let panDigitUnmasking = $(this).find('.panDigitUnmasking').text();
            let id = $(this).find('.uid').text();

			var ob = new Object();
			ob.binRangeStart = binRangeStart;
			ob.binRangeEnd = binRangeEnd;
            ob.cardNumLength = cardNumLength;
            ob.panDigitUnmasking = panDigitUnmasking;
			ob.uid = id;
			arlist.push(ob);
        }); 
       
        var arlist2 = [];
        var datac = $('[name="ch_card[]"]');
       // var html = "";
      
        $.each(datac, function() {
            var $this = $(this);
			//console.log($this.data("binrangeend"));
            // check if the checkbox is checked
            if($this.is(":checked")) {
                var ob = new Object();
                let find = 0;
                if(arlist.length > 0)
                {
                    for(var i=0; i<arlist.length; i++)
                    {
                        if(arlist[i].uid == $this.data("id"))
                        {
                            find = 1;
                            break;
                        }
                    }
                }
                
                if(find == 0) 
                {
                    ob.binRangeEnd = $this.data("binrangeend");
                    ob.binRangeStart = $this.data("binrangestart");
                    ob.cardNumLength = $this.data("cardnumlength");
                    ob.panDigitUnmasking = $this.data("pandigitunmasking");
                    ob.uid = $this.data("id");
                    arlist2.push(ob);
                }
                
            }
        });	

        let arr = [...arlist, ...arlist2];
        //console.log('1',arlist2);
        let mergedArr = [...new Set(arr)];
        //console.log('2',mergedArr);

        var d = "";		
		for(var i=0; i<mergedArr.length; i++)
		{
		    //let no = i+1;
			let binRangeStart = mergedArr[i].binRangeStart
            let binRangeEnd = mergedArr[i].binRangeEnd;
			let cardNumLength = mergedArr[i].cardNumLength;
			let panDigitUnmasking = mergedArr[i].panDigitUnmasking;
            let uid = mergedArr[i].uid;
			let act = `<a href="#" class="btn-delete"><img class="trash" src="`+baseUrl+`/assets/images/icon/trash.png" /></a>`;
			d+=`<tr class="tr-dt">`+
				    `<td><span class="binRangeStart">`+binRangeStart+`</span></td>`+
                    `<td><span class="binRangeEnd">`+binRangeEnd+`</span></td>`+
					`<td><span class="cardNumLength">`+cardNumLength+`</span></td>`+
					`<td><span class="panDigitUnmasking">`+panDigitUnmasking+`</span><span class="uid" style="display:none;">`+uid+`</span></td>`+
					`<td>`+act+`</td>`+
				`</tr>`;
		}

        $('#table-card-issuer-form>tbody').find('tr').remove();  
        $('#table-card-issuer-form>tbody').html(d);       
        $('#modal-list-cards').modal('hide');
	});
	$('#table-card-issuer-form').on('click', '.btn-delete', function() {
            $(this).parent().parent().remove();
    });

 

})(jQuery);
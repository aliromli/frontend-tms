(function($) {
    "use strict";
    $('#dataTableDeviceprofile').wrap('<div class="dataTables_scroll" />');
    var dataTableDeviceprofile = null;
    if ($('#dataTableDeviceprofile').length) {
        // You can use 'alert' for alert message
        // or throw to 'throw' javascript error
        // or none to 'ignore' and hide error
        // or you own function
        // please read https://datatables.net/reference/event/error
        // for more information
        $.fn.dataTable.ext.errMode = 'none';
        
        dataTableDeviceprofile =  $('#dataTableDeviceprofile').DataTable({
        processing: true,
        serverSide: true,
        //dom : '',
        dom: 'lrtip',
        "searching": false,
        //"scrollX": true,
        //"sScrollX": '100%',
        //"sScrollXInner": "110%",
        pageLength: 10,
        lengthMenu: [[10, 25, 50, 100], [10, 25, 50, 100]],
        pagingType: 'full_numbers',
        ajax: {
            url:   baseUrl+"/device-profile-datatables",
            type: 'GET',
            data:  function(d){
                d.name= $('#search-device-profile').val();
                
            }
        },
        language: {
            
        },
        //rowId: 'TRANSPORT_ID',
        columns: [
            {data: "DT_RowIndex", 
               sortable: false, 
               searchable: false,
               "render": function (data, type, row, meta) {      
                          return meta.row + meta.settings._iDisplayStart + 1;     
               }  
            },
            {data: "version", name: "version", visible:false},
            {data: "name", name: "name"},
            {data: "default", name: "default"}, 
            {data: "id", sortable: false, searchable: false, }
        ],
        columnDefs:[
            {
                "targets": 0,
                "className": "text-center",
                

            },
            {
                "targets": 1,
                "className": "mleft",
                render: function(d,data,row) {
                    return `<span>`+d+`</span>`;
                }

            },
            {
                "targets": 3,
                "className": "mleft",
                render: function(d,data,row) {
                    return `<span>`+d+`</span>`;
                }

            },
          
	        {
                targets: 4,
                "className": "text-center padd-row-table",
                render: function(d,data,row) {
                 
                    let v = row.version;
                   
                    return `
                    <span>
                    <a href="device-profile/detail/`+d+`" data-id="`+d+`" tooltip="Detail"><i class="fa fa-cog"></i></a>&nbsp;
                    <a href="device-profile/`+d+`" data-id="`+d+`"><img class="pencil" src="`+baseUrl+`/assets/images/icon/pencil.png" /></a>&nbsp;
                    <a class="btn-delete" data-id="`+d+`" data-version="`+v+`"><img class="trash" src="`+baseUrl+`/assets/images/icon/trash.png" /></a>
                    </span>
                    `;
                }
            }
	        
        ]
        });
      
    }

    
    $('#btn-ti-search-dp ').click(function() {
        dataTableDeviceprofile.draw(true);
    });

      /* filter tugel */
     // Toggle filter
    //  $('.filter .tugel').click(function() {
    //     $(this).toggleClass('open');
    //     $('.filter-title').toggleClass('hidden');
    //     $('.form-filter form').toggleClass('open-filter');
        
    // });

    // clear
    $('.clear_filter').click(function() {
        $('#search-device-profile').val('');
       
        dataTableDeviceprofile.draw(true);
    });

    // Add New  or update
    $('#btn-submit-dp').click(function(){

        // Update when city id has value
        var url = baseUrl + '/device-profile/update';
        var action = "update";
        if(!$('#dp-id').val()) {
            url = baseUrl + '/device-profile/save';
            action = "save";
        }

        if($('#dm-id').val()) {
            if(!$('#version-dp').val()) {
                $.smallBox({
                    //height: 50,
                    title : "Error",
                    content : 'Version can\'t be empty',
                    color : "#dc3912",
                    sound_file: "smallbox",
                    timeout: 3000
                });
                $('#version-dp').focus();
                return;
            }
        }

        // Has error
        // var hasError = false;
        // Check requirement input
        if(!$('#dp-name').val()) {
            $.smallBox({
                //height: 50,
                title : "Error",
                content : 'Name Name can\'t be empty',
                color : "#dc3912",
                sound_file: "smallbox",
                timeout: 3000
            });
            $('#dp-name').focus();
            return;
        }

        if(!$('#dp-heartbeatInterval').val()) {
            $.smallBox({
                //height: 50,
                title : "Error",
                content : 'Heartbeat Interval can\'t be empty',
                color : "#dc3912",
                sound_file: "smallbox",
                timeout: 3000
            });
            $('#dp-heartbeatInterval').focus();
            return;
        }
        if(!$('#dp-diagnosticInterval').val()) {
            $.smallBox({
                //height: 50,
                title : "Error",
                content : 'Diagnostic Interval can\'t be empty',
                color : "#dc3912",
                sound_file: "smallbox",
                timeout: 3000
            });
            $('#dp-diagnosticInterval').focus();
            return;
        }

        if(!$('#dp-scheduleRebootTime').val()) {
            $.smallBox({
                //height: 50,
                title : "Error",
                content : 'Schedule Reboot Time can\'t be empty',
                color : "#dc3912",
                sound_file: "smallbox",
                timeout: 3000
            });
            $('#dp-scheduleRebootTime').focus();
            return;
        }
        // if(!$('#dp-movingThreshold').val()) {
        //     $.smallBox({
        //         //height: 50,
        //         title : "Error",
        //         content : 'Moving Threshold can\'t be empty',
        //         color : "#dc3912",
        //         sound_file: "smallbox",
        //         timeout: 3000
        //     });
        //     $('#dp-movingThreshold').focus();
        //     return;
        // }

        // if(!$('#dp-adminPassword').val()) {
        //     $.smallBox({
        //         //height: 50,
        //         title : "Error",
        //         content : 'Admin Password  can\'t be empty',
        //         color : "#dc3912",
        //         sound_file: "smallbox",
        //         timeout: 3000
        //     });
        //     $('#dp-adminPassword').focus();
        //     return;
        // }

       

        if(!$('input[name="maskHomeButton"]:checked').val())
        {
            $.smallBox({
                //height: 50,
                title : "Error",
                content : 'Mask Home Button  not checked',
                color : "#dc3912",
                sound_file: "smallbox",
                timeout: 3000
            });
          
            return;
        }

        if(!$('input[name="maskStatusBar"]:checked').val())
        {
            $.smallBox({
                //height: 50,
                title : "Error",
                content : 'Mask Status Bar  not checked',
                color : "#dc3912",
                sound_file: "smallbox",
                timeout: 3000
            });
          
            return;
        }

        if(!$('input[name="scheduleReboot"]:checked').val())
        {
            $.smallBox({
                //height: 50,
                title : "Error",
                content : 'Schedule Reboot  not checked',
                color : "#dc3912",
                sound_file: "smallbox",
                timeout: 3000
            });
          
            return;
        }

        // if(!$('input[name="default"]:checked').val())
        // {
        //     $.smallBox({
        //         //height: 50,
        //         title : "Error",
        //         content : 'Default  not checked',
        //         color : "#dc3912",
        //         sound_file: "smallbox",
        //         timeout: 3000
        //     });
          
        //     return;
        // }

        // if(!$('input[name="relocationAlert"]:checked').val())
        // {
        //     $.smallBox({
        //         //height: 50,
        //         title : "Error",
        //         content : 'Relocation Alert not checked',
        //         color : "#dc3912",
        //         sound_file: "smallbox",
        //         timeout: 3000
        //     });
          
        //     return;
        // }

        // if(!$('#dp-frontApp').val()) {
        //     $.smallBox({
        //         //height: 50,
        //         title : "Error",
        //         content : 'Front App  can\'t be empty',
        //         color : "#dc3912",
        //         sound_file: "smallbox",
        //         timeout: 3000
        //     });
        //     $('#dp-frontApp').focus();
        //     return;
        // }

        
        // Show loder
        $('.page-loader').removeClass('hidden');

        // Send data
        $.ajax({
            url: url,
            type: 'POST',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: {
                'id': $('#dp-id').val(),
                'name' : $('#dp-name').val(),
                'heartbeatInterval': $('#dp-heartbeatInterval').val(),
                'diagnosticInterval': $('#dp-diagnosticInterval').val(),
                'scheduleRebootTime' : $('#dp-scheduleRebootTime').val(),
                'movingThreshold' : $('#dp-movingThreshold').val(),
                'adminPassword' : $('#dp-adminPassword').val(),
                'frontApp' : $('#dp-frontApp').val(),
                'maskHomeButton': $('input[name="maskHomeButton"]:checked').val(),
                'maskStatusBar': $('input[name="maskStatusBar"]:checked').val(),
                'scheduleReboot': $('input[name="scheduleReboot"]:checked').val(),
                'default': $('input[name="default"]').is(':checked') ? 'true' : 'false', 
                'relocationAlert': $('input[name="relocationAlert"]').is(':checked') ? 'true' : 'false',
                'version': $('#version-dp').val(),
                
                
            },
            success: function(resp) {
                
                if(resp.responseCode == '0000') { //sukses
                    // Send success message
                    $.smallBox({
                        height: 50,
                        title : resp.responseStatus, 
                        content : resp.responseMessage,
                        color : "#109618",
                        sound_file: "voice_on",
                        timeout: 3000
                        //icon : "fa fa-bell swing animated"
                    });
                    //     if($('#dp-id').val()=="") {
                       
                    //     $('#dp-name').val("");
                    //     $('#dp-heartbeatInterval').val("");
                    //     $('#dp-diagnosticInterval').val("");
                    //     $('#dp-scheduleRebootTime').val("");
                    //     $('#dp-movingThreshold').val("");
                    //     $('#dp-adminPassword').val("");
                    //     $('#dp-frontApp').val("");

                      
                    //      $('input[name="default"]').prop('checked', false);
                    //      $('input[name="relocationAlert"]').prop('checked', false);
                    //      $('input[name="maskHomeButton"]').prop('checked', false);
                    //      $('input[name="maskStatusBar"]').prop('checked', false);
                    //      $('input[name="scheduleReboot"]').prop('checked', false);
                        
                    // }
                    // else
                    //{
                        window.location.href=baseUrl+"/device-profile"
                    //}
				
 
                } else if(resp.responseCode == '3333' || resp.responseCode == '0400'){ //exception and not found
					
					 $.smallBox({
                        height: 50,
                        title : resp.responseStatus,
                        content : resp.responseMessage,
                        color : "#dc3912",
                        sound_file: "voice_on",
                        timeout: 3000
                        //icon : "fa fa-bell swing animated"
                    });
                }
				else if(resp.responseCode == '5555')//validaator
				{
					
						 var data = Object.values(resp.responseMessage);
						 var ln = data.length;
						 var w  = 3000 * ln; 
						 data.map(function(d){
							 
								$.smallBox({
										height: 50,
										title : resp.responseStatus,
										content : d,
										color : "#dc3912",
										sound_file: "smallbox",
										timeout: w
										//icon : "fa fa-bell swing animated"
									});
						});
					
				}	
                // Hide loder
                $('.page-loader').addClass('hidden');
            },
            error: function(xhr, ajaxOptions, thrownError) {
                $.smallBox({
                    title : "Error",
                    content : xhr.statusText,
                    color : "#dc3912",
                    timeout: 3000
                    //icon : "fa fa-bell swing animated"
                });
                // Hide loder
                $('.page-loader').addClass('hidden');
            }
        });

    });

    // Edit 
    //$('#dataTableMerchant').on('click', '.btn-edit', function() {
        //$('#modal-city').modal('show');
        //$('#modal-city .modal-title').html('Edit City');
        //$('#email').attr('readonly','readonly');
        //$('#div-m-version').show();
       
        // Hide loder
        // $('.page-loader').removeClass('hidden');

        // // Get data
        // // Send data
        // $.ajax({
        //     url: baseUrl + '/merchant/' + $(this).data('id'),
        //     type: 'GET',
        //     success: function(resp) {
        //         if(resp.responseCode === 200) {
        //             //console.log(resp.responseMessage[0].country.id);

        //             $('#merchant-id').val(resp.responseMessage[0].id);
        //             $('#merchant-name').val(resp.responseMessage[0].name);
        //             $('#merchant-company').val(resp.responseMessage[0].company);
        //             $('#merchant-address').val(resp.responseMessage[0].address);
        //             $('#merchant-district').val(resp.responseMessage[0].state.id).trigger('change');
        //             $('#merchant-district').focus();
        //             $('#merchant-merchantType').val(resp.responseMessage[0].state.id).trigger('change');
        //             $('#merchant-merchantType').focus();
        //             $('#version-zipcode').val(resp.responseMessage[0].zipcode);

        //         }
        //         else
        //         {
        //             $.smallBox({
        //                 height: 50,
        //                 title : "Error",
        //                 content : resp.responseMessage,
        //                 color : "#dc3912",
        //                 sound_file: "smallbox",
        //                 timeout: 3000
        //                 //icon : "fa fa-bell swing animated"
        //             });
        //         }
               
        //         // Hide loder
        //         $('.page-loader').addClass('hidden');
        //     },
        //     error: function(xhr, ajaxOptions, thrownError) {
        //         $.smallBox({
        //             title : "Error",
        //             content : xhr.statusText,
        //             color : "#dc3912",
        //             timeout: 3000
        //             //icon : "fa fa-bell swing animated"
        //         });
        //         // Hide loder
        //         $('.page-loader').addClass('hidden');
        //     }
        // });
   // });

    $('#dataTableDeviceprofile').on('click', '.btn-delete', function() {

        let text;
        if (confirm("Are you sure to remove!") == true) {
            
             // Send data
            $.ajax({
                url:  baseUrl + '/device-profile/delete',
                type: 'POST',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data: {
                    'id': $(this).data('id'),
                    'version': $(this).data('version')
                },
                success: function(resp) {
                    if(resp.responseCode === 200) {
                        // Reload datatable
                        dataTableDeviceprofile.ajax.reload();
                        
                        // Send success message
                        $.smallBox({
                            height: 50,
                            title : "Success",
                            content : resp.responseMessage,
                            color : "#109618",
                            sound_file: "voice_on",
                            timeout: 3000
                            //icon : "fa fa-bell swing animated"
                        });
                    } else {
                        $.smallBox({
                            height: 50,
                            title : "Error",
                            content : resp.responseMessage,
                            color : "#dc3912",
                            sound_file: "smallbox",
                            timeout: 3000
                            //icon : "fa fa-bell swing animated"
                        });
                    }
                    // Hide loder
                    $('.page-loader').addClass('hidden');
                },
                error: function(xhr, ajaxOptions, thrownError) {
                    $.smallBox({
                        title : "Error",
                        content : xhr.statusText,
                        color : "#dc3912",
                        timeout: 3000
                        //icon : "fa fa-bell swing animated"
                    });
                    // Hide loder
                    $('.page-loader').addClass('hidden');
                }
            });

       
        } 

    });


    


})(jQuery);
(function($) {
    "use strict";
    $('#dataTableCapk').wrap('<div class="dataTables_scroll" />');
    var dataTableCapk = null;
    if ($('#dataTableCapk').length) {
        // You can use 'alert' for alert message
        // or throw to 'throw' javascript error
        // or none to 'ignore' and hide error
        // or you own function
        // please read https://datatables.net/reference/event/error
        // for more information
        $.fn.dataTable.ext.errMode = 'none';
        
        dataTableCapk =  $('#dataTableCapk').DataTable({
        processing: true,
        serverSide: true,
        //dom : '',
        dom: 'lrtip',
        "searching": false,
        //"scrollX": true,
        //"sScrollX": '100%',
        //"sScrollXInner": "110%",
        pageLength: 10,
        lengthMenu: [[10, 25, 50, 100], [10, 25, 50, 100]],
        pagingType: 'full_numbers',
        ajax: {
            url:   baseUrl+"/capk-datatable",
            type: 'GET',
            data:  function(d){
                d.name= $('#search-capk').val();
                
            }
        },
        language: {
            
        },
        //rowId: 'TRANSPORT_ID',
        columns: [
            {data: "DT_RowIndex", 
               sortable: false, 
               searchable: false,
               "render": function (data, type, row, meta) {      
                          return meta.row + meta.settings._iDisplayStart + 1;     
               }  
            },
            {data: "version", name: "version", visible : false},
            {data: "name", name: "name"},
            {data: "idx", name: "idx"}, 
            {data: "rid", name: "rid"},   
            {data: "hash", name: "hash"},   
            {data: "expiryDate", name: "expiryDate"},   
            {data: "remark", name: "remark"},   
            {data: "id", sortable: false, searchable: false, }
        ],
		
        columnDefs:[
			{
                "targets": 0, // your case first column
                "className": "text-center",
            },
			{
                "targets": 2, 
                "className": "mleft",
				 render: function(d,data,row) {
					 return `<span>`+d+`</span>`;
				 }
            },
			{
                "targets": 3, 
                "className": "mleft",
				 render: function(d,data,row) {
					 return `<span>`+d+`</span>`;
				 }
            },
			{
                "targets": 4, 
                "className": "mleft",
				 render: function(d,data,row) {
					 return `<span>`+d+`</span>`;
				 }
            },
			{
                "targets": 6, 
                "className": "mleft",
				 render: function(d,data,row) {
					 return `<span>`+d+`</span>`;
				 }
            },
			{
                "targets": 7, 
                "className": "mleft",
				 render: function(d,data,row) {
					 return `<span>`+d+`</span>`;
				 }
            },
	        {
                targets: 8,
				"className": "text-center padd-row-table",
                render: function(d,data,row) {
                 
                    let v = row.version;
                    let id = d;
                   
					return `
                    <span>
                    <a href="capk/`+d+`" data-id="`+d+`"><img class="pencil" src="`+baseUrl+`/assets/images/icon/pencil.png" /></a>&nbsp;
                    <a class="btn-delete" data-id="`+d+`" data-version="`+v+`"><img class="trash" src="`+baseUrl+`/assets/images/icon/trash.png" /></a>
                    </span>
                    `;
                }
            }
        ]
        });
      
    }

    
    $('#btn-search-capk').click(function() {
        dataTableCapk.draw(true);
    });

      /* filter tugel */
     // Toggle filter
    //  $('.filter .tugel').click(function() {
    //     $(this).toggleClass('open');
    //     $('.filter-title').toggleClass('hidden');
    //     $('.form-filter form').toggleClass('open-filter');
        
    // });

    // clear
    $('.clear_filter').click(function() {
        $('#search-capk').val('');
       
        dataTableCapk.draw(true);
    });

    // Add New  or update
    $('#btn-submit-capk').click(function(){

        // Update when city id has value
        var url = baseUrl + '/capk/update';
        var action = "update";
        if(!$('#capk-id').val()) {
            url = baseUrl + '/capk/save';
            action = "save";
        }

        if($('#capk-id').val()) {
            if(!$('#version-capk').val()) {
                $.smallBox({
                    //height: 50,
                    title : "Error",
                    content : 'Version can\'t be empty',
                    color : "#dc3912",
                    sound_file: "smallbox",
                    timeout: 3000
                });
                $('#version-capk').focus();
                return;
            }
        }

        // Has error
        // var hasError = false;
        // Check requirement input
        if(!$('#capk-name').val()) {
            $.smallBox({
                //height: 50,
                title : "Error",
                content : 'Name  can\'t be empty',
                color : "#dc3912",
                sound_file: "smallbox",
                timeout: 3000
            });
            $('#capk-name').focus();
            return;
        }

        if(!$('#capk-idx').val()) {
            $.smallBox({
                //height: 50,
                title : "Error",
                content : 'Capk can\'t be empty',
                color : "#dc3912",
                sound_file: "smallbox",
                timeout: 3000
            });
            $('#capk-idx').focus();
            return;
        }
		
		
		 if(!$('#capk-rid').val()) {
            $.smallBox({
                //height: 50,
                title : "Error",
                content : 'Rid can\'t be empty',
                color : "#dc3912",
                sound_file: "smallbox",
                timeout: 3000
            });
            $('#capk-rid').focus();
            return;
        }
		
		if(!$('#capk-modulus').val()) {
            $.smallBox({
                //height: 50,
                title : "Error",
                content : 'Modulus can\'t be empty',
                color : "#dc3912",
                sound_file: "smallbox",
                timeout: 3000
            });
            $('#capk-modulus').focus();
            return;
        }
		
	

		if(!$('#capk-exponent').val()) {
            $.smallBox({
                //height: 50,
                title : "Error",
                content : 'Exponent can\'t be empty',
                color : "#dc3912",
                sound_file: "smallbox",
                timeout: 3000
            });
            $('#capk-exponent').focus();
            return;
        }
		
		if(!$('#capk-algo').val()) {
            $.smallBox({
                //height: 50,
                title : "Error",
                content : 'algo can\'t be empty',
                color : "#dc3912",
                sound_file: "smallbox",
                timeout: 3000
            });
            $('#capk-algo').focus();
            return;
        }
		
		if(!$('#capk-hash').val()) {
            $.smallBox({
                //height: 50,
                title : "Error",
                content : 'Hash can\'t be empty',
                color : "#dc3912",
                sound_file: "smallbox",
                timeout: 3000
            });
            $('#capk-hash').focus();
            return;
        }
		
		
		
		// Show loder
        $('.page-loader').removeClass('hidden');
		$('.loading>img').removeClass('hidden');

        // Send data
        $.ajax({
            url: url,
            type: 'POST',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: {
                'id': $('#capk-id').val(),
                'name' : $('#capk-name').val(),
                'idx' : $('#capk-idx').val(),
                'rid' : $('#capk-rid').val(),
                'modulus' : $('#capk-modulus').val(),
                'exponent' : $('#capk-exponent').val(),
                'algo' : $('#capk-algo').val(),
                'hash' : $('#capk-hash').val(),
                'expiryDate' : $('#capk-expiryDate').val(),
                'remark' : $('#capk-remark').val(),
                'version': $('#version-capk').val(),
                
            },
		
            success: function(resp) {
				
				
                if(resp.responseCode == '0000') { //sukses
                   
                    // Send success message
                    $.smallBox({
                        height: 50,
                        title : resp.responseStatus, 
                        content : resp.responseMessage,
                        color : "#109618",
                        sound_file: "voice_on",
                        timeout: 3000
                        //icon : "fa fa-bell swing animated"
                    });

                    if($('#capk-id').val()=="") {
                       
						$('#capk-name').val('');
                        $('#capk-idx').val('');
						$('#capk-rid').val();
						$('#capk-modulus').val('');
						$('#capk-exponent').val('');
						$('#capk-algo').val('');
						$('#capk-hash').val('');
						$('#capk-expiryDate').val('');
						$('#capk-remark').val('');
                    }

 
                } else if(resp.responseCode == '3333' || resp.responseCode == '0400'){ //exception and not found
					
					 $.smallBox({
                        height: 50,
                        title : resp.responseStatus,
                        content : resp.responseMessage,
                        color : "#dc3912",
                        sound_file: "voice_on",
                        timeout: 3000
                        //icon : "fa fa-bell swing animated"
                    });
                }
				else if(resp.responseCode == '5555')//validaator
				{
					
						 var data = Object.values(resp.responseMessage);
						 var ln = data.length;
						 var w  = 3000 * ln; 
						 data.map(function(d){
							 
								$.smallBox({
										height: 50,
										title : resp.responseStatus,
										content : d,
										color : "#dc3912",
										sound_file: "smallbox",
										timeout: w
										//icon : "fa fa-bell swing animated"
									});
						});
					
				}	
                // Hide loder
                $('.page-loader').addClass('hidden');
				$('.loading>img').addClass('hidden');
            },
            error: function(xhr, ajaxOptions, thrownError) {
                $.smallBox({
                    title : "Error",
                    content : xhr.statusText,
                    color : "#dc3912",
                    timeout: 3000
                    //icon : "fa fa-bell swing animated"
                });
                // Hide loder
                $('.page-loader').addClass('hidden');
				$('.loading>img').addClass('hidden');
            }
        });

    });

    // Edit 
    $('#dataTableCapk').on('click', '.btn-delete', function() {

        let text;
        if (confirm("Are you sure to remove!") == true) {
            
             // Send data
            $.ajax({
                url:  baseUrl + '/capk/delete',
                type: 'POST',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data: {
                    'id': $(this).data('id'),
                    'version': $(this).data('version')
                },
                success: function(resp) {
                    if(resp.responseCode === 200) {
                        // Reload datatable
                        dataTableCapk.ajax.reload();
                        
                        // Send success message
                        $.smallBox({
                            height: 50,
                            title : "Success",
                            content : resp.responseMessage,
                            color : "#109618",
                            sound_file: "voice_on",
                            timeout: 3000
                            //icon : "fa fa-bell swing animated"
                        });
                    } else {
                        $.smallBox({
                            height: 50,
                            title : "Error",
                            content : resp.responseMessage,
                            color : "#dc3912",
                            sound_file: "smallbox",
                            timeout: 3000
                            //icon : "fa fa-bell swing animated"
                        });
                    }
                    // Hide loder
                    $('.page-loader').addClass('hidden');
                },
                error: function(xhr, ajaxOptions, thrownError) {
                    $.smallBox({
                        title : "Error",
                        content : xhr.statusText,
                        color : "#dc3912",
                        timeout: 3000
                        //icon : "fa fa-bell swing animated"
                    });
                    // Hide loder
                    $('.page-loader').addClass('hidden');
                }
            });

       
        } 

    });


    function isJson(str) {
	  try {
		JSON.parse(str);
	  } catch (e) {
		return false;
	  }  
	  return true;
	}


})(jQuery);
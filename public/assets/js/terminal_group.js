(function($) {
    "use strict";
    $('#dataTableTerminalGroup').wrap('<div class="dataTables_scroll" />');
    var dataTableTerminalGroup = null;
    if ($('#dataTableTerminalGroup').length) {
        // You can use 'alert' for alert message
        // or throw to 'throw' javascript error
        // or none to 'ignore' and hide error
        // or you own function
        // please read https://datatables.net/reference/event/error
        // for more information
        $.fn.dataTable.ext.errMode = 'none';
        
        dataTableTerminalGroup =  $('#dataTableTerminalGroup').DataTable({
        processing: true,
        serverSide: true,
        //dom : '',
        dom: 'lrtip',
        "searching": false,
        //"scrollX": true,
        //"sScrollX": '100%',
        //"sScrollXInner": "110%",
        pageLength: 10,
        lengthMenu: [[10, 25, 50, 100], [10, 25, 50, 100]],
        pagingType: 'full_numbers',
        ajax: {
            url:   baseUrl+"/terminal-group-datatables",
            type: 'GET',
            data:  function(d){
                d.name= $('#search-name-terminal-group').val();
                d.terminalId =  $('#search-terminal-id-g').val();
               
            }
        },
        language: {
            
        },
        //rowId: 'TRANSPORT_ID',
        columns: [
            {data: "DT_RowIndex", 
               sortable: false, 
               searchable: false,
               "render": function (data, type, row, meta) {      
                          return meta.row + meta.settings._iDisplayStart + 1;     
               }  
            },
            {data: "version", name: "version", visible:false},
            {data: "name", name: "name"}, 
            {data: "description", name: "description"}, 
            {data: "id", sortable: false, searchable: false, }
        ],
        columnDefs:[
		    {
                "targets": 0,
                "className": "text-center",
            },
		    {
                "targets": 1,
                "className": "mleft",
                render: function(d,data,row) {
                    return `<span>`+d+`</span>`;
                }

            },
            {
                "targets": 2,
                "className": "mleft",
                render: function(d,data,row) {
                    return `<span>`+d+`</span>`;
                }

            },
			{
                "targets": 3,
                "className": "mleft",
                render: function(d,data,row) {
                    return `<span>`+d+`</span>`;
                }

            },
			{
                targets: 4,
				"className": "text-center padd-row-table",
                render: function(d,data,row) {
                 
                    let v = row.version;
                    let id = d;
                    //let lock = row.locked;
                    return `<span>
					 <a href="terminal-group/detail/`+d+`" data-id="`+d+`" tooltip="Detail"><img class="mata" src="`+baseUrl+`/assets/images/icon/mata.png" /></a>&nbsp;
                     <a href="terminal-group/`+d+`" data-id="`+d+`"><img class="pencil" src="`+baseUrl+`/assets/images/icon/pencil.png" /></a>&nbsp;
                     <a class="btn-delete" data-id="`+d+`" data-version="`+v+`"><img class="trash" src="`+baseUrl+`/assets/images/icon/trash.png" /></a>
                   
					</span>
                    `;
                }
            }
        ]
        });
      
    }

    
    $('#btn-ti-search-terminal-group').click(function() {
        dataTableTerminalGroup.draw(true);
       
    });

      /* filter tugel */
     // Toggle filter
     $('.filter .tugel').click(function() {
        $(this).toggleClass('open');
        $('.filter-title').toggleClass('hidden');
        $('.form-filter form').toggleClass('open-filter');
        
    });

    // clear
    $('.clear_filter').click(function() {
        $('#search-name-terminal-group').val('');
        dataTableTerminalGroup.draw(true);
    });
	
    // Add New  or update
	$('body').on('click', '#btn-submit-terminal-group', function() {
	
        // Update when city id has value
        var url = baseUrl + '/terminal-group/update';
        var action = "update";
        if(!$('#terminal-g-id').val()) {
            url = baseUrl + '/terminal-group/save';
            action = "save";
        }

        if($('#terminal-g-id').val()) {
            if(!$('#version-terminal-g').val()) {
                $.smallBox({
                    //height: 50,
                    title : "Error",
                    content : 'Version can\'t be empty',
                    color : "#dc3912",
                    sound_file: "smallbox",
                    timeout: 3000
                });
                $('#version-terminal-g').focus();
                return;
            }
        }

        // Has error
        // var hasError = false;
        // Check requirement input
        if(!$('#terminal-g-name').val()) {
            $.smallBox({
                //height: 50,
                title : "Error",
                content : 'Name can\'t be empty',
                color : "#dc3912",
                sound_file: "smallbox",
                timeout: 3000
            });
            $('#terminal-g-name').focus();
            return;
        }

        if(!$('#terminal-g-description').val()) {
            $.smallBox({
                //height: 50,
                title : "Error",
                content : 'Description can\'t be empty',
                color : "#dc3912",
                sound_file: "smallbox",
                timeout: 3000
            });
            $('#terminal-g-description').focus();
            return;
        }

        var arlist = []; 
        $("#table-form-terminal-group>tbody>tr").each(function(){
            let d = $(this).find('.uid').text();
            arlist.push(d);
        }); 

        /*if(arlist.length == 0)
		{
			$.smallBox({
                //height: 50,
                title : "Error",
                content : 'Terminal can\'t be empty',
                color : "#dc3912",
                sound_file: "smallbox",
                timeout: 3000
            });
            return;
		}*/

        
        // Show loder
        $('.page-loader').removeClass('hidden');

        // Send data
        $.ajax({
            url: url,
            type: 'POST',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: {
                "id": $('#terminal-g-id').val(),
                "version": $('#version-terminal-g').val(),
                "name" : $('#terminal-g-name').val(),
                "description" : $('#terminal-g-description').val(),
                "terminalIds" : JSON.stringify(arlist)
                
            },
            success: function(resp) {
               
				if(resp.responseCode == '0000') { //sukses
						// Send success message
						$.smallBox({
							height: 50,
							title : resp.responseStatus, 
							content : resp.responseMessage,
							color : "#109618",
							sound_file: "voice_on",
							timeout: 3000
							//icon : "fa fa-bell swing animated"
						});
                        
						if($('#terminal-g-id').val()=="") {
                       
							$('#terminal-g-name').val("");
							$('#terminal-g-description').val("");
							//$('#list-group').html('');
						  
						}
						else
						{
							window.location.href=  baseUrl+"/terminal-group";
						}
                        
                   
				
 
                } else if(resp.responseCode == '3333' || resp.responseCode == '0400'){ //exception and not found
					
					 $.smallBox({
                        height: 50,
                        title : resp.responseStatus,
                        content : resp.responseMessage,
                        color : "#dc3912",
                        sound_file: "voice_on",
                        timeout: 3000
                        //icon : "fa fa-bell swing animated"
                    });
                }
				else if(resp.responseCode == '5555')//validaator
				{
					
						 var data = Object.values(resp.responseMessage);
						 var ln = data.length;
						 var w  = 3000 * ln; 
						 data.map(function(d){
							 
								$.smallBox({
										height: 50,
										title : resp.responseStatus,
										content : d,
										color : "#dc3912",
										sound_file: "smallbox",
										timeout: w
										//icon : "fa fa-bell swing animated"
									});
						});
					
				}	
                // Hide loder
                $('.page-loader').addClass('hidden');
            },
            error: function(xhr, ajaxOptions, thrownError) {
                $.smallBox({
                    title : "Error",
                    content : xhr.statusText,
                    color : "#dc3912",
                    timeout: 3000
                    //icon : "fa fa-bell swing animated"
                });
                // Hide loder
                $('.page-loader').addClass('hidden');
            }
        });

    });
    $('#dataTableTerminalGroup').on('click', '.btn-delete', function() {
 
        let text;
        if (confirm("Are you sure to remove!") == true) {
            
             // Send data
            $.ajax({
                url:  baseUrl + '/terminal-group/delete',
                type: 'POST',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data: {
                    'id': $(this).data('id'),
                    'version': $(this).data('version')
                },
                success: function(resp) {
                    if(resp.responseCode === 200) {
                        // Reload datatable
                        dataTableTerminalGroup.ajax.reload();
                        // Send success message
                        $.smallBox({
                            height: 50,
                            title : "Success",
                            content : resp.responseMessage,
                            color : "#109618",
                            sound_file: "voice_on",
                            timeout: 3000
                            //icon : "fa fa-bell swing animated"
                        });
                    } else {
                        $.smallBox({
                            height: 50,
                            title : "Error",
                            content : resp.responseMessage,
                            color : "#dc3912",
                            sound_file: "smallbox",
                            timeout: 3000
                            //icon : "fa fa-bell swing animated"
                        });
                    }
                    // Hide loder
                    $('.page-loader').addClass('hidden');
                },
                error: function(xhr, ajaxOptions, thrownError) {
                    $.smallBox({
                        title : "Error",
                        content : xhr.statusText,
                        color : "#dc3912",
                        timeout: 3000
                        //icon : "fa fa-bell swing animated"
                    });
                    // Hide loder
                    $('.page-loader').addClass('hidden');
                }
            });

       
        } 

    });
	
	
	// terminal list by termilangroupId when klik detail view
	$('#table_form_terminal_group_sn_terminal').wrap('<div class="dataTables_scroll" />');
	var table_form_terminal_group_sn_terminal = null;
	if ($('#table_form_terminal_group_sn_terminal').length) {
        // You can use 'alert' for alert message
        // or throw to 'throw' javascript error
        // or none to 'ignore' and hide error
        // or you own function
        // please read https://datatables.net/reference/event/error
        // for more information
        $.fn.dataTable.ext.errMode = 'none';
        
        table_form_terminal_group_sn_terminal =  $('#table_form_terminal_group_sn_terminal').DataTable({
        processing: true,
        serverSide: true,
        //dom : '',
        dom: 'lrtip',
        "searching": false,
        //"scrollX": true,
        //"sScrollX": '100%',
        //"sScrollXInner": "110%",
        pageLength: 10,
        lengthMenu: [[10, 25, 50, 100], [10, 25, 50, 100]],
        pagingType: 'full_numbers',
        ajax: {
            url:   baseUrl+"/terminal-group-terminal-datatables",
            type: 'GET',
            data:  function(d){
                d.id= $('#terminal_g_detail_id').text();
                
            }
        },
        language: {
            
        },
        //rowId: 'TRANSPORT_ID',
        columns: [
            {data: "DT_RowIndex", 
               sortable: false, 
               searchable: false,
               "render": function (data, type, row, meta) {      
                          return meta.row + meta.settings._iDisplayStart + 1;     
               }  
            },
            {data: "version", name: "version", visible:false},
            {data: "sn", name: "sn"}, 
            {data: "terminalId", name: "terminalId",  visible:false}, 
            {data: "id", sortable: false, searchable: false, }
        ],
        columnDefs:[
			{
                "targets": 0,
                "className": "text-center",
            },
			{
                "targets": 2,
                "className": "text-center",
				
            },
            {
                targets: 4,
				"className": "text-center padd-row-table",
                render: function(d,data,row) {
                 
                    let v = row.version;
                    let sn = row.sn;
                    let id = d;
                    let tId = row.terminalId;
                    
                    return `
                     <span>
					 <a class="btn-delete-list-t" data-id="`+d+`" data-tid="`+tId+`" data-version="`+v+`"><img class="trash" src="`+baseUrl+`/assets/images/icon/trash.png" /></a>
					 </span>
                    `;
                }
            }
        ]
        });
      
    }
	
	/* delete from list terminal view */ 
	$('body').on('click', '.btn-delete-list-t', function() {
		
		  var id = $(this).data('id');
		  var tId = [$(this).data('tid')];
		  var version = $(this).data('version');
		  
		  let text = "Do you want to delete. Press button OK!\nEither Cancel.";
		  if (confirm(text) == true) {
			
			$('.page-loader').removeClass('hidden');
			$.ajax({
					url: baseUrl + '/terminal-group/deleteTerminals',
					type: 'POST',
					headers: {
						'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
					},
					 data: {
						"terminalGroupId" : id,
						"version" : version,
						"terminalIds" : JSON.stringify(tId)
					},
			
					success: function(resp) {
						if(resp.responseCode == '0000') { //sukses
							// Send success message
							$.smallBox({
								height: 50,
								title : resp.responseStatus, 
								content : resp.responseMessage,
								color : "#109618",
								sound_file: "voice_on",
								timeout: 3000
								//icon : "fa fa-bell swing animated"
							});
							table_form_terminal_group_sn_terminal.draw(true); //disini
							//var d = table_form_terminal_group_sn_terminal.row(0).data();
							//update version terminal group
							//console.log(d);
							//$('#terminal-g-id').val(d.id);
							//var id = parseInt($('#version-terminal-g').val()) + 1 ;
							//$('#version-terminal-g').val(id)
							getTerminalGroup($('#terminal_g_detail_id').text());
							
					
						} else if(resp.responseCode == '3333' || resp.responseCode == '0200' || resp.responseCode == '0001'){ //exception and not found and exis
							
							 $.smallBox({
								height: 50,
								title : resp.responseStatus,
								content : resp.responseMessage,
								color : "#dc3912",
								sound_file: "voice_on",
								timeout: 3000
								//icon : "fa fa-bell swing animated"
							});
						}
						else if(resp.responseCode == '5555')//validaator
						{
							
								 var data = Object.values(resp.responseMessage);
								 var ln = data.length;
								 var w  = 3000 * ln; 
								 data.map(function(d){
									 
										$.smallBox({
												height: 50,
												title : resp.responseStatus,
												content : d,
												color : "#dc3912",
												sound_file: "smallbox",
												timeout: w
												//icon : "fa fa-bell swing animated"
											});
								});
							
						}	
						
				
						$('.page-loader').addClass('hidden');
					},
					error: function(xhr, ajaxOptions, thrownError) {
						$("#process_id").val("");
						$.smallBox({
							title : "Error",
							content : xhr.statusText,
							color : "#dc3912",
							sound_file: "smallbox",
							timeout: 3000
						});
						$('.page-loader').addClass('hidden');
					}
			});
			
			
		  } 
		  
    });

	
	
	//------------------------------------------------
   
  

    /* list terminal modal */
    $('#dataTableTerminalListG').wrap('<div class="dataTables_scroll" />');
    var dataTableTerminalListG = null;
    if ($('#dataTableTerminalListG').length) {
        // You can use 'alert' for alert message
        // or throw to 'throw' javascript error
        // or none to 'ignore' and hide error
        // or you own function
        // please read https://datatables.net/reference/event/error
        // for more information
        $.fn.dataTable.ext.errMode = 'none';
        
        dataTableTerminalListG =  $('#dataTableTerminalListG').DataTable({
        processing: true,
        serverSide: true,
        //dom : '',
        dom: 'lrtip',
        "searching": false,
        //"scrollX": true,
        //"sScrollX": '100%',
        //"sScrollXInner": "110%",
        pageLength: 10,
        lengthMenu: [[10, 25, 50, 100], [10, 25, 50, 100]],
        pagingType: 'full_numbers',
        ajax: {
            url:   baseUrl+"/terminal-datatables",
            type: 'GET',
            data:  function(d){
                d.name= $('#search-name').val();
                
            }
        },
        language: {
            
        },
        //rowId: 'TRANSPORT_ID',
        columns: [
            {data: "DT_RowIndex", 
               sortable: false, 
               searchable: false,
               "render": function (data, type, row, meta) {      
                          return meta.row + meta.settings._iDisplayStart + 1;     
               }  
            },
            {data: "version", name: "version"},
            {data: "sn", name: "sn"}, 
            {data: "modelName", name: "modelName"}, 
            {data: "id", sortable: false, searchable: false, class: "action"}
        ],
        columnDefs:[
			{
                "targets": 0,
                "className": "text-center",
            },
            {
                targets: 4,
				"className": "text-center padd-row-table",
                render: function(d,data,row) {
                 
                    let v = row.version;
                    let id = d;
                    let lock = row.locked;
                    return `
                     <span><button type="button" class="btn btn-flat btn-success btn-xs btn-select-terminal" data-id="`+d+`" data-version="`+v+`">Select</button>&nbsp;
					 </span>
                    `;
                }
            }
        ]
        });
      
    }

   
    $('#search-terminal-id-g').on('click',function(){
   
        $('#modal-list-terminal').modal('show');
        $('#modal-list-terminal .modal-title').html('List Terminal');
        dataTableTerminalListG.draw(true);

    });
    
    $('#dataTableTerminalListG').on('click', '.btn-select-terminal', function() {
        $("#search-terminal-id-g").val($(this).data('id'));
        $('#modal-list-terminal').modal('hide');
    });
	
	/* list terminal import temp form */
    	$('#dataTableListSN_TG_Result').wrap('<div class="dataTables_scroll" />');
	var dataTableListSN_TG_Result = null;
	if ($('#dataTableListSN_TG_Result').length) {
			
		//$.fn.dataTable.ext.errMode = 'none';
        
        dataTableListSN_TG_Result =  $('#dataTableListSN_TG_Result').DataTable({
				dom : '<"dt-toolbar row"<"col-sm-6 col-xs-12 "l><"col-sm-6 col-xs-12 text-right"<"toolbar">>>rt<"dt-toolbar-footer"<"col-md-6 "i><"col-md-6  "p>><"clear">',
				//processing: true,
				//serverSide: true,
				dom: 'lrtip',
				"searching": true,
				//"scrollX": true,
				//"sScrollX": '100%',
				//"sScrollXInner": "110%",
				//pageLength: 10,
				//lengthMenu: [[10, 25, 50, 100], [10, 25, 50, 100]],
				//pagingType: 'full_numbers',
				"lengthChange": false,
				language: {
					paginate : {
						first : '&laquo;',
						last : '&raquo;',
						next : '&rsaquo;',
						previous : '&lsaquo;',
					}
				},
				columnDefs:[
					{
						"targets": 0,
						"className": "text-center",
					},
					{
						"targets": 1,
						"className": "text-center",
					},
					{
						targets: 2,
						"className": "text-center padd-row-table",
						render: function(d,data,row) {
						 
							//let v = row.version;
							//let id = d;
							var f = '<span style="color:#5A84E9;" class="result">'+d+'</span>'
							if(d!=='found')
							{
								f = '<span style="color:#EE6F6F;" class="result">'+d+'</span>'
							}
								
							return f;
						}
					}
				],  
				
				'select' : {
						style : 'single'
					},
        });
		
	}
	

    /* list terminal modal form */
    $('#dataTableTerminalListGForm').wrap('<div class="dataTables_scroll" />');
    var dataTableTerminalListGForm = null;
    if ($('#dataTableTerminalListGForm').length) {
        // You can use 'alert' for alert message
        // or throw to 'throw' javascript error
        // or none to 'ignore' and hide error
        // or you own function
        // please read https://datatables.net/reference/event/error
        // for more information
        $.fn.dataTable.ext.errMode = 'none';        
        dataTableTerminalListGForm =  $('#dataTableTerminalListGForm').DataTable({
        dom : '<"dt-toolbar row"<"col-sm-6 col-xs-12 "l><"col-sm-6 col-xs-12 text-right"<"toolbar">>>rt<"dt-toolbar-footer"<"col-md-6 "i><"col-md-6  "p>><"clear">',
        processing: true,
        serverSide: true,
        //dom: 'lrtip',
        dom: 'tip',
        "searching": false,
        //"scrollX": true,
        //"sScrollX": '100%',
        //"sScrollXInner": "110%",
        pageLength: 10,
        lengthMenu: [[10, 25, 50, 100], [10, 25, 50, 100]],
        pagingType: 'full_numbers',
        ajax: {
            url:   baseUrl+"/terminal-datatables",
            type: 'GET',
            data:  function(d){
                d.sn= $('#search-sn').val();
            }
        },
        language: {
            paginate : {
				first : '&laquo;',
				last : '&raquo;',
				next : '&rsaquo;',
				previous : '&lsaquo;',
			}
        },
        //rowId: 'TRANSPORT_ID',
        columns: [
            {data: "DT_RowIndex", 
               sortable: false, 
               searchable: false,
               "render": function (data, type, row, meta) {      
                          return meta.row + meta.settings._iDisplayStart + 1;     
               }  
            },
            {data: "sn", name: "sn"},
            {data: "modelName", name: "modelName"},
            {data : "merchantName", name : "merchantName"},
            {data: "id", sortable: false, searchable: false, }
        ],
        columnDefs:[
			{
                "targets": 0,
                "className": "text-center",
            },
			{
                "targets": 1,
                "className": "mleft",
				 render: function(d,data,row) {
					 return `<span>`+d+`</span>`
				 }
            },
			{
                "targets": 2,
                "className": "mleft",
				 render: function(d,data,row) {
					 return `<span>`+d+`</span>`
				 }
            },
			{
                "targets": 3,
                "className": "mleft",
				 render: function(d,data,row) {
					 return `<span>`+d+`</span>`
				 }
            },
            {
                targets: 4,
				"className": "text-center",
                render: function(d,data,row) {
                    return `<span>
                       <input 
                            class="checkbox"
                            type="checkbox" 
                            name="ch[]" 
                            value="true"  
                            data-id="`+d+`"
                            data-sn="`+row.sn+`"
                       />
					   </span>
                     `;
                 }
                
            }
        ],
            'select' : {
                style : 'single'
            },
        });

        //$("div.toolbar").html(`
        //   <input type="text" class="form-control form-control-sm input-xs" id="search-sn"/>
        //`);
        
    }

    $('body').on('click', '#btn-search-sn', function() {
        dataTableTerminalListGForm.ajax.reload();
    });
    $('body').on('click', '#btn-clear-se', function() {
        $("#search-sn").val("");
        dataTableTerminalListGForm.ajax.reload();
    });
	
	//click import  result to db 
	$('body').on('click', '#btn-submit-import-result', function() {
		var arlist = [];
		$('#dataTableListSN_TG_Result>tbody>tr').each(function(){
			var tid = $(this).find('.t_id').text();
			var res = $(this).find('.result').text();
			if(res=='found')
			{
				arlist.push(tid);
				//console.log(tid);
			}	
		});
		
		$('.page-loader').removeClass('hidden');
        $.ajax({
            url: baseUrl + '/terminal-group/addTerminals',
            type: 'POST',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
             data: {
				"terminalGroupId" : $('#idg').val(),
				"version" : $('#version-terminal-gim').val(),
				"terminalIds" : JSON.stringify(arlist)
			},
			
            success: function(resp) {
                if(resp.responseCode == '0000') { //sukses
                    // Send success message
                    $.smallBox({
                        height: 50,
                        title : resp.responseStatus, 
                        content : resp.responseMessage,
                        color : "#109618",
                        sound_file: "voice_on",
                        timeout: 3000
                        //icon : "fa fa-bell swing animated"
                    });
					
					//$('#modal-list-terminal-form').modal('hide');
					//table_form_terminal_group_sn_terminal.draw(true);
					window.location.href = baseUrl+"/terminal-group/detail/"+$('#idg').val();
					//getTerminalGroup($('#terminal_g_detail_id').text());
					
                    

                } else if(resp.responseCode == '3333' || resp.responseCode == '0200' || resp.responseCode == '0001'){ //exception and not found and exis
					
					 $.smallBox({
                        height: 50,
                        title : resp.responseStatus,
                        content : resp.responseMessage,
                        color : "#dc3912",
                        sound_file: "voice_on",
                        timeout: 3000
                        //icon : "fa fa-bell swing animated"
                    });
                }
				else if(resp.responseCode == '5555')//validaator
				{
					
						 var data = Object.values(resp.responseMessage);
						 var ln = data.length;
						 var w  = 3000 * ln; 
						 data.map(function(d){
							 
								$.smallBox({
										height: 50,
										title : resp.responseStatus,
										content : d,
										color : "#dc3912",
										sound_file: "smallbox",
										timeout: w
										//icon : "fa fa-bell swing animated"
									});
						});
					
				}	
				
				
                $('.page-loader').addClass('hidden');
            },
            error: function(xhr, ajaxOptions, thrownError) {
                $("#process_id").val("");
				$.smallBox({
                    title : "Error",
                    content : xhr.statusText,
                    color : "#dc3912",
                    sound_file: "smallbox",
                    timeout: 3000
                });
                $('.page-loader').addClass('hidden');
            }
        }); 
		
	});
	/*
	$('#dataTableTerminalListGForm').on('change', '.checkbox', function() {
		if(this.checked) {
		   alert($(this).data('id'));
		   //
		   $.ajax({
			  type: 'GET',
			  url: baseUrl +"/terminal-group-terminal-datatables"
			  data: {id: $('#terminal-g-id').val(), },
			  dataType: 'json',
			  success: function (data) {
				var names = data
				$('#cand').html(data);
			  }
			});
		}
	});*/
	
	/* whern show groud detail */
	var gidx = $('#terminal_g_detail_id').text().trim();
	if(gidx!=="")
	{
		//alert($('#terminal-g-detail-id').val());
		getTerminalGroup(gidx);
	}	

    $('body').on('click', '#add-terminal-to-list', function() {
	  
        $('#modal-list-terminal-form').modal('show');
        $('#modal-list-terminal-form .modal-title').html('Select List Terminal');
        dataTableTerminalListGForm.draw(true);

    });
	
	$('body').on('click', '#add-terminal-to-list-import', function() {

        $('#modal-list-terminal-import').modal('show');
        $('#modal-list-terminal-import .modal-title').html('Import List Terminal');
        
    });
	
	$('body').on('click', '#btn-select-to-list-tg', function() {
		var arlist2 = [];
		var arlist3 = [];
        var datac = $('[name="ch[]"]');
		$.each(datac, function() {
            var $this = $(this);

            // check if the checkbox is checked
            if($this.is(":checked")) {
                var ob = new Object();
				ob.sn = $this.data("sn");
				ob.uid = $this.data("id");
				var uid = $this.data("id");
				arlist3.push(ob);
				arlist2.push(uid);
            }
        });	
		
		$('.page-loader').removeClass('hidden');
        $('.loading>img').removeClass('hidden');
        
        $.ajax({
            url: baseUrl + '/terminal-group/addTerminals',
            type: 'POST',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
             data: {
				"terminalGroupId" : $('#terminal_g_detail_id').text(),
				"version" : $('#version-terminal-g-detail').val(),
				"terminalIds" : JSON.stringify(arlist2)
			},
			
            success: function(resp) {
                if(resp.responseCode == '0000') { //sukses
                    // Send success message
                    $.smallBox({
                        height: 50,
                        title : resp.responseStatus, 
                        content : resp.responseMessage,
                        color : "#109618",
                        sound_file: "voice_on",
                        timeout: 3000
                        //icon : "fa fa-bell swing animated"
                    });
					
					$('#modal-list-terminal-form').modal('hide');
					table_form_terminal_group_sn_terminal.draw(true);
					
					getTerminalGroup($('#terminal_g_detail_id').text());
					
                    

                } else if(resp.responseCode == '3333' || resp.responseCode == '0200' || resp.responseCode == '0001'){ //exception and not found and exis
					
					 $.smallBox({
                        height: 50,
                        title : resp.responseStatus,
                        content : resp.responseMessage,
                        color : "#dc3912",
                        sound_file: "voice_on",
                        timeout: 3000
                        //icon : "fa fa-bell swing animated"
                    });
                }
				else if(resp.responseCode == '5555')//validaator
				{
					
						 var data = Object.values(resp.responseMessage);
						 var ln = data.length;
						 var w  = 3000 * ln; 
						 data.map(function(d){
							 
								$.smallBox({
										height: 50,
										title : resp.responseStatus,
										content : d,
										color : "#dc3912",
										sound_file: "smallbox",
										timeout: w
										//icon : "fa fa-bell swing animated"
									});
						});
					
				}	
				
				
                $('.page-loader').addClass('hidden');
                $('.loading>img').addClass('hidden');
            },
            error: function(xhr, ajaxOptions, thrownError) {
                $("#process_id").val("");
				$.smallBox({
                    title : "Error",
                    content : xhr.statusText,
                    color : "#dc3912",
                    sound_file: "smallbox",
                    timeout: 3000
                });
                $('.page-loader').addClass('hidden');
                $('.loading>img').addClass('hidden');
            }
        }); 
		
			
	});
	
	/*
	$('body').on('click', '#btn-select-to-list', function() {
		var arlist2 = [];
		var arlist3 = [];
        var datac = $('[name="ch[]"]');
		$.each(datac, function() {
            var $this = $(this);

            // check if the checkbox is checked
            if($this.is(":checked")) {
                var ob = new Object();
				ob.sn = $this.data("sn");
				ob.uid = $this.data("id");
				var uid = $this.data("id");
				arlist3.push(ob);
				arlist2.push(uid);
            }
        });	
		
		$('.page-loader').removeClass('hidden');
        $.ajax({
            url: baseUrl + '/terminal-group/addTerminals',
            type: 'POST',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
             data: {
				"terminalGroupId" : $('#terminal-g-id').val(),
				"version" : $('#version-terminal-g').val(),
				"terminalIds" : JSON.stringify(arlist3)
			},
			
            success: function(resp) {
                if(resp.responseCode == '0000') { //sukses
                    // Send success message
                    $.smallBox({
                        height: 50,
                        title : resp.responseStatus, 
                        content : resp.responseMessage,
                        color : "#109618",
                        sound_file: "voice_on",
                        timeout: 3000
                        //icon : "fa fa-bell swing animated"
                    });
					
					var d = "";		
					for(var i=0; i<arlist2.length; i++)
					{
						var no = 1+i;
						let sn = arlist2[i].sn;
						let uid = arlist2[i].uid;
						let act = `<a  class="btn-delete-list-t"><i class="fa fa-remove"></i></a>`;
						d+=`<tr class="tr-dt">`+
								`<td>`+no+`</td>`+
								`<td><span class="sn">`+sn+`</span><span class="uid" style="display:none;">`+uid+`</span></td>`+
								`<td>`+act+`</td>`+
							`</tr>`;
					}

					$('#table-form-terminal-group>tbody').find('tr').remove();  
					$('#table-form-terminal-group>tbody').html(d);       
					$('#modal-list-terminal-group-form').modal('hide');
					
                    

                } else if(resp.responseCode == '3333' || resp.responseCode == '0200' || resp.responseCode == '0001'){ //exception and not found and exis
					
					 $.smallBox({
                        height: 50,
                        title : resp.responseStatus,
                        content : resp.responseMessage,
                        color : "#dc3912",
                        sound_file: "voice_on",
                        timeout: 3000
                        //icon : "fa fa-bell swing animated"
                    });
                }
				else if(resp.responseCode == '5555')//validaator
				{
					
						 var data = Object.values(resp.responseMessage);
						 var ln = data.length;
						 var w  = 3000 * ln; 
						 data.map(function(d){
							 
								$.smallBox({
										height: 50,
										title : resp.responseStatus,
										content : d,
										color : "#dc3912",
										sound_file: "smallbox",
										timeout: w
										//icon : "fa fa-bell swing animated"
									});
						});
					
				}	
				
				
                $('.page-loader').addClass('hidden');
            },
            error: function(xhr, ajaxOptions, thrownError) {
                $("#process_id").val("");
				$.smallBox({
                    title : "Error",
                    content : xhr.statusText,
                    color : "#dc3912",
                    sound_file: "smallbox",
                    timeout: 3000
                });
                $('.page-loader').addClass('hidden');
            }
        });
		
	});
	*/
   
	
	
	$('#btn-select-to-list-import').click(function(){
		var gid = $('#terminal_g_detail_id').text();
        if(!$('#excelfile').val()) {
            $.smallBox({
                height: 50,
                title : "Warning",
                content : 'Please select file to upload',
                color : "#c79121",
                sound_file: "smallbox",
                timeout: 3000
            });
            return;
        }
        // Create form data
        var form = new FormData();
        form.append('file', $('#excelfile')[0].files[0]);
        form.append('group_id', gid);

        // Show loader
        $('.page-loader').removeClass('hidden');
        $('.loading>img').removeClass('hidden');
        $.ajax({
            url: baseUrl + '/terminal-group-import',
            type: 'POST',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            processData: false, // prevent jQuery from automatically transforming the data into a query string
            contentType: false, // is imperative, since otherwise jQuery will set it incorrectly.
            data: form,
            success: function(resp) {
                if(resp.responseCode === 200) {
					//$("#process_id").val(resp.processId);
                    //checkProcess(resp.processId);
					window.location.href=baseUrl+"/terminal-group-import/"+gid
                } else {
					//$("#process_id").val("");
                    $.smallBox({
                        height: 50,
                        title : "Error",
                        content : resp.responseMessage,
                        color : "#dc3912",
                        sound_file: "smallbox",
                        timeout: 3000
                    });
                }
                $('.page-loader').addClass('hidden');
                $('.loading>img').addClass('hidden');
            },
            error: function(xhr, ajaxOptions, thrownError) {
                //$("#process_id").val("");
				$.smallBox({
                    title : "Error",
                    content : xhr.statusText,
                    color : "#dc3912",
                    sound_file: "smallbox",
                    timeout: 3000
                });
                $('.page-loader').addClass('hidden');
                $('.loading>img').addClass('hidden');
            }
        });
		
    });
	
   
})(jQuery);

function getTerminalGroup(idG)
{
	
	$('.page-loader').removeClass('hidden');
	$.ajax({
			url: baseUrl + '/terminal-group/getById', 
			type: 'POST',
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},
			 data: {
				"id" : idG,
			},
			success: function(resp) {
				if(resp.responseCode == '0000') { //sukses
					
					var data = resp.responseMessage;
					console.log(data[0].name);
					$('#version-terminal-g-detail').val(data[0].version);
					$('#terminal-g-detail-gn').html(data[0].name);
					$('#terminal-g-detail-gd').html(data[0].description);
				
			
				} else if(resp.responseCode == '3333' || resp.responseCode == '0200' || resp.responseCode == '0400'){ //exception and not found and exis
					
					 $.smallBox({
						height: 50,
						title : resp.responseStatus,
						content : resp.responseMessage,
						color : "#dc3912",
						sound_file: "voice_on",
						timeout: 3000
						//icon : "fa fa-bell swing animated"
					});
				}
				else if(resp.responseCode == '5555')//validaator
				{
					
						 var data = Object.values(resp.responseMessage);
						 var ln = data.length;
						 var w  = 3000 * ln; 
						 data.map(function(d){
							 
								$.smallBox({
										height: 50,
										title : resp.responseStatus,
										content : d,
										color : "#dc3912",
										sound_file: "smallbox",
										timeout: w
										//icon : "fa fa-bell swing animated"
									});
						});
					
				}	
				
		
				$('.page-loader').addClass('hidden');
			},
			error: function(xhr, ajaxOptions, thrownError) {
				$("#process_id").val("");
				$.smallBox({
					title : "Error",
					content : xhr.statusText,
					color : "#dc3912",
					sound_file: "smallbox",
					timeout: 3000
				});
				$('.page-loader').addClass('hidden');
			}
	});
}

function BindTable(jsondata, tableid) {
	
	var arlist = [];
	$("#table-form-terminal-group>tbody>tr").each(function(){

		let a = $(this).find('.sn').text();
		//let b = $(this).find('.model').text();
		//let c = $(this).find('.merchant').text();
		let d = $(this).find('.uid').text();

		var ob = new Object();
		ob.sn = a;
		//ob.model = b;
		//ob.merchant = c;
		ob.uid = d;
		arlist.push(ob);
	}); 
	
	
	
	var arlist2 = [];
	for (var i = 0; i < jsondata.length; i++) {  
		
			var p = jsondata[i];
			var ob = new Object();
			let find = 0;
			if(arlist.length > 0)
			{
				for(var i=0; i<arlist.length; i++)
				{
					if(arlist[i].uid == getUid(p))
					{
						find = 1;
						break;
					}
				}
			}
			
			if(find == 0) 
			{
				var c =0;
				for (const [key, value] of Object.entries(p)) {
				  
					if(c==0)
					{
						 ob.sn = value;
					}	
					else if(c==1)
					{
						ob.uid = value;
					}	
					//console.log(`${key}: ${value}`);
				  
				  c++;
				}
				arlist2.push(ob);
			}
		
	}
	//console.log(arlist2);
	let arr = [...arlist, ...arlist2];
        //console.log('1',arr);
    let mergedArr = [...new Set(arr)];
        //console.log('2',mergedArr);

	var d = "";		
	for(var i=0; i<mergedArr.length; i++)
	{
		var no = 1+i;
		let sn = mergedArr[i].sn;
		//let model = mergedArr[i].model;
		//let merchant = mergedArr[i].merchant;
		let uid = mergedArr[i].uid;
		let act = `<a  class="btn-delete-list-t"><i class="fa fa-remove"></i></a>`;
		d+=`<tr class="tr-dt">`+
				`<td>`+no+`</td>`+
				`<td><span class="sn">`+sn+`</span><span class="uid" style="display:none;">`+uid+`</span></td>`+
				//`<td><span class="model">`+model+`</span></td>`+
				//`<td><span class="merchant">`+merchant+`</span><span class="uid" style="display:none;">`+uid+`</span></td>`+
				`<td>`+act+`</td>`+
			`</tr>`;
	}

	$('#table-form-terminal-group>tbody').find('tr').remove();  
	$('#table-form-terminal-group>tbody').html(d);       
	$('#modal-list-terminal-group-form').modal('hide');
	
}

function getUid(p)
{
	var c =0;
	for (const [key, value] of Object.entries(p)) {
	  
		if(c==1)
		{
			return value;
		}	
	
	  
	  c++;
	}
}

/*
//Function used to convert the JSON array to Html Table
function BindTable2(jsondata, tableid) {  
	 var columns = BindTableHeader(jsondata, tableid); 
	 //Gets all the column headings of Excel 
	 for (var i = 0; i < jsondata.length; i++) {  
		 var row$ = $('<tr/>');  
		 for (var colIndex = 0; colIndex < columns.length; colIndex++) {  
		 //for (var colIndex = 0; colIndex < 2; colIndex++) {  
			 var cellValue = jsondata[i][columns[colIndex]];  
			 if (cellValue == null)  
				 cellValue = "";  
			 row$.append($('<td/>').html(cellValue));  
		 }  
		 $(tableid).append(row$);  
	 }  
 }  
 function BindTableHeader(jsondata, tableid) {
	 //Function used to get all column names from JSON and bind the html table header 
	 var columnSet = [];  
	 //var headerTr$ = $('<tr/>');  
	 for (var i = 0; i < jsondata.length; i++) {  
		 var rowHash = jsondata[i];  
		 for (var key in rowHash) {  
			 if (rowHash.hasOwnProperty(key)) {  
				 if ($.inArray(key, columnSet) == -1) {
					 //Adding each unique column names to a variable array 
					 columnSet.push(key);  
					 //headerTr$.append($('<th/>').html(key));  
				 }  
			 }  
		 }  
	 }  
	 //$(tableid).append(headerTr$);  
	 return columnSet;  
 }  
 */
 

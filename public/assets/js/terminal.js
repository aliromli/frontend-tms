(function($) {
    "use strict";
    $('#dataTableTerminal').wrap('<div class="dataTables_scroll" />');
    var dataTableTerminal = null;
    if ($('#dataTableTerminal').length) {
        // You can use 'alert' for alert message
        // or throw to 'throw' javascript error
        // or none to 'ignore' and hide error
        // or you own function
        // please read https://datatables.net/reference/event/error
        // for more information
        $.fn.dataTable.ext.errMode = 'none';
        dataTableTerminal =  $('#dataTableTerminal').DataTable({
        processing: true,
        serverSide: true,
        //dom : '',
        dom: 'lrtip',
        "searching": false,
        //"scrollX": true,
        //"sScrollX": '100%',
        //"sScrollXInner": "110%",
        pageLength: 10,
        lengthMenu: [[10, 25, 50, 100], [10, 25, 50, 100]],
        pagingType: 'full_numbers',
        ajax: {
            url:   baseUrl+"/terminal-datatables",
            type: 'GET',
            data:  function(d){
                d.deviceModelId= $('#ter-deviceModel-id-search').val();
                d.merchantId= $('#ter-merchant-id-search').val();
                d.profileId= $('#ter-profile-id-search').val();
                d.sn= $('#ter-sn-search').val();
            }
        },
        language: {
            
        },
        //rowId: 'TRANSPORT_ID',
        columns: [
            {data: "DT_RowIndex", 
               sortable: false, 
               searchable: false,
               "render": function (data, type, row, meta) {      
                          return meta.row + meta.settings._iDisplayStart + 1;     
               }  
            },
            {data: "version", name: "version", visible:false},
            {data: "sn", name: "sn"},
            {data: "modelName", name: "modelName"}, 
            {data: "merchantName", name: "merchantName"}, 
            {data: "profileName", name: "profileName"}, 
            {data: "id", sortable: false, searchable: false, }
        ],
        columnDefs:[
			{
                "targets": 0,
                "className": "text-center",
            },
			{
                "targets": 1,
                "className": "mleft",
                render: function(d,data,row) {
                    return `<span>`+d+`</span>`;
                }

            },
			{
                "targets": 2,
                "className": "mleft",
                render: function(d,data,row) {
                    return `<span>`+d+`</span>`;
                }

            },
			 {
                "targets": 3,
                "className": "mleft",
                render: function(d,data,row) {
                    return `<span>`+d+`</span>`;
                }

            },
            {
                "targets": 4,
                "className": "mleft",
                render: function(d,data,row) {
                    return `<span>`+d+`</span>`;
                }

            },
			 {
                "targets": 5,
                "className": "mleft",
                render: function(d,data,row) {
                    return `<span>`+d+`</span>`;
                }

            },
	        {
                targets: 6,
				"className": "text-center padd-row-table",
                render: function(d,data,row) {
                 
                    let v = row.version;
                    let sn = row.sn;
                    let id = d;
                    return `<span>
					 <a href="terminal/`+d+`/`+sn+`" data-id="`+d+`" data-sn="`+sn+`" ><img class="mata" src="`+baseUrl+`/assets/images/icon/mata.png" /></a>&nbsp;
                     <a href="terminal-edit-form/`+d+`" data-id="`+d+`"><img class="pencil" src="`+baseUrl+`/assets/images/icon/pencil.png" /></a>&nbsp;
                     <a class="btn-delete" data-id="`+d+`" data-sn="`+sn+`" data-version="`+v+`"><img class="trash" src="`+baseUrl+`/assets/images/icon/trash.png" /></a>
                   </span>
                    `;
                }
            }
        ]
        });
      
    }

    
    $('#btn-cari-terminal').click(function() {
        dataTableTerminal.draw(true);
    });

    /* filter tugel */
    // Toggle filter
    // $('.filter .tugel').click(function() {
    //     $(this).toggleClass('open');
    //     $('.filter-title').toggleClass('hidden');
    //     $('.form-filter form').toggleClass('open-filter');
        
    // });

    // clear
    $('.clear_filter').click(function() {
       
        // $('#ter-deviceModel-id-search').val('');
        // $('#ter-deviceModel-search').val('');
        // $('#ter-merchant-id-search').val('');
        // $('#ter-merchant-search').val('');
        // $('#ter-profile-id-search').val('');
        // $('#ter-profile-search').val('');
        $('#ter-sn-search').val('');
        dataTableTerminal.draw(true);
    });

    // Add New  or update
    $('#btn-submit-terminal').click(function(){

        // Update when city id has value
        var url = baseUrl + '/terminal/update';
        var action = "terminal";
        if(!$('#terminal-id').val()) {
            url = baseUrl + '/terminal/save';
            action = "save";
        }

        if($('#terminal-id').val()) {
            if(!$('#version-terminal').val()) {
                $.smallBox({
                    //height: 50,
                    title : "Error",
                    content : 'Version can\'t be empty',
                    color : "#dc3912",
                    sound_file: "smallbox",
                    timeout: 3000
                });
                $('#version-terminal').focus();
                return;
            }
        }

        // Has error
        // var hasError = false;
        // Check requirement input
        if(!$('#terminal-sn').val()) {
            $.smallBox({
                //height: 50,
                title : "Error",
                content : 'SN can\'t be empty',
                color : "#dc3912",
                sound_file: "smallbox",
                timeout: 3000
            });
            $('#terminal-sn').focus();
            return;
        }

        if(!$('#terminal-device-model').val()) {
            $.smallBox({
                //height: 50,
                title : "Error",
                content : 'Device Model can\'t be empty',
                color : "#dc3912",
                sound_file: "smallbox",
                timeout: 3000
            });
            $('#terminal-device-model').focus();
            return;
        }
        if(!$('#terminal-merchant').val()) {
            $.smallBox({
                //height: 50,
                title : "Error",
                content : 'Merchant can\'t be empty',
                color : "#dc3912",
                sound_file: "smallbox",
                timeout: 3000
            });
            $('#terminal-merchant').focus();
            return;
        }
        
        if(!$('#terminal-profile').val()) {
            $.smallBox({
                //height: 50,
                title : "Error",
                content : 'Device Profile can\'t be empty',
                color : "#dc3912",
                sound_file: "smallbox",
                timeout: 3000
            });
            $('terminal-profile').focus();
            return;
        }
      
        
        //var arlistTg = []; 
        //$("#list-group-tg-t option").each(function(index,element){
            ////let d = $(this).find('.uid').text();
          //  arlistTg.push(element.value);
        //}); 

        var arlistTg = [];
        $("#list-group-tg-t>li").each(function(){
           
            let a = $(this).find('.uid').text();
            arlistTg.push(a);
        }); 

        if(arlistTg.length == 0)
		{
			$.smallBox({
                //height: 50,
                title : "Error",
                content : 'Terminal Group can\'t be empty',
                color : "#dc3912",
                sound_file: "smallbox",
                timeout: 3000
            });
            return;
		}
       
        // Show loder
        $('.page-loader').removeClass('hidden');

        // Send data
        $.ajax({
            url: url,
            type: 'POST',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: {
                "id": $('#terminal-id').val(),
                "sn" : $('#terminal-sn').val(),
                "modelId" : $('#terminal-device-model').val(),
                "merchantId" : $('#terminal-merchant').val(),
                "profileId" : $('#terminal-profile').val(),
                'terminalGroupIds' :  JSON.stringify(arlistTg),
                "version": $('#version-terminal').val(),
                
            },
            success: function(resp) {
               
				if(resp.responseCode == '0000') { //sukses
                    // Send success message
                    $.smallBox({
                        height: 50,
                        title : resp.responseStatus, 
                        content : resp.responseMessage,
                        color : "#109618",
                        sound_file: "voice_on",
                        timeout: 3000
                        //icon : "fa fa-bell swing animated"
                    });
					window.location.replace(baseUrl +"/terminal");

                    // if($('#terminal-id').val()=="") {
                       
                    //     $('#terminal-sn').val("");
                    //     $('#terminal-devicemodel').val("");
                      
                    //     $('#terminal-deviceprofile').val("");
                    //     $("#list-group-tg-t option").remove();
                    // }

                } else if(resp.responseCode == '3333' || resp.responseCode == '0400'){ //exception and not found
					
					 $.smallBox({
                        height: 50,
                        title : resp.responseStatus,
                        content : resp.responseMessage,
                        color : "#dc3912",
                        sound_file: "voice_on",
                        timeout: 3000
                        //icon : "fa fa-bell swing animated"
                    });
                }
				else if(resp.responseCode == '5555')//validaator
				{
					
						 var data = Object.values(resp.responseMessage);
						 var ln = data.length;
						 var w  = 3000 * ln; 
						 data.map(function(d){
							 
								$.smallBox({
										height: 50,
										title : resp.responseStatus,
										content : d,
										color : "#dc3912",
										sound_file: "smallbox",
										timeout: w
										//icon : "fa fa-bell swing animated"
									});
						});
					
				}	
                // Hide loder
                $('.page-loader').addClass('hidden');
            },
            error: function(xhr, ajaxOptions, thrownError) {
                $.smallBox({
                    title : "Error",
                    content : xhr.statusText,
                    color : "#dc3912",
                    timeout: 3000
                    //icon : "fa fa-bell swing animated"
                });
                // Hide loder
                $('.page-loader').addClass('hidden');
            }
        });

    });
    // terminal group modal

     // Terminal group modal tabel
     $('#dataTableTgListGFormT').wrap('<div class="dataTables_scroll" />');
     var dataTableTgListGFormT = null;
     if ($('#dataTableTgListGFormT').length) {
         // You can use 'alert' for alert message
         // or throw to 'throw' javascript error
         // or none to 'ignore' and hide error
         // or you own function
         // please read https://datatables.net/reference/event/error
         // for more information
         $.fn.dataTable.ext.errMode = 'none';
         
         dataTableTgListGFormT =  $('#dataTableTgListGFormT').DataTable({
         processing: true,
         serverSide: true,
         //dom : '',
         dom: 'lrtip',
         "searching": false,
         
         pageLength: 10,
         lengthMenu: [[10, 25, 50, 100], [10, 25, 50, 100]],
         pagingType: 'full_numbers',
         ajax: {
             url:   baseUrl+"/terminal-group-datatables",
             type: 'GET',
             data:  function(d){
 
                 //d.model = $('#search-model').val();
                 //d.vendorName = $('#search-vendor-name').val();
                 //d.vendorCountry = $('#search-vendor-country').val();
                 
             }
         },
         language: {
          
         },
         //rowId: 'TRANSPORT_ID',
         columns: [
             {data: "DT_RowIndex", 
                sortable: false, 
                searchable: false,
                "render": function (data, type, row, meta) {      
                           return meta.row + meta.settings._iDisplayStart + 1;     
                }  
             },
             {data: "version", name: "version", visible:false},
             {data: "name", name: "name"},
             {data : "description", name : "description"},
             {data: "id", sortable: false, searchable: false, }
         ],
         columnDefs:[
            {
                "targets": 0,
                "className": "text-center",
            },  
            {
                "targets": 2,
                "className": "mleft",
                render: function(d,data,row) {
                    return `<span>`+d+`</span>`;
                }

            },   
            {
                "targets": 3,
                "className": "mleft",
                render: function(d,data,row) {
                    return `<span>`+d+`</span>`;
                }

             },             
             {
                 targets: 4,
                 "className": "text-center padd-row-table",
                 render: function(d,data,row) {
                     let id = d;
                     let v = row.version;
                    
                     return `<span>
                     <input 
                     type="checkbox" 
                     name="chtg[]" 
                     value="true"  
                     data-id="`+d+`" 
                     data-name="`+row.name+`" 
                     data-description="`+row.description+`"                      
                     data-version="`+v+`"
                     /></span>
                   `;
                 }
             }
         ]
         });
       
     }
     

    $('body').on('click', '#add-tg-to-list-t', function() {
        
        $('#modal-list-tg-form-t').modal('show');
        $('#modal-list-tg-form-t .modal-title').html('List Terminal Group');
        dataTableTgListGFormT.draw(true);
       
    });

    $('body').on('click', '#btn-tg-to-list-t', function() {
        
        var arlist = [];
        //$('#list-group-tg-t option').each(function(index,element){
        $("#list-group-tg-t>li").each(function(){
           
            //var ob = new Object();
			//ob.name = element.text;
			//ob.uid = element.value;
			//arlist.push(ob);
            let a = $(this).find('.nm').text();
            let b = $(this).find('.uid').text();
          
			var ob = new Object();
			ob.name = a;
			ob.uid = b;
			arlist.push(ob);
        });
        var arlist2 = [];
        var datac = $('[name="chtg[]"]');
       // var html = "";
      
        $.each(datac, function() {
            var $this = $(this);

            // check if the checkbox is checked
            if($this.is(":checked")) {
                var ob = new Object();
                let find = 0;
                if(arlist.length > 0)
                {
                    for(var i=0; i<arlist.length; i++)
                    {
                        if(arlist[i].uid == $this.data("id"))
                        {
                            find = 1;
                            break;
                        }
                    }
                }
                
                if(find == 0) 
                {
                    ob.name = $this.data("name");
                    ob.uid = $this.data("id");
                    arlist2.push(ob);
                }
                
            }
        });	

        let arr = [...arlist, ...arlist2];
        //console.log('1',arr);
        let mergedArr = [...new Set(arr)];
        //console.log('2',mergedArr);

        var d = "";		
		for(var i=0; i<mergedArr.length; i++)
		{
		    //let name = mergedArr[i].name;
			//let description = mergedArr[i].description;
            //let uid = mergedArr[i].uid;
			//let act = `<button type="button" class="btn btn-flat btn-danger btn-xs btn-delete-list-tg-t"><i class="fa fa-remove"></i></button>`;
			//d+=`<option value="`+uid+`">`+name+`</option>`;

            let name = '<span class="nm">'+mergedArr[i].name+'</span>';
		    let uid = '<span class="uid" style="display:none;">'+mergedArr[i].uid+'</span>';
		    let rem = '<i class="fa fa-trash-o btn-delete-list-tg-t" style="color:#717682;margin-left:20px;"></i>'; 
			d+='<li class="ch-li">'+name+' '+rem+' '+uid+'</li>'

		}
        $('#list-group-tg-t').find('li').remove();  
        $('#list-group-tg-t').html(d);       
		$('#modal-list-tg-form-t').modal('hide');
       
    });

    $('body').on('click', '.btn-delete-list-tg-t', function() {
        $(this).parent().remove();
    });
    //end == tg
    $('body').on('click', '#btn-import-terminal', function() {
       //modal-list-terminal
       $('#modal-list-terminal-excel').modal('show');
       $('#modal-list-terminal-excel .modal-title').html('Import Terminals From excel');
       //dataTableTerminalListGForm.draw(true);
    });


    $('#btn-select-to-list-import-terminal').click(function(){
		var un = $('#userName-terminal').val();
        if(!$('#excelfile').val()) {
            $.smallBox({
                height: 50,
                title : "Warning",
                content : 'Please select file to upload',
                color : "#c79121",
                sound_file: "smallbox",
                timeout: 3000
            });
            return;
        }
        // Create form data
        var form = new FormData();
        form.append('file', $('#excelfile')[0].files[0]);
        form.append('userName', un);

        // Show loader
        $('.page-loader').removeClass('hidden');
        $('.loading>img').removeClass('hidden');
        
        $.ajax({
            url: baseUrl + '/terminal-import',
            type: 'POST',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            processData: false, // prevent jQuery from automatically transforming the data into a query string
            contentType: false, // is imperative, since otherwise jQuery will set it incorrectly.
            data: form,
            success: function(resp) {
                if(resp.responseCode === 200) {
					//$("#process_id").val(resp.processId);
                    //checkProcess(resp.processId);
					window.location.href=baseUrl+"/terminal-import/"+un;
                } else {
					//$("#process_id").val("");
                    $.smallBox({
                        height: 50,
                        title : "Error",
                        content : resp.responseMessage,
                        color : "#dc3912",
                        sound_file: "smallbox",
                        timeout: 3000
                    });
                }
                $('.page-loader').addClass('hidden');
                $('.loading>img').addClass('hidden');
        
            },
            error: function(xhr, ajaxOptions, thrownError) {
                $("#process_id").val("");
				$.smallBox({
                    title : "Error",
                    content : xhr.statusText,
                    color : "#dc3912",
                    sound_file: "smallbox",
                    timeout: 3000
                });
                $('.page-loader').addClass('hidden');
                $('.loading>img').addClass('hidden');
            }
        });
		
    });


    $('#dataTableTerminal').on('click', '.btn-delete', function() {
        let text;
        if (confirm("Are you sure to remove!") == true) {
            
             // Send data
            $.ajax({
                url:  baseUrl + '/terminal/delete',
                type: 'POST',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data: {
                    'id': $(this).data('id'),
                    'version': $(this).data('version'),
                    'sn' : $(this).data('sn'),
                },
                success: function(resp) {
                    if(resp.responseCode === 200) {
                        // Reload datatable
                        dataTableTerminal.ajax.reload();
                        
                        // Send success message
                        $.smallBox({
                            height: 50,
                            title : "Success",
                            content : resp.responseMessage,
                            color : "#109618",
                            sound_file: "voice_on",
                            timeout: 3000
                            //icon : "fa fa-bell swing animated"
                        });
                    } else {
                        $.smallBox({
                            height: 50,
                            title : "Error",
                            content : resp.responseMessage,
                            color : "#dc3912",
                            sound_file: "smallbox",
                            timeout: 3000
                            //icon : "fa fa-bell swing animated"
                        });
                    }
                    // Hide loder
                    $('.page-loader').addClass('hidden');
                },
                error: function(xhr, ajaxOptions, thrownError) {
                    $.smallBox({
                        title : "Error",
                        content : xhr.statusText,
                        color : "#dc3912",
                        timeout: 3000
                        //icon : "fa fa-bell swing animated"
                    });
                    // Hide loder
                    $('.page-loader').addClass('hidden');
                }
            });

       
        } 

    });

    //----------  autocomplite devicemodel ----  
    /*//var path = "{{ route('autocomplete') }}";
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    $( "#ter-deviceModel-search" ).autocomplete({
        delay: 100,
        // classes: {
        //     "ui-autocomplete": "highlight"
        //   },
        source: function( request, response ) {
        // Fetch data
        $.ajax({
            //url:"{{route('application-auto)}}",
            url:  baseUrl + '/terminal-device-model-auto',
            type: 'post',
            dataType: "json",
            data: {
            _token: CSRF_TOKEN,
            search: request.term
            },
            success: function( data ) {
            response( data );
            }
        });
        },
        minLength: 3,
        select: function (event, ui) {
            // Set selection
            $('#ter-deviceModel-search').val(ui.item.label); // display the selected text
            $('#ter-deviceModel-id-search').val(ui.item.value); // save selected id to input
            return false;
        },
   
    });

    $( "#ter-merchant-search" ).autocomplete({
        delay: 100,
        // classes: {
        //     "ui-autocomplete": "highlight"
        //   },
        source: function( request, response ) {
        // Fetch data
        $.ajax({
            //url:"{{route('application-auto)}}",
            url:  baseUrl + '/terminal-merchant-auto',
            type: 'post',
            dataType: "json",
            data: {
            _token: CSRF_TOKEN,
            search: request.term
            },
            success: function( data ) {
            response( data );
            }
        });
        },
        minLength: 3,
        select: function (event, ui) {
            // Set selection
            $('#ter-merchant-search').val(ui.item.label); // display the selected text
            $('#ter-merchant-id-search').val(ui.item.value); // save selected id to input
            return false;
        },
   
    });

    $( "#ter-profile-search" ).autocomplete({
        delay: 100,
        // classes: {
        //     "ui-autocomplete": "highlight"
        //   },
        source: function( request, response ) {
        // Fetch data
        $.ajax({
            //url:"{{route('application-auto)}}",
            url:  baseUrl + '/terminal-profile-auto',
            type: 'post',
            dataType: "json",
            data: {
            _token: CSRF_TOKEN,
            search: request.term
            },
            success: function( data ) {
            response( data );
            }
        });
        },
        minLength: 3,
        select: function (event, ui) {
            // Set selection
            $('#ter-profile-search').val(ui.item.label); // display the selected text
            $('#ter-profile-id-search').val(ui.item.value); // save selected id to input
            return false;
        },
   
    });
    */
   /* list terminal import temp form */
   $('#dataTableListTerminal_Result_x').wrap('<div class="dataTables_scroll" />');
   var dataTableListTerminal_Result = null;
   if ($('#dataTableListTerminal_Result_X').length) {
           
       //$.fn.dataTable.ext.errMode = 'none';
       
       dataTableListTerminal_Result =  $('#dataTableListTerminal_Result_X').DataTable({
               dom : '<"dt-toolbar row"<"col-sm-6 col-xs-12 "l><"col-sm-6 col-xs-12 text-right"<"toolbar">>>rt<"dt-toolbar-footer"<"col-md-6 "i><"col-md-6  "p>><"clear">',
               //processing: true,
               //serverSide: true,
               dom: 'lrtip',
               "searching": true,
               //"scrollX": true,
               //"sScrollX": '100%',
               //"sScrollXInner": "110%",
               //pageLength: 10,
               //lengthMenu: [[10, 25, 50, 100], [10, 25, 50, 100]],
               //pagingType: 'full_numbers',
               "lengthChange": false,
               language: {
                   paginate : {
                       first : '&laquo;',
                       last : '&raquo;',
                       next : '&rsaquo;',
                       previous : '&lsaquo;',
                   }
               },
               columnDefs:[
                   {
                       "targets": 0,
                       "className": "text-center",
                   },
                   {
                       "targets": 1,
                       "className": "text-center",
                   },{
                       targets: 2,
                       "className": "mleft",
                       render: function(d,data,row) {
                             
                           return `<span>`+d+`</span>`;
                       }
                   },{
                    targets: 3, 
                    "className": "mleft",
                        render: function(d,data,row) {
                            
                            return `<span>`+d+`</span>`;
                        }
                    },{
                        targets: 4,
                        "className": "mleft",
                            render: function(d,data,row) {
                                
                                return `<span>`+d+`</span>`;
                            }
                     }
                    //,{
                    //     targets: 5,
                    //     "className": "mleft",
                    //         render: function(d,data,row) {
                                
                                
                    //             var myarr = d.split(",");
                    //             var s = "";
                    //             for(var i=0; i< myarr.length;i++)
                    //             {
                    //                 s+="<span class='badge badge-success'>"+myarr[i]+"</span>"
                    //             }
                    //             return `<span>`+s+`</span>`;
                    //         }
                    // }
               ],  
               
               'select' : {
                       style : 'single'
                   },
       });
       
   }
 
   $('body').on('click', '#btn-submit-import-tt', function() {
        var arlist = [];
        $('#dataTableListTerminal_Result>tbody>tr').each(function(){
            var sn = $(this).find('.sn').text();
            var model_id = $(this).find('.model_id').text();
            var profile_id = $(this).find('.profile_id').text();
            var merchant_id = $(this).find('.merchant_id').text();
            var group_id = $(this).find('.group_id').text();

            var ob = new Object();
            ob.sn = sn;
            ob.model_id = model_id;
            ob.profile_id = profile_id;
            ob.merchant_id = merchant_id;
            ob.group_id = group_id;
            arlist.push(ob);
           	
        });
    
        $('.page-loader').removeClass('hidden');
        $('.loading>img').removeClass('hidden');

        //console.log("LENGHT-",arlist.length);
        
        $.ajax({
            url: baseUrl + '/terminal/addTerminals',
            type: 'POST',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: {
                //"terminalGroupId" : $('#idg').val(),
                //"version" : $('#version-terminal-gim').val(),
                "userName" : $('#userName-import-view').val(),
                "terminals" : JSON.stringify(arlist)
            },
            
            success: function(resp) {

                if(resp.responseCode == '0000') { //sukses
                    // Send success message
                    // $.smallBox({
                    //     height: 50,
                    //     title : resp.responseStatus, 
                    //     content : resp.responseMessage,
                    //     color : "#109618",
                    //     sound_file: "voice_on",
                    //     timeout: 6000
                    //     //icon : "fa fa-bell swing animated"
                    // });
                    alert(""+resp.responseMessage+"");
                    window.location.href = baseUrl+"/terminal";
                } 
                else if(resp.responseCode == '0002' || resp.responseCode == '0001')
                {
                    // $.smallBox({
                    //     height: 50,
                    //     title : resp.responseStatus,
                    //     content : resp.responseMessage,
                    //     color : "#dc3912",
                    //     sound_file: "voice_on",
                    //     timeout: 6000
                    //     //icon : "fa fa-bell swing animated"
                    // });
                    alert(""+resp.responseMessage+"");
                    window.location.href = baseUrl+"/terminal";
                }   
                
                $('.page-loader').addClass('hidden');
                $('.loading>img').addClass('hidden');
            },
            error: function(xhr, ajaxOptions, thrownError) {
                $("#process_id").val("");
                $.smallBox({
                    title : "Error",
                    content : xhr.statusText,
                    color : "#dc3912",
                    sound_file: "smallbox",
                    timeout: 3000
                });
                $('.page-loader').addClass('hidden');
                $('.loading>img').addClass('hidden');
            }
        }); 
        
    });

    //  Unlocked
    $('body').on('click', '#lockUnlock', function() {
        /*
        if($("#status-terminal").text()=="Unlocked")
        {
            //alert("System already Unlocked");
            if (confirm("Are you sure to Lock !!") == true) {
               
                $('.loading>img').removeClass('hidden');
                $.ajax({
                    url:  baseUrl + '/terminal/lockUnlock',
                    type: 'POST',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    data: {
                        'id': $("#id-terminal").val(),
                        'version': $("#version-terminal").val(),
                        'action' : "LOCK",
                        'lockReason' : "Kunci aja"
            
            
                    },
                    success: function(resp) {
                        if(resp.responseCode === 200) {
                            // Reload datatable
                            window.location.reload();
                            
                            // Send success message
                            $.smallBox({
                                height: 50,
                                title : "Success",
                                content : resp.responseMessage,
                                color : "#109618",
                                sound_file: "voice_on",
                                timeout: 3000
                                //icon : "fa fa-bell swing animated"
                            });
                        } else {
                            $.smallBox({
                                height: 50,
                                title : "Error",
                                content : resp.responseMessage,
                                color : "#dc3912",
                                sound_file: "smallbox",
                                timeout: 3000
                                //icon : "fa fa-bell swing animated"
                            });
                        }
                        // Hide loder
                        $('.page-loader').addClass('hidden');
                        $('.loading>img').addClass('hidden');
                    },
                    error: function(xhr, ajaxOptions, thrownError) {
                        $.smallBox({
                            title : "Error",
                            content : xhr.statusText,
                            color : "#dc3912",
                            timeout: 3000
                            //icon : "fa fa-bell swing animated"
                        });
                        // Hide loder
                        $('.page-loader').addClass('hidden');
                        $('.loading>img').addClass('hidden');
                    }
                });
    
            }
        } 
        else
        {
            if (confirm("Are you sure to UnLock !!") == true) {

            $('.loading>img').removeClass('hidden');
            $.ajax({
                url:  baseUrl + '/terminal/lockUnlock',
                type: 'POST',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data: {
                    'id': $("#id-terminal").val(),
                    'version': $("#version-terminal").val(),
                    'action' : "UNLOCK",
                    'lockReason' : "Tidak dikuncu aja"
        
        
                },
                success: function(resp) {
                    if(resp.responseCode === 200) {
                        // Reload datatable
                        //dataTableDeviceprofile.ajax.reload();
                        window.location.reload();
                        // Send success message
                        $.smallBox({
                            height: 50,
                            title : "Success",
                            content : resp.responseMessage,
                            color : "#109618",
                            sound_file: "voice_on",
                            timeout: 3000
                            //icon : "fa fa-bell swing animated"
                        });
                    } else {
                        $.smallBox({
                            height: 50,
                            title : "Error",
                            content : resp.responseMessage,
                            color : "#dc3912",
                            sound_file: "smallbox",
                            timeout: 3000
                            //icon : "fa fa-bell swing animated"
                        });
                    }
                    // Hide loder
                    $('.page-loader').addClass('hidden');
                },
                error: function(xhr, ajaxOptions, thrownError) {
                    $.smallBox({
                        title : "Error",
                        content : xhr.statusText,
                        color : "#dc3912",
                        timeout: 3000
                        //icon : "fa fa-bell swing animated"
                    });
                    // Hide loder
                    $('.page-loader').addClass('hidden');
                    $('.loading>img').addClass('hidden');
                }
            });

            
            }
           // $('#modal-unlock').modal('show');
           
            //$('#email').attr('readonly','readonly');
        }
        */
   });
   
   $('body').on('click', '#btn-restart', function() {

        if (confirm("Are you sure to Restart!") == true) {

            //restart di API belum ada perintahnya ?
        }


    });

    $('body').on('click', '#ForceDiagnostic', function() {

        if (confirm("Are you sure to Force Diagnostic!") == true) {

            //restart di API belum ada perintahnya ?
        }


    });

    $('body').on('click', '#ForceHeartbeat', function() {

        if (confirm("Are you sure to Force Heartbeat!") == true) {

            //restart di API belum ada perintahnya ?
        }


    });

    if($("#lat").text()=="")
	{
		$("#googleMap").html("Can't Display Map. Latitide Or Longitude is empty");
	}

    //--- form 2 ---
    $('#btn-submit-terminal2').click(function(){

        // Update when city id has value
        var url = baseUrl + '/terminal/update';
        var action = "terminal";
        if(!$('#terminal-id2').val()) {
            url = baseUrl + '/terminal/save';
            action = "save";
        }

        if($('#terminal-id2').val()) {
            if(!$('#version-terminal2').val()) {
                $.smallBox({
                    //height: 50,
                    title : "Error",
                    content : 'Version can\'t be empty',
                    color : "#dc3912",
                    sound_file: "smallbox",
                    timeout: 3000
                });
                $('#version-terminal').focus();
                return;
            }
        }

        // Has error
        // var hasError = false;
        // Check requirement input
        if(!$('#terminal-sn2').val()) {
            $.smallBox({
                //height: 50,
                title : "Error",
                content : 'SN can\'t be empty',
                color : "#dc3912",
                sound_file: "smallbox",
                timeout: 3000
            });
            $('#terminal-sn2').focus();
            return;
        }

        if(!$('#terminal-device-model2').val()) {
            $.smallBox({
                //height: 50,
                title : "Error",
                content : 'Device Model can\'t be empty',
                color : "#dc3912",
                sound_file: "smallbox",
                timeout: 3000
            });
            $('#terminal-device-model2').focus();
            return;
        }
        if(!$('#terminal-merchant2').val()) {
            $.smallBox({
                //height: 50,
                title : "Error",
                content : 'Merchant can\'t be empty',
                color : "#dc3912",
                sound_file: "smallbox",
                timeout: 3000
            });
            $('#terminal-merchant2').focus();
            return;
        }
        
        if(!$('#terminal-profile2').val()) {
            $.smallBox({
                //height: 50,
                title : "Error",
                content : 'Device Profile can\'t be empty',
                color : "#dc3912",
                sound_file: "smallbox",
                timeout: 3000
            });
            $('terminal-profile').focus();
            return;
        }
      
        
        //var arlistTg = []; 
        //$("#list-group-tg-t option").each(function(index,element){
            ////let d = $(this).find('.uid').text();
          //  arlistTg.push(element.value);
        //}); 
        //var jk = $("#list-group-tg-t2").val().trim();
        var arlistTg2 = [];
        $("#list-group-tg-t>li").each(function(){
           
            let a = $(this).find('.uid').text();
            arlistTg2.push(a);
        }); 
       

        if(arlistTg2.length == 0)
		{
			$.smallBox({
                //height: 50,
                title : "Error",
                content : 'Terminal Group can\'t be empty',
                color : "#dc3912",
                sound_file: "smallbox",
                timeout: 3000
            });
            return;
		}
       
        // Show loder
        $('.page-loader').removeClass('hidden');

        // Send data
        $.ajax({
            url: url,
            type: 'POST',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: {
                "id": $('#terminal-id2').val(),
                "sn" : $('#terminal-sn2').val(),
                "modelId" : $('#terminal-device-model2').val(),
                "merchantId" : $('#terminal-merchant2').val(),
                "profileId" : $('#terminal-profile2').val(),
                'terminalGroupIds' :  JSON.stringify(arlistTg2),
                "version": $('#version-terminal2').val(),
                
            },
            success: function(resp) {
               
				if(resp.responseCode == '0000') { //sukses
                    // Send success message
                    $.smallBox({
                        height: 50,
                        title : resp.responseStatus, 
                        content : resp.responseMessage,
                        color : "#109618",
                        sound_file: "voice_on",
                        timeout: 3000
                        //icon : "fa fa-bell swing animated"
                    });
					//window.location.replace(baseUrl +"/terminal");
                    $('#terminal-sn2').val('');
                    // if($('#terminal-id').val()=="") {
                       
                    //     $('#terminal-sn').val("");
                    //     $('#terminal-devicemodel').val("");
                      
                    //     $('#terminal-deviceprofile').val("");
                    //     $("#list-group-tg-t option").remove();
                    // }

                } else if(resp.responseCode == '3333' || resp.responseCode == '0400'){ //exception and not found
					
					 $.smallBox({
                        height: 50,
                        title : resp.responseStatus,
                        content : resp.responseMessage,
                        color : "#dc3912",
                        sound_file: "voice_on",
                        timeout: 3000
                        //icon : "fa fa-bell swing animated"
                    });
                }
				else if(resp.responseCode == '5555')//validaator
				{
					
						 var data = Object.values(resp.responseMessage);
						 var ln = data.length;
						 var w  = 3000 * ln; 
						 data.map(function(d){
							 
								$.smallBox({
										height: 50,
										title : resp.responseStatus,
										content : d,
										color : "#dc3912",
										sound_file: "smallbox",
										timeout: w
										//icon : "fa fa-bell swing animated"
									});
						});
					
				}	
                // Hide loder
                $('.page-loader').addClass('hidden');
            },
            error: function(xhr, ajaxOptions, thrownError) {
                $.smallBox({
                    title : "Error",
                    content : xhr.statusText,
                    color : "#dc3912",
                    timeout: 3000
                    //icon : "fa fa-bell swing animated"
                });
                // Hide loder
                $('.page-loader').addClass('hidden');
            }
        });

    });
    
    // terminal group modal
    $('#btn-submit-terminal3').click(function(){
      
       var str = $("#terminal-sn3").val();
       var str = str.split("\n").join(",").slice(0, -1);
       //console.log(str);
       var ar_str = str.split(",");
       //for(var i=0; i<ar_str.length; i++)
       //{
        //   console.log("hai ",ar_str[i]);
       //}

       var url = baseUrl + '/terminal/update';
       var action = "terminal";
       if(!$('#terminal-id3').val()) {
           url = baseUrl + '/terminal/save';
           action = "save";
       }

       if($('#terminal-id3').val()) {
           if(!$('#version-terminal3').val()) {
               $.smallBox({
                   //height: 50,
                   title : "Error",
                   content : 'Version can\'t be empty',
                   color : "#dc3912",
                   sound_file: "smallbox",
                   timeout: 3000
               });
               $('#version-terminal3').focus();
               return;
           }
       }

       // Has error
       // var hasError = false;
       // Check requirement input
       if(!$('#terminal-sn3').val()) {
           $.smallBox({
               //height: 50,
               title : "Error",
               content : 'SN can\'t be empty',
               color : "#dc3912",
               sound_file: "smallbox",
               timeout: 3000
           });
           $('#terminal-sn3').focus();
           return;
       }

       if(!$('#terminal-device-model3').val()) {
           $.smallBox({
               //height: 50,
               title : "Error",
               content : 'Device Model can\'t be empty',
               color : "#dc3912",
               sound_file: "smallbox",
               timeout: 3000
           });
           $('#terminal-device-model3').focus();
           return;
       }
        if(!$('#terminal-merchant3').val()) {
           $.smallBox({
               //height: 50,
               title : "Error",
               content : 'Merchant can\'t be empty',
               color : "#dc3912",
               sound_file: "smallbox",
               timeout: 3000
           });
           $('#terminal-merchant3').focus();
           return;
        }
       
        if(!$('#terminal-profile3').val()) {
           $.smallBox({
               //height: 50,
               title : "Error",
               content : 'Device Profile can\'t be empty',
               color : "#dc3912",
               sound_file: "smallbox",
               timeout: 3000
           });
           $('terminal-profile3').focus();
           return;
        }
     
        var arlistTg3 = [];
        $("#list-group-tg-t>li").each(function(){
           let a = $(this).find('.uid').text();
           arlistTg3.push(a);
        }); 
      

        if(arlistTg3.length == 0)
        {
           $.smallBox({
               //height: 50,
               title : "Error",
               content : 'Terminal Group can\'t be empty',
               color : "#dc3912",
               sound_file: "smallbox",
               timeout: 3000
           });
           return;
        }
      
       // Show loder
        //$('.page-loader').removeClass('hidden');
        for(var i=0; i<ar_str.length; i++)
        {
            //setTimeout(function () {},1500);
            //$("#tmp_sn").val(ar_str[i]);
            //console.log($("#tmp_sn").val());
            // Send data
            $.ajax({
                url: url,
                type: 'POST',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data: {
                    "id": $('#terminal-id3').val(),
                    "sn" : ar_str[i],
                    "modelId" : $('#terminal-device-model3').val(),
                    "merchantId" : $('#terminal-merchant3').val(),
                    "profileId" : $('#terminal-profile3').val(),
                    'terminalGroupIds' :  JSON.stringify(arlistTg3),
                    "version": $('#version-terminal3').val(),
                    
                },
                success: function(resp) {
                    
                    if(resp.responseCode == '0000') { //sukses
                        // Send success message
                        $.smallBox({
                            height: 50,
                            title : resp.responseStatus, 
                            content : resp.responseMessage,
                            color : "#109618",
                            //sound_file: "voice_on",
                            timeout: 3000
                            //icon : "fa fa-bell swing animated"
                        });
                        //window.location.replace(baseUrl +"/terminal");
                        //$('#terminal-sn3').val('');
                        $("#cnt-gagal-save").show();
                        var tb1 = $("#table-gagal-save").find('tbody');
                        var rowCount = $("#table-gagal-save tr").length + 1;
                        var sn = ar_str[$("#table-gagal-save tr").length];
                        var r1 = '<tr><td>'+rowCount+'</td><td>'+sn+'</td><td>Success</td></tr>';
                        tb1.append(r1);

                    } else if(resp.responseCode == '3333' || resp.responseCode == '0400'){ //exception and not found
                        
                        $("#cnt-gagal-save").show();
                        var tb2 = $("#table-gagal-save").find('tbody');
                        var rowCount = $("#table-gagal-save tr").length + 1;
                        var sn = ar_str[$("#table-gagal-save tr").length];
                        var r2 = '<tr><td>'+rowCount+'</td><td>'+sn+'</td><td>Gagal</td></tr>';
                        tb2.append(r2);

                        $.smallBox({
                            height: 50,
                            title : resp.responseStatus,
                            content : resp.responseMessage,
                            color : "#dc3912",
                            //sound_file: "voice_on",
                            timeout: 3000
                            //icon : "fa fa-bell swing animated"
                        });
                    }
                    else if(resp.responseCode == '5555')//validaator
                    {
                                var data = Object.values(resp.responseMessage);
                                var ln = data.length;
                                var w  = 3000 * ln; 
                                data.map(function(d){
                                    
                                    $.smallBox({
                                            height: 50,
                                            title : resp.responseStatus,
                                            content : d,
                                            color : "#dc3912",
                                            sound_file: "smallbox",
                                            timeout: w
                                            //icon : "fa fa-bell swing animated"
                                        });
                            });
                        
                    }	
                    // Hide loder
                    $('.page-loader').addClass('hidden');
                },
                error: function(xhr, ajaxOptions, thrownError) {
                    $.smallBox({
                        title : "Error",
                        content : xhr.statusText,
                        color : "#dc3912",
                        timeout: 3000
                        //icon : "fa fa-bell swing animated"
                    });
                    // Hide loder
                    $('.page-loader').addClass('hidden');
                }
            });
        
        }
    });
    //-------------- 4 ------
    $('#btn-submit-terminal4').click(function(){
      
        var str = $("#terminal-sn4").val();
        var str = str.split("\n").join(",").slice(0, -1);
        //console.log(str);
        var ar_str = str.split(",");
      
 
        var url = baseUrl + '/terminal/update';
        var action = "terminal";
        if(!$('#terminal-id4').val()) {
            url = baseUrl + '/terminal/save';
            action = "save";
        }
 
        if($('#terminal-id4').val()) {
            if(!$('#version-terminal4').val()) {
                $.smallBox({
                    //height: 50,
                    title : "Error",
                    content : 'Version can\'t be empty',
                    color : "#dc3912",
                    sound_file: "smallbox",
                    timeout: 3000
                });
                $('#version-terminal4').focus();
                return;
            }
        }
 
        // Has error
        // var hasError = false;
        // Check requirement input
        if(!$('#terminal-sn4').val()) {
            $.smallBox({
                //height: 50,
                title : "Error",
                content : 'SN can\'t be empty',
                color : "#dc3912",
                sound_file: "smallbox",
                timeout: 3000
            });
            $('#terminal-sn4').focus();
            return;
        }
 
        if(!$('#terminal-device-model4').val()) {
            $.smallBox({
                //height: 50,
                title : "Error",
                content : 'Device Model can\'t be empty',
                color : "#dc3912",
                sound_file: "smallbox",
                timeout: 3000
            });
            $('#terminal-device-model4').focus();
            return;
        }
         if(!$('#terminal-merchant4').val()) {
            $.smallBox({
                //height: 50,
                title : "Error",
                content : 'Merchant can\'t be empty',
                color : "#dc3912",
                sound_file: "smallbox",
                timeout: 3000
            });
            $('#terminal-merchant4').focus();
            return;
         }
        
         if(!$('#terminal-profile4').val()) {
            $.smallBox({
                //height: 50,
                title : "Error",
                content : 'Device Profile can\'t be empty',
                color : "#dc3912",
                sound_file: "smallbox",
                timeout: 3000
            });
            $('terminal-profile4').focus();
            return;
         }
      
         var arlistTg3 = [];
         $("#list-group-tg-t>li").each(function(){
            let a = $(this).find('.uid').text();
            arlistTg3.push(a);
         }); 
       
 
         if(arlistTg3.length == 0)
         {
            $.smallBox({
                //height: 50,
                title : "Error",
                content : 'Terminal Group can\'t be empty',
                color : "#dc3912",
                sound_file: "smallbox",
                timeout: 3000
            });
            return;
         }
       
        // Show loder
         //$('.page-loader').removeClass('hidden');
         //for(var i=0; i<ar_str.length; i++)
         //{
            
            /*
            $.ajax({
                 url: url,
                 type: 'POST',
                 headers: {
                     'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                 },
                 data: {
                     "id": $('#terminal-id3').val(),
                     "sn" : ar_str[i],
                     "modelId" : $('#terminal-device-model4').val(),
                     "merchantId" : $('#terminal-merchant4').val(),
                     "profileId" : $('#terminal-profile4').val(),
                     'terminalGroupIds' :  JSON.stringify(arlistTg3),
                     "version": $('#version-terminal3').val(),
                     
                 },
                 success: function(resp) {
                     
                     if(resp.responseCode == '0000') { //sukses
                         // Send success message
                         $.smallBox({
                             height: 50,
                             title : resp.responseStatus, 
                             content : resp.responseMessage,
                             color : "#109618",
                             //sound_file: "voice_on",
                             timeout: 3000
                             //icon : "fa fa-bell swing animated"
                         });
                         //window.location.replace(baseUrl +"/terminal");
                         //$('#terminal-sn3').val('');
                         $("#cnt-gagal-save").show();
                         var tb1 = $("#table-gagal-save").find('tbody');
                         var rowCount = $("#table-gagal-save tr").length + 1;
                         var sn = ar_str[$("#table-gagal-save tr").length];
                         var r1 = '<tr><td>'+rowCount+'</td><td>'+sn+'</td><td>Success</td></tr>';
                         tb1.append(r1);
 
                     } else if(resp.responseCode == '3333' || resp.responseCode == '0400'){ //exception and not found
                         
                         $("#cnt-gagal-save").show();
                         var tb2 = $("#table-gagal-save").find('tbody');
                         var rowCount = $("#table-gagal-save tr").length + 1;
                         var sn = ar_str[$("#table-gagal-save tr").length];
                         var r2 = '<tr><td>'+rowCount+'</td><td>'+sn+'</td><td>Gagal</td></tr>';
                         tb2.append(r2);
 
                         $.smallBox({
                             height: 50,
                             title : resp.responseStatus,
                             content : resp.responseMessage,
                             color : "#dc3912",
                             //sound_file: "voice_on",
                             timeout: 3000
                             //icon : "fa fa-bell swing animated"
                         });
                     }
                     else if(resp.responseCode == '5555')//validaator
                     {
                                 var data = Object.values(resp.responseMessage);
                                 var ln = data.length;
                                 var w  = 3000 * ln; 
                                 data.map(function(d){
                                     
                                     $.smallBox({
                                             height: 50,
                                             title : resp.responseStatus,
                                             content : d,
                                             color : "#dc3912",
                                             sound_file: "smallbox",
                                             timeout: w
                                             //icon : "fa fa-bell swing animated"
                                         });
                             });
                         
                     }	
                     // Hide loder
                     $('.page-loader').addClass('hidden');
                 },
                 error: function(xhr, ajaxOptions, thrownError) {
                     $.smallBox({
                         title : "Error",
                         content : xhr.statusText,
                         color : "#dc3912",
                         timeout: 3000
                         //icon : "fa fa-bell swing animated"
                     });
                     // Hide loder
                     $('.page-loader').addClass('hidden');
                 }
            });
            */


         
        // }
        var allAJAX = ar_str.map((m,i)=>{
            return $.ajax({
                url: url,
                type: 'POST',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data: {
                    "id": $('#terminal-id3').val(),
                    "sn" : ar_str[i],
                    "modelId" : $('#terminal-device-model4').val(),
                    "merchantId" : $('#terminal-merchant4').val(),
                    "profileId" : $('#terminal-profile4').val(),
                    'terminalGroupIds' :  JSON.stringify(arlistTg3),
                    "version": $('#version-terminal3').val(),
                    
                }}).done(function(resp){
                    //console.log(data)
                    if(resp.responseCode == '0000') { //sukses
                        // Send success message
                        $.smallBox({
                            height: 50,
                            title : resp.responseStatus, 
                            content : resp.responseMessage,
                            color : "#109618",
                            //sound_file: "voice_on",
                            timeout: 3000
                            //icon : "fa fa-bell swing animated"
                        });
                        //window.location.replace(baseUrl +"/terminal");
                        //$('#terminal-sn3').val('');
                        $("#cnt-gagal-save").show();
                        var tb1 = $("#table-gagal-save").find('tbody');
                        var rowCount = $("#table-gagal-save tr").length + 1;
                        var sn = ar_str[$("#table-gagal-save tr").length];
                        var r1 = '<tr><td>'+rowCount+'</td><td>'+sn+'</td><td>Success</td></tr>';
                        tb1.append(r1);

                    } else if(resp.responseCode == '3333' || resp.responseCode == '0400'){ //exception and not found
                        
                        $("#cnt-gagal-save").show();
                        var tb2 = $("#table-gagal-save").find('tbody');
                        var rowCount = $("#table-gagal-save tr").length + 1;
                        var sn = ar_str[$("#table-gagal-save tr").length];
                        var r2 = '<tr><td>'+rowCount+'</td><td>'+sn+'</td><td>Gagal</td></tr>';
                        tb2.append(r2);

                        $.smallBox({
                            height: 50,
                            title : resp.responseStatus,
                            content : resp.responseMessage,
                            color : "#dc3912",
                            //sound_file: "voice_on",
                            timeout: 3000
                            //icon : "fa fa-bell swing animated"
                        });
                    }
                    else if(resp.responseCode == '5555')//validaator
                    {
                                var data = Object.values(resp.responseMessage);
                                var ln = data.length;
                                var w  = 3000 * ln; 
                                data.map(function(d){
                                    
                                    $.smallBox({
                                            height: 50,
                                            title : resp.responseStatus,
                                            content : d,
                                            color : "#dc3912",
                                            sound_file: "smallbox",
                                            timeout: w
                                            //icon : "fa fa-bell swing animated"
                                        });
                            });
                        
                    }	
                }).fail(function(data){
                    console.log("FAIL", data);
                    $.smallBox({
                        height: 50,
                        title : 'error',
                        content : data,
                        color : "#dc3912",
                        sound_file: "smallbox",
                        timeout: w
                        //icon : "fa fa-bell swing animated"
                    });
                });
            });
            Promise.all(allAJAX).then(function() {
                //$("#disp")[0].innerHTML = tblStructure;
                console.log("This is completed");
                $.smallBox({
                    height: 50,
                    title : 'completed',
                    content : 'Process is completed',
                    color : "#109618",
                    //sound_file: "voice_on",
                    timeout: 3000
                    //icon : "fa fa-bell swing animated"
                });
            });
     });
})(jQuery);

  /*
            function consolePrint() {
                var _array = [1, 2, 3, 4, 5, 6, 7];
                var tblStructure =
                    "<table style='border:1px solid'><thead>This is generated after multiple ajax calls within a loop</thead>";
                var allAJAX = _array.map(m => {
                    return $.ajax(
                        "https://abc.mockapi.io/LoremIpsum/1/users/" +
                            m
                    ).done(function(data) {
                        tblStructure +=
                            "<tr><td>" + data.id + "). " + data.name + "</td></tr>";
                        console.log(data.id);
                    });
                });
                Promise.all(allAJAX).then(function() {
                    $("#disp")[0].innerHTML = tblStructure;
                    console.log("This is completed");
                });
            }

        */
      
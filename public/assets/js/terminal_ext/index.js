(function($) {
    "use strict";
    $('#dataTableTe').wrap('<div class="dataTables_scroll" />');
    var dataTableTe = null;
    if ($('#dataTableTe').length) {
        // You can use 'alert' for alert message
        // or throw to 'throw' javascript error
        // or none to 'ignore' and hide error
        // or you own function
        // please read https://datatables.net/reference/event/error
        // for more information
        $.fn.dataTable.ext.errMode = 'none';
        
        dataTableTe =  $('#dataTableTe').DataTable({
        processing: true,
        serverSide: true,
        //dom : '',
        dom: 'lrtip',
        "searching": false,
        //"scrollX": true,
        //"sScrollX": '100%',
        //"sScrollXInner": "110%",
        pageLength: 10,
        lengthMenu: [[10, 25, 50, 100], [10, 25, 50, 100]],
        pagingType: 'full_numbers',
        ajax: {
            url:   baseUrl+"/terminalExt-datatable",
            type: 'GET',
            data:  function(d){
                d.tid= $('#search-ter-ext-id').val();
                
            }
        },
        language: {
            
        },
        //rowId: 'TRANSPORT_ID',
        columns: [
            {data: "DT_RowIndex", 
               sortable: false, 
               searchable: false,
               "render": function (data, type, row, meta) {      
                          return meta.row + meta.settings._iDisplayStart + 1;     
               }  
            },
            {data: "version", name: "version", visible:false},
            {data: "tid", name: "tid"}, 
            {data: "mid", name: "mid"},   
            {data: "merchantName1", name: "merchantName1"},   
            {data: "merchantName2", name: "merchantName2"},   
            {data: "merchantName3", name: "merchantName3"},   
            {data: "id", sortable: false, searchable: false, } 
        ],
		
        columnDefs:[
			{
                "targets": 0, // your case first column
                "className": "text-center",
            },
			{
                "targets": 2, 
                "className": "mleft",
				 render: function(d,data,row) {
					 return `<span>`+d+`</span>`;
				 }
            },
			{
                "targets": 3, 
                "className": "mleft",
				 render: function(d,data,row) {
					 return `<span>`+d+`</span>`;
				 }
            },{
                "targets": 4, 
                "className": "mleft",
				 render: function(d,data,row) {
					 return `<span>`+d+`</span>`;
				 }
            },
			{
                "targets": 5, 
                "className": "mleft",
				 render: function(d,data,row) {
					 return `<span>`+d+`</span>`;
				 }
            },{
                "targets": 6, 
                "className": "mleft",
				 render: function(d,data,row) {
					 return `<span>`+d+`</span>`;
				 }
            },
			{
                targets: 7,
				"className": "text-center padd-row-table",
                render: function(d,data,row) {
                 
                    let v = row.version;
                    let id = d;
					
					return `
					<span>
						<a href="terminalExt/`+d+`" data-id="`+d+`"><img class="pencil" src="`+baseUrl+`/assets/images/icon/pencil.png" /></a>&nbsp;
						<a class="btn-delete" data-id="`+d+`" data-version="`+v+`"><img class="trash" src="`+baseUrl+`/assets/images/icon/trash.png" /></a>
                    </span>
                    `;
					
                }
            }
        ]
        });
      
    }

    
    $('#btn-search-ter-ext-id').click(function() {
        dataTableTe.draw(true);
    });

      /* filter tugel */
     // Toggle filter
    //  $('.filter .tugel').click(function() {
    //     $(this).toggleClass('open');
    //     $('.filter-title').toggleClass('hidden');
    //     $('.form-filter form').toggleClass('open-filter');
        
    // });

    // clear
    $('.clear_filter').click(function() {
        $('#search-ter-ext-id').val('');
        dataTableTe.draw(true);
    });

	$('#import-terminal-ext').click(function() {
		 
		$('#modal-list-import-te').modal('show'); 
        $('#modal-list-import-te .modal-title').html('Import Terminal ext');
       
	});
	$('#btn-select-to-list-import-terminalExt').click(function() {
		 
		//var un = $('#userName-terminal').val();
        if(!$('#excelfile').val()) {
            $.smallBox({
                height: 50,
                title : "Warning",
                content : 'Please select file to upload',
                color : "#c79121",
                sound_file: "smallbox",
                timeout: 3000
            });
            return;
        }
        // Create form data
        var form = new FormData();
        form.append('file', $('#excelfile')[0].files[0]);
        //form.append('userName', un);

        // Show loader
        $('.page-loader').removeClass('hidden');
        $('.loading>img').removeClass('hidden');
        
        $.ajax({
            url: baseUrl + '/terminalExt-import',
            type: 'POST',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            processData: false, // prevent jQuery from automatically transforming the data into a query string
            contentType: false, // is imperative, since otherwise jQuery will set it incorrectly.
            data: form,
            success: function(resp) {
                if(resp.responseCode === 200) {
					//$("#process_id").val(resp.processId);
                    //checkProcess(resp.processId);
					window.location.href=baseUrl+"/terminalExt-import/"+resp.un;
                } else {
					
                    $.smallBox({
                        height: 50,
                        title : "Error",
                        content : resp.responseMessage,
                        color : "#dc3912",
                        sound_file: "smallbox",
                        timeout: 3000
                    });
                }
                $('.page-loader').addClass('hidden');
                $('.loading>img').addClass('hidden');
        
            },
            error: function(xhr, ajaxOptions, thrownError) {
                $("#process_id").val("");
				$.smallBox({
                    title : "Error",
                    content : xhr.statusText,
                    color : "#dc3912",
                    sound_file: "smallbox",
                    timeout: 3000
                });
                $('.page-loader').addClass('hidden');
                $('.loading>img').addClass('hidden');
            }
        });
		
		
       
	});




     // Edit 
    $('#dataTableTe').on('click', '.btn-delete', function() {

        let text;
        if (confirm("Are you sure to remove!") == true) {
            
             // Send data
            $.ajax({
                url:  baseUrl + '/terminalExt/delete',
                type: 'POST',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data: {
                    'id': $(this).data('id'),
                    'version': $(this).data('version')
                },
                success: function(resp) {
                    if(resp.responseCode === 200) {
                        // Reload datatable
                        dataTableTe.ajax.reload();
                        
                        // Send success message
                        $.smallBox({
                            height: 50,
                            title : "Success",
                            content : resp.responseMessage,
                            color : "#109618",
                            sound_file: "voice_on",
                            timeout: 3000
                            //icon : "fa fa-bell swing animated"
                        });
                    } else {
                        $.smallBox({
                            height: 50,
                            title : "Error",
                            content : resp.responseMessage,
                            color : "#dc3912",
                            sound_file: "smallbox",
                            timeout: 3000
                            //icon : "fa fa-bell swing animated"
                        });
                    }
                    // Hide loder
                    $('.page-loader').addClass('hidden');
                },
                error: function(xhr, ajaxOptions, thrownError) {
                    $.smallBox({
                        title : "Error",
                        content : xhr.statusText,
                        color : "#dc3912",
                        timeout: 3000
                        //icon : "fa fa-bell swing animated"
                    });
                    // Hide loder
                    $('.page-loader').addClass('hidden');
                }
            });

       
        } 

    });

	
	 /* list terminal import temp form */ 
   $('#dataTableListTerminal_Result_Ext_Temp').wrap('<div class="dataTables_scroll" />');
   var dataTableListTerminal_Result_Ext_Temp = null;
   if ($('#dataTableListTerminal_Result_Ext_Temp').length) {
           
       //$.fn.dataTable.ext.errMode = 'none';
       
       dataTableListTerminal_Result_Ext_Temp =  $('#dataTableListTerminal_Result_Ext_Temp').DataTable({
               dom : '<"dt-toolbar row"<"col-sm-6 col-xs-12 "l><"col-sm-6 col-xs-12 text-right"<"toolbar">>>rt<"dt-toolbar-footer"<"col-md-6 "i><"col-md-6  "p>><"clear">',
               //processing: true,
               //serverSide: true,
               dom: 'lrtip',
               "searching": true,
               //"scrollX": true,
               //"sScrollX": '100%',
               //"sScrollXInner": "110%",
               //pageLength: 10,
               //lengthMenu: [[10, 25, 50, 100], [10, 25, 50, 100]],
               //pagingType: 'full_numbers',
               "lengthChange": false,
               language: {
                   paginate : {
                       first : '&laquo;',
                       last : '&raquo;',
                       next : '&rsaquo;',
                       previous : '&lsaquo;',
                   }
               },
               columnDefs:[
                   {
                       "targets": 0,
                       "className": "text-center",
                   },
                   {
                       "targets": 1,
                       "className": "text-center",
                   },{
                       targets: 2,
                       "className": "mleft",
                       render: function(d,data,row) {
                             
                           return `<span>`+d+`</span>`;
                       }
                   },{
                    targets: 3, 
                    "className": "mleft",
                        render: function(d,data,row) {
                            
                            return `<span>`+d+`</span>`;
                        }
                    },{
                        targets: 4,
                        "className": "mleft",
                            render: function(d,data,row) {
                                
                                return `<span>`+d+`</span>`;
                            }
                    },{
                        targets: 5,
                        "className": "mleft",
                            render: function(d,data,row) {
                                
                                return `<span>`+d+`</span>`;
                            }
                    },{
                        targets: 6,
                        "className": "mleft",
                            render: function(d,data,row) {
                                
                                return `<span>`+d+`</span>`;
                            }
                    },{
                        targets: 7,
                        "className": "mleft",
                            render: function(d,data,row) {
                                
                                return `<span>`+d+`</span>`;
                            }
                    }
                   
                   
                   
                   
               ],  
               
               'select' : {
                       style : 'single'
                   },
       });
       
   }
  
  
    $('body').on('click', '#btn-submit-import-te', function() {
        var arlist = [];
        $('#dataTableListTerminal_Result_Ext_Temp>tbody>tr').each(function(){
            var tid = $(this).find('.tid').text();
            var mid = $(this).find('.mid').text();
            var mn1 = $(this).find('.mn1').text();
            var mn2 = $(this).find('.mn2').text();
            var mn3 = $(this).find('.mn3').text();
            var template_id = $(this).find('.template_id').text();
	
			
            var ob = new Object();
            ob.tid = tid;
            ob.mid = mid;
            ob.mn1 = mn1;
            ob.mn2 = mn2;
            ob.mn3 = mn3;
            ob.template_id = template_id;
            if(template_id !='' || template_id!=null)
			{
				arlist.push(ob);
			}	
			
           	
        });
    
        $('.page-loader').removeClass('hidden');
        $('.loading>img').removeClass('hidden');
        
        $.ajax({
            url: baseUrl + '/terminalExt/addTerminals',
            type: 'POST',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: {
                
                "userName" : $('#userName-import-view').val(),
                "terminals" : JSON.stringify(arlist)
            },
            
            success: function(resp) {

                if(resp.responseCode == '0000') { //sukses
                    // Send success message
                    // $.smallBox({
                    //     height: 50,
                    //     title : resp.responseStatus, 
                    //     content : resp.responseMessage,
                    //     color : "#109618",
                    //     sound_file: "voice_on",
                    //     timeout: 6000
                    //     //icon : "fa fa-bell swing animated"
                    // });
                    //alert(""+resp.responseMessage+"");
                    window.location.href = baseUrl+"/terminalExt";
                } 
                else if(resp.responseCode == '0002' || resp.responseCode == '0001')
                {
                    // $.smallBox({
                    //     height: 50,
                    //     title : resp.responseStatus,
                    //     content : resp.responseMessage,
                    //     color : "#dc3912",
                    //     sound_file: "voice_on",
                    //     timeout: 6000
                    //     //icon : "fa fa-bell swing animated"
                    // });
                    //alert(""+resp.responseMessage+"");
                    window.location.href = baseUrl+"/terminalExt";
                }   
                
                $('.page-loader').addClass('hidden');
                $('.loading>img').addClass('hidden');
            },
            error: function(xhr, ajaxOptions, thrownError) {
                $("#process_id").val("");
                $.smallBox({
                    title : "Error",
                    content : xhr.statusText,
                    color : "#dc3912",
                    sound_file: "smallbox",
                    timeout: 3000
                });
                $('.page-loader').addClass('hidden');
                $('.loading>img').addClass('hidden');
            }
        }); 
        
    });

})(jQuery);
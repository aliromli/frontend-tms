(function($) {
    "use strict";
   

    // Add New  or update
    $('#btn-submit-te').click(function(){

        // Update when city id has value
        var url = baseUrl + '/terminalExt/update';
        var action = "update";
        if(!$('#te-id').val()) {
            url = baseUrl + '/terminalExt/save';
            action = "save";
        }

        if($('#te-id').val()) {
            if(!$('#version-te').val()) {
                $.smallBox({
                    //height: 50,
                    title : "Error",
                    content : 'Version can\'t be empty',
                    color : "#dc3912",
                    sound_file: "smallbox",
                    timeout: 3000
                });
                $('#version-te').focus();
                return;
            }
        }

        // Has error
        // var hasError = false;
        // Check requirement input
        if(!$('#te-tid').val()) {
            $.smallBox({
                //height: 50,
                title : "Error",
                content : 'TID  can\'t be empty',
                color : "#dc3912",
                sound_file: "smallbox",
                timeout: 3000
            });
            $('#te-tid').focus();
            return;
        }
		
		if(!$('#te-mid').val()) {
            $.smallBox({
                //height: 50,
                title : "Error",
                content : 'MID  can\'t be empty',
                color : "#dc3912",
                sound_file: "smallbox",
                timeout: 3000
            });
            $('#te-mid').focus();
            return;
        }

        if(!$('#te-merchantName1').val()) {
            $.smallBox({
                //height: 50,
                title : "Error",
                content : 'MerchantName1 can\'t be empty',
                color : "#dc3912",
                sound_file: "smallbox",
                timeout: 3000
            });
            $('#te-merchantName1').focus();
            return;
        }
		
		if(!$('#select_terminal_tempalte').val()) {
            $.smallBox({
                //height: 50,
                title : "Error",
                content : 'Template can\'t be empty',
                color : "#dc3912",
                sound_file: "smallbox",
                timeout: 3000
            });
            $('#select_terminal_tempalte').focus();
            return;
        }
		
		
		// Show loder
         $('.page-loader').removeClass('hidden');
		 $('.loading>img').removeClass('hidden');

        // Send data
        $.ajax({
            url: url,
            type: 'POST',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: {
                'id': $('#te-id').val(),   
				'tid' : $('#te-tid').val(),
				'mid' : $('#te-mid').val(),
				'merchantName1' : $('#te-merchantName1').val(),
				'merchantName2' : $('#te-merchantName2').val(),
				'merchantName3' : $('#te-merchantName3').val(),
				'templateId' : $('#select_terminal_tempalte').val(),
                'version': $('#version-te').val(),
                
            },
		
            success: function(resp) {
				
				
                if(resp.responseCode == '0000') { //sukses
                   
                    // Send success message
                    $.smallBox({
                        height: 50,
                        title : resp.responseStatus, 
                        content : resp.responseMessage,
                        color : "#109618",
                        sound_file: "voice_on",
                        timeout: 3000
                        //icon : "fa fa-bell swing animated"
                    });

                    window.location.href = baseUrl +"/terminalExt";

 
                } else if(resp.responseCode == '3333' || resp.responseCode == '0400'){ //exception and not found
					
					 $.smallBox({
                        height: 50,
                        title : resp.responseStatus,
                        content : resp.responseMessage,
                        color : "#dc3912",
                        sound_file: "voice_on",
                        timeout: 3000
                        //icon : "fa fa-bell swing animated"
                    });
                }
				else if(resp.responseCode == '5555')//validaator
				{
					
						 var data = Object.values(resp.responseMessage);
						 var ln = data.length;
						 var w  = 3000 * ln; 
						 data.map(function(d){
							 
								$.smallBox({
										height: 50,
										title : resp.responseStatus,
										content : d,
										color : "#dc3912",
										sound_file: "smallbox",
										timeout: w
										//icon : "fa fa-bell swing animated"
									});
						});
					
				}	
                // Hide loder
                $('.page-loader').addClass('hidden');
				$('.loading>img').addClass('hidden');
            },
            error: function(xhr, ajaxOptions, thrownError) {
                $.smallBox({
                    title : "Error",
                    content : xhr.statusText,
                    color : "#dc3912",
                    timeout: 3000
                    //icon : "fa fa-bell swing animated"
                });
                // Hide loder
                $('.page-loader').addClass('hidden');
				$('.loading>img').addClass('hidden');
            }
        });

    });
	

   


})(jQuery);
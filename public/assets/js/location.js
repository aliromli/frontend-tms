(function($) {
    "use strict";
	
    // Terminal group modal tabel
    $('#dataTableTgListGFormon').wrap('<div class="dataTables_scroll" />');
    var dataTableTgListGFormon = null;
    if ($('#dataTableTgListGFormon').length) {
        // You can use 'alert' for alert message
        // or throw to 'throw' javascript error
        // or none to 'ignore' and hide error
        // or you own function
        // please read https://datatables.net/reference/event/error
        // for more information
        $.fn.dataTable.ext.errMode = 'none';
        dataTableTgListGFormon =  $('#dataTableTgListGFormon').DataTable({
        processing: true,
        serverSide: true,
        //dom : '',
        dom: 'lrtip',
        "searching": false,
        pageLength: 10,
        lengthMenu: [[10, 25, 50, 100], [10, 25, 50, 100]],
        pagingType: 'full_numbers',
        ajax: {
            url:   baseUrl+"/terminal-group-datatables",
            type: 'GET',
            data:  function(d){
                d.name = $('#search-name-g-hb').val();
            }
        },
        language: {
         
        },
        //rowId: 'TRANSPORT_ID',
        columns: [
            {data: "DT_RowIndex", 
               sortable: false, 
               searchable: false,
               "render": function (data, type, row, meta) {      
                          return meta.row + meta.settings._iDisplayStart + 1;     
               }  
            },
            {data: "version", name: "version", visible:false},
            {data: "name", name: "name"},
            {data : "description", name : "description"},
            {data: "id", sortable: false, searchable: false, }
        ],
        columnDefs:[
			{
				"targets": 0,
				"className": "text-center",
			},
			{
					"targets": 2,
					"className": "mleft",
					 render: function(d,data,row) {
						 return `<span>`+d+`</span>`
					 }
			},
			{
					"targets": 3,
					"className": "mleft",
					 render: function(d,data,row) {
						 return `<span>`+d+`</span>`
					 }
			},
	        {
                targets: 4,
				"className": "text-center",
                render: function(d,data,row) {
                    let id = d;
                    let v = row.version;
                   
                    return `
                    <input 
                    type="checkbox" 
                    name="chtg_on[]" 
                    value="true"  
                    data-id="`+d+`" 
                    data-name="`+row.name+`" 
                    data-description="`+row.description+`"                      
                    data-version="`+v+`"
                    />
                  `;
                }
            }
        ]
        });
      
    }
	
	// terminal modal tabel
    $('#dataTableTerListGFormon').wrap('<div class="dataTables_scroll" />');
    var dataTableTerListGFormon = null;
    if ($('#dataTableTerListGFormon').length) {
        // You can use 'alert' for alert message
			// or throw to 'throw' javascript error
			// or none to 'ignore' and hide error
			// or you own function
			// please read https://datatables.net/reference/event/error
			// for more information
			$.fn.dataTable.ext.errMode = 'none';
			
			dataTableTerListGFormon =  $('#dataTableTerListGFormon').DataTable({
			processing: true,
			serverSide: true,
			//dom: 'lrtip',
			dom: 'tip',
			"searching": false,
			
			pageLength: 10,
			lengthMenu: [[10, 25, 50, 100], [10, 25, 50, 100]],
			pagingType: 'full_numbers',
			ajax: { 
				url:   baseUrl+"/terminal-datatables", 
				type: 'GET',
				data:  function(d){

					d.sn = $('#search-sn-hb').val();
					
				}
			},
			language: {
			 
			},
			//rowId: 'TRANSPORT_ID',
			columns: [
				{data: "DT_RowIndex", 
				   sortable: false, 
				   searchable: false,
				   "render": function (data, type, row, meta) {      
							  return meta.row + meta.settings._iDisplayStart + 1;     
				   }  
				},
				{data: "sn", name: "sn"},
				//{data: "terminalId", name: "terminalId",visible:false},
				{data: "id", sortable: false, searchable: false, }
			],
			columnDefs:[
				{
					"targets": 0,
					"className": "text-center",
				},
				//{
				//	"targets": 1,
				//	"className": "mleft",
				//	 render: function(d,data,row) {
				//		 return `<span>`+d+`</span>`
				//	 }
				//},
				{
					targets: 2,
					"className": "text-center",
					render: function(d,data,row) {
						let id = d;
						//let idT = row.id;
					 	return `<span>
							<input 
							type="checkbox" 
							name="chter_on[]" 
							value="true"  
							data-id="`+d+`" 
							data-sn="`+row.sn+`"
							
							/></span>
							`;
					   
					}
				}
			]
		});
	
	}
	
    
    $('#btn-search-name-on').click(function() {
        dataTableTgListGFormon.draw(true);
    });
	$('#btn-clear-name-hb').click(function() {
		$('#search-name-g-on').val('');
        dataTableTgListGFormon.draw(true);
    });
	
	$('body').on('click', '#btn-add-terminal-group-on', function() {
		
		$('#modal-list-tg-on').modal('show');
        $('#modal-list-tg-on .modal-title').html('List Terminal Group');
		dataTableTgListGFormon.draw(true);
		
	});
	//=== =====
	 $('#btn-search-sn-on').click(function() {
        dataTableTerListGFormon.draw(true);
    });
	$('#btn-clear-sn-on').click(function() {
		$('#search-sn-on').val('');
        dataTableTerListGFormon.draw(true);
    });
	
	$('body').on('click', '#btn-add-terminal-list-on', function() {
		
		$('#modal-list-t-on').modal('show');
        $('#modal-list-t-on .modal-title').html('List Terminal');
		dataTableTerListGFormon.draw(true);
	});
	
	
	
	
	$('body').on('click', '#btn-tg-to-list-on', function() {
		var arlist = [];
        $('#select-tg-on option').each(function(index,element){
           
            var ob = new Object();
			ob.name = element.text;
			ob.uid = element.value;
			arlist.push(ob);
        });
        var arlist2 = [];
        var datac = $('[name="chtg_on[]"]');
        // var html = "";
        $.each(datac, function() {
            var $this = $(this);

            // check if the checkbox is checked
            if($this.is(":checked")) {
                var ob = new Object();
                let find = 0;
                if(arlist.length > 0)
                {
                    for(var i=0; i<arlist.length; i++)
                    {
                        if(arlist[i].uid == $this.data("id"))
                        {
                            find = 1;
                            break;
                        }
                    }
                }
                
                if(find == 0) 
                {
                    ob.name = $this.data("name");
                    ob.uid = $this.data("id");
                    arlist2.push(ob);
                }
                
            }
        });	

        let arr = [...arlist, ...arlist2];
        //console.log('1',arr);
        let mergedArr = [...new Set(arr)];
        //console.log('2',mergedArr);

        var d = "";		
		for(var i=0; i<mergedArr.length; i++)
		{
		    let name = mergedArr[i].name;
			//let description = mergedArr[i].description;
            let uid = mergedArr[i].uid;
			//let act = `<button type="button" class="btn btn-flat btn-danger btn-xs btn-delete-list-tg-t"><i class="fa fa-remove"></i></button>`;
			d+=`<option value="`+uid+`">`+name+`</option>`;
		} 
        $('#select-tg-on option').remove();  
        $('#select-tg-on').html(d);       
		$('#modal-list-tg-on').modal('hide');
		
	});
	
	$('body').on('click', '#btn-t-to-list-on', function() {
		
		var arlist = [];
        $('#select-t-on option').each(function(index,element){
           
            var ob = new Object();
			ob.sn = element.text;
			ob.uid = element.value;
			arlist.push(ob);
        });
        var arlist2 = [];
        var datac = $('[name="chter_on[]"]');
       // var html = "";
      
        $.each(datac, function() {
            var $this = $(this);

            // check if the checkbox is checked
            if($this.is(":checked")) {
                var ob = new Object();
                let find = 0;
                if(arlist.length > 0)
                {
                    for(var i=0; i<arlist.length; i++)
                    {
                        if(arlist[i].uid == $this.data("id"))
                        {
                            find = 1;
                            break;
                        }
                    }
                }
                
                if(find == 0) 
                {
                    ob.sn = $this.data("sn");
                    ob.uid = $this.data("id");
                    arlist2.push(ob);
                }
                
            }
        });	

        let arr = [...arlist, ...arlist2];
        //console.log('1',arr);
        let mergedArr = [...new Set(arr)];
        //console.log('2',mergedArr);

        var d = "";		
		for(var i=0; i<mergedArr.length; i++)
		{
		    let sn = mergedArr[i].sn;
			//let description = mergedArr[i].description;
            let uid = mergedArr[i].uid;
			//let act = `<button type="button" class="btn btn-flat btn-danger btn-xs btn-delete-list-tg-t"><i class="fa fa-remove"></i></button>`;
			d+=`<option value="`+uid+`">`+sn+`</option>`;
		} 
        $('#select-t-on option').remove();  
        $('#select-t-on').html(d);       
		$('#modal-list-t-on').modal('hide');
		
	});
	
	
	
	
	$('body').on('change', '#export_all_on', function() {
		if(this.checked) {
		    // checkbox is checked
		    //alert("cek");
		}
		else
		{
			//alert('no');
		}
	});
	
	
	$('body').on('click', '#export-online', function() {
		
		var data = new FormData();
		data.append('bagian',$('#bagian').val());
		$.ajax({
            url: baseUrl+"/online/export", 
            type: 'POST',
			contentType: false,
			processData: false,
			cache: false,
			xhrFields: {
                responseType: 'blob'
            },
			headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
			data: data,
			success: function (data, textStatus, xhr) {
                // check for a filename
                var filename = "";
                var disposition = xhr.getResponseHeader('Content-Disposition');
                if (disposition && disposition.indexOf('attachment') !== -1) {
                    var filenameRegex = /filename[^;=\n]*=((['"]).*?\2|[^;\n]*)/;
                    var matches = filenameRegex.exec(disposition);
                    if (matches != null && matches[1]) filename = matches[1].replace(/['"]/g, '');
                    var a = document.createElement('a');
                    var url = window.URL.createObjectURL(data);
                    a.href = url;
                    a.download = filename;
                    document.body.append(a);
                    a.click();
                    a.remove();
                    window.URL.revokeObjectURL(url);
                }
                else {
                    alert("Error");
                }
               
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {

            },

        });
		
	});
  
})(jQuery);
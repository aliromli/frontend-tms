(function($) {
    "use strict";
    
   

     // Add New Or Update Country
     $('#btn-submit').click(function(){

        // Update when user id has value
        var url = baseUrl + '/country/update';
        var action = "update";
        if(!$('#country-id').val()) {
            url = baseUrl + '/country/save';
            action = "save";
        }

        if($('#country-id').val()) {
            if(!$('#version').val()) {
                $.smallBox({
                    //height: 50,
                    title : "Error",
                    content : 'Version can\'t be empty',
                    color : "#dc3912",
                    sound_file: "smallbox",
                    timeout: 3000
                });
                $('#version').focus();
                return;
            }
        }

        // Has error
        // var hasError = false;
        // Check requirement input
        if(!$('#code').val()) {
            $.smallBox({
                //height: 50,
                title : "Error",
                content : 'Code can\'t be empty',
                color : "#dc3912",
                sound_file: "smallbox",
                timeout: 3000
            });
            $('#code').focus();
            return;
        }
        if(!$('#name').val()) {
            $.smallBox({
                //height: 50,
                title : "Error",
                content : 'Name can\'t be empty',
                color : "#dc3912",
                sound_file: "smallbox",
                timeout: 3000
            });
            $('#name').focus();
            return;
        }

       

        // Show loder
        $('.page-loader').removeClass('hidden');

        // Send data
        $.ajax({
            url: url,
            type: 'POST',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: {
                'id': $('#country-id').val(),
                'code': $('#code').val(),
                'name': $('#name').val(),
                'version' : $('#version').val()
               
            },
            success: function(resp) {
                
				if(resp.responseCode == '0000') { //sukses
                    // Send success message
                    $.smallBox({
                        height: 50,
                        title : resp.responseStatus, 
                        content : resp.responseMessage,
                        color : "#109618",
                        sound_file: "voice_on",
                        timeout: 3000
                        //icon : "fa fa-bell swing animated"
                    });
					 //let g = baseUrl + "/country-form-index";
					 //$( "#my-content-div" ).load(g); 
                     window.location.href = baseUrl + "/country";
                   
 
                } else if(resp.responseCode == '3333' || resp.responseCode == '0400'){ //exception and not found
					
					 $.smallBox({
                        height: 50,
                        title : resp.responseStatus,
                        content : resp.responseMessage,
                        color : "#dc3912",
                        sound_file: "voice_on",
                        timeout: 3000
                        //icon : "fa fa-bell swing animated"
                    });
                }
				else if(resp.responseCode == '5555')//validaator
				{
					
						 var data = Object.values(resp.responseMessage);
						 var ln = data.length;
						 var w  = 3000 * ln; 
						 data.map(function(d){
							 
								$.smallBox({
										height: 50,
										title : resp.responseStatus,
										content : d,
										color : "#dc3912",
										sound_file: "smallbox",
										timeout: w
										//icon : "fa fa-bell swing animated"
									});
						});
					
				}	
                // Hide loder
                $('.page-loader').addClass('hidden');
            },
            error: function(xhr, ajaxOptions, thrownError) {
                $.smallBox({
                    title : "Error",
                    content : xhr.statusText,
                    color : "#dc3912",
                    timeout: 3000
                    //icon : "fa fa-bell swing animated"
                });
                // Hide loder
                $('.page-loader').addClass('hidden');
            }
        });

    });
	
   
   

})(jQuery);

(function($) {
    "use strict";
    
    $('#dataTableCountry').wrap('<div class="dataTables_scroll" />');
    var dataTableCountry = null;
    if ($('#dataTableCountry').length) {
        // You can use 'alert' for alert message
        // or throw to 'throw' javascript error
        // or none to 'ignore' and hide error
        // or you own function
        // please read https://datatables.net/reference/event/error
        // for more information
        $.fn.dataTable.ext.errMode = 'none';
        
        dataTableCountry =  $('#dataTableCountry').DataTable({
        processing: true,
        serverSide: true,
        dom: 'lrtip',
        "searching": false,
        "lengthChange": true,
        pageLength: 10,
        lengthMenu: [[10, 25, 50, 100], [10, 25, 50, 100]],
        pagingType: 'full_numbers',
        "responsive": true,
        "stateSave": true,
        //"stateLoaded": function (settings, data) {
            //alert( 'Saved filter was: '+data.search.search );
        //},
        ajax: {
            url:   baseUrl+"/country-datatables",
            type: 'GET',
            data:  function(d){
                d.name = $('#search-country').val();
                }
        },
        language: {
            //processing: '<div style="display: none"></div>',
            // info: 'Menampilkan _START_ - _END_ dari _TOTAL_ ',
            //search: '<span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>',
            // zeroRecords: 'Tidak ada data yang cocok dengan kriteria pencarian',
            // emptyTable: 'Data tidak tersedia',
            // paginate: {
            //     first: '&laquo;',
            //     last: '&raquo;',
            //     next: '&rsaquo;',
            //     previous: '&lsaquo;'
            // },
            //lengthMenu: "Baris per halaman: _MENU_ "
        },
        //rowId: 'TRANSPORT_ID',
        columns: [
            {data: "DT_RowIndex", 
               sortable: false, 
               searchable: false,
               "render": function (data, type, row, meta) {      
                          return meta.row + meta.settings._iDisplayStart + 1;     
               }  
            },
            {data: "version", name: "version", visible:false},
            {data: "code", name: "code"},
            {data: "name", name: "name"},
            {data: "id", sortable: false, searchable: false, }
        ],
        columnDefs:[
			{
                "targets": 0, // your case first column
                "className": "text-center",
            },
			
			{
                "targets": 2, // your case first column
                "className": "text-center",
            },
			{
                "targets": 3, 
                "className": "mleft",
                render : function(d){
                    return '<span>'+d+'</span>';
                }
            },
	        {
                targets: 4,
				"className": "text-center padd-row-table",
                render: function(d,data,row) {
                 
                    let v = row.version;
                   
                    return `<span>
                    <a  class="btn-edit" data-id="`+d+`" ><img class="pencil" src="`+baseUrl+`/assets/images/icon/pencil.png" /></a>&nbsp;
                    <a  class="btn-delete" data-id="`+d+`" data-version="`+v+`"><img class="trash" src="`+baseUrl+`/assets/images/icon/trash.png" /></a>
                    </span>`;
                }
            }
        ]
        });

      
    }

   
    //$('.data-tables>.page-link').addClass('btn-xs');
        //$('#dataTableCountry').DataTable().draw(true);
    $('body').on('click', '#btn-cari-country', function(){
        dataTableCountry.draw(true);
        //$('#dataTableCountry').DataTable().draw(true);
    });

    // Add  
    $('body').on('click', '#btn-add-country', function(){
       	 window.location.href = baseUrl + "/country-form";
         //let g = baseUrl + "/country-form";
         //$( "#my-content-div" ).load(g); 
        
    });

      /* filter tugel */
     // Toggle filter
    //$('.filter .tugel').click(function() {
    //    $(this).toggleClass('open');
    //    $('.filter-title').toggleClass('hidden');
    //    $('.form-filter form').toggleClass('open-filter');
        
    //});

     // clear
     $('.clear_filter').click(function() {
       
        $('#search-country').val('');
        dataTableCountry.draw(true);
    });

    
    // Edit 
	$('#dataTableCountry').on('click', '.btn-edit', function() {
		let g = baseUrl + "/country/"+$(this).data('id');
        //$( "#my-content-div" ).load(g); 
        window.location.href = g;
	});
    /*$('#dataTableCountry').on('click', '.btn-edit', function() {
        //$('#modal-country').modal('show');
        //$('#modal-country .modal-title').html('Edit Country');
        //$('#email').attr('readonly','readonly');
        //$('#divct-version').show();
       
        // Hide loder
        $('.page-loader').removeClass('hidden');

        // Get data
        // Send data
        $.ajax({
            url: baseUrl + '/country/' + $(this).data('id'),
            type: 'GET',
            success: function(resp) {
                if(resp.responseCode === 200) {
                   
                    $('#country-id').val(resp.responseMessage[0].id);
                    $('#name').val(resp.responseMessage[0].name);
                    $('#code').val(resp.responseMessage[0].code);
                    $('#version').val(resp.responseMessage[0].version);

                }
                else
                {
                    $.smallBox({
                        height: 50,
                        title : "Error",
                        content : resp.responseMessage,
                        color : "#dc3912",
                        sound_file: "smallbox",
                        timeout: 3000
                        //icon : "fa fa-bell swing animated"
                    });
                }
               
                // Hide loder
                $('.page-loader').addClass('hidden');
            },
            error: function(xhr, ajaxOptions, thrownError) {
                $.smallBox({
                    title : "Error",
                    content : xhr.statusText,
                    color : "#dc3912",
                    timeout: 3000
                    //icon : "fa fa-bell swing animated"
                });
                // Hide loder
                $('.page-loader').addClass('hidden');
            }
        });
    });
    */
    $('#dataTableCountry').on('click', '.btn-delete', function() {

        let text;
        if (confirm("Are you sure to remove!") == true) {
            
             // Send data
            $.ajax({
                url:  baseUrl + '/country/delete',
                type: 'POST',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data: {
                    'id': $(this).data('id'),
                    'version': $(this).data('version')
                },
                success: function(resp) {
                    if(resp.responseCode === 200) {
                        // Reload datatable
                        dataTableCountry.ajax.reload();
                        
                        // Send success message
                        $.smallBox({
                            height: 50,
                            title : "Success",
                            content : resp.responseMessage,
                            color : "#109618",
                            sound_file: "voice_on",
                            timeout: 3000
                            //icon : "fa fa-bell swing animated"
                        });
                    } else {
                        $.smallBox({
                            height: 50,
                            title : "Error",
                            content : resp.responseMessage,
                            color : "#dc3912",
                            sound_file: "smallbox",
                            timeout: 3000
                            //icon : "fa fa-bell swing animated"
                        });
                    }
                    // Hide loder
                    $('.page-loader').addClass('hidden');
                },
                error: function(xhr, ajaxOptions, thrownError) {
                    $.smallBox({
                        title : "Error",
                        content : xhr.statusText,
                        color : "#dc3912",
                        timeout: 3000
                        //icon : "fa fa-bell swing animated"
                    });
                    // Hide loder
                    $('.page-loader').addClass('hidden');
                }
            });

       
        } 

    });
    
   

})(jQuery);

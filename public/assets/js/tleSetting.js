(function($) {
    "use strict";
    $('#dataTableTleSetting').wrap('<div class="dataTables_scroll" />');
    var dataTableTleSetting = null;
    if ($('#dataTableTleSetting').length) {
        // You can use 'alert' for alert message
        // or throw to 'throw' javascript error
        // or none to 'ignore' and hide error
        // or you own function
        // please read https://datatables.net/reference/event/error
        // for more information
        $.fn.dataTable.ext.errMode = 'none';
        
        dataTableTleSetting =  $('#dataTableTleSetting').DataTable({
        processing: true,
        serverSide: true,
        //dom : '',
        dom: 'lrtip',
        "searching": false,
        //"scrollX": true,
        //"sScrollX": '100%',
        //"sScrollXInner": "110%",
        pageLength: 10,
        lengthMenu: [[10, 25, 50, 100], [10, 25, 50, 100]],
        pagingType: 'full_numbers',
        ajax: {
            url:   baseUrl+"/tleSetting-datatable",
            type: 'GET',
            data:  function(d){
                d.tleId= $('#search-tle').val();
                
            }
        },
        language: {
            
        },
        //rowId: 'TRANSPORT_ID',
        columns: [
            {data: "DT_RowIndex", 
               sortable: false, 
               searchable: false,
               "render": function (data, type, row, meta) {      
                          return meta.row + meta.settings._iDisplayStart + 1;     
               }  
            },
            {data: "version", name: "version", visible:false},
            {data: "tleEftSec", name: "tleEftSec"}, 
            {data: "tleId", name: "tleId"},   
            {data: "id", sortable: false, searchable: false, }
        ],
		
        columnDefs:[
	        {
                "targets": 0, // your case first column
                "className": "text-center",
            },
			{
                "targets": 2, 
                "className": "mleft",
				 render: function(d,data,row) {
					 return `<span>`+d+`</span>`;
				 }
            },
			{
                "targets": 3, 
                "className": "mleft",
				 render: function(d,data,row) {
					 return `<span>`+d+`</span>`;
				 }
            },
			{
                targets: 4,
				"className": "text-center padd-row-table",
                render: function(d,data,row) {
                 
                    let v = row.version;
                    let id = d;
                    return `
					<span>
						<a href="tleSetting/`+d+`" data-id="`+d+`"><img class="pencil" src="`+baseUrl+`/assets/images/icon/pencil.png" /></a>&nbsp;
						<a class="btn-delete" data-id="`+d+`" data-version="`+v+`"><img class="trash" src="`+baseUrl+`/assets/images/icon/trash.png" /></a>
                    </span>
                    `;
                }
            }
        ]
        });
      
    }

    
    $('#btn-search-tle').click(function() {
        dataTableTleSetting.draw(true);
    });

      /* filter tugel */
     // Toggle filter
    //  $('.filter .tugel').click(function() {
    //     $(this).toggleClass('open');
    //     $('.filter-title').toggleClass('hidden');
    //     $('.form-filter form').toggleClass('open-filter');
        
    // });

    // clear
    $('.clear_filter').click(function() {
        $('#search-tle').val('');
       
        dataTableTleSetting.draw(true);
    });

    // Add New  or update
    $('#btn-submit-tl').click(function(){

        // Update when city id has value
        var url = baseUrl + '/tleSetting/update';
        var action = "update";
        if(!$('#tl-id').val()) {
            url = baseUrl + '/tleSetting/save';
            action = "save";
        }

        if($('#tl-id').val()) {
            if(!$('#version-tl').val()) {
                $.smallBox({
                    //height: 50,
                    title : "Error",
                    content : 'Version can\'t be empty',
                    color : "#dc3912",
                    sound_file: "smallbox",
                    timeout: 3000
                });
                $('#version-tl').focus();
                return;
            }
        }
		
		if(!$('#tl-tleId').val()) {
           
                $.smallBox({
                    //height: 50,
                    title : "Error",
                    content : 'TleId can\'t be empty',
                    color : "#dc3912",
                    sound_file: "smallbox",
                    timeout: 3000
                });
                $('#tl-tleId').focus();
                return;
           
        }
		
		if(!$('#tl-tleEftSec').val()) {
           
                $.smallBox({
                    //height: 50,
                    title : "Error",
                    content : 'TleEftSec can\'t be empty',
                    color : "#dc3912",
                    sound_file: "smallbox",
                    timeout: 3000
                });
                $('#tl-tleEftSec').focus();
                return;
            
        }
		
		if(!$('#tl-acquirerId').val()) {
            
                $.smallBox({
                    //height: 50,
                    title : "Error",
                    content : 'AcquirerId can\'t be empty',
                    color : "#dc3912",
                    sound_file: "smallbox",
                    timeout: 3000
                });
                $('#tl-acquirerId').focus();
                return;
       
        }
		
		if(!$('#tl-ltmkAid').val()) {
            
                $.smallBox({
                    //height: 50,
                    title : "Error",
                    content : 'AcquirerId can\'t be empty',
                    color : "#dc3912",
                    sound_file: "smallbox",
                    timeout: 3000
                });
                $('#tl-acquirerId').focus();
                return;
           
        }
		
		if(!$('#tl-vendorId').val()) {
            
                $.smallBox({
                    //height: 50,
                    title : "Error",
                    content : 'VendorId can\'t be empty',
                    color : "#dc3912",
                    sound_file: "smallbox",
                    timeout: 3000
                });
                $('#tl-vendorId').focus();
                return;
           
        }
		
		if(!$('#tl-tleVer').val()) {
            
                $.smallBox({
                    //height: 50,
                    title : "Error",
                    content : 'TleVer can\'t be empty',
                    color : "#dc3912",
                    sound_file: "smallbox",
                    timeout: 3000
                });
                $('#tl-tleVer').focus();
                return;
           
        }
		 
		
		if(!$('#version-tl').val()) {
            $.smallBox({
                //height: 50,
                title : "Error",
                content : 'Version can\'t be empty',
                color : "#dc3912",
                sound_file: "smallbox",
                timeout: 3000
            });
            $('#version-tl').focus();
            return;
        }
		
		// Show loder
        $('.page-loader').removeClass('hidden');
		$('.loading>img').removeClass('hidden');

        // Send data
        $.ajax({
            url: url,
            type: 'POST',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: {
                'id': $('#tl-id').val(),
                'tleId': $('#tl-tleId').val(),
                'tleEftSec': $('#tl-tleEftSec').val(),
                'acquirerId': $('#tl-acquirerId').val(),
                'ltmkAid': $('#tl-ltmkAid').val(),
                'vendorId': $('#tl-vendorId').val(),
                'tleVer': $('#tl-tleVer').val(),
                'kmsSecureNii': $('#tl-kmsSecureNii').val(),
                'edcSecureNii': $('#tl-edcSecureNii').val(),
                'capkExponent': $('#tl-capkExponent').val(),
                'capkLength': $('#tl-capkLength').val(),
                'capkValue': $('#tl-capkValue').val(),
                'aidLength': $('#tl-aidLength').val(),
                'aidValue': $('#tl-aidValue').val(),
                'encryptedField1': $('#tl-encryptedField1').val(),
                'encryptedField2': $('#tl-encryptedField2').val(),
                'encryptedField3': $('#tl-encryptedField3').val(),
                'encryptedField4': $('#tl-encryptedField4').val(),
                'encryptedField5': $('#tl-encryptedField5').val(),
                'encryptedField6': $('#tl-encryptedField6').val(),
                'encryptedField7': $('#tl-encryptedField7').val(),
                'encryptedField8': $('#tl-encryptedField8').val(),
                'encryptedField9': $('#tl-encryptedField9').val(),
                'encryptedField10': $('#tl-encryptedField10').val(),
                'version': $('#version-tl').val(),
                
            },
		
            success: function(resp) {
				
				
                if(resp.responseCode == '0000') { //sukses
                   
                    // Send success message
                    $.smallBox({
                        height: 50,
                        title : resp.responseStatus, 
                        content : resp.responseMessage,
                        color : "#109618",
                        sound_file: "voice_on",
                        timeout: 3000
                        //icon : "fa fa-bell swing animated"
                    });

                    if($('#tl-id').val()=="") {
                       
						
                        $('#tl-tleId').val('');
						$('#tl-tleEftSec').val('');
						$('#tl-acquirerId').val('');
						$('#tl-ltmkAid').val('');
						$('#tl-vendorId').val('');
						$('#tl-tleVer').val('');
						$('#tl-kmsSecureNii').val('');
						$('#tl-edcSecureNii').val('');
						$('#tl-capkExponent').val('');
						$('#tl-capkLength').val('');
						$('#tl-capkValue').val('');
						$('#tl-aidLength').val('');
						$('#tl-aidValue').val('');
						$('#tl-encryptedField1').val('');
						$('#tl-encryptedField2').val('');
						$('#tl-encryptedField3').val('');
						$('#tl-encryptedField4').val('');
						$('#tl-encryptedField5').val('');
						$('#tl-encryptedField6').val('');
						$('#tl-encryptedField7').val('');
						$('#tl-encryptedField8').val('');
						$('#tl-encryptedField9').val('');
						$('#tl-encryptedField10').val('');
						
                    }

 
                } else if(resp.responseCode == '3333' || resp.responseCode == '0400'){ //exception and not found
					
					 $.smallBox({
                        height: 50,
                        title : resp.responseStatus,
                        content : resp.responseMessage,
                        color : "#dc3912",
                        sound_file: "voice_on",
                        timeout: 3000
                        //icon : "fa fa-bell swing animated"
                    });
                }
				else if(resp.responseCode == '5555')//validaator
				{
					
						 var data = Object.values(resp.responseMessage);
						 var ln = data.length;
						 var w  = 3000 * ln; 
						 data.map(function(d){
							 
								$.smallBox({
										height: 50,
										title : resp.responseStatus,
										content : d,
										color : "#dc3912",
										sound_file: "smallbox",
										timeout: w
										//icon : "fa fa-bell swing animated"
									});
						});
					
				}	
                // Hide loder
                $('.page-loader').addClass('hidden');
				$('.loading>img').addClass('hidden');
            },
            error: function(xhr, ajaxOptions, thrownError) {
                $.smallBox({
                    title : "Error",
                    content : xhr.statusText,
                    color : "#dc3912",
                    timeout: 3000
                    //icon : "fa fa-bell swing animated"
                });
                // Hide loder
                $('.page-loader').addClass('hidden');
				$('.loading>img').addClass('hidden');
            }
        });

    });

    // Edit 
    $('#dataTablePublicKey').on('click', '.btn-delete', function() {

        let text;
        if (confirm("Are you sure to remove!") == true) {
            
             // Send data
            $.ajax({
                url:  baseUrl + '/publicKey/delete',
                type: 'POST',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data: {
                    'id': $(this).data('id'),
                    'version': $(this).data('version')
                },
                success: function(resp) {
                    if(resp.responseCode === 200) {
                        // Reload datatable
                        dataTablePublicKey.ajax.reload();
                        
                        // Send success message
                        $.smallBox({
                            height: 50,
                            title : "Success",
                            content : resp.responseMessage,
                            color : "#109618",
                            sound_file: "voice_on",
                            timeout: 3000
                            //icon : "fa fa-bell swing animated"
                        });
                    } else {
                        $.smallBox({
                            height: 50,
                            title : "Error",
                            content : resp.responseMessage,
                            color : "#dc3912",
                            sound_file: "smallbox",
                            timeout: 3000
                            //icon : "fa fa-bell swing animated"
                        });
                    }
                    // Hide loder
                    $('.page-loader').addClass('hidden');
                },
                error: function(xhr, ajaxOptions, thrownError) {
                    $.smallBox({
                        title : "Error",
                        content : xhr.statusText,
                        color : "#dc3912",
                        timeout: 3000
                        //icon : "fa fa-bell swing animated"
                    });
                    // Hide loder
                    $('.page-loader').addClass('hidden');
                }
            });

       
        } 

    });



})(jQuery);
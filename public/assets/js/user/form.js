(function($) {
    "use strict";


    // Add New Or Update Program
    $('#btn-submit').click(function(){

        // Update when user id has value
        var url = baseUrl + '/user/update';
        var action = "update";
        if(!$('#user-id').val()) {
            url = baseUrl + '/user/save';
            action = "save";
        }

        // Has error
        // var hasError = false;
        // Check requirement input
        if(!$('#email').val()) {
            $.smallBox({
                //height: 50,
                title : "Error",
                content : 'Email can\'t be empty',
                color : "#dc3912",
                sound_file: "smallbox",
                timeout: 3000
            });
            $('#email').focus();
            return;
        }
        if(!$('#name').val()) {
            $.smallBox({
                //height: 50,
                title : "Error",
                content : 'Name can\'t be empty',
                color : "#dc3912",
                sound_file: "smallbox",
                timeout: 3000
            });
            $('#name').focus();
            return;
        }
        if(!$('#password').val() && action === "save") {
            $.smallBox({
                title : "Error",
                content : 'Password can\'t be empty',
                color : "#dc3912",
                sound_file: "smallbox",
                timeout: 3000
            });
            $('#password').focus();
            return;
        }
        if(!$('#confirm-password').val() && action === 'save') {
            $.smallBox({
                title : "Error",
                content : 'Please confirm password',
                color : "#dc3912",
                sound_file: "smallbox",
                timeout: 3000
            });
            $('#confirm-password').focus();
            return;
        }
        if($('#password').val() !== $('#confirm-password').val()) {
            $.smallBox({
                title : "Error",
                content : 'Password doest\'t match',
                color : "#dc3912",
                sound_file: "smallbox",
                timeout: 3000
            });
            $('#confirm-password').focus();
            return;
        }
        //if(!$('#user-group').val() && $('#user-userGroup-id').val() != '3') {
        if(!$('#user-group').val()) {
            $.smallBox({
                title : "Error",
                content : 'User group can\'t be empty',
                color : "#dc3912",
                sound_file: "smallbox",
                timeout: 3000
            });
            $('#user-group').focus();
            return;
        }

        if(!$('#tenant').val()) {
        //if(!$('#tenant').val() && $('#user-userGroup-id').val() != '3') {
            $.smallBox({
                title : "Error",
                content : 'Tenant can\'t be empty',
                color : "#dc3912",
                sound_file: "smallbox",
                timeout: 3000
            });
            $('#tenant').focus();
            return;
        }
        
    
        if(!$('input[type="radio"][name="active[]"]:checked').val()) {
            $.smallBox({
                title : "Error",
                content : 'Active can\'t be empty',
                color : "#dc3912",
                sound_file: "smallbox",
                timeout: 3000
            });
            //$('#user-group').focus();
            
            return;
        }


        if(!$('input[type="radio"][name="force[]"]:checked').val()) {
            $.smallBox({
                title : "Error",
                content : 'Force can\'t be empty',
                color : "#dc3912",
                sound_file: "smallbox",
                timeout: 3000
            });
            //$('#user-group').focus();
            
            return;
        }

        // Show loder
       
        $('.loading-in-button').css({'display':'block'});
        $('.text-in-button').css({'display':'none'});
        
        // Send data
        $.ajax({
            url: url,
            type: 'POST',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: {
                'id': $('#user-id').val(),
                'email': $('#email').val(),
                'name': $('#name').val(),
                'password': $('#password').val(),
                'group': ($('#user-group').val()==undefined) ? $('#user-userGroup-id').val() :$('#user-group').val() ,
                'tenant':($('#tenant').val()==undefined)  ? $('#user-tenant-id').val() :$('#tenant').val(),
                //'parent_id': $('#vendor').val(),
                'active': $('input[type="radio"][name="active[]"]:checked').val(),
                'force': $('input[type="radio"][name="force[]"]:checked').val()
            },
            success: function(resp) {
                if(resp.responseCode === 200) {
                   
                    // Send success message
                    $.smallBox({
                        height: 50,
                        title : "Success",
                        content : resp.responseMessage,
                        color : "#109618",
                        sound_file: "voice_on",
                        timeout: 3000
                        //icon : "fa fa-bell swing animated"
                    });
                    window.location.href =baseUrl +"/user";
                } 
                else if(resp.responseCode === 501) {
                    $.smallBox({
                        height: 50,
                        title : "Error",
                        content : resp.responseMessage,
                        color : "#dc3912",
                        sound_file: "smallbox",
                        timeout: 3000
                        //icon : "fa fa-bell swing animated"
                    });
                }else {
                    $.smallBox({
                        height: 50,
                        title : "Error",
                        content : resp.responseMessage,
                        color : "#dc3912",
                        sound_file: "smallbox",
                        timeout: 5000
                        //icon : "fa fa-bell swing animated"
                    });
                }
                // Hide loder
               // $('.page-loader').addClass('hidden');
               $('.loading-in-button').css({'display':'none'});
               $('.text-in-button').css({'display':'block'});
            },
            error: function(xhr, ajaxOptions, thrownError) {
                $.smallBox({
                    title : "Error",
                    content : xhr.statusText,
                    color : "#dc3912",
                    timeout: 5000
                    //icon : "fa fa-bell swing animated"
                });
                // Hide loder
                //$('.page-loader').addClass('hidden');
                $('.loading-in-button').css({'display':'none'});
                $('.text-in-button').css({'display':'block'});
            }
        }); 

    });


})(jQuery);
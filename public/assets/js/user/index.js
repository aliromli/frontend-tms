(function($) {
    "use strict";

    $('#dataTableUser').wrap('<div class="dataTables_scroll" />');
    
    var userDataTable = null;
    if ($('#dataTableUser').length) {
        // You can use 'alert' for alert message
        // or throw to 'throw' javascript error
        // or none to 'ignore' and hide error
        // or you own function
        // please read https://datatables.net/reference/event/error
        // for more information
        $.fn.dataTable.ext.errMode = 'none';

        

        userDataTable =  $('#dataTableUser').DataTable({
        //dom: '<"dt-toolbar"<"col-sm-6 col-xs-12 hidden-xs"f><"col-sm-6 col-xs-12 hidden-xs text-right"<"toolbar">>>rt<"dt-toolbar-footer"<"col-sm-6 col-xs-12"i><"col-sm-6 col-xs-12 hidden-xs"p>><"clear">',
        processing: true,
        serverSide: true,
        dom: 'lrtip',
        "searching": false,
        //"lengthChange": true,
        pageLength: 10,
        lengthMenu: [[10, 25, 50, 100], [10, 25, 50, 100]],
        pagingType: 'full_numbers',
        ajax: {
            url:   baseUrl+"/user-datatables",
            type: 'GET',
            data:  function(d){
                d.name = $('#search-name').val();
                d.tenant = $('#search-tenant').val();
                d.group = $('#search-group').val();
               
            }
        },
        language: {
            //processing: '<div style="display: none"></div>',
            // info: 'Menampilkan _START_ - _END_ dari _TOTAL_ ',
            //search: '<span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>',
            // zeroRecords: 'Tidak ada data yang cocok dengan kriteria pencarian',
            // emptyTable: 'Data tidak tersedia',
            // paginate: {
            //     first: '&laquo;',
            //     last: '&raquo;',
            //     next: '&rsaquo;',
            //     previous: '&lsaquo;'
            // },
            //lengthMenu: "Baris per halaman: _MENU_ "
        },
        //rowId: 'TRANSPORT_ID', {data: "id", sortable: false, searchable: false},
        columns: [
            //{data: "DT_RowIndex", sortable: false, searchable: false},
            {data: "id", 
               sortable: false, 
               searchable: false,
               "render": function (data, type, row, meta) {      
                          return meta.row + meta.settings._iDisplayStart + 1;     
               }  
            },
            {data: "name", name: "name"},
            {data: "email", name: "email"},
            {data : "email_verified_at", name : "email_verified_at"},
            //{data: "user_group.name", name: "userGroup.name"},
            {data: "userGroup", name: "userGroup"},
            {data: "tenant", name: "tenant"},
            {data: "active", name: "active"},
            {data: "force_change_password", name: "force_change_password"},
            {data: "id", sortable: false, searchable: false, }
        ],
        columnDefs:[
			{
                targets: 0,
				"className": "text-center",
                
            },{
                targets: 1,
                "className": "mleft",
                render: function(d,data,row) {
                    return `<span>`+d+`</span>`;
                  
                } 
            },{
                targets: 3,
                "className": "mleft",
                render: function(d,data,row) {
                    console.log('tess', );
                    if(d=="N" && row.userGroup != 'Administrator')
                         return '<span class="badge badge-warning">Not Verified</span>' 
                    else
                        return '<span class="badge badge-success">Verified</span>'    
                    
                } 
            },{
                targets: 6,
                "className": "mleft",
                render: function(d,data,row) {
                    return `<span>`+d+`</span>`;
                  
                } 
            },{
                targets: 7,
                "className": "mleft",
                render: function(d,data,row) {
                    return `<span>`+d+`</span>`;
                  
                } 
            },{
                targets: 8,
				"className": "text-center padd-row-table",
                render: function(d,data,row) {
                    
                
                    let btnActive= '';
                    if(row.active == "Y"){
                        btnActive = `<a class="btn-inactive" data-id="`+d+`"><i class="fa fa-ban"></i></a>&nbsp;`;
                    }
                    else
                    {
                        btnActive = `<a class="btn-active" data-id="`+d+`"><i class="fa fa-check-circle"></i></a>`;

                    }
                    let edit = '';
                    let hps = '';
                    edit = `<a  class="btn-edit" data-id="`+d+`"><img class="pencil2" src="`+baseUrl+`/assets/images/icon/pencil.png" /></a>&nbsp;`;
                    hps = `<a  class="btn-hps" data-id="`+d+`"><img class="pencil" src="`+baseUrl+`/assets/images/icon/trash.png" /></a>&nbsp;`;
                    
                    let showBtnAct = '';
                    //if(row.userGroup !== 'Admin Tenant' )
                    //{
                        showBtnAct = btnActive;
                       
                    //}
                   
                    return `<span>`+edit+``+showBtnAct+``+hps+`</span>`;
                
                }
            }
        ]
        });
      
    }

    
    $('#btn-cari-user').click(function() {
        userDataTable.draw(true);
    });

    
    // clear
    $('.clear-search').click(function() {
        $('#search-name').val(''); 
        $('#search-group').val(''); 
        userDataTable.draw(true);
    });
	

    // Add User
    //$('body').on('click', '#btn-add-user', function(){
        //$('#modal-user').modal('show');
        //$('#modal-user .modal-title').html('Add New User');
        //$('#modal-user input').val('');
        //$('#modal-user input[type=text],#modal-user input[type=hidden],#modal-user input[type=password],#modal-user input[type=email],#modal-user input[type=number]').val('').removeAttr('readonly');
    //});

    

    // Edit user
    $('#dataTableUser').on('click', '.btn-edit', function() {
        window.location.href= baseUrl +"/user-form/"+$(this).data('id');
        /*$('#modal-user').modal('show');
        $('#modal-user .modal-title').html('Edit User');
        $('#email').attr('readonly','readonly');

        // Hide loder
        $('.page-loader').removeClass('hidden');

        // Get data
        // Send data
        $.ajax({
            url: baseUrl + '/user/' + $(this).data('id'),
            type: 'GET',
            success: function(resp) {
                $('#user-id').val(resp.id);
                $('#name').val(resp.name);
                $('#email').val(resp.email);
                $('#user-group').val(resp.user_group_id).trigger('change');
                $('#tenant').val(resp.tenant_id).trigger('change');
                //$('#vendor').val(resp.vendor_id).trigger('change');
                $('input[type="password"]'). val('');
                if(resp.active === 'Y') {
                    $('#user-active-yes').prop('checked', true);
                    $('#user-active-no').prop('checked', false);
                } else {
                    $('#user-active-yes').prop('checked', false);
                    $('#user-active-no').prop('checked', true);
                }

                // Hide loder
                $('.page-loader').addClass('hidden');
            },
            error: function(xhr, ajaxOptions, thrownError) {
                $.smallBox({
                    title : "Error",
                    content : xhr.statusText,
                    color : "#dc3912",
                    timeout: 3000
                    //icon : "fa fa-bell swing animated"
                });
                // Hide loder
                $('.page-loader').addClass('hidden');
            }
        });*/
    });

    $('#dataTableUser').on('click', '.btn-hps', function() {

        if(!confirm('Area you sure want to Delete this user?')) {
            return;
        }

        $.ajax({
            url: baseUrl + '/user/delete/'+$(this).data('id'),
            type: 'GET',
            success: function(resp) {
                if(resp.responseCode === 200) {
                    // Reload datatable
                    $.smallBox({
                        height: 50,
                        title : "Success",
                        content : resp.responseMessage,
                        color : "#109618",
                        sound_file: "voice_on",
                        timeout: 3000
                        //icon : "fa fa-bell swing animated"
                    });

                    userDataTable.ajax.reload();

                } else {
                    $.smallBox({
                        height: 50,
                        title : "Error",
                        content : resp.responseMessage,
                        color : "#dc3912",
                        sound_file: "smallbox",
                        timeout: 3000
                        //icon : "fa fa-bell swing animated"
                    });
                }
            },
            error: function(xhr, ajaxOptions, thrownError) {
                $.smallBox({
                    title : "Error",
                    content : xhr.statusText,
                    color : "#dc3912",
                    timeout: 3000
                    //icon : "fa fa-bell swing animated"
                });
            }
        });
    });

     // Disabled user
     $('#dataTableUser').on('click', '.btn-active', function() {

        if(!confirm('Area you sure want to activate this user?')) {
            return;
        }

        // Get data
        // Send data
        $.ajax({
            url: baseUrl + '/user/activate/'+$(this).data('id'),
            type: 'GET',
            success: function(resp) {
                if(resp.responseCode === 200) {
                    // Reload datatable
                    userDataTable.ajax.reload();
                    $.smallBox({
                        height: 50,
                        title : "Success",
                        content : resp.responseMessage,
                        color : "#109618",
                        sound_file: "voice_on",
                        timeout: 3000
                        //icon : "fa fa-bell swing animated"
                    });
                } else {
                    $.smallBox({
                        height: 50,
                        title : "Error",
                        content : resp.responseMessage,
                        color : "#dc3912",
                        sound_file: "smallbox",
                        timeout: 3000
                        //icon : "fa fa-bell swing animated"
                    });
                }
            },
            error: function(xhr, ajaxOptions, thrownError) {
                $.smallBox({
                    title : "Error",
                    content : xhr.statusText,
                    color : "#dc3912",
                    timeout: 3000
                    //icon : "fa fa-bell swing animated"
                });
            }
        });

    });

    // Activate user
    $('#dataTableUser').on('click', '.btn-inactive', function() {

        if(!confirm('Area you sure want to inactivate this user?')) {
            return;
        }

        // Get data
        // Send data
        $.ajax({
            url: baseUrl + '/user/inactivate/'+$(this).data('id'),
            type: 'GET',
            success: function(resp) {
                if(resp.responseCode === 200) {
                    // Reload datatable
                    userDataTable.ajax.reload();
                    $.smallBox({
                        height: 50,
                        title : "Success",
                        content : resp.responseMessage,
                        color : "#109618",
                        sound_file: "voice_on",
                        timeout: 3000
                        //icon : "fa fa-bell swing animated"
                    });
                } else {
                    $.smallBox({
                        height: 50,
                        title : "Error",
                        content : resp.responseMessage,
                        color : "#dc3912",
                        sound_file: "smallbox",
                        timeout: 3000
                        //icon : "fa fa-bell swing animated"
                    });
                }
            },
            error: function(xhr, ajaxOptions, thrownError) {
                $.smallBox({
                    title : "Error",
                    content : xhr.statusText,
                    color : "#dc3912",
                    timeout: 3000
                    //icon : "fa fa-bell swing animated"
                });
            }
        });
    });

      /* filter tugel */
     // Toggle filter
     $('.filter .tugel').click(function() {
        $(this).toggleClass('open');
        $('.filter-title').toggleClass('hidden');
        $('.form-filter form').toggleClass('open-filter');
        
    });



})(jQuery);
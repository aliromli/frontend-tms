(function($) {
    "use strict";

    $('#dataTableTenant').wrap('<div class="dataTables_scroll" />');
    var dataTableTenant = null;
    if ($('#dataTableTenant').length) {
        // You can use 'alert' for alert message
        // or throw to 'throw' javascript error
        // or none to 'ignore' and hide error
        // or you own function
        // please read https://datatables.net/reference/event/error
        // for more information
        $.fn.dataTable.ext.errMode = 'none';
        
        dataTableTenant =  $('#dataTableTenant').DataTable({
        processing: true,
        serverSide: true,
        //dom : '',
        dom: 'lrtip',
        "searching": false,
        //"lengthChange": false,
        pageLength: 10,
        lengthMenu: [[10, 25, 50, 100], [10, 25, 50, 100]],
        pagingType: 'full_numbers',
        ajax: {
            url:   baseUrl+"/tenant-datatables",
            type: 'GET',
            data:  function(d){
                d.name = $('#search-tenant').val();
              
                
            }
        },
        language: {
            //processing: '<div style="display: none"></div>',
            // info: 'Menampilkan _START_ - _END_ dari _TOTAL_ ',
            //search: '<span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>',
            // zeroRecords: 'Tidak ada data yang cocok dengan kriteria pencarian',
            // emptyTable: 'Data tidak tersedia',
            // paginate: {
            //     first: '&laquo;',
            //     last: '&raquo;',
            //     next: '&rsaquo;',
            //     previous: '&lsaquo;'
            // },
            //lengthMenu: "Baris per halaman: _MENU_ "
        },
        //rowId: 'TRANSPORT_ID',
        columns: [
            {data: "DT_RowIndex", 
               sortable: false, 
               searchable: false,
               "render": function (data, type, row, meta) {      
                          return meta.row + meta.settings._iDisplayStart + 1;     
               }  
            },
            {data: "version", name: "version", visible:false},
            {data: "name", name: "name"},
            {data: "tenantSuper", name: "tenantSuper"},
            {data: "is_super", name: "is_super"},
            {data: "id", sortable: false, searchable: false, }
        ],
        columnDefs:[
			{
                targets: 0,
				"className": "text-center",
				                
            },
			{
                targets: 1,
				"className": "mleft",
				render : function(d,data,row) {
							if(d==null)
							{
								return '<span></span';
								
							}	
							else
							{
								return '<span>'+d+'</span';
							}
                            
                        }
				                
            },
			{
                targets: 2,
				"className": "mleft",
				render : function(d,data,row) {
							if(d==null)
							{
								return '<span></span';
								
							}	
							else
							{
								return '<span>'+d+'</span';
							}
                            
                        }
				                
            },
			{
                targets: 3,
				"className": "mleft",
				render : function(d,data,row) {
							if(d==null)
							{
								return '<span></span';
								
							}	
							else
							{
								return '<span>'+d+'</span';
							}
                            
                        }
				                
            },
	        {
                targets: 5,
				  "className": "text-center padd-row-table",
                render: function(d,data,row) {
                 
                    let v = row.version;
                    return `
						<span>
							<a href="#" class="btn-edit"  data-id="`+d+`"><img class="pencil" src="`+baseUrl+`/assets/images/icon/pencil.png" /></a>&nbsp;
							<a class="btn-delete" data-id="`+d+`" data-version="`+v+`"><img class="trash" src="`+baseUrl+`/assets/images/icon/trash.png" /></a>
						</span>
                    `;
                   
                }
            }
        ]
        });
      
    }

    
    $('#btn-cari-tenant').click(function() {
        dataTableTenant.draw(true);
    });

    // Add 
    $('body').on('click', '#btn-add-tenant', function(){
        $('#modal-tenant').modal('show');
        $('#modal-tenant .modal-title').html('Add New Tenant');
        $('#modal-tenant input[type=text],#modal-tenant input[type=hidden],#modal-tenant input[type=password],#modal-tenant input[type=email],#modal-tenant input[type=number]').val('').removeAttr('readonly');
        $('#divtn-version').css('display','none');
    });

      /* filter tugel */
     // Toggle filter
     $('.filter .tugel').click(function() {
        $(this).toggleClass('open');
        $('.filter-title').toggleClass('hidden');
        $('.form-filter form').toggleClass('open-filter');
        
    });

    // clear
    $('.clear-search').click(function() {
        $('#search-tenant').val(''); 
        dataTableTenant.draw(true);
    });

    // Add New State or update
    $('#btn-submit-tnt').click(function(){

        // Update when user id has value
        var url = baseUrl + '/tenant/update';
        var action = "update";
        if(!$('#tenant-id').val()) {
            url = baseUrl + '/tenant/save';
            action = "save";
        }

        // if($('#tenant-id').val()) {
        //     if(!$('#version-tnt').val()) {
        //         $.smallBox({
        //             //height: 50,
        //             title : "Error",
        //             content : 'Version can\'t be empty',
        //             color : "#dc3912",
        //             sound_file: "smallbox",
        //             timeout: 3000
        //         });
        //         $('#version-tnt').focus();
        //         return;
        //     }
        // }

        // Has error
        // var hasError = false;
        // Check requirement input
        if(!$('#tenant-name').val()) {
            $.smallBox({
                //height: 50,
                title : "Error",
                content : 'Tenant can\'t be empty',
                color : "#dc3912",
                sound_file: "smallbox",
                timeout: 3000
            });
            $('#tenant-name').focus();
            return;
        }
        // if(!$('#super-tenant').val()) {
        //     $.smallBox({
        //         //height: 50,
        //         title : "Error",
        //         content : 'Super Tenant can\'t be empty',
        //         color : "#dc3912",
        //         sound_file: "smallbox",
        //         timeout: 3000
        //     });
        //     $('#super-tenant').focus();
        //     return;
        // }

        // if(!$('input[name="is_super[]"]:checked').val())
        // {
        //     $.smallBox({
        //         //height: 50,
        //         title : "Error",
        //         content : 'Is Super  not checked',
        //         color : "#dc3912",
        //         sound_file: "smallbox",
        //         timeout: 3000
        //     });
          
        //     return;
        // }

        // Show loder
        $('.page-loader').removeClass('hidden');

        // Send data
        $.ajax({
            url: url,
            type: 'POST',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: {
                'id': $('#tenant-id').val(),
                'name': $('#tenant-name').val(),
                //'super_tenant_id': $('#super-tenant').val(),
                'version' : $('#version-tnt').val(),
                //'is_super': $('input[type="radio"][name="is_super[]"]:checked').val()
               
               

            },
            success: function(resp) {
                if(resp.responseCode === 200) {
                    // Reload datatable
                    dataTableTenant.ajax.reload();
                    // Reset Form
                    $('#modal-tenant input[type=text],#modal-tenant input[type=hidden],#modal-tenant input[type=password],#modal-tenant input[type=email],#modal-tenant input[type=number]').val('').removeAttr('readonly');
   
                    // Close modal
                    $('#modal-tenant').modal('hide');
                    // Send success message
                    $.smallBox({
                        height: 50,
                        title : "Success",
                        content : resp.responseMessage,
                        color : "#109618",
                        sound_file: "voice_on",
                        timeout: 3000
                        //icon : "fa fa-bell swing animated"
                    });
                } else {
                    $.smallBox({
                        height: 50,
                        title : "Error",
                        content : resp.responseMessage,
                        color : "#dc3912",
                        sound_file: "smallbox",
                        timeout: 3000
                        //icon : "fa fa-bell swing animated"
                    });
                }
                // Hide loder
                $('.page-loader').addClass('hidden');
            },
            error: function(xhr, ajaxOptions, thrownError) {
                $.smallBox({
                    title : "Error",
                    content : xhr.statusText,
                    color : "#dc3912",
                    timeout: 3000
                    //icon : "fa fa-bell swing animated"
                });
                // Hide loder
                $('.page-loader').addClass('hidden');
            }
        });

    });

    // Edit tenant
    $('#dataTableTenant').on('click', '.btn-edit', function() {
        $('#modal-tenant').modal('show');
        $('#modal-tenant .modal-title').html('Edit Tenant');
        $('#divtn-version').show();
       
        // Hide loder
        $('.page-loader').removeClass('hidden');

        // Get data
        // Send data
        $.ajax({
            url: baseUrl + '/tenant/' + $(this).data('id'),
            type: 'GET',
            success: function(resp) {
                if(resp.responseCode === 200) {
                    $('#tenant-id').val(resp.responseMessage[0].id);
                    $('#tenant-name').val(resp.responseMessage[0].name);
                    $('#super-tenant').val(resp.responseMessage[0].super_tenant_id).trigger('change');
                    $('#super-tenant').focus();
                    $('#version-tnt').val(resp.responseMessage[0].version);
                   
                    if(resp.responseMessage[0].is_super == true) {
                        
                        $('#tenant-is-super-true').prop('checked', true);
                        $('#tenant-is-super-false').prop('checked', false);
                    } else {
                       
                        $('#tenant-is-super-true').prop('checked', false);
                        $('#tenant-is-super-false').prop('checked', true);
                    }

                }
                else
                {
                    $.smallBox({
                        height: 50,
                        title : "Error",
                        content : resp.responseMessage,
                        color : "#dc3912",
                        sound_file: "smallbox",
                        timeout: 3000
                        //icon : "fa fa-bell swing animated"
                    });
                }
               
                // Hide loder
                $('.page-loader').addClass('hidden');
            },
            error: function(xhr, ajaxOptions, thrownError) {
                $.smallBox({
                    title : "Error",
                    content : xhr.statusText,
                    color : "#dc3912",
                    timeout: 3000
                    //icon : "fa fa-bell swing animated"
                });
                // Hide loder
                $('.page-loader').addClass('hidden');
            }
        });
    });

    $('#dataTableTenant').on('click', '.btn-delete', function() {

        let text;
        if (confirm("Are you sure to remove!") == true) {
            
             // Send data
            $.ajax({
                url:  baseUrl + '/tenant/delete',
                type: 'POST',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data: {
                    'id': $(this).data('id'),
                    'version': $(this).data('version')
                },
                success: function(resp) {
                    if(resp.responseCode === 200) {
                        // Reload datatable
                        dataTableTenant.ajax.reload();
                        
                        // Send success message
                        $.smallBox({
                            height: 50,
                            title : "Success",
                            content : resp.responseMessage,
                            color : "#109618",
                            sound_file: "voice_on",
                            timeout: 3000
                            //icon : "fa fa-bell swing animated"
                        });
                    } else {
                        $.smallBox({
                            height: 50,
                            title : "Error",
                            content : resp.responseMessage,
                            color : "#dc3912",
                            sound_file: "smallbox",
                            timeout: 3000
                            //icon : "fa fa-bell swing animated"
                        });
                    }
                    // Hide loder
                    $('.page-loader').addClass('hidden');
                },
                error: function(xhr, ajaxOptions, thrownError) {
                    $.smallBox({
                        title : "Error",
                        content : xhr.statusText,
                        color : "#dc3912",
                        timeout: 3000
                        //icon : "fa fa-bell swing animated"
                    });
                    // Hide loder
                    $('.page-loader').addClass('hidden');
                }
            });

       
        } 

    });


    


})(jQuery);
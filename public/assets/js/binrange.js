(function($) {
    "use strict";
    $('#dataTableBR').wrap('<div class="dataTables_scroll" />');
    var dataTableBR = null;
    if ($('#dataTableBR').length) {
        // You can use 'alert' for alert message
        // or throw to 'throw' javascript error
        // or none to 'ignore' and hide error
        // or you own function
        // please read https://datatables.net/reference/event/error
        // for more information
        $.fn.dataTable.ext.errMode = 'none';
        
        dataTableBR =  $('#dataTableBR').DataTable({
        processing: true,
        serverSide: true,
        //dom : '',
        dom: 'lrtip',
        "searching": false,
        //"scrollX": true,
        //"sScrollX": '100%',
        //"sScrollXInner": "110%",
        pageLength: 10,
        lengthMenu: [[10, 25, 50, 100], [10, 25, 50, 100]],
        pagingType: 'full_numbers',
        ajax: {
            url:   baseUrl+"/binrange-datatable",
            type: 'GET',
            data:  function(d){
                d.name= $('#search-name-br').val();
                
            }
        },
        language: {
            
        },
        //rowId: 'TRANSPORT_ID',
        columns: [
            {data: "DT_RowIndex", 
               sortable: false, 
               searchable: false,
               "render": function (data, type, row, meta) {      
                          return meta.row + meta.settings._iDisplayStart + 1;     
               }  
            },
            {data: "version", name: "version", visible: false},
            {data: "name", name: "name"},
            {data: "binRangeStart", name: "binRangeStart"}, 
            {data: "binRangeEnd", name: "binRangeEnd"}, 
            {data: "id", sortable: false, searchable: false, }
        ],
		
        columnDefs:[
			{
                "targets": 0, // your case first column
                "className": "text-center",
            },
			{
                "targets": 2, 
                "className": "mleft",
				 render: function(d,data,row) {
					 return `<span>`+d+`</span>`;
				 }
            },
			{
                "targets": 4, 
                "className": "mleft",
				 render: function(d,data,row) {
					 return `<span>`+d+`</span>`;
				 }
            },
			
	        {
                targets: 5,
				"className": "text-center padd-row-table",
                render: function(d,data,row) {
                 
                    let v = row.version;
                    let id = d;
					return `
						<span>
							<a href="binrange/`+d+`" data-id="`+d+`"><img class="pencil" src="`+baseUrl+`/assets/images/icon/pencil.png" /></a>&nbsp;
							<a class="btn-del" data-id="`+d+`" data-version="`+v+`"><img class="trash" src="`+baseUrl+`/assets/images/icon/trash.png" /></a>
						</span>
                    `;
                   
                }
            }
        ]
        });
      
    }
	
	
    $('#btn-search-name-br').click(function() {
        dataTableBR.draw(true);
    });

      /* filter tugel */
     // Toggle filter
    //  $('.filter .tugel').click(function() {
    //     $(this).toggleClass('open');
    //     $('.filter-title').toggleClass('hidden');
    //     $('.form-filter form').toggleClass('open-filter');
        
    // });

    // clear
    $('.clear_filter').click(function() {
        $('#search-name-br').val('');
       
        dataTableBR.draw(true);
    });

    // Add New  or update
    $('#btn-submit-br').click(function(){

        // Update when city id has value
        var url = baseUrl + '/card/update';
        var action = "update";
        if(!$('#br-id').val()) {
            url = baseUrl + '/card/save';
            action = "save";
        }

        if($('#br-id').val()) {
            if(!$('#version-br').val()) {
                $.smallBox({
                    //height: 50,
                    title : "Error",
                    content : 'Version can\'t be empty',
                    color : "#dc3912",
                    sound_file: "smallbox",
                    timeout: 3000
                });
                $('#version-br').focus();
                return;
            }
        }
   
        // Has error
        // var hasError = false;
        // Check requirement input
        if(!$('#br-name').val()) {
            $.smallBox({
                //height: 50,
                title : "Error",
                content : 'Name  can\'t be empty',
                color : "#dc3912",
                sound_file: "smallbox",
                timeout: 3000
            });
            $('#is-name').focus();
            return;
        }

        if(!$('#br-Card_Num_Range_from').val()) {
            $.smallBox({
                //height: 50,
                title : "Error",
                content : 'Card Num Range from can\'t be empty',
                color : "#dc3912",
                sound_file: "smallbox",
                timeout: 3000
            });
            $('#br-Card_Num_Range_from').focus();
            return;
        }
		if(!$('#br-Card_Num_Range_end').val()) {
            $.smallBox({
                //height: 50,
                title : "Error",
                content : 'Card Num Range End can\'t be empty',
                color : "#dc3912",
                sound_file: "smallbox",
                timeout: 3000
            });
            $('#br-Card_Num_Range_end').focus();
            return;
        }
		if(!$('#br-Card_Num_Length').val()) {
            $.smallBox({
                //height: 50,
                title : "Error",
                content : 'Card Num Length can\'t be empty',
                color : "#dc3912",
                sound_file: "smallbox",
                timeout: 3000
            });
            $('#br-Card_Num_Length').focus();
            return;
        }
		if(!$('#br-PAN-Digit-unmasking').val()) {
            $.smallBox({
                //height: 50,
                title : "Error",
                content : 'PAN Digit unmasking can\'t be empty',
                color : "#dc3912",
                sound_file: "smallbox",
                timeout: 3000
            });
            $('#br-PAN-Digit-unmasking').focus();
            return;
        }
		// Show loder
        $('.page-loader').removeClass('hidden');
		$('.loading>img').removeClass('hidden');
        // Send data
        $.ajax({
            url: url,
            type: 'POST',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: {
                'id': $('#br-id').val(),
                'name': $('#br-name').val(),
                'binRangeStart' : $('#br-Card_Num_Range_from').val(),
                'binRangeEnd' : $('#br-Card_Num_Range_end').val(),
                //'panDigitUnmasking' : $('#br-PAN-Digit-unmasking').val(),
                'panDigitUnmasking' : $('input[name="PAN_Digit_unmasking"]:checked').val(), 
                'cardNumLength' : $('#br-Card_Num_Length').val(),
                'pinLength' : $('#br-PIN_Length').val(), 
                'pinPrompt' : $('input[name="PIN_Prompt"]:checked').val(), 
                'printCardholderCopy' : $('input[name="Print_Cardholder_Copy"]:checked').val(), 
                'printMerchantCopy' : $('input[name="Print_Merchant_Copy"]:checked').val(), 
                'printBankCopy' : $('input[name="Print_Bank_Copy"]:checked').val(), 
                'version': $('#version-br').val(),
                //'acquirerId': $('#is-acquirerId').val(),
                //'onUs': $('input[name="OnUs[]"]:checked').val(),
                
            },
		    success: function(resp) {
		        if(resp.responseCode == '0000') { //sukses
                    // Send success message
                    $.smallBox({
                        height: 50,
                        title : resp.responseStatus, 
                        content : resp.responseMessage,
                        color : "#109618",
                        sound_file: "voice_on",
                        timeout: 3000
                        //icon : "fa fa-bell swing animated"
                    });
					window.location.replace(baseUrl +"/binrange");

                    /*if($('#is-id').val()=="") {
                       
						$('#is-name').val('');
                        $('#is-issuerId').val('');
						$('#is-acquirerId').val('');
						//$('input[name="OnUs[]"]:checked').val(''); 
						
						//$('#input[type="radio":checked]').each(function(){
							//  $(this).checked = false;  
						//});
						$("input:radio").removeAttr("checked");
						
                    }*/

 
                } else if(resp.responseCode == '3333' || resp.responseCode == '0400'){ //exception and not found
					
					 $.smallBox({
                        height: 50,
                        title : resp.responseStatus,
                        content : resp.responseMessage,
                        color : "#dc3912",
                        sound_file: "voice_on",
                        timeout: 3000
                        //icon : "fa fa-bell swing animated"
                    });
                }
				else if(resp.responseCode == '5555')//validaator
				{
					
						 var data = Object.values(resp.responseMessage);
						 var ln = data.length;
						 var w  = 3000 * ln; 
						 data.map(function(d){
							 
								$.smallBox({
										height: 50,
										title : resp.responseStatus,
										content : d,
										color : "#dc3912",
										sound_file: "smallbox",
										timeout: w
										//icon : "fa fa-bell swing animated"
									});
						});
					
				}	
                // Hide loder
                $('.page-loader').addClass('hidden');
				$('.loading>img').addClass('hidden');
            },
            error: function(xhr, ajaxOptions, thrownError) {
                $.smallBox({
                    title : "Error",
                    content : xhr.statusText,
                    color : "#dc3912",
                    timeout: 3000
                    //icon : "fa fa-bell swing animated"
                });
                // Hide loder
                $('.page-loader').addClass('hidden');
				$('.loading>img').addClass('hidden');
            }
        });

    });

    // delete 
    $('#dataTableBR').on('click', '.btn-del', function() {
	 
        let text;
        if (confirm("Are you sure to remove!") == true) {
            
             // Send data
            $.ajax({
                url:  baseUrl + '/binrange/delete',
                type: 'POST',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data: {
                    'id': $(this).data('id'),
                    'version': $(this).data('version')
                },
                success: function(resp) {
                    if(resp.responseCode === 200) {
                        // Reload datatable
                        dataTableBR.ajax.reload();
                        
                        // Send success message
                        $.smallBox({
                            height: 50,
                            title : "Success",
                            content : resp.responseMessage,
                            color : "#109618",
                            sound_file: "voice_on",
                            timeout: 3000
                            //icon : "fa fa-bell swing animated"
                        });
                    } else {
                        $.smallBox({
                            height: 50,
                            title : "Error",
                            content : resp.responseMessage,
                            color : "#dc3912",
                            sound_file: "smallbox",
                            timeout: 3000
                            //icon : "fa fa-bell swing animated"
                        });
                    }
                    // Hide loder
                    $('.page-loader').addClass('hidden');
                },
                error: function(xhr, ajaxOptions, thrownError) {
                    $.smallBox({
                        title : "Error",
                        content : xhr.statusText,
                        color : "#dc3912",
                        timeout: 3000
                        //icon : "fa fa-bell swing animated"
                    });
                    // Hide loder
                    $('.page-loader').addClass('hidden');
                }
            });

       
        } 

    });


 

})(jQuery);
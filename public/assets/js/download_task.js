(function ($) {
    "use strict";

    $("#dataTableDownloadTask").wrap('<div class="dataTables_scroll" />');
    var dataTableDownloadTask = null;
    if ($("#dataTableDownloadTask").length) {
        // You can use 'alert' for alert message
        // or throw to 'throw' javascript error
        // or none to 'ignore' and hide error
        // or you own function
        // please read https://datatables.net/reference/event/error
        // for more information
        $.fn.dataTable.ext.errMode = "none";

        dataTableDownloadTask = $("#dataTableDownloadTask").DataTable({
            processing: true,
            serverSide: true,
            //dom : '',
            dom: "lrtip",
            searching: false,

            pageLength: 10,
            lengthMenu: [
                [10, 25, 50, 100],
                [10, 25, 50, 100],
            ],
            pagingType: "full_numbers",
            ajax: {
                url: baseUrl + "/download-task-datatables",
                type: "GET",
                data: function (d) {
                    d.name = $("#search-name-dnt").val();
                    //d.sn = $('#search-sn-dnt').val();
                    //d.packageName = $('#search-pn-dnt').val();
                    //d.applicationId = JSON.stringify($("#search-application-dnt option:selected").toArray().map(item => item.value));
                    //d.terminalId = JSON.stringify($("#search-terminal-dnt option:selected").toArray().map(item => item.value));
                    //d.terminalGroupId = JSON.stringify($("#search-terminalgroup-dnt option:selected").toArray().map(item => item.value));
                    //d.model = $('#search-model').val();
                    //d.vendorName = $('#search-vendor-name').val();
                    //d.vendorCountry = $('#search-vendor-country').val();
                    // $('#SelectQButton option:selected')
                    //.toArray().map(item => item.value);
                },
            },
            language: {},
            //rowId: 'TRANSPORT_ID',
            columns: [
                {
                    data: "DT_RowIndex",
                    sortable: false,
                    searchable: false,
                    render: function (data, type, row, meta) {
                        return meta.row + meta.settings._iDisplayStart + 1;
                    },
                },
                { data: "version", name: "version", visible: false },
                { data: "name", name: "name" },
                { data: "tasktime", name: "tasktime" },
                { data: "status", name: "status" },
                { data: "id", sortable: false, searchable: false },
            ],
            columnDefs: [
                {
                    targets: 0,
                    className: "text-center",
                },
                {
                    targets: 2,
                    className: "mleft",
                    render: function (d, data, row) {
                        return "<span>" + d + "</span";
                    },
                },
                {
                    targets: 4,
                    className: "mleft",
                    render: function (d, data, row) {
                        return "<span>" + d + "</span";
                    },
                },
                //{
                //     targets: 3,
                // 	"className": "mleft",
                //     render : function(d,data,row) {
                //         return '<span>'+d+'</span';
                //     }

                // },
                {
                    targets: 5,
                    className: "text-center padd-row-table",
                    render: function (d, data, row) {
                        let v = row.version;
                        let id = d;
                        return (
                            `<span>
                    <a href="download-task/` +
                            d +
                            `"  class="" data-id="` +
                            d +
                            `"><img class="mata" src="` +
                            baseUrl +
                            `/assets/images/icon/mata.png" /></a>&nbsp;
                    <a class="btn-republish" data-id="` +
                            d +
                            `"><img class="republish" src="` +
                            baseUrl +
                            `/assets/images/icon/republish.png" /></a>&nbsp;
                    <a class="btn-cancel" data-id="` +
                            d +
                            `" data-version="` +
                            v +
                            `"><img class="cancel" src="` +
                            baseUrl +
                            `/assets/images/icon/cancel.png" /></a>
					
                    </span>`
                        );
                    },
                },
            ],
        });
    }
    //<span class="loading"><img class="hidden" width="40" src="{{asset('assets/images/icon/loader.gif')}}"/></span>

    $("#btn-ti-search-dt").click(function () {
        dataTableDownloadTask.draw(true);
    });

    /* filter tugel */
    // Toggle filter
    //  $('.filter .tugel').click(function() {
    //     $(this).toggleClass('open');
    //     $('.filter-title').toggleClass('hidden');
    //     $('.form-filter form').toggleClass('open-filter');

    // });

    // clear
    $(".clear_filter").click(function () {
        $("#search-name-dnt").val("");
        // $('#search-sn-dnt').val('');
        // $('#search-pn-dnt').val('');
        // $('#search-application-dnt').val('');
        // $('#search-terminal-dnt').val('');
        // $('#search-terminalgroup-dnt').val('');
        dataTableDownloadTask.draw(true);
    });

    // Add New or update
    $("#btn-submit-dt").click(function () {
        // Update when city id has value
        var url = baseUrl + "/download-task/update";
        var action = "download-task";
        if (!$("#download-task-id").val()) {
            url = baseUrl + "/download-task/save";
            action = "save";
        }

        if ($("#download-task-id").val()) {
            if (!$("#version-dt").val()) {
                $.smallBox({
                    //height: 50,
                    title: "Error",
                    content: "Version can't be empty",
                    color: "#dc3912",
                    sound_file: "smallbox",
                    timeout: 3000,
                });
                $("#version-dm").focus();
                return;
            }
        }

        // Has error
        // var hasError = false;
        // Check requirement input d

        if (!$("#download-task-name").val()) {
            $.smallBox({
                //height: 50,
                title: "Error",
                content: "Name can't be empty",
                color: "#dc3912",
                sound_file: "smallbox",
                timeout: 3000,
            });
            $("#download-task-name").focus();
            return;
        }

        if (!$("#download-task-Installation-Notification").val()) {
            $.smallBox({
                //height: 50,
                title: "Error",
                content: "Installation Notification can't be empty",
                color: "#dc3912",
                sound_file: "smallbox",
                timeout: 3000,
            });
            $("#download-task-Installation-Notification").focus();
            return;
        }

        if (!$("#download-task-downloadTime").val()) {
            $.smallBox({
                //height: 50,
                title: "Error",
                content: "Download Time can't be empty",
                color: "#dc3912",
                sound_file: "smallbox",
                timeout: 3000,
            });
            $("#download-task-downloadTime").focus();
            return;
        }

        if (!$("#download-task-downloadTimeType").val()) {
            $.smallBox({
                //height: 50,
                title: "Error",
                content: "Download Time Type  can't be empty",
                color: "#dc3912",
                sound_file: "smallbox",
                timeout: 3000,
            });
            $("#download-task-downloadTimeType").focus();
            return;
        }

        if (!$("#download-task-publishTimeType").val()) {
            $.smallBox({
                //height: 50,
                title: "Error",
                content: "Publish Time Type  can't be empty",
                color: "#dc3912",
                sound_file: "smallbox",
                timeout: 3000,
            });
            $("#download-task-publishTimeType").focus();
            return;
        }

        if (!$("#download-task-publishTime").val()) {
            $.smallBox({
                //height: 50,
                title: "Error",
                content: "Publish Time can't be empty",
                color: "#dc3912",
                sound_file: "smallbox",
                timeout: 3000,
            });
            $("#download-task-publishTime").focus();
            return;
        }

        /*if(!$('#download-task-publish_time_type').val()) {
            $.smallBox({
                //height: 50,
                title : "Error",
                content : 'Publish Time Type can\'t be empty',
                color : "#dc3912",
                sound_file: "smallbox",
                timeout: 3000
            });
            $('#download-task-publish_time_type').focus();
            return;
        }*/

        if (!$("#download-task-installation_time").val()) {
            $.smallBox({
                //height: 50,
                title: "Error",
                content: "Isntallation Time can't be empty",
                color: "#dc3912",
                sound_file: "smallbox",
                timeout: 3000,
            });
            $("#download-task-installation_time").focus();
            return;
        }

        if (!$("#download-task-installation_time_type").val()) {
            $.smallBox({
                //height: 50,
                title: "Error",
                content: "Isntallation Time Type can't be empty",
                color: "#dc3912",
                sound_file: "smallbox",
                timeout: 3000,
            });
            $("#download-task-installation_time_type").focus();
            return;
        }

        var arlistApp = [];
        $("#list-group-app>tbody>tr").each(function () {
            let d = $(this).find(".uid").text();

            arlistApp.push(d);
        });

        if (arlistApp.length == 0) {
            $.smallBox({
                //height: 50,
                title: "Error",
                content: "Application can't be empty",
                color: "#dc3912",
                sound_file: "smallbox",
                timeout: 3000,
            });
            return;
        }

        var arlistTg = [];
        $("#exampleFormControlSelect-tg>li").each(function () {
            let a = $(this).find(".uid").text();
            arlistTg.push(a);
        });

        // if(arlistTg.length == 0)
        // {
        // $.smallBox({
        //height: 50,
        // title : "Error",
        // content : 'Terminal Group can\'t be empty',
        // color : "#dc3912",
        // sound_file: "smallbox",
        // timeout: 3000
        // });
        // return;
        // }

        var arlistT = [];
        $("#terminal-list-sn-dlt>li").each(function () {
            let d = $(this).find(".uid").text();
            // arlistT.push(element.value);
            arlistT.push(d);
        });

        /* if(arlistT.length == 0)
		{
			$.smallBox({
                //height: 50,
                title : "Error",
                content : 'Terminal can\'t be empty',
                color : "#dc3912",
                sound_file: "smallbox",
                timeout: 3000
            });
            return;
		} */

        if (arlistT.length == 0 && arlistApp.length == 0) {
            $.smallBox({
                //height: 50,
                title: "Error",
                content: "Terminal Or Group Terminal can't be empty",
                color: "#dc3912",
                sound_file: "smallbox",
                timeout: 3000,
            });
            return;
        }

        // Show loder
        $(".page-loader").removeClass("hidden");
        $(".loading>img").removeClass("hidden");
        // Send data
        $.ajax({
            url: url,
            type: "POST",
            headers: {
                "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
            },
            data: {
                id: $("#download-task-id").val(),
                name: $("#download-task-name").val(),
                downloadTimeType: $("#download-task-downloadTimeType").val(),
                installationTimeType: $(
                    "#download-task-installation_time_type"
                ).val(),
                installationTime: $("#download-task-installation_time").val(),
                publishTimeType: $("#download-task-publishTimeType").val(),
                downloadTime: $("#download-task-downloadTime").val(),
                publishTime: $("#download-task-publishTime").val(),
                status: $('input[name="downloadstatus[]"]:checked').val(),
                installationNotification: $(
                    "#download-task-Installation-Notification"
                ).val(),
                applicationIds: JSON.stringify(arlistApp),
                terminalGroupIds: JSON.stringify(arlistTg),
                terminalIds: JSON.stringify(arlistT),
                version: $("#version-dt").val(),
            },
            success: function (resp) {
                if (resp.responseCode == "0000") {
                    //sukses
                    // Send success message
                    $.smallBox({
                        height: 50,
                        title: resp.responseStatus,
                        content: resp.responseMessage,
                        color: "#109618",
                        sound_file: "voice_on",
                        timeout: 3000,
                        //icon : "fa fa-bell swing animated"
                    });

                    window.location.replace(baseUrl + "/download-task");
                } else if (
                    resp.responseCode == "3333" ||
                    resp.responseCode == "0200" ||
                    resp.responseCode == "0400" ||
                    resp.responseCode == "0001"
                ) {
                    //exception and not found and exis

                    $.smallBox({
                        height: 50,
                        title: resp.responseStatus,
                        content: resp.responseMessage,
                        color: "#dc3912",
                        sound_file: "voice_on",
                        timeout: 3000,
                        //icon : "fa fa-bell swing animated"
                    });
                } else if (resp.responseCode == "5555") {
                    //validaator
                    var data = Object.values(resp.responseMessage);
                    var ln = data.length;
                    var w = 3000 * ln;
                    data.map(function (d) {
                        $.smallBox({
                            height: 50,
                            title: resp.responseStatus,
                            content: d,
                            color: "#dc3912",
                            sound_file: "smallbox",
                            timeout: w,
                            //icon : "fa fa-bell swing animated"
                        });
                    });
                }

                // Hide loder
                $(".page-loader").addClass("hidden");
                $(".loading>img").addClass("hidden");
            },
            error: function (xhr, ajaxOptions, thrownError) {
                $.smallBox({
                    title: "Error",
                    content: xhr.statusText,
                    color: "#dc3912",
                    timeout: 3000,
                    //icon : "fa fa-bell swing animated"
                });
                // Hide loder
                $(".page-loader").addClass("hidden");
                $(".loading>img").addClass("hidden");
            },
        });
    });

    $("#dataTableDownloadTask").on("click", ".btn-republish", function () {
        let text;
        if (confirm("Are you sure to Republish!") == true) {
            /*
			$('.loading>img').removeClass('hidden');
            // Send data
            $.ajax({ 
                url:  baseUrl + '/download-task/republish', 
                type: 'POST',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data: {
                    'id': $(this).data('id'),
                    'version': $(this).data('version')
                },
                success: function(resp) {
					
					if(resp.responseCode == '0000') 
					{ //sukses
						// Send success message
						$.smallBox({
							height: 50,
							title : resp.responseStatus, 
							content : resp.responseMessage,
							color : "#109618",
							sound_file: "voice_on",
							timeout: 3000
							//icon : "fa fa-bell swing animated"
						});
						
						dataTableDownloadTask.ajax.reload();
						
					}else if(resp.responseCode == '3333' || resp.responseCode == '0200' ||  resp.responseCode == '0500' || resp.responseCode == '0400' || resp.responseCode == '0001'){ //exception and not found and exis
						
						 $.smallBox({
							height: 50,
							title : resp.responseStatus,
							content : resp.responseMessage,
							color : "#dc3912",
							sound_file: "voice_on",
							timeout: 3000
							//icon : "fa fa-bell swing animated"
						});
					}
					else if(resp.responseCode == '5555')//validaator
					{
						
							 var data = Object.values(resp.responseMessage);
							 var ln = data.length;
							 var w  = 3000 * ln; 
							 data.map(function(d){
								 
									$.smallBox({
											height: 50,
											title : resp.responseStatus,
											content : d,
											color : "#dc3912",
											sound_file: "smallbox",
											timeout: w
											//icon : "fa fa-bell swing animated"
										});
							});
						
					}	
					
                    
                    // Hide loder
                    $('.page-loader').addClass('hidden');
					$('.loading>img').addClass('hidden');
                },
                error: function(xhr, ajaxOptions, thrownError) {
                    $.smallBox({
                        title : "Error",
                        content : xhr.statusText,
                        color : "#dc3912",
                        timeout: 3000
                        //icon : "fa fa-bell swing animated"
                    });
                    // Hide loder
                    $('.page-loader').addClass('hidden');
					$('.loading>img').addClass('hidden');
                }
            });
			*/
        }
    });

    $("body").on("click", "#btn-export-dtl", function () {
        var data = new FormData();
        data.append("bagian", $("#bagian").val());
        $.ajax({
            url: baseUrl + "/download-task/export",
            type: "POST",
            contentType: false,
            processData: false,
            cache: false,
            xhrFields: {
                responseType: "blob",
            },
            headers: {
                "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
            },
            data: data,
            success: function (data, textStatus, xhr) {
                // check for a filename
                var filename = "";
                var disposition = xhr.getResponseHeader("Content-Disposition");
                if (disposition && disposition.indexOf("attachment") !== -1) {
                    var filenameRegex =
                        /filename[^;=\n]*=((['"]).*?\2|[^;\n]*)/;
                    var matches = filenameRegex.exec(disposition);
                    if (matches != null && matches[1])
                        filename = matches[1].replace(/['"]/g, "");
                    var a = document.createElement("a");
                    var url = window.URL.createObjectURL(data);
                    a.href = url;
                    a.download = filename;
                    document.body.append(a);
                    a.click();
                    a.remove();
                    window.URL.revokeObjectURL(url);
                } else {
                    alert("Error");
                }
                //i = i + 1;
                //if (i < max) {
                //    DownloadFile(list);
                //}
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {},
        });
    });

    $("#dataTableDownloadTask").on("click", ".btn-cancel", function () {
        let text;
        if (confirm("Are you sure to Cancel!") == true) {
            $(".loading>img").removeClass("hidden");
            // Send data
            $.ajax({
                url: baseUrl + "/download-task/cancel",
                type: "POST",
                headers: {
                    "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr(
                        "content"
                    ),
                },
                data: {
                    id: $(this).data("id"),
                    version: $(this).data("version"),
                },
                success: function (resp) {
                    if (resp.responseCode == "0000") {
                        //sukses
                        // Send success message
                        $.smallBox({
                            height: 50,
                            title: resp.responseStatus,
                            content: resp.responseMessage,
                            color: "#109618",
                            sound_file: "voice_on",
                            timeout: 3000,
                            //icon : "fa fa-bell swing animated"
                        });

                        dataTableDownloadTask.ajax.reload();
                    } else if (
                        resp.responseCode == "3333" ||
                        resp.responseCode == "0200" ||
                        resp.responseCode == "0500" ||
                        resp.responseCode == "0400" ||
                        resp.responseCode == "0001"
                    ) {
                        //exception and not found and exis

                        $.smallBox({
                            height: 50,
                            title: resp.responseStatus,
                            content: resp.responseMessage,
                            color: "#dc3912",
                            sound_file: "voice_on",
                            timeout: 3000,
                            //icon : "fa fa-bell swing animated"
                        });
                    } else if (resp.responseCode == "5555") {
                        //validaator
                        var data = Object.values(resp.responseMessage);
                        var ln = data.length;
                        var w = 3000 * ln;
                        data.map(function (d) {
                            $.smallBox({
                                height: 50,
                                title: resp.responseStatus,
                                content: d,
                                color: "#dc3912",
                                sound_file: "smallbox",
                                timeout: w,
                                //icon : "fa fa-bell swing animated"
                            });
                        });
                    }

                    // Hide loder
                    $(".page-loader").addClass("hidden");
                    $(".loading>img").addClass("hidden");
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    $.smallBox({
                        title: "Error",
                        content: xhr.statusText,
                        color: "#dc3912",
                        timeout: 3000,
                        //icon : "fa fa-bell swing animated"
                    });
                    // Hide loder
                    $(".page-loader").addClass("hidden");
                    $(".loading>img").addClass("hidden");
                },
            });
        }
    });

    $(document).on("input", ".numeric", function () {
        this.value = this.value.replace(/\D/g, "");
    });

    // $('#download-task-installation_time').datepicker();

    // $('#download-task-installation_time').datetimepicker({
    //   format: 'Y-m-d H:i'
    //});

    //$("#download-task-installation_time").datepicker();
    //$("#download-task-publish_time").datepicker();

    //----------- sub table application ------
    $("body").on("click", "#btn-app-to-list", function () {
        var arlist = [];
        $("#list-group-app>tbody>tr").each(function () {
            let a = $(this).find(".app_version").text();
            let b = $(this).find(".name").text();
            let c = $(this).find(".package_name").text();
            let d = $(this).find(".uid").text();

            var ob = new Object();
            ob.app_version = a;
            ob.name = b;
            ob.packageName = c;
            ob.uid = d;
            arlist.push(ob);
        });

        var arlist2 = [];
        var datac = $('[name="chapp_dt[]"]');
        // var html = "";

        $.each(datac, function () {
            var $this = $(this);

            // check if the checkbox is checked
            if ($this.is(":checked")) {
                var ob = new Object();
                let find = 0;
                if (arlist.length > 0) {
                    for (var i = 0; i < arlist.length; i++) {
                        if (arlist[i].uid == $this.data("id")) {
                            find = 1;
                            break;
                        }
                    }
                }

                if (find == 0) {
                    ob.app_version = $this.data("app_version");
                    ob.name = $this.data("name");
                    ob.packageName = $this.data("package_name");
                    ob.uid = $this.data("id");
                    arlist2.push(ob);
                }
            }
        });

        let arr = [...arlist, ...arlist2];
        //console.log('1',arr);
        let mergedArr = [...new Set(arr)];
        //console.log('2',mergedArr);

        var d = "";
        for (var i = 0; i < mergedArr.length; i++) {
            let no = i + 1;
            let name = mergedArr[i].name;
            let app_version = mergedArr[i].app_version;
            let packageName = mergedArr[i].packageName;
            let uid = mergedArr[i].uid;
            let act = `<a href="#" class="btn-delete-list-app-dt"><i class="fa fa-remove"></i></a>`;
            d +=
                `<tr class="tr-dt">` +
                `<td>` +
                no +
                `</td>` +
                `<td><span class="app_version">` +
                app_version +
                `</span></td>` +
                `<td><span class="name">` +
                name +
                `</span></td>` +
                `<td><span class="package_name">` +
                packageName +
                `</span><span class="uid" style="display:none;">` +
                uid +
                `</span></td>` +
                `<td>` +
                act +
                `</td>` +
                `</tr>`;
        }

        $("#list-group-app>tbody").find("tr").remove();
        $("#list-group-app>tbody").html(d);
        $("#modal-list-app-form").modal("hide");
    });
    $("body").on("click", ".btn-delete-list-app-dt", function () {
        $(this).parent().parent().remove();
    });

    $("body").on("click", "#btn-searcg-name-app-dlt", function () {
        dataTableAppListGForm.draw(true);
    });

    $("body").on("click", "#btn-clear-name-app-dlt", function () {
        $("#search-name-app-dlt").val("");
        dataTableAppListGForm.draw(true);
    });

    //$('#download-task-publish_time').datetimepicker({
    // showSecond: true,
    // dateFormat: 'yyyy-mm-dd',
    // timeFormat: 'hh:mm:ss',
    // stepHour: 2,
    // stepMinute: 10,
    // stepSecond: 10
    //format: 'Y-m-d H:i'
    // });

    $("#dataTableAppListGForm").wrap('<div class="dataTables_scroll" />');
    var dataTableAppListGForm = null;
    if ($("#dataTableAppListGForm").length) {
        // You can use 'alert' for alert message
        // or throw to 'throw' javascript error
        // or none to 'ignore' and hide error
        // or you own function
        // please read https://datatables.net/reference/event/error
        // for more information
        $.fn.dataTable.ext.errMode = "none";

        dataTableAppListGForm = $("#dataTableAppListGForm").DataTable({
            processing: true,
            serverSide: true,
            //dom : '',
            dom: "lrtip",
            searching: false,

            pageLength: 10,
            lengthMenu: [
                [10, 25, 50, 100],
                [10, 25, 50, 100],
            ],
            pagingType: "full_numbers",
            ajax: {
                url: baseUrl + "/application-datatables",
                type: "GET",
                data: function (d) {
                    d.name = $("#search-name-app-dlt").val();
                },
            },
            language: {},
            //rowId: 'TRANSPORT_ID',
            columns: [
                {
                    data: "DT_RowIndex",
                    sortable: false,
                    searchable: false,
                    render: function (data, type, row, meta) {
                        return meta.row + meta.settings._iDisplayStart + 1;
                    },
                },
                { data: "appVersion", name: "appVersion" },
                { data: "name", name: "name" },
                { data: "deviceModel", name: "deviceModel" },
                { data: "packageName", name: "packageName" },
                { data: "id", sortable: false, searchable: false },
            ],
            columnDefs: [
                {
                    targets: 0,
                    className: "text-center",
                },
                {
                    targets: 1,
                    className: "mleft",
                    render: function (d, data, row) {
                        return `<span>` + d + `</span>`;
                    },
                },
                {
                    targets: 2,
                    className: "mleft",
                    render: function (d, data, row) {
                        return `<span>` + d + `</span>`;
                    },
                },
                {
                    targets: 3,
                    className: "mleft",
                    render: function (d, data, row) {
                        return `<span>` + d + `</span>`;
                    },
                },
                {
                    targets: 4,
                    className: "mleft",
                    render: function (d, data, row) {
                        return `<span>` + d + `</span>`;
                    },
                },
                {
                    targets: 5,
                    className: "text-center",
                    render: function (d, data, row) {
                        let id = d;
                        let v = row.version;
                        return (
                            `
                      <input type="checkbox" 
                      name="chapp_dt[]"
                      value="true" lass="custom-control-input checkListdt" 
                      data-id="` +
                            d +
                            `" 
                      data-app_version="` +
                            row.appVersion +
                            `"
                      data-name="` +
                            row.name +
                            `"
                      data-package_name="` +
                            row.packageName +
                            `"
                      data-version="` +
                            v +
                            `">
                    `
                        );
                    },
                },
            ],
        });
    }

    $("body").on("click", "#add-app-to-list", function () {
        $("#modal-list-app-form").modal("show");
        $("#modal-list-app-form .modal-title").html("List Application");
        dataTableAppListGForm.draw(true);
    });

    //dataTableViewDownloadTask
    $("#dataTableViewDownloadTask").wrap('<div class="dataTables_scroll" />');
    var dataTableViewDownloadTask = null;
    if ($("#dataTableViewDownloadTask").length) {
        // You can use 'alert' for alert message
        // or throw to 'throw' javascript error
        // or none to 'ignore' and hide error
        // or you own function
        // please read https://datatables.net/reference/event/error
        // for more information
        $.fn.dataTable.ext.errMode = "none";
        dataTableViewDownloadTask = $("#dataTableViewDownloadTask").DataTable({
            processing: true,
            serverSide: true,
            //dom : '',
            dom: "lrtip",
            searching: false,

            pageLength: 10,
            lengthMenu: [
                [10, 25, 50, 100],
                [10, 25, 50, 100],
            ],
            pagingType: "full_numbers",
            ajax: {
                url: baseUrl + "/download-task/history",
                type: "GET",
                data: function (d) {
                    //d.name = $('#search-name-g-dlt').val();
                },
            },
            language: {},
            //rowId: 'TRANSPORT_ID',
            columns: [
                {
                    data: "DT_RowIndex",
                    sortable: false,
                    searchable: false,
                    render: function (data, type, row, meta) {
                        return meta.row + meta.settings._iDisplayStart + 1;
                    },
                },
                { data: "sn", name: "sn" },
                { data: "group", name: "group" },
                { data: "activity", name: "activity" },
                //{data: "id", sortable: false, searchable: false, }
            ],
            columnDefs: [
                {
                    targets: 0,
                    className: "text-center",
                },
                {
                    targets: 1,
                    className: "mleft",
                    render: function (d, data, row) {
                        return `<span>` + d + `</span>`;
                    },
                },
                {
                    targets: 2,
                    className: "mleft",
                    render: function (d, data, row) {
                        return `<span>` + d + `</span>`;
                    },
                },
                {
                    targets: 3,
                    className: "mleft",
                    render: function (d, data, row) {
                        return `<span>` + d + `</span>`;
                    },
                },
            ],
        });
    }

    // Terminal group modal tabel
    $("#dataTableTgListGForm").wrap('<div class="dataTables_scroll" />');
    var dataTableTgListGForm = null;
    if ($("#dataTableTgListGForm").length) {
        // You can use 'alert' for alert message
        // or throw to 'throw' javascript error
        // or none to 'ignore' and hide error
        // or you own function
        // please read https://datatables.net/reference/event/error
        // for more information
        $.fn.dataTable.ext.errMode = "none";
        dataTableTgListGForm = $("#dataTableTgListGForm").DataTable({
            processing: true,
            serverSide: true,
            //dom : '',
            dom: "lrtip",
            searching: false,

            pageLength: 10,
            lengthMenu: [
                [10, 25, 50, 100],
                [10, 25, 50, 100],
            ],
            pagingType: "full_numbers",
            ajax: {
                url: baseUrl + "/terminal-group-datatables",
                type: "GET",
                data: function (d) {
                    d.name = $("#search-name-g-dlt").val();
                },
            },
            language: {},
            //rowId: 'TRANSPORT_ID',
            columns: [
                {
                    data: "DT_RowIndex",
                    sortable: false,
                    searchable: false,
                    render: function (data, type, row, meta) {
                        return meta.row + meta.settings._iDisplayStart + 1;
                    },
                },
                { data: "version", name: "version", visible: false },
                { data: "name", name: "name" },
                { data: "description", name: "description" },
                { data: "id", sortable: false, searchable: false },
            ],
            columnDefs: [
                {
                    targets: 0,
                    className: "text-center",
                },
                {
                    targets: 2,
                    className: "mleft",
                    render: function (d, data, row) {
                        return `<span>` + d + `</span>`;
                    },
                },
                {
                    targets: 3,
                    className: "mleft",
                    render: function (d, data, row) {
                        return `<span>` + d + `</span>`;
                    },
                },
                {
                    targets: 4,
                    className: "text-center",
                    render: function (d, data, row) {
                        let id = d;
                        let v = row.version;

                        return (
                            `
                    <input 
                    type="checkbox" 
                    name="chtg_dlt[]" 
                    value="true"  
                    data-id="` +
                            d +
                            `" 
                    data-name="` +
                            row.name +
                            `" 
                    data-description="` +
                            row.description +
                            `"                      
                    data-version="` +
                            v +
                            `"
                    />
                  `
                        );
                    },
                },
            ],
        });
    }

    // terminal modal tabel
    $("#dataTableTerListGForm").wrap('<div class="dataTables_scroll" />');
    var dataTableTerListGForm = null;
    if ($("#dataTableTerListGForm").length) {
        // You can use 'alert' for alert message
        // or throw to 'throw' javascript error
        // or none to 'ignore' and hide error
        // or you own function
        // please read https://datatables.net/reference/event/error
        // for more information
        $.fn.dataTable.ext.errMode = "none";

        dataTableTerListGForm = $("#dataTableTerListGForm").DataTable({
            processing: true,
            serverSide: true,
            //dom: 'lrtip',
            dom: "tip",
            searching: false,

            pageLength: 10,
            lengthMenu: [
                [10, 25, 50, 100],
                [10, 25, 50, 100],
            ],
            pagingType: "full_numbers",
            ajax: {
                url: baseUrl + "/terminal-datatables",
                type: "GET",
                data: function (d) {
                    d.sn = $("#search-sn-dlt").val();
                },
            },
            language: {},
            //rowId: 'TRANSPORT_ID',
            columns: [
                {
                    data: "DT_RowIndex",
                    sortable: false,
                    searchable: false,
                    render: function (data, type, row, meta) {
                        return meta.row + meta.settings._iDisplayStart + 1;
                    },
                },
                { data: "sn", name: "sn" },
                //{data: "terminalId", name: "terminalId",visible:false},
                { data: "id", sortable: false, searchable: false },
            ],
            columnDefs: [
                {
                    targets: 0,
                    className: "text-center",
                },
                //{
                //	"targets": 1,
                //	"className": "mleft",
                //	 render: function(d,data,row) {
                //		 return `<span>`+d+`</span>`
                //	 }
                //},
                {
                    targets: 2,
                    className: "text-center",
                    render: function (d, data, row) {
                        let id = d;
                        //let idT = row.id;
                        return (
                            `<span>
							<input 
							type="checkbox" 
							name="chter_dlt[]" 
							value="true"  
							data-id="` +
                            d +
                            `" 
							data-sn="` +
                            row.sn +
                            `"
							
							/></span>
							`
                        );
                    },
                },
            ],
        });
    }

    $("body").on("click", "#btn-add-terminal-group-dlt", function () {
        $("#modal-list-tg-form-dlt").modal("show");
        $("#modal-list-tg-form-dlt .modal-title").html("List Terminal Group");
        dataTableTgListGForm.draw(true);
    });

    $("body").on("click", "#btn-search-name-dlt", function () {
        dataTableTgListGForm.draw(true);
    });

    $("body").on("click", "#btn-clear-name-dlt", function () {
        $("#search-name-g-dlt").val("");
        dataTableTgListGForm.draw(true);
    });

    $("body").on("click", "#btn-tg-to-list-dlt", function () {
        //alert("ok");
        var arlist = [];
        // $('#exampleFormControlSelect-tg option').each(function(index,element){

        //     var ob = new Object();
        // 	ob.name = element.text;
        // 	ob.uid = element.value;
        // 	arlist.push(ob);
        // });
        $("#exampleFormControlSelect-tg>li").each(function () {
            let b = $(this).find(".nm").text();
            let a = $(this).find(".uid").text();

            var ob = new Object();
            ob.name = b;
            ob.uid = a;
            arlist.push(ob);
        });
        var arlist2 = [];
        var datac = $('[name="chtg_dlt[]"]');
        // var html = "";
        $.each(datac, function () {
            var $this = $(this);

            // check if the checkbox is checked
            if ($this.is(":checked")) {
                var ob = new Object();
                let find = 0;
                if (arlist.length > 0) {
                    for (var i = 0; i < arlist.length; i++) {
                        if (arlist[i].uid == $this.data("id")) {
                            find = 1;
                            break;
                        }
                    }
                }

                if (find == 0) {
                    ob.name = $this.data("name");
                    ob.uid = $this.data("id");
                    arlist2.push(ob);
                }
            }
        });

        let arr = [...arlist, ...arlist2];
        //console.log('1',arr);
        let mergedArr = [...new Set(arr)];
        //console.log('2',mergedArr);

        var d = "";
        for (var i = 0; i < mergedArr.length; i++) {
            let name = '<span class="nm">' + mergedArr[i].name + "</span>";
            let uid =
                '<span class="uid" style="display:none;">' +
                mergedArr[i].uid +
                "</span>";
            let rem =
                '<i class="fa fa-trash-o btn-delete-list-tg-t" style="color:#717682;margin-left:20px;"></i>';
            d += '<li class="ch-li">' + name + " " + rem + " " + uid + "</li>";
        }
        $("#exampleFormControlSelect-tg").find("li").remove();
        $("#exampleFormControlSelect-tg").html(d);
        $("#modal-list-tg-form-dlt").modal("hide");
    });

    $("body").on("click", "#btn-add-terminal-list-dlt", function () {
        $("#modal-list-t-form-dlt").modal("show");
        $("#modal-list-t-form-dlt .modal-title").html("List Terminal");
        dataTableTerListGForm.draw(true);
    });
    $("body").on("click", "#btn-search-sn-dlt", function () {
        dataTableTerListGForm.draw(true);
    });
    $("body").on("click", "#btn-clear-sn-dlt", function () {
        $("#search-sn-dlt").val("");
        dataTableTerListGForm.draw(true);
    });

    $("body").on("click", "#btn-t-to-list-dlt", function () {
        var arlist = [];
        $("#terminal-list-sn-dlt>li").each(function (index, element) {
            let b = $(this).find(".nm").text();
            let a = $(this).find(".uid").text();

            var ob = new Object();
            ob.sn = b;
            ob.uid = a;
            arlist.push(ob);
        });
        var arlist2 = [];
        var datac = $('[name="chter_dlt[]"]');
        // var html = "";

        $.each(datac, function () {
            var $this = $(this);

            // check if the checkbox is checked
            if ($this.is(":checked")) {
                var ob = new Object();
                let find = 0;
                if (arlist.length > 0) {
                    for (var i = 0; i < arlist.length; i++) {
                        if (arlist[i].uid == $this.data("id")) {
                            find = 1;
                            break;
                        }
                    }
                }

                if (find == 0) {
                    ob.sn = $this.data("sn");
                    ob.uid = $this.data("id");
                    arlist2.push(ob);
                }
            }
        });

        let arr = [...arlist, ...arlist2];
        //console.log('1',arr);
        let mergedArr = [...new Set(arr)];
        //console.log('2',mergedArr);

        var d = "";
        for (var i = 0; i < mergedArr.length; i++) {
            /*let sn = mergedArr[i].sn;
			//let description = mergedArr[i].description;
            let uid = mergedArr[i].uid;
			//let act = `<button type="button" class="btn btn-flat btn-danger btn-xs btn-delete-list-tg-t"><i class="fa fa-remove"></i></button>`;
			d+=`<option value="`+uid+`">`+sn+`</option>`;*/
            let nameModel = '<span class="nm">' + mergedArr[i].sn + "</span>";
            let uid =
                '<span class="uid" style="display:none;">' +
                mergedArr[i].uid +
                "</span>";
            let rem =
                '<i class="fa fa-trash-o btn-delete-list-tg-t" style="color:#717682;margin-left:20px;"></i>';

            d +=
                '<li class="ch-li">' +
                nameModel +
                " " +
                rem +
                " " +
                uid +
                "</li>";
        }
        $("#terminal-list-sn-dlt").find("li").remove();
        $("#terminal-list-sn-dlt").html(d);
        $("#modal-list-t-form-dlt").modal("hide");
    });
    $("body").on("click", ".btn-delete-list-tg-t", function () {
        $(this).parent().remove();
    });
    $("body").on("click", ".btn-delete-list-t-t", function () {
        $(this).parent().remove();
    });
})(jQuery);

<?php

return [
    'version' => 'Version',
    'model_information' => 'Model Information',
    'vendorName' => 'Vendor Name',
    'vendorCountry' => 'Vendor Country',
    'new' => 'Add Device Model',
    'edit' => 'Edit Device Model',
    'model' => 'Model'
 ];

<?php

return [
    'version' => 'Version',
    'new_state' => 'Add New State',
    'edit_state' => 'Edit State',
    'country' => 'Country',
    'name' => 'State'
 ];

<?php

return [
    'user_group' => 'User Group',
    'new_user_group' => 'New User Group',
    'edit_user_group' => 'Edit User Group',
    'name' => 'Name',
    'group_name' => 'Group Name',
    'description' => 'Description'
];

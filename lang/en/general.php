<?php

return [

    'add' => 'Add',
    'delete' => 'Delete',
    'bulk_delete' => 'Bulk Delete',
    'edit' => 'Edit',
    'save' => 'SAVE',
    'update' => 'Update',
    'submit' => 'Submit',
    'print' => 'Print',
    'download' => 'Download',
    'upload' => 'Upload',
    'import' => 'Import',
    'export' => 'Export',
    'view' => 'View',
    'create' => 'Create',
    'new' => 'New',
    'action' => 'Action',
    'close' => 'Close',
    'select' => 'Select',
    'back' => 'Back',
    'yes' => 'Yes',
    'no' => 'No',
    'compare' => 'Compare',
    'required_field' => 'Required field',
    'all' => 'All',
	'cancel' => 'CANCEL',
    
	
];

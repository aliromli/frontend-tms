<?php

return [
    'version' => 'Version',
    'new_country' => 'Add New Country',
    'edit_country' => 'Edit Country',
    'code' => 'Code',
    'name' => 'Name'
 ];

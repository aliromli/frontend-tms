<?php

return [
    'version' => 'Version',
    'new_city' => 'Add New City',
    'edit_city' => 'Edit City',
    'state' => 'State',
    'name' => 'City'
 ];

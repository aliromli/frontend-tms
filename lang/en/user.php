<?php

return [
    'user' => 'User',
    'new_user' => 'New User',
    'edit_user' => 'Edit User',
    'name' => 'Name',
    'email' => 'Email',
    'group' => 'Group',
    'active' => 'Active',
    'all' => 'All'
];

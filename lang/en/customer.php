<?php

return [
    'customer' => 'Customer',
    'new_customer' => 'New Customer',
    'edit_customer' => 'Edit Customer',
    'name' => 'Name',
    'address' => 'Address',
    'city' => 'City',
    'zip_code' => 'Zip Code',
    'phone' => 'Phone',
    'fax' => 'Fax',
    'email' => 'Email'
];

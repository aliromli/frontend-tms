<?php

return [
    'version' => 'Version',
    'new' => 'Add Application',
    'edit' => 'Edit Application',
    'packageName' => 'Package Name',
    'name' => 'Name',
    'description' => 'Description',
    'appVersion' => 'App Version',
    'companyName' => 'Company Name',
    'uninstallable' => 'Uninstallable',
    'iconUrl' => 'Icon Url',
    'fileSize' => 'File Size',
	'deviceModel' => 'Device Model',
 ];

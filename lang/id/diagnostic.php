<?php

return [
    'version' => 'Version',
    'name' => 'Name',
    'default' => 'Default',
    'new' => 'Add Profile',
    'heartbeatInterval' => 'Heart Beat Interval',
    'diagnosticInterval' => 'Diagnostic Interval',
    'maskHomeButton' => 'Mask Home Button',
    'maskStatusBar' => 'Mask Status Bar',
    'scheduleReboot' => 'Schedule Reboot',
    'relocationAlert' => 'Relocation Alert',
    'scheduleRebootTime' => 'Schedule Reboot Time',
    'movingThreshold' => 'Moving Threshold',
    'adminPassword' => 'Admin Password',
    'frontApp' => 'Front App',
  
 ];

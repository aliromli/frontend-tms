<?php

return [
    'version' => 'Version',
    'new' => 'Add Terminal',
    'edit' => 'Edit Terminal',
    'sn' => 'SN',
    'modelName' => 'Model Name',
    'merchantName' => 'Merchant Name',
    'profileName' => 'Profile Name',
    'locked' => 'Locked',
	'merchant' => 'Merchant',
	'devicemodel' => 'Device model',
	'deviceprofile' => 'Device Profile',
   
 ];

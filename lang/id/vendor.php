<?php

return [
    'vendor' => 'Mitra',
    'new_vendor' => 'Mitra Baru',
    'edit_vendor' => 'Ubah Mitra',
    'info_vendor' => 'Detail Mitra',
    'name' => 'Nama',
    'address' => 'Alamat',
    'city' => 'Kota',
    'zip_code' => 'Kode Pos',
    'phone' => 'Telepon',
    'fax' => 'Faksimili',
    'email' => 'Surel',
    'image' => 'Logo',
	'sign' => 'Tanda Tangan',
    'doctor_name' => 'Nama Dokter',
    'doctor_license' => 'Nomor Register',
];

<?php

return [
    'version' => 'Version',
    'new_state' => 'Add New Country',
    'edit_state' => 'Edit Country',
    'country' => 'Country',
    'name' => 'State'
 ];

<?php

return [
    'version' => 'Version',
    'new' => 'Add MerchantType',
    'edit' => 'Edit MerchantType',
    'tenant' => 'Tenant',
    'name' => 'Merchant Type',
    'description' => 'description'
 ];

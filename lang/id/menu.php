<?php

return [
    'dashboard' => 'Dasbor',
    'terminal_group' => 'Terminal Group',
    'terminal' => 'Terminal',
    'application' => 'Application',
    'device_profile' => 'Model Profile',
    'device_model' => 'Model Device',
    'merchanttype' => 'Merchant Type',
    'tenant' => 'Tenant',
    'merchant' => 'Merchant',
    'logout' => 'Keluar',
    'user_management' => 'Manajemen Pengguna',
    'user' => 'Pengguna',
    'user_group' => 'Grup Pengguna',
    'menu' => 'Menu',
    'city' => 'Kota',
    'district' => 'Kecamatan',
    'state' => 'Provinsi',
    'country' => 'Negara',
    'master' => 'Master',
	'delete_task' => 'Delete Task',
    'diagnostic' => 'Diagnostic',
    'download_task' => 'Download Task',
    // Menu page
    'builder' => 'Pembangun',
    'new_menu' => 'Menu Baru',
    'edit_menu' => 'Ubah Menu',
    'add_new_menu' => 'Tambah Menu Baru',
    'new_menu_item' => 'Butir Menu Baru',
    'edit_menu_item' => 'Ubah Butir Menu',
    'name' => 'Nama',
    'description' => 'Deskripsi',
    'tooltip' => 'Tooltip',
    'icon' => 'Ikon',
    'url' => 'Tautan'

];

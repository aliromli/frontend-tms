<?php

return [
    'user_group' => 'Grup Pengguna',
    'new_user_group' => 'Grup Pengguna Baru',
    'edit_user_group' => 'Ubah Grup Pengguna',
    'name' => 'Nama',
    'group_name' => 'Nama Grup',
    'description' => 'Deskripsi'
];

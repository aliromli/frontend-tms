<?php

return [
    'user' => 'Pengguna',
    'new_user' => 'Pengguna Baru',
    'edit_user' => 'Ubah Pengguna',
    'name' => 'Nama',
    'email' => 'Surel',
    'group' => 'Grup',
    'active' => 'Aktif',
    'all' => 'Semua'
];

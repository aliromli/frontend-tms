<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\CountriesController;
use App\Http\Controllers\StatesController;
use App\Http\Controllers\CitiesController;
use App\Http\Controllers\DistrictsController;
use App\Http\Controllers\MerchantController;
use App\Http\Controllers\TenantController;
use App\Http\Controllers\MerchantTypeController;
use App\Http\Controllers\DeviceModelController;
use App\Http\Controllers\DeviceProfileController;
use App\Http\Controllers\TerminalGroupController;
use App\Http\Controllers\ApplicationController;
use App\Http\Controllers\TerminalController;
use App\Http\Controllers\DownloadTaskController;
use App\Http\Controllers\DeleteTaskController;
use App\Http\Controllers\DiagnosticController;
use App\Http\Controllers\AidController;
use App\Http\Controllers\CapkController;
use App\Http\Controllers\PublicKeyController;
use App\Http\Controllers\TleSettingController; 
use App\Http\Controllers\TerminalExtController;
use App\Http\Controllers\AcquirerController;
use App\Http\Controllers\IssuerController;
use App\Http\Controllers\CardController;
use App\Http\Controllers\HeartBeatController;
use App\Http\Controllers\TaskController;
use App\Http\Controllers\OnlineController;
use App\Http\Controllers\TerminalTemplateController;
use App\Http\Controllers\ResponseCodeController;
use App\Http\Controllers\BinRangeController;
use App\Http\Controllers\LocationController;
use App\Http\Controllers\AuthOtpController;




  



/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});

use Illuminate\Support\Facades\Session;

Auth::routes();

Route::middleware(['auth','locale'])->group(function () {
    //Route::get('/', 'HomeController@index');
    //Route::get('/home', 'HomeController@index');
    Route::get('/', [HomeController::class, 'index']);
    Route::get('/home', [HomeController::class, 'index']);
    ///Route::get('/login', [HomeController::class, 'index']);
    Route::get('/setup', 'HomeController@setup')->name('setup');
   

     //tenant
     Route::get('/tenant', [TenantController::class, 'index']);
     Route::get('/tenant-datatables', [TenantController::class, 'list']);
     Route::post('/tenant/save', [TenantController::class, 'store']);
     Route::post('/tenant/update', [TenantController::class, 'update']);
     Route::post('/tenant/delete', [TenantController::class, 'delete']);
     Route::get('/tenant/{id}', [TenantController::class, 'show']);

    //country
    Route::get('/country', [CountriesController::class, 'index']);
    Route::get('/country-datatables', [CountriesController::class, 'list']);
    Route::post('/country/save', [CountriesController::class, 'store']);
    Route::post('/country/update', [CountriesController::class, 'update']);
    Route::post('/country/delete', [CountriesController::class, 'delete']);
    //Route::get('/country/{id}', [CountriesController::class, 'show']);
	Route::get('/country-form', [CountriesController::class, 'form']); 
    Route::get('/country/{id}', [CountriesController::class, 'formEdit']);
    //Route::get('/country-form-index', [CountriesController::class, 'formIndex']); 
	
	
	//state
    Route::get('/state', [StatesController::class, 'index']);
    Route::get('/state-datatables', [StatesController::class, 'list']);
    Route::post('/state/save', [StatesController::class, 'store']);
    Route::post('/state/update', [StatesController::class, 'update']);
    Route::post('/state/delete', [StatesController::class, 'delete']);
    Route::get('/state/{id}', [StatesController::class, 'show']);
    Route::get('/state-form', [StatesController::class, 'form']);
    Route::get('/state/{id}', [StatesController::class, 'formEdit']);

	//city
    Route::get('/city', [CitiesController::class, 'index']);
    Route::get('/city-datatables', [CitiesController::class, 'list']);
    Route::post('/city/save', [CitiesController::class, 'store']);
    Route::post('/city/update', [CitiesController::class, 'update']);
    Route::post('/city/delete', [CitiesController::class, 'delete']);
    Route::get('/city/{id}', [CitiesController::class, 'show']);
    Route::get('/city-form', [CitiesController::class, 'form']);
    Route::get('/city/{id}', [CitiesController::class, 'formEdit']);
	
	//districs district 
    Route::get('/district', [DistrictsController::class, 'index']);
    Route::get('/district-datatables', [DistrictsController::class, 'list']);
    Route::post('/district/save', [DistrictsController::class, 'store']);
    Route::post('/district/update', [DistrictsController::class, 'update']);
    Route::post('/district/delete', [DistrictsController::class, 'delete']);
    Route::get('/district/{id}', [DistrictsController::class, 'show']);
    Route::get('/district-form', [DistrictsController::class, 'form']);
    Route::get('/district/{id}', [DistrictsController::class, 'formEdit']);


    //merchant
    Route::get('/merchant', [MerchantController::class, 'index']);
    Route::get('/merchant-datatables', [MerchantController::class, 'list']);
    Route::post('/merchant/save', [MerchantController::class, 'store']);
    Route::post('/merchant/update', [MerchantController::class, 'update']);
    Route::post('/merchant/delete', [MerchantController::class, 'delete']);
    Route::get('/merchant/{id}', [MerchantController::class, 'formEdit']);
    Route::get('/merchant-form', [MerchantController::class, 'form']);
  
    //merchant type
    Route::get('/merchanttype', [MerchantTypeController::class, 'index']);
    Route::get('/merchanttype-datatables', [MerchantTypeController::class, 'list']);
    Route::post('/merchanttype/save', [MerchantTypeController::class, 'store']);
    Route::post('/merchanttype/update', [MerchantTypeController::class, 'update']);
    Route::post('/merchanttype/delete', [MerchantTypeController::class, 'delete']);
    Route::get('/merchanttype/{id}', [MerchantTypeController::class, 'show']);
    Route::get('/merchanttype-form', [MerchantTypeController::class, 'form']);
    Route::get('/merchanttype/{id}', [MerchantTypeController::class, 'formEdit']);
    

    //device model
    Route::get('/device-model', [DeviceModelController::class, 'index']);
    Route::get('/device-model-datatables', [DeviceModelController::class, 'list']);
    Route::post('/device-model/save', [DeviceModelController::class, 'store']);
    Route::post('/device-model/update', [DeviceModelController::class, 'update']);
    Route::post('/device-model/delete', [DeviceModelController::class, 'delete']);
    Route::get('/device-model/{id}', [DeviceModelController::class, 'formEdit']);
    Route::get('/device-model-form', [DeviceModelController::class, 'form']);
  
	
	//device profile
    Route::get('/device-profile', [DeviceProfileController::class, 'index']);
    Route::get('/device-profile-datatables', [DeviceProfileController::class, 'list']);
    Route::post('/device-profile/save', [DeviceProfileController::class, 'store']);
    Route::post('/device-profile/update', [DeviceProfileController::class, 'update']);
    Route::post('/device-profile/delete', [DeviceProfileController::class, 'delete']);
    Route::get('/device-profile/{id}', [DeviceProfileController::class, 'formEdit']);
    Route::get('/device-profile/{detail}/{id}', [DeviceProfileController::class, 'detail']);
	Route::get('/device-profile-form', [DeviceProfileController::class, 'form']);

    //dapplication
    Route::get('/application', [ApplicationController::class, 'index']);
    Route::get('/application-datatables', [ApplicationController::class, 'list']);
    Route::post('/application-auto', [ApplicationController::class, 'autoCompleteDeviceModel']);
    Route::get('/application-form', [ApplicationController::class, 'form']);
    Route::post('/application/save', [ApplicationController::class, 'store']);
    Route::post('/application/update', [ApplicationController::class, 'update']);
    Route::post('/application/delete', [ApplicationController::class, 'delete']);
    Route::get('/application/{id}', [ApplicationController::class, 'formEdit']);
    Route::get('/application-get-apk/{id}', [ApplicationController::class,'getApk']);
    
    

    //terminal 
   
    Route::get('/terminal', [TerminalController::class, 'index']);
    Route::get('/terminal-datatables', [TerminalController::class, 'list']);
    Route::post('/terminal/save', [TerminalController::class, 'store']);
    Route::get('/terminal-form', [TerminalController::class, 'form']);
    Route::get('/terminal/{id}/{sn}', [TerminalController::class, 'detail']);
	Route::post('/terminal/update', [TerminalController::class, 'update']);
    Route::post('/terminal/delete', [TerminalController::class, 'delete']);
    Route::post('/terminal/lockUnlock', [TerminalController::class, 'lockUnlock']);
    Route::post('/terminal/restart', [TerminalController::class, 'restart']);
    Route::post('/terminal-device-model-auto', [TerminalController::class, 'autoCompleteDeviceModel']);
    Route::post('/terminal-merchant-auto', [TerminalController::class, 'autoComplateMerchant']);
    Route::post('/terminal-profile-auto', [TerminalController::class, 'autoComplateProfile']);
    Route::post('/terminal-import', [TerminalController::class, 'importTerminal']);
	Route::get('/terminal-import/{id}', [TerminalController::class, 'importView']);
    Route::post('/terminal/addTerminals', [TerminalController::class, 'addTerminalImport']);
    Route::get('/terminal-edit-form/{id}', [TerminalController::class, 'edit']);
    Route::get('/terminal-form2', [TerminalController::class, 'form2']);
    Route::get('/terminal-form3', [TerminalController::class, 'form3']);
    Route::get('/terminal-form4', [TerminalController::class, 'form4']);
    
    //terminal group
    Route::post('/terminal-group-get-terminal-auto', [TerminalGroupController::class, 'autoCompleteTerminal']);
    Route::get('/terminal-group', [TerminalGroupController::class, 'index']);
    Route::get('/terminal-group-datatables', [TerminalGroupController::class, 'list']);
    Route::get('/terminal-group-terminal-datatables', [TerminalGroupController::class, 'listTerminal']);
    Route::post('/terminal-group/save', [TerminalGroupController::class, 'store']);
    Route::get('/terminal-group-form', [TerminalGroupController::class, 'form']);
    Route::get('/terminal-group/{id}', [TerminalGroupController::class, 'formEdit']);
    Route::post('/terminal-group/deleteTerminals', [TerminalGroupController::class, 'deleteTerminals']);
    Route::post('/terminal-group/delete', [TerminalGroupController::class, 'delete']);
    Route::post('/terminal-group/saveTerminal', [TerminalGroupController::class, 'addTerminals']);
    Route::post('/terminal-group/update', [TerminalGroupController::class, 'update']);
    Route::get('/terminal-group/detail/{id}', [TerminalGroupController::class, 'detail']);
    Route::post('/terminal-group/getById', [TerminalGroupController::class, 'show']);
	Route::post('/terminal-group-import', [TerminalGroupController::class, 'importTerminal']);
    Route::get('/terminal-group-import/{gid}', [TerminalGroupController::class, 'importView']);
    Route::post('/terminal-group/addTerminals', [TerminalGroupController::class, 'addTerminals']);
   
    
    //downloadTask
    Route::get('/download-task', [DownloadTaskController::class,'index']);
    Route::get('/download-task-datatables', [DownloadTaskController::class,'list']);
    Route::get('/download-task-form', [DownloadTaskController::class, 'form']);
    Route::post('/download-task/save', [DownloadTaskController::class, 'store']);
    Route::get('/download-task/{id}', [DownloadTaskController::class, 'formEdit']);
    Route::get('/download-task/listTerminal', [DownloadTaskController::class,'listTerminal']);
    Route::get('/download-task/listTerminalGroup', [DownloadTaskController::class,'listGroup']);
    Route::get('/download-task/get',  [DownloadTaskController::class,'show']);
    Route::post('/download-task/update',  [DownloadTaskController::class,'update']);
    Route::post('/download-task/delete',  [DownloadTaskController::class,'delete']);
    Route::post('/download-task/cancel',  [DownloadTaskController::class,'cancel']);
    Route::post('/download-task/history',  [DownloadTaskController::class,'history']);
    Route::post('/download-task/terminalHistory',  [DownloadTaskController::class,'terminalHistory']);
    Route::get('/download-task-app-auto', [DownloadTaskController::class, 'autoCompleteApplication']);
    Route::get('/download-task-terminal-auto', [DownloadTaskController::class, 'autoCompleteTerminal']);
    Route::get('/download-task-terminalgroup-auto', [DownloadTaskController::class, 'autoCompleteTerminalGroup']);
    Route::post('/download-task/export',  [DownloadTaskController::class,'export']);
    
    //diagnostic
    Route::get('/diagnostic', [DiagnosticController::class, 'index']);
    Route::post('/diagnostic/export', [DiagnosticController::class, 'export']);
   
    //heartbeat
    Route::get('/heartbeat', [HeartBeatController::class, 'index']);
    Route::post('/heartbeat/export', [HeartBeatController::class, 'export']); 
	
	//task
    Route::get('/task', [TaskController::class, 'index']);
	Route::post('/task/export', [TaskController::class, 'export']); 
	
	//online
    Route::get('/online', [OnlineController::class, 'index']);
	Route::post('/online/export', [OnlineController::class, 'export']); 

    //location
    Route::get('/location', [LocationController::class, 'index']);
	
    
    
    //deletetask
    Route::get('/delete-task', [DeleteTaskController::class, 'index']);
    Route::get('/delete-task-datatable', [DeleteTaskController::class, 'list']);
    Route::post('/delete-task/save', [DeleteTaskController::class, 'store']);
    Route::get('/delete-task-form', [DeleteTaskController::class, 'form']);
    Route::get('/delete-task/{id}', [DeleteTaskController::class, 'formEdit']);
    Route::post('/delete-task/update', [DeleteTaskController::class, 'update']);
    Route::post('/delete-task/delete', [DeleteTaskController::class, 'delete']);
    Route::post('/delete-task/export',  [DeleteTaskController::class,'export']);
    Route::post('/delete-task/history',  [DeleteTaskController::class,'history']);
    Route::post('/delete-task/cancel',  [DeleteTaskController::class,'cancel']);

	//aid	
	Route::get('/aid-datatable', [AidController::class, 'list']);
	Route::get('/aid', [AidController::class, 'index']);
	Route::post('/aid/save', [AidController::class, 'store']);
	Route::get('/aid-form', [AidController::class, 'form']);
	Route::get('/aid/{id}', [AidController::class, 'formEdit']);
	Route::post('/aid/update', [AidController::class, 'update']);
	Route::post('/aid/delete',  [AidController::class,'delete']);
	
	 /* router capk*/ 
	Route::get('/capk-datatable', [CapkController::class,'list']);
	Route::get('/capk', [CapkController::class,'index']);
	Route::post('/capk/save', [CapkController::class,'store']);
	Route::get('/capk-form', [CapkController::class,'form']);
	Route::get('/capk/{id}', [CapkController::class, 'formEdit']);
	Route::post('/capk/update', [CapkController::class, 'update']);
	Route::post('/capk/delete',  [CapkController::class,'delete']);
	
	 /* router publicKey*/ 
	Route::get('/publicKey-datatable', [PublicKeyController::class,'list']);
	Route::get('/publicKey', [PublicKeyController::class,'index']);
	Route::post('/publicKey/save', [PublicKeyController::class,'store']);
	Route::get('/publicKey-form', [PublicKeyController::class,'form']);
	Route::get('/publicKey/{id}', [PublicKeyController::class, 'formEdit']);
	Route::post('/publicKey/update', [PublicKeyController::class, 'update']);
	Route::post('/publicKey/delete',  [PublicKeyController::class,'delete']);
	
	/* router tleSetting*/ 
	Route::get('/tleSetting-datatable', [TleSettingController::class,'list']);
	Route::get('/tleSetting', [TleSettingController::class,'index']);
	Route::post('/tleSetting/save', [TleSettingController::class,'store']);
	Route::get('/tleSetting-form', [TleSettingController::class,'form']);
	Route::get('/tleSetting/{id}', [TleSettingController::class, 'formEdit']);
	Route::post('/tleSetting/update', [TleSettingController::class, 'update']);
	Route::post('/tleSetting/delete',  [TleSettingController::class,'delete']);
	
	/* router terminalExt*/ 
 	Route::get('/terminalExt-datatable', [TerminalExtController::class,'list']);  //L
	Route::get('/terminalExt', [TerminalExtController::class,'index']);
	Route::post('/terminalExt/save', [TerminalExtController::class,'store']);
	Route::get('/terminalExt-form', [TerminalExtController::class,'form']);
	Route::get('/terminalExt/{id}', [TerminalExtController::class, 'formEdit']);  //L
	Route::post('/terminalExt/update', [TerminalExtController::class, 'update']);  //L
	Route::post('/terminalExt/delete',  [TerminalExtController::class,'delete']);
    Route::post('/terminalExt-import', [TerminalExtController::class, 'importTerminal']);
	Route::get('/terminalExt-import/{id}', [TerminalExtController::class, 'importView']);
    Route::post('/terminalExt/addTerminals', [TerminalExtController::class,'addTerminalImport']);

    /* router acquirer*/ 
    Route::get('/acquirer-datatable', [AcquirerController::class,'list']);
	Route::get('/acquirer', [AcquirerController::class,'index']);
	Route::post('/acquirer/save', [AcquirerController::class,'store']);
	Route::get('/acquirer-form', [AcquirerController::class,'form']);
	Route::get('/acquirer/{id}', [AcquirerController::class, 'formEdit']);
	Route::post('/acquirer/update', [AcquirerController::class, 'update']);
	Route::post('/acquirer/delete',  [AcquirerController::class,'delete']);
	
	/* router issuer*/ 
	Route::get('/issuer-datatable', [IssuerController::class,'list']);
	Route::get('/issuer', [IssuerController::class,'index']);
	Route::post('/issuer/save', [IssuerController::class,'store']);
	Route::get('/issuer-form', [IssuerController::class,'form']);
	Route::get('/issuer/{id}', [IssuerController::class, 'formEdit']);
	Route::post('/issuer/update', [IssuerController::class, 'update']);
	Route::post('/issuer/delete',  [IssuerController::class,'delete']);
	Route::post('/issuer/linkUnlink',  [IssuerController::class,'linkUnlink']);//L
	
	/* router card*/ 
	Route::get('/card-datatable', [CardController::class,'list']);
	Route::get('/card', [CardController::class,'index']);
	Route::post('/card/save', [CardController::class,'store']);
	Route::get('/card-form', [CardController::class,'form']);
	Route::get('/card/{id}', [CardController::class, 'formEdit']);
	Route::post('/card/update', [CardController::class, 'update']);
	Route::post('/card/delete',  [CardController::class,'delete']);
	Route::post('/card/linkUnlink',  [CardController::class,'linkUnlink']);//L  

    /* router binrange*/ 
	Route::get('/binrange-datatable', [BinRangeController::class,'list']);
	Route::get('/binrange', [BinRangeController::class,'index']);
	Route::post('/binrange/save', [BinRangeController::class,'store']);
	Route::get('/binrange-form', [BinRangeController::class,'form']);
	Route::get('/binrange/{id}', [BinRangeController::class, 'formEdit']);
	Route::post('/binrange/update', [BinRangeController::class, 'update']);
	Route::post('/binrange/delete',  [BinRangeController::class,'delete']);

     /* router  response-code*/ 
	Route::get('/response-code-datatable', [ResponseCodeController::class,'list']);
	Route::get('/response-code', [ResponseCodeController::class,'index']);
	Route::post('/response-code/save', [ResponseCodeController::class,'store']);
	Route::get('/response-code-form', [ResponseCodeController::class,'form']);
	Route::get('/response-code/{id}', [ResponseCodeController::class, 'formEdit']);
	Route::post('/response-code/update', [ResponseCodeController::class, 'update']);
	Route::post('/response-code/delete',  [ResponseCodeController::class,'delete']);
   
     /* router   terminal-template*/ 
	Route::get('/terminal-template-datatable', [TerminalTemplateController::class,'list']);
    Route::get('/terminal-template-listAcquirer-datatable', [TerminalTemplateController::class,'listAcquirer']);
	Route::get('/terminal-template', [TerminalTemplateController::class,'index']);
	Route::post('/terminal-template/save', [TerminalTemplateController::class,'store']);
	Route::get('/terminal-template-form', [TerminalTemplateController::class,'form']);
	Route::get('/terminal-template/{id}', [TerminalTemplateController::class, 'formEdit']);
	Route::post('/terminal-template/update', [TerminalTemplateController::class, 'update']);
	Route::post('/terminal-template/delete',  [TerminalTemplateController::class,'delete']);
    
        
   
    // Users
    Route::get('/user', 'UserController@index');
    Route::get('/user-form', 'UserController@form');
    Route::get('/user-form/{id}', 'UserController@formEdit');
    Route::get('/user/profile', 'UserController@profile');
    Route::get('/user/change-password', 'UserController@changePassword');
    Route::get('/user/setting', 'UserController@setting');
    Route::get('/user/{id}', 'UserController@show');
    Route::get('/user/activate/{id}', 'UserController@activateUser');
    Route::get('/user/inactivate/{id}', 'UserController@inactivateUser');
    Route::get('/user-datatables', 'UserController@datatables');
    Route::get('/user-export', 'UserController@export');
    Route::post('/user-import', 'UserController@import');
    Route::post('/user/save', 'UserController@store');
    Route::post('/user/update', 'UserController@update');
    Route::post('/user/save2', 'UserController@store2');
    Route::post('/user/update2', 'UserController@update2');
    Route::post('/user/change-password', 'UserController@storeChangePassword');
    Route::post('/user/update-profile', 'UserController@updateProfile');
    Route::post('/user/setting', 'UserController@storeSetting');
    Route::get('/user/delete/{id}', 'UserController@deleteUser');
  
    // User Groups
    Route::get('/user-group', 'UserGroupController@index');
    Route::get('/user-group/disable/', 'UserGroupController@disable');
    Route::get('/user-group-datatables', 'UserGroupController@datatables');
    Route::get('/user-group/detail/{id}', 'UserGroupController@show');
    Route::get('/user-group/disable/{id}', 'UserGroupController@disableUserGroup');
    Route::get('/user-group/activate/{id}', 'UserGroupController@activateUserGroup');
    Route::get('/user-group/privileges/{id}', 'UserGroupController@getPrivileges');
    Route::post('/user-group/privileges', 'UserGroupController@storePrivileges');
    Route::post('/user-group/save', 'UserGroupController@store');
    Route::post('/user-group/update', 'UserGroupController@update');

     // Menus
    Route::get('/menu', 'MenuController@index');
    //Route::get('/menu-form', 'MenuController@form');
    Route::get('/menu/detail/{id}', 'MenuController@show');
    Route::get('/menu/delete/{id}', 'MenuController@destroy');
    Route::post('/menu/save', 'MenuController@store');
    Route::post('/menu/update', 'MenuController@update');
    Route::post('/menu/build', 'MenuController@build');

    
});
 Route::get('/verification/otp/{v?}', 'AuthOtpController@verificationOtp');
 Route::post('/verification-otpcheck', 'AuthOtpController@otpCheck')->name('otp.check');
 Route::get('/verification-otpSuccess', [AuthOtpController::class, 'otpCheckSuccess'])->name('auth.checkSuccess');
 Route::get('/verification-auth-otp/{v?}', 'AuthOtpController@authOtp')->name('auth.otp');
  


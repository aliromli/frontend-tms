<?php
namespace Database\Seeders;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class VendorsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('vendor')->insert([
            [
                'id'        => 1,
                'name'      => 'PT. Indonesia Health Protection',
                'address1'  => 'Jl. M. Toha Priuk - Tangerang',
                'address2'  => 'Tangerang',
                'city'      => 'Tangerang',
                'zip_code'  => '',
                'phone'     => '08158748890',
                'fax'       => '',
                'email'     => 'customer@ihp.co.id',
                'active'    => 'Y',
                'image'    => 'vendor/sample.jpg'
            ], [
                'id'        => 2,
                'name'      => 'Klinik ABC',
                'address1'  => 'Jl. Jalan Aja Sih',
                'address2'  => 'Palem Semi',
                'city'      => 'Tangerang',
                'zip_code'  => '77745',
                'phone'     => '021-98750983',
                'fax'       => '021-98750983',
                'email'     => 'care@klinik-abc.co.id',
                'active'    => 'Y',
                'image'     => 'vendor/sample.jpg'
            ], [
                'id'        => 3,
                'name'      => 'Plaza Medis Indonesia',
                'address1'  => 'Jl. M. Toha.',
                'address2'  => 'Palem Semi',
                'city'      => 'Tangerang',
                'zip_code'  => '77745',
                'phone'     => '021-0989089999',
                'fax'       => '021-9898909977',
                'email'     => 'care@plazamedis.com',
                'active'    => 'Y',
                'image'     => 'vendor/sample.jpg'
            ]
        ]);
    }
}

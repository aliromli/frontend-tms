<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UsersTableSeeder::class);
        $this->call(UserGroupsTableSeeder::class);
        $this->call(UserGroupActionsTableSeeder::class);
        $this->call(MenuActionsTableSeeder::class);
        $this->call(MenusTableSeeder::class);
        $this->call(CustomerTableSeeder::class);
        $this->call(VendorsTableSeeder::class);
        $this->call(VendorCustomerSeeder::class);
       
    }
}

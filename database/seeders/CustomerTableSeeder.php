<?php
namespace Database\Seeders;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CustomerTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('customer')->insert([
            [
                'id'        => 8,
                'name'      => 'PT. ABC',
                'address1'  => 'Kawasan Industri Bukit Indah Golf',
                'address2'  => 'Purwakarta',
                'city'      => 'Purwakarta',
                'zip_code'  => '',
                'phone'     => '00',
                'fax'       => '',
                'email'     => 'xx@xx.co.id',
                'active'    => 'Y'
            ]
        ]);
    }
}

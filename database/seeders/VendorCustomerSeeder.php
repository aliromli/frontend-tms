<?php
namespace Database\Seeders;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class VendorCustomerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('vendor_customer')->insert([
            [
                'id'            => 27,
                'vendor_id'     => 1,
                'customer_id'   => 27
            ],
			[
                'id'            => 28,
                'vendor_id'     => 3,
                'customer_id'   => 39
            ]
        ]);
    }
}

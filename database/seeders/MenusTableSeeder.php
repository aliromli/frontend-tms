<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class MenusTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('menus')->insert([
            [
                'name' => 'Dashboard',
                'action_url' => '/home',
                'icon' => '<i class="fa fa-fw fa-lg fa-dashboard"></i>',
                'tooltip' => 'dashboard',
                'parent_id' => null,
                'sequence' => 1
            ],[
                'name' => 'Customer',
                'action_url' => '/customer',
                'icon' => '<i class="fa fa-fw fa-lg fa-user"></i>',
                'tooltip' => 'customer',
                'parent_id' => null,
                'sequence' => 2
            ],[
                'name' => 'Vendor',
                'action_url' => '/vendor',
                'icon' => '<i class="fa fa-fw fa-lg fa-user"></i>',
                'tooltip' => 'vendor',
                'parent_id' => null,
                'sequence' => 3
            ],[
                'name' => 'Project',
                'action_url' => '/project',
                'icon' => '<i class="fa fa-fw fa-lg fa-file"></i>',
                'tooltip' => 'project',
                'parent_id' => null,
                'sequence' => 4
            ],[
                'name' => 'Database',
                'action_url' => '#',
                'icon' => '<i class="fa fa-fw fa-lg fa-database"></i>',
                'tooltip' => 'database',
                'parent_id' => null,
                'sequence' => 5
            ], [
                'name' => 'Report',
                'action_url' => '#',
                'icon' => '<i class="fa fa-fw fa-lg fa-pie-chart"></i>',
                'tooltip' => 'report',
                'parent_id' => null,
                'sequence' => 6
            ], [
                'name' => 'User Management',
                'action_url' => '#',
                'icon' => '<i class="fa fa-fw fa-lg fa-user"></i>',
                'tooltip' => 'user_management',
                'parent_id' => null,
                'sequence' => 7
            ], [
                'name' => 'Tools',
                'action_url' => '#',
                'icon' => '<i class="fa fa-fw fa-lg fa-wrench"></i>',
                'tooltip' => 'tools',
                'parent_id' => null,
                'sequence' => 8
            ], [
                'name' => 'Documentation',
                'action_url' => '#',
                'icon' => '<i class="fa fa-fw fa-lg fa-file"></i>',
                'tooltip' => 'documentation',
                'parent_id' => null,
                'sequence' => 9
            ], [
                'name' => 'Medical Check Up',
                'action_url' => '/database/medical-check-up',
                'icon' => '<i class="fa fa-fw fa-lg fa-database"></i>',
                'tooltip' => 'medical_check_up',
                'parent_id' => 5,
                'sequence' => 1
            ], [
                'name' => 'Audiometri',
                'action_url' => '/database/audiometri',
                'icon' => '<i class="fa fa-fw fa-lg fa-database"></i>',
                'tooltip' => 'audiometri',
                'parent_id' => 5,
                'sequence' => 2
            ], [
                'name' => 'Radiologi',
                'action_url' => '/database/rontgen',
                'icon' => '<i class="fa fa-fw fa-lg fa-database"></i>',
                'tooltip' => 'radiology',
                'parent_id' => 5,
                'sequence' => 3
            ], [
                'name' => 'Drug Screening',
                'action_url' => '/database/drug-screening',
                'icon' => '<i class="fa fa-fw fa-lg fa-database"></i>',
                'tooltip' => 'drug_screening',
                'parent_id' => 5,
                'sequence' => 4
            ], [
                'name' => 'Pasien',
                'action_url' => '#',
                'icon' => '<i class="fa fa-fw fa-lg fa-pie-chart"></i>',
                'tooltip' => 'patient',
                'parent_id' => 6,
                'sequence' => 1
            ], [
                'name' => 'Top 10 Diseases',
                'action_url' => '/report/top-ten-diseases',
                'icon' => '<i class="fa fa-fw fa-lg fa-pie-chart"></i>',
                'tooltip' => 'top_ten_diseases',
                'parent_id' => 6,
                'sequence' => 2
            ], [
                'name' => 'All Diseases',
                'action_url' => '/report/all-diseases',
                'icon' => '<i class="fa fa-fw fa-lg fa-pie-chart"></i>',
                'tooltip' => 'all_diseases',
                'parent_id' => 6,
                'sequence' => 3
            ], [
                'name' => 'Radiologi',
                'action_url' => '/report/patient/radiologi',
                'icon' => '<i class="fa fa-fw fa-lg fa-pie-chart"></i>',
                'tooltip' => 'radiology',
                'parent_id' => 6,
                'sequence' => 6
            ], [
                'name' => 'Statistik Umum',
                'action_url' => '/report/statistik-umum',
                'icon' => '<i class="fa fa-fw fa-lg fa-pie-chart"></i>',
                'tooltip' => 'general_statistic',
                'parent_id' => 6,
                'sequence' => 7
            ], [
                'name' => 'User',
                'action_url' => '/user',
                'icon' => '<i class="fa fa-fw fa-lg fa-user"></i>',
                'tooltip' => 'user',
                'parent_id' => 7,
                'sequence' => 1
            ], [
                'name' => 'User Group',
                'action_url' => '/user-group',
                'icon' => '<i class="fa fa-fw fa-lg fa-user"></i>',
                'tooltip' => 'user_group',
                'parent_id' => 7,
                'sequence' => 2
            ], [
                'name' => 'Menu',
                'action_url' => '/menu',
                'icon' => '<i class="fa fa-fw fa-lg fa-user"></i>',
                'tooltip' => 'menu',
                'parent_id' => 7,
                'sequence' => 3
            ], [
                'name' => 'Backup Database',
                'action_url' => '/tools/backup-database',
                'icon' => '<i class="fa fa-fw fa-lg fa-wrench"></i>',
                'tooltip' => 'backup_database',
                'parent_id' => 8,
                'sequence' => 1
            ], [
                'name' => 'Restore Database',
                'action_url' => '/tools/restore-database',
                'icon' => '<i class="fa fa-fw fa-lg fa-wrench"></i>',
                'tooltip' => 'restore_database',
                'parent_id' => 8,
                'sequence' => 2
            ], [
                'name' => 'Medical Check Up',
                'action_url' => '/report/patient/medical-check-up',
                'icon' => '<i class="fa fa-fw fa-lg fa-pie-chart"></i>',
                'tooltip' => 'medical_check_up',
                'parent_id' => 14,
                'sequence' => 1
            ], [
                'name' => 'Diagnosa Kesehatan Kerja',
                'action_url' => '/report/patient/diagnosa-kesehatan-kerja',
                'icon' => '<i class="fa fa-fw fa-lg fa-pie-chart"></i>',
                'tooltip' => 'work_health_diagnosis',
                'parent_id' => 14,
                'sequence' => 2
            ], [
                'name' => 'Diagnosa Terbanyak',
                'action_url' => '/report/patient/diagnosa-terbanyak',
                'icon' => '<i class="fa fa-fw fa-lg fa-pie-chart"></i>',
                'tooltip' => 'most_diagnosis',
                'parent_id' => 14,
                'sequence' => 3
            ], [
                'name' => 'Elektrokardiografi',
                'action_url' => '/report/patient/elektrokardiografi',
                'icon' => '<i class="fa fa-fw fa-lg fa-pie-chart"></i>',
                'tooltip' => 'electrocardiograph',
                'parent_id' => 14,
                'sequence' => 4
            ], [
                'name' => 'Audiometri',
                'action_url' => '/report/patient/audiometri',
                'icon' => '<i class="fa fa-fw fa-lg fa-pie-chart"></i>',
                'tooltip' => 'audiometri',
                'parent_id' => 6,
                'sequence' => 4
            ], [
                'name' => 'Spirometri',
                'action_url' => '/report/patient/spirometri',
                'icon' => '<i class="fa fa-fw fa-lg fa-pie-chart"></i>',
                'tooltip' => 'spirometri',
                'parent_id' => 6,
                'sequence' => 5
            ], [
                'name' => 'Drug Screening',
                'action_url' => '/report/patient/drug-screening',
                'icon' => '<i class="fa fa-fw fa-lg fa-pie-chart"></i>',
                'tooltip' => 'drug_screening',
                'parent_id' => 14,
                'sequence' => 5
            ], [
                'name' => 'Formula',
                'action_url' => '#',
                'icon' => '<i class="fa fa-fw fa-lg fa-database"></i>',
                'tooltip' => 'formula',
                'parent_id' => 5,
                'sequence' => 5
            ], [
                'name' => 'Parameter',
                'action_url' => '/database/parameter',
                'icon' => '<i class="fa fa-fw fa-lg fa-database"></i>',
                'tooltip' => 'parameter',
                'parent_id' => 31,
                'sequence' => 9
            ], [
                'name' => 'Formula',
                'action_url' => '/database/formula',
                'icon' => '<i class="fa fa-fw fa-lg fa-database"></i>',
                'tooltip' => 'formula',
                'parent_id' => 31,
                'sequence' => 12
            ]
        ]);
    }
}

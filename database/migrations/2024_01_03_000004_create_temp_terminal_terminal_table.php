<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

  
class CreateTempTerminalTerminalTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('temporary_terminal_terminal', function (Blueprint $table) {
            $table->string('user_name')->nullable();
            $table->string('terminal_id')->nullable();
			$table->string('sn')->nullable();
            $table->text('group_id')->nullable();
            $table->text('groupName')->nullable();
            $table->string('model_id')->nullable();
            $table->string('modelName')->nullable();
            $table->string('profile_id')->nullable();
            $table->string('profileName')->nullable();
            $table->string('merchant_id')->nullable();
            $table->string('merchantName')->nullable();
            $table->string('note')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('temporary_terminal_terminal');
    }
}

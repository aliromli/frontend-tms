<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTempTerminalExtTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('temporary_terminal_ext', function (Blueprint $table) {
            $table->string('user_name')->nullable();
            $table->string('terminal_id')->nullable();
            $table->string('merchant_id')->nullable();
            $table->string('merchant_name1')->nullable();
            $table->string('merchant_name2')->nullable();
            $table->string('merchant_name3')->nullable();
            $table->string('template_name')->nullable();
            $table->string('status')->nullable();
            $table->string('note')->nullable();
            $table->string('template_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('temporary_terminal_ext');
    }
}

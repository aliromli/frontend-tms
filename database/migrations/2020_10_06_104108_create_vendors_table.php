<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVendorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vendor', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name',45);
            $table->string('address1',150);
            $table->string('address2',150)->nullable();
            $table->string('city',50);
            $table->string('zip_code',5)->nullable();
            $table->string('phone',25);
            $table->string('fax',25)->nullable();
            $table->string('email',50);
            $table->enum('active',['Y','N'])->default('Y');
            $table->string('image', 200)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vendors');
    }
}

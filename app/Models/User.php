<?php


namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Notifications\PasswordReset;


class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email','email_verified_at','password', 'user_group_id', 'active', 'avatar','merchant_id','tenant_id','force_change_password','changed_password'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * User group
     *
     * @return type
     */
    public function userGroup()
    {
        return $this->belongsTo('App\Models\UserGroup');
    }

    /**
     * Customer
     *
     * @return type
     */
    public function customer()
    {
        return $this->belongsTo('App\Models\Customer');
    }
	
	
	 /**
     * Venodr
     *
     * @return type
     */
    public function vendor()
    {
        return $this->belongsTo('App\Models\Vendor');
    }

    /**
     * Setting
     *
     * @return void
     */
    public function setting()
    {
        return $this->hasOne('App\Models\Setting');
    }

    public function sendPasswordResetNotification($token)
    {
        $this->notify(new PasswordReset($token));
    }
}

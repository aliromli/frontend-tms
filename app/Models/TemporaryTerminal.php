<?php


namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TemporaryTerminal extends Model
{
    protected $table = 'temporary_terminal';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['terminal_id', 'group_id', 'sn', 'result'];

   
}
  

<?php


namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TemporaryTerminalExt extends Model
{
    protected $table = 'temporary_terminal_ext';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['user_name','terminal_id', 'merchant_id', 'merchant_name1','merchant_name2','merchant_name3', 'template_name','status','note','template_id'];

   
}
  

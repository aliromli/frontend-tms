<?php


namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TemporaryTerminalTerminal extends Model
{
    protected $table = 'temporary_terminal_terminal';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['user_name','terminal_id', 'group_id','groupName', 'sn','model_id','modelName','profile_id','profileName','merchant_id','merchantName','note'];

   
}
  
<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Privilege;

class MerchantTypeController extends Controller
{
     /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
    	Privilege::visibleView($request->segments()[0],'READ'); 
       
		return view('pages.merchant_type.index', [
					'data' => null,
				]);
	 
		
       
    }

    /**
     * city datatables
     *
     * @return type JSON city
     */
    public function list(Request $request)
    {
        // Initial Order
        $orderIndex = (int) $request->order[0]['column'];
        $orderDir = $request->order[0]['dir'];
        $orderColumn = $request->columns[$orderIndex]['data'];
		
        
        $param['pageNum']   = ($request->start / $request->length) + 1;
        $param['pageSize']   = $request->length; 
        $param['name']   = $request->name; 
        $response = $this->httpWithHeaders()
        ->get( $this->apiTms()  . 'api/v1/merchantType/list', $param)->json();
   

        if($response['responseCode'] == "0000")
        {
            return response()->json([
                'draw'              => $request->draw,
                'recordsTotal'      => $response['total'],
                'recordsFiltered'   => $response['total'], 
                'data'              => $response['rows'],
                'input'             => [
                    'start' => $request->start,
                    'draw' => $request->draw,
                    'length' =>  $request->length,
                    'order' => $orderIndex,
                    'orderDir' => $orderDir,
                    'orderColumn' => $request->columns[$orderIndex]['data']
                ]
            ]);
        }
        else
        {
            return response()->json([
                'draw'              => $request->draw,
                'recordsTotal'      => 0,
                'recordsFiltered'   => 0, 
                'data'              => [],
                'input'             => [
                    'start' => $request->start,
                    'draw' => $request->draw,
                    'length' =>  $request->length,
                    'order' => $orderIndex,
                    'orderDir' => $orderDir,
                    'orderColumn' => $request->columns[$orderIndex]['data']
                ]
            ]);
        }
       
    }
	
	public function form(Request $request)
    {
        return view('pages.merchant_type.form', [
           
            'data'=>null,
            'edit' => 'no'
        ]);

    }

    public function formEdit(Request $request)
    {
        
        $response = $this->httpWithHeaders()
        ->get( $this->apiTms() . 'api/v1/merchantType/get', [
            'id' => $request->id
        ])->json();
		
		//print_r($response);
		//exit;

        return view('pages.merchant_type.form', [
            'data' => $response['data'],
            'edit' => 'ya'
        ]);

    }

    public function store(Request $request)
    {
        $response = $this->httpWithHeaders()
            ->post( $this->apiTms()  . 'api/v1/merchantType/add', [
                'description' => $request->description,
                'name' => $request->name
            ])->json();
           
        return $this->responseCode($response,'MerchantType has been added successfull');  
        
    }

    public function  show(Request $request)
    {
        $response = $this->httpWithHeaders()
            ->get( $this->apiTms() . 'api/v1/merchantType/get', [
                'id' => $request->id
            ])->json();
           
      
        if($response['responseCode'] =='0000'){
         
            return response()->json(['responseCode' => 200, 'responseStatus' => 'OK', 'responseMessage' => $response['data']]);
     
          }
        else{
            return response()->json(['responseCode' => 500, 'responseStatus' => 'Failed', 'responseMessage' => $response['responseDesc']]);
        }
    }

    public function update(Request $request)
    {
        
        $response = $this->httpWithHeaders()
         
            ->post( $this->apiTms() . 'api/v1/merchantType/update', [

                'name' => $request->name,
                'description' => $request->description,
                'version' => $request->version,
                'id' => $request->id,


            ])->json();
			
		return $this->responseCode($response,'MerchantType has been updated successfull');  
           
      
    }

    public function delete(Request $request)
    {
      
		$response = $this->httpWithHeaders()
		->post( $this->apiTms() . 'api/v1/merchantType/delete', [
			'version' => $request->version,
			'id' => $request->id,
		])->json();
           
        if($response['responseCode'] =='0000'){
            return response()->json(['responseCode' => 200, 'responseStatus' => 'OK', 'responseMessage' => 'MerchantType has been deleted successfully']);
        }
        else if($response['responseCode'] =='0400'){
            return response()->json(['responseCode' => 500, 'responseStatus' => 'No Data', 'responseMessage' => $response['responseDesc'] ]);
        }
        else
        {
            return response()->json(['responseCode' => 501, 'responseStatus' => 'Exception', 'responseMessage' => $response['responseDesc']]);
        }

    }


}

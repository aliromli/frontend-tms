<?php

namespace App\Http\Controllers;

use App\Models\TemporaryTerminalTerminal;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Box\Spout\Reader\Common\Creator\ReaderEntityFactory;
use Privilege;


class TerminalController extends Controller
{
     /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
       
        Privilege::visibleView($request->segments()[0],'READ');
        return view('pages.terminal.index', [
				    'data'=>null,
				]);
		
       
    }

    /**
     * city datatables
     *
     * @return type JSON city
     */
    public function list(Request $request)
    {
        // Initial Order
        $orderIndex = (int) $request->order[0]['column'];
        $orderDir = $request->order[0]['dir'];
        $orderColumn = $request->columns[$orderIndex]['data'];
		
        
        $param['pageNum']   = ($request->start / $request->length) + 1;
        $param['pageSize']   = $request->length; 
        $param['modelId']   = $request->deviceModelId; 
        $param['merchantId']   = $request->merchantId; 
        $param['profileId']   = $request->profileId; 
        $param['sn']   = $request->sn; 
        
        $response = $this->httpWithHeaders()
        ->get( $this->apiTms()  . 'api/v1/terminal/list', $param)->json();
   

        if($response['responseCode'] == "0000")
        {
            return response()->json([
                'draw'              => $request->draw,
                'recordsTotal'      => $response['total'],
                'recordsFiltered'   => $response['total'], 
                'data'              => $response['rows'],
                'input'             => [
                    'start' => $request->start,
                    'draw' => $request->draw,
                    'length' =>  $request->length,
                    'order' => $orderIndex,
                    'orderDir' => $orderDir,
                    'orderColumn' => $request->columns[$orderIndex]['data']
                ]
            ]);
        }
        else
        {
            return response()->json([
                'draw'              => $request->draw,
                'recordsTotal'      => 0,
                'recordsFiltered'   => 0, 
                'data'              => [],
                'input'             => [
                    'start' => $request->start,
                    'draw' => $request->draw,
                    'length' =>  $request->length,
                    'order' => $orderIndex,
                    'orderDir' => $orderDir,
                    'orderColumn' => $request->columns[$orderIndex]['data']
                ]
            ]);
        }
       
    }

    public function form(Request $request)
    {
        
        $param['pageNum']   = 1;
        $param['pageSize']   = 1000;
        
       
        $responseMer = $this->httpWithHeaders()
        ->send('GET',  $this->apiTms() . 'api/v1/merchant/list', [
           'body' => json_encode($param)
        ])->json();

        $responsedm = $this->httpWithHeaders()
        ->send('GET',  $this->apiTms() . 'api/v1/deviceModel/list', [
            'body' => json_encode($param)
        ])->json();

        $responsedp = $this->httpWithHeaders()
        ->send('GET',  $this->apiTms() . 'api/v1/profile/list', [
            'body' => json_encode($param)
        ])->json();

        return view('pages.terminal.form', [
           
            'merchant'=> ($responseMer['responseCode'] =='0000') ? $responseMer['rows'] : null ,
            'devicemodel'=> ($responsedm['responseCode'] =='0000') ? $responsedm['rows'] : null,
            'deviceprofile'=>($responsedp['responseCode'] =='0000') ? $responsedp['rows'] : null,
            'data'=>null,
            'edit' => 'no'
        ]);

    }

    public function edit(Request $request)
    {
        $param['pageNum']   = 1;
        $param['pageSize']   = 1000;
        
        $responseData = $this->httpWithHeaders()
        ->get( $this->apiTms() . 'api/v1/terminal/get', [
            'id' => $request->id
        ])->json();
       
        $responseMer = $this->httpWithHeaders()
        ->send('GET',  $this->apiTms() . 'api/v1/merchant/list', [
           'body' => json_encode($param)
        ])->json();

        $responsedm = $this->httpWithHeaders()
        ->send('GET',  $this->apiTms() . 'api/v1/deviceModel/list', [
            'body' => json_encode($param)
        ])->json();

        $responsedp = $this->httpWithHeaders()
        ->send('GET',  $this->apiTms() . 'api/v1/profile/list', [
            'body' => json_encode($param)
        ])->json();

       
       

        return view('pages.terminal.form', [
           
            'merchant'=> ($responseMer['responseCode'] =='0000') ? $responseMer['rows'] : null ,
            'devicemodel'=> ($responsedm['responseCode'] =='0000') ? $responsedm['rows'] : null,
            'deviceprofile'=>($responsedp['responseCode'] =='0000') ? $responsedp['rows'] : null,
            //'gp'=> ($responseGp['responseCode'] =='0000') ? $responseGp['rows'] : null ,
            'data'=>$responseData['data'],
            'edit' => 'ya'
        ]);
    }

    public function detail(Request $request)
    {
        Privilege::visibleEdit($request->segments()[0],'UPDATE'); 		
        /*
		$param['pageNum']   = 1;
        $param['pageSize']   = 1000;
        $responseMer = $this->httpWithHeaders()
        ->send('GET',  $this->apiTms() . 'api/v1/merchant/list', [
            'body' => json_encode($param)
        ])->json();

        $responsedm = $this->httpWithHeaders()
        ->send('GET',  $this->apiTms() . 'api/v1/deviceModel/list', [
            'body' => json_encode($param)
        ])->json();

        $responsedp = $this->httpWithHeaders()
        ->send('GET',  $this->apiTms() . 'api/v1/profile/list', [
            'body' => json_encode($param)
        ])->json();
        //V1E0178092
		*/
        $response = $this->httpWithHeaders()
        ->get( $this->apiTms() . 'api/v1/terminal/get', [
            'id' => $request->id
        ])->json();

       
        $response_HB = $this->httpWithHeaders()
        ->get( $this->apiTms() . 'api/v1/diagnostic/lastHeartbeat', [
            //'terminalId' => $request->id
            //'sn' => 'V1E0434880'
            'sn'=> $request->sn
        ])->json();
       
        $response_DN = $this->httpWithHeaders()
        ->get( $this->apiTms() . 'api/v1/diagnostic/lastDiagnostic', [
            //'terminalId' => "aa161b7b-8f06-4de4-fb71-e44ac48a4e51"//$request->id
            //'sn'=>"V1E0800867"
            'sn'=> $request->sn
        ])->json();
        
        $data = ($response['responseCode'] =='0000') ? $response['data'] : null;
        $rHB = ($response_HB['responseCode'] =='0000') ? $response_HB['data'] : null;
        $rDN = ($response_DN['responseCode'] =='0000') ? $response_DN['data'] : null;
		return view('pages.terminal.detail', [
            'data'=>$data,
            'dataHB'=>$rHB,
            'dataDN'=>$rDN,
        ]);

    }

    //importTerminal
	public function importView($id)
	{
		
        // $response = $this->httpWithHeaders()
        // ->get( $this->apiTms() . 'api/v1/terminal/get', [
        //    'id' => $id
        // ])->json();
        
        return view('pages.terminal.import', [
            'user_name'=>$id,
            'data_temp'=>TemporaryTerminalTerminal::where('user_name',$id)->get()
        ]);
	}
    
    public function store(Request $request)
    {
        $response = $this->httpWithHeaders()
            ->post( $this->apiTms()  . 'api/v1/terminal/add', [
                'sn' => $request->sn,
                'modelId' => $request->modelId,
                'merchantId' => $request->merchantId,
                'profileId' => $request->profileId,
                'terminalGroupIds' =>  json_decode($request->terminalGroupIds),
                
            ])->json();
           
		return $this->responseCode($response,'Terminal has been added successfully');  
       
    }

    public function  show(Request $request)
    {
        

        $response = $this->httpWithHeaders()
            ->get( $this->apiTms() . 'api/v1/terminal/get', [
                'id' => $request->id
            ])->json();
           
      
        if($response['responseCode'] =='0000'){
         
            return response()->json(['responseCode' => 200, 'responseStatus' => 'OK', 'responseMessage' => $response['data']]);
     
          }
        else{
            return response()->json(['responseCode' => 500, 'responseStatus' => 'Failed', 'responseMessage' => $response['responseDesc']]);
        }
    }

    public function update(Request $request)
    {
        $response = $this->httpWithHeaders()
         
            ->post( $this->apiTms() . 'api/v1/terminal/update', [

                'sn' => $request->sn,
                'modelId' => $request->modelId,
                'merchantId' => $request->merchantId,
                'profileId' => $request->profileId,
                'terminalGroupIds' =>  json_decode($request->terminalGroupIds),
                'id' => $request->id,
                'version' => $request->version
            ])->json();
           
		return $this->responseCode($response,'Terminal has been updated successfully');  
    }

    public function delete(Request $request)
    {
        if(Privilege::visibleDelete($request->segments()[0],'DELETE')=='Y' )
        {
            return response()->json(['responseCode' => 401, 'responseStatus' => 'No Authorized', 'responseMessage' => 'No Authorized' ]);
        }

        $response = $this->httpWithHeaders()
        
        ->post( $this->apiTms() . 'api/v1/terminal/delete', [

            'version' => $request->version,

            'id' => $request->id,

            'sn' => $request->sn


        ])->json();
        
      
        if($response['responseCode'] =='0000'){
            return response()->json(['responseCode' => 200, 'responseStatus' => 'OK', 'responseMessage' => 'Terminal has been deleted successfully']);
        }
        else if($response['responseCode'] =='0400'){
            return response()->json(['responseCode' => 500, 'responseStatus' => 'No Data', 'responseMessage' => $response['responseDesc'] ]);
        }
        else
        {
            return response()->json(['responseCode' => 501, 'responseStatus' => 'Exception', 'responseMessage' => $response['responseDesc']]);
        }

    }

    public function lockUnlock(Request $request)
    {
        $response = $this->httpWithHeaders()
            
        ->post( $this->apiTms() . 'api/v1/terminal/lockUnlock', [

            'version' => $request->version,

            'id' => $request->id,

            'action' => $request->action,
   
            'lockReason' => $request->lockReason


        ])->json();
       
  
        if($response['responseCode'] =='0000'){
            return response()->json(['responseCode' => 200, 'responseStatus' => 'OK', 'responseMessage' => 'OK']);
        }
        else if($response['responseCode'] =='0400'){
            return response()->json(['responseCode' => 500, 'responseStatus' => 'No Data', 'responseMessage' => $response['responseDesc'] ]);
        }
        else
        {
            return response()->json(['responseCode' => 501, 'responseStatus' => 'Exception', 'responseMessage' => $response['responseDesc']]);
        }
    }

    public function restart(Request $request)
    {
        $response = $this->httpWithHeaders()
            
        ->post( $this->apiTms() . 'api/v1/terminal/restart', [

            'version' => $request->version,

            'id' => $request->id,


        ])->json();
       
  
        if($response['responseCode'] =='0000'){
            return response()->json(['responseCode' => 200, 'responseStatus' => 'OK', 'responseMessage' => 'OK']);
        }
        else if($response['responseCode'] =='0400'){
            return response()->json(['responseCode' => 500, 'responseStatus' => 'No Data', 'responseMessage' => $response['responseDesc'] ]);
        }
        else
        {
            return response()->json(['responseCode' => 501, 'responseStatus' => 'Exception', 'responseMessage' => $response['responseDesc']]);
        }
    }
    
    public function autoComplateMerchant(Request $request)
    {
        $search = $request->search;
        $response = null;
        if($search !== ''){
            $response = $this->httpWithHeaders()
            ->get( $this->apiTms() . 'api/v1/merchant/list', [
                'name' =>$search
            ])->json();
           
        }
        // else{
        //     $response = $this->httpWithHeaders()
        //     ->get( $this->apiTms() . 'api/v1/deviceModel/list', [
        //         'modelName' => $search
        //     ])->json();
        // }

        //print_r($response);
        
        if($response['responseCode'] == '0000')
        {
            $r = array();
            foreach($response['rows'] as $data){
                $r[] = array("value"=>$data['id'],"label"=>$data['name']);
            }

            return response()->json($r);

        }

        
    }


    public function autoCompleteDeviceModel(Request $request)
    {
        $search = $request->search;
        $response = null;
        if($search !== ''){
            $response = $this->httpWithHeaders()
            ->get( $this->apiTms() . 'api/v1/deviceModel/list', [
                'modelName' =>$search
            ])->json();
           
        }
        // else{
        //     $response = $this->httpWithHeaders()
        //     ->get( $this->apiTms() . 'api/v1/deviceModel/list', [
        //         'modelName' => $search
        //     ])->json();
        // }

        //print_r($response);
        
        if($response['responseCode'] == '0000')
        {
            $r = array();
            foreach($response['rows'] as $data){
                $r[] = array("value"=>$data['id'],"label"=>$data['model']);
            }

            return response()->json($r);

        }

        
    }

    public function autoComplateProfile(Request $request)
    {
        $search = $request->search;
        $response = null;
        if($search !== ''){
            $response = $this->httpWithHeaders()
            ->get( $this->apiTms() . 'api/v1/profile/list', [
                'name' =>$search
            ])->json();
           
        }
        // else{
        //     $response = $this->httpWithHeaders()
        //     ->get( $this->apiTms() . 'api/v1/deviceModel/list', [
        //         'modelName' => $search
        //     ])->json();
        // }

        //print_r($response);
        
        if($response['responseCode'] == '0000')
        {
            $r = array();
            foreach($response['rows'] as $data){
                $r[] = array("value"=>$data['id'],"label"=>$data['name']);
            }

            return response()->json($r);

        }

        
    }

  
    public function importTerminal(Request $request)
	{
        if ($request->hasFile('file')) {

            $response = $this->httpWithHeaders();
            $userName   = $request->userName;
            $image = $request->file('file');
            $fileName = $request->file('file')->getClientOriginalName();
            $response = $response->attach('file', $image->get(),$fileName);
           
            $response = $response->post( $this->apiTms() . 'api/v1/importTerminalCek/importCek',$request->all())->json();
		    if($response['responseCode'] == '0000')
            {
                $data = $response['result'];
                TemporaryTerminalTerminal::where('user_name',$userName)->delete();
                $ff = TemporaryTerminalTerminal::insert($data);
                if($ff)
                {
                    return response()->json([
                        'responseCode' => 200,
                        'responseMessage' => 'Excel has been Uploaded To Temporary'
                        
                    ]); 
                }
                else
                {
                    return response()->json([
                        'responseCode' => 500,
                        'responseMessage' =>'Excel has been failed',
                        
                    ]); 
                }
            }
            else
            {
                return response()->json([
                    'responseCode' => 500,
                    'responseMessage' => $response['responseDesc']
                ]); 
            }
            
        }
        else
        {
            return response()->json([
            'responseCode' => 500,
            'responseMessage' => 'File is empty'
        ]); 
        }
    }

    public function importTerminal_x(Request $request)
	{
		
        if ($request->hasFile('file')) {

            $file = $request->file('file');
            $userName   = $request->userName;
            $processId = null;
            $data = null;
            if($file->isValid()) {
                $nameStripped = str_replace(" ", "-", $file->getClientOriginalName());
                $f = $file->storeAs('upload', $nameStripped,'public'); // linux
                $data = $this->actionImport($f,$userName);
				
                }
             if($data)
             {
                return response()->json([
                    'responseCode' => 200,
                    'responseMessage' => 'Uploaded',
                    'processId' => $processId
                ]); 
             }
             else
             {
                return response()->json([
                    'responseCode' => 500,
                    'responseMessage' => 'Gagal Import',
                    'processId' => $processId
                ]); 
             }
             
        }
	}

    public function actionImport($f,$userName)
    {
        try {
            $path = storage_path('app'.DIRECTORY_SEPARATOR.'public'.DIRECTORY_SEPARATOR.$f);
            $reader = ReaderEntityFactory::createReaderFromFile($path);
            $reader->open($path);

            foreach ($reader->getSheetIterator() as $sheet) {
                if($sheet->getIndex() === 0) {
                    //$this->calculateRow($sheet, $this->processId);
                    //$this->readSheet($sheet, $this->processId);
                    return $this->readSheet($sheet,$userName);
                    break;
                }
            }

            $reader->close();
            // delete file
            unlink($path);

        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }
    //session()->get('user.name')
    private function readSheet($sheet,$userName)
    {
        $data = array();        
        foreach ($sheet->getRowIterator() as $i => $row)
		{

            if($i == 1) {
                continue;
            }

            $r = $row->toArray();
   
            $s = $this->cekSn($r[0]);
            $m = $this->cekModel($r[1]);
            $mer = $this->cekMerchant($r[2]);
            $p = $this->cekProfile($r[3]);
            $g = $this->cekGroup($r[4]);

            $bo = true;
            if($s[1]==0)
            {
                $bo = false;
            }
            if($m[1]==0)
            {
                $bo = false;
            }
            if($mer[1]==0)
            {
                $bo = false;
            }
            if($p[1]==0)
            {
                $bo = false;
            }

            if($g[1]==0)
            {
                $bo = false;
            }

            if($bo==true)
            {
                $grupId = $g[0];
                $grupN = $g[2];
                
                //print_r($grup);
                //$gName = "";
                //for($j=0; $j<count($grup); $j++)
                //{
                    //$gName.=$grup[$j];
                //}
                $gId = implode(',', $grupId); 
                $gN = implode(',', $grupN);    
                //$string = join(', ', $tags);
                
                array_push($data,array(
                    "user_name"=>$userName,
                    "terminal_id"=>null,
                    "sn"=>$r[0],
                    "model_id"=>$m[0],
                    "modelName"=>$r[1],
                    "profile_id"=>$p[0],
                    "profileName"=>$r[3],
                    "merchant_id"=>$mer[0],
                    "merchantName"=>$r[2],
                    "group_id" => $gId,
                    "groupName" => $gN,
                    "note" => "Group yang dapat diimport sebanyak ". count($grupId) ." dari ". count(explode(",",$r[4])),
                    )
                );
   
            }
        }
        //temporary_terminal
        TemporaryTerminalTerminal::where('user_name',$userName)->delete();
        return TemporaryTerminalTerminal::insert($data);
       
    }

    private function cekSn($sn)
    {
        $param['sn']   = $sn; 
        $response = $this->httpWithHeaders()
        ->get( $this->apiTms()  . 'api/v1/terminal/list', $param)->json();
   
        
        if($response['responseCode'] == "0000")
        {
            //return [null,0];
        }
        else
        {
            return [$sn,1]; 
            //return [$response['rows'][0]['id'],1];
        }
    }

    private function cekProfile($p)
    {
        $param['name']   = $p; 
        $response = $this->httpWithHeaders()
        ->get( $this->apiTms()  . 'api/v1/profile/list', $param)->json();
   
        
        if($response['responseCode'] == "0000")
        {
            
            return [$response['rows'][0]['id'],1];
        }
        else
        {
            return [null,0];
            
        }
    }

    private function cekModel($m)
    {
        $param['modelName']   = $m; 
        $response = $this->httpWithHeaders()
        ->get( $this->apiTms()  . 'api/v1/deviceModel/list', $param)->json();
   
        
        if($response['responseCode'] == "0000")
        {
            
           
            return [$response['rows'][0]['id'],1];
        }
        else
        {
            return [null,0];
        }
    }

    private function cekMerchant($m)
    {
        $param['name']   = $m; 
        $response = $this->httpWithHeaders()
        ->get( $this->apiTms()  . 'api/v1/merchant/list', $param)->json();
   
        
        if($response['responseCode'] == "0000")
        {
           
           
            return [$response['rows'][0]['id'],1];
        }
        else
        {
            return [null,0];
        }
    }

    private function cekGroup($g)
    {
        
        $r = explode(",",$g);
        $ar = array();
        $arG = array();

        if(count($r)>0)
        {
            for($i=0; $i<count($r); $i++)
            {
                $param['name']   = $r[$i]; 
                $response = $this->httpWithHeaders()
                ->get( $this->apiTms()  . 'api/v1/terminalGroup/list', $param)->json();
        
                
                if($response['responseCode'] == "0000")
                {
                    
                    //return [null,0];
                    array_push($ar,$response['rows'][0]['id']);
                    array_push($arG,$r[$i]);
                    
                }
                
            }
        }

        if(count($r)==0 or count($ar)==0  )
        {
            return [null,0];
        }
        else
        {
            return [$ar,1,$arG];
        }

        
    }
    public function addTerminalImport(Request $request){
        if($request->terminals) 
        {
            $userName = $request->userName;
            //$r_ok = array();
            //$r_f = array();
            $data = array();
            foreach(json_decode($request->terminals) as $t)
            {
               
                $gl = explode(",",$t->group_id);
                $rg = array();
                for($i=0; $i<count($gl); $i++)
                {
                    $rg[$i] = $gl[$i];
                }
                array_push($data,array(
                    'sn' => $t->sn,
                    'modelId' => $t->model_id, 
                    'merchantId' => $t->merchant_id,
                    'profileId' => $t->profile_id,
                    'terminalGroupIds' =>  $rg
                ));
                

            }
            // echo '<pre>';
            // var_dump($data);
            // echo '</pre>';
            $response = $this->httpWithHeaders() 
            ->post( $this->apiTms()  . 'api/v1/importTerminalBatch/importBatch', [
                'dataArrayTerminal' =>  json_encode($data),
            ])->json();
           // print_r($response);
            if($response['responseCode']=="0000")
            {
                TemporaryTerminalTerminal::where('user_name',$userName)->delete();
                return response()->json([
                    'responseCode' => "0000",
                    'responseMessage' => 'Data has been Uploaded To DB'
                ]); 
            }
            else
            {
                return response()->json([
                    'responseCode' => "0001",
                    'responseMessage' => 'Gagal Import'. $response['responseCode'],
                    'responseDesc' => $response['responseDesc'],
                ]); 
            }   
            
            /*$response = $this->httpWithHeaders() 
            ->post( $this->apiTms()  . 'api/v1/importTerminalBatch/importBatch', [
                'dataArrayTerminal' =>  json_decode($request->terminals),
            ])->json();

            if($response['responseCode']=="0000")
            {
                TemporaryTerminalTerminal::where('user_name',$userName)->delete();
                return response()->json([
                    'responseCode' => 200,
                    'responseMessage' => 'Data has been Uploaded To DB'
                ]); 
               
            }
            else
            {
                return response()->json([
                    'responseCode' => 500,
                    'responseMessage' => 'Gagal Import'. $response['responseCode']
                ]); 
            }   
            */ 

        }
    }
    public function addTerminalImport_x(Request $request){
        

        if($request->terminals) 
        { 
            //print_r($request->terminals);
            $userName = $request->userName;
            $r_ok = array();
            $r_f = array();
            foreach(json_decode($request->terminals) as $t)
            {
                
                
                $gl = explode(",",$t->group_id);
                $rg = array();
                for($i=0; $i<count($gl); $i++)
                {
                    $rg[$i] = $gl[$i];
                }

                $response = $this->httpWithHeaders()
                ->post( $this->apiTms()  . 'api/v1/terminal/add', [
                    'sn' => $t->sn,
                    'modelId' => $t->model_id,
                    'merchantId' => $t->merchant_id,
                    'profileId' => $t->profile_id,
                    'terminalGroupIds' =>  $rg,
                ])->json();

                if($response['responseCode']=="0000")
                {
                    array_push($r_ok,"ok");
                    TemporaryTerminalTerminal::where('user_name',$userName)->where("sn",$t->sn)->delete();
                }
                else
                {
                    array_push($r_f,"no"); 
                }    

           
            }

            if(count($r_ok)>0)
            {
                
                $str = "Data success diImport sebanyak ".count($r_ok).". Dan Gagal Sebanyak ".count($r_f)." ";
                return response()->json(['responseCode' => '0000', 'responseStatus' => 'OK', 'responseMessage' => $str]);
			
            }
            else
            {
                
                $str = "Tidak ada data yang berhasil diimport ";
                return response()->json(['responseCode' => '0002', 'responseStatus' => 'OK', 'responseMessage' => $str]);
			
               
            }
        }
        else
        {
            return response()->json(['responseCode' => '0001', 'responseStatus' => 'Failed', 'responseMessage' => "Data Kosong"]);
			
        }
        
    }

    public function form2(Request $request)
    {
        
        $param['pageNum']   = 1;
        $param['pageSize']   = 1000;
        
       
        $responseMer = $this->httpWithHeaders()
        ->send('GET',  $this->apiTms() . 'api/v1/merchant/list', [
           'body' => json_encode($param)
        ])->json();

        $responsedm = $this->httpWithHeaders()
        ->send('GET',  $this->apiTms() . 'api/v1/deviceModel/list', [
            'body' => json_encode($param)
        ])->json();

        $responsedp = $this->httpWithHeaders()
        ->send('GET',  $this->apiTms() . 'api/v1/profile/list', [
            'body' => json_encode($param)
        ])->json();

        return view('pages.terminal.form2', [
           
            'merchant'=> ($responseMer['responseCode'] =='0000') ? $responseMer['rows'] : null ,
            'devicemodel'=> ($responsedm['responseCode'] =='0000') ? $responsedm['rows'] : null,
            'deviceprofile'=>($responsedp['responseCode'] =='0000') ? $responsedp['rows'] : null,
            'data'=>null,
            'edit' => 'no'
        ]);

    }

    public function form3(Request $request)
    {
        
        $param['pageNum']   = 1;
        $param['pageSize']   = 1000;
        
       
        $responseMer = $this->httpWithHeaders()
        ->send('GET',  $this->apiTms() . 'api/v1/merchant/list', [
           'body' => json_encode($param)
        ])->json();

        $responsedm = $this->httpWithHeaders()
        ->send('GET',  $this->apiTms() . 'api/v1/deviceModel/list', [
            'body' => json_encode($param)
        ])->json();

        $responsedp = $this->httpWithHeaders()
        ->send('GET',  $this->apiTms() . 'api/v1/profile/list', [
            'body' => json_encode($param)
        ])->json();

        return view('pages.terminal.form3', [
           
            'merchant'=> ($responseMer['responseCode'] =='0000') ? $responseMer['rows'] : null ,
            'devicemodel'=> ($responsedm['responseCode'] =='0000') ? $responsedm['rows'] : null,
            'deviceprofile'=>($responsedp['responseCode'] =='0000') ? $responsedp['rows'] : null,
            'data'=>null,
            'edit' => 'no'
        ]);

    }

    public function form4(Request $request)
    {
        
        $param['pageNum']   = 1;
        $param['pageSize']   = 1000;
        
       
        $responseMer = $this->httpWithHeaders()
        ->send('GET',  $this->apiTms() . 'api/v1/merchant/list', [
           'body' => json_encode($param)
        ])->json();

        $responsedm = $this->httpWithHeaders()
        ->send('GET',  $this->apiTms() . 'api/v1/deviceModel/list', [
            'body' => json_encode($param)
        ])->json();

        $responsedp = $this->httpWithHeaders()
        ->send('GET',  $this->apiTms() . 'api/v1/profile/list', [
            'body' => json_encode($param)
        ])->json();

        return view('pages.terminal.form4', [
            'merchant'=> ($responseMer['responseCode'] =='0000') ? $responseMer['rows'] : null ,
            'devicemodel'=> ($responsedm['responseCode'] =='0000') ? $responsedm['rows'] : null,
            'deviceprofile'=>($responsedp['responseCode'] =='0000') ? $responsedp['rows'] : null,
            'data'=>null,
            'edit' => 'no'
        ]);

    }
        

}

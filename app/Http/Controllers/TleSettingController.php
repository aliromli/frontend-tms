<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use DateTime;
use Privilege;

class TleSettingController extends Controller
{
     /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
    	Privilege::visibleView($request->segments()[0],'READ');
    	return view('pages.tleSetting.index', [
					'data' => null
				]);
	}

    /**
     * city datatables
     *
     * @return type JSON city
     */
    public function list(Request $request)
    {
        // Initial Order
        $orderIndex = (int) $request->order[0]['column'];
        $orderDir = $request->order[0]['dir'];
        $orderColumn = $request->columns[$orderIndex]['data'];
		
        
        $param['pageNum']   = ($request->start / $request->length) + 1;
        $param['pageSize']   = $request->length; 
       
        $param['tleId']   = $request->tleId; 
        
        $response = $this->httpWithHeaders()
        ->get( $this->apiTms()  . 'api/v1/tleSetting/list', $param)->json();
   
		//print_r($response);
		
        if($response['responseCode'] == "0000")
        {
            return response()->json([
                'draw'              => $request->draw,
                'recordsTotal'      => $response['total'],
                'recordsFiltered'   => $response['total'], 
                'data'              => $response['rows'],
                'input'             => [
                    'start' => $request->start,
                    'draw' => $request->draw,
                    'length' =>  $request->length,
                    'order' => $orderIndex,
                    'orderDir' => $orderDir,
                    'orderColumn' => $request->columns[$orderIndex]['data']
                ]
            ]);
        }
        else
        {
            return response()->json([
                'draw'              => $request->draw,
                'recordsTotal'      => 0,
                'recordsFiltered'   => 0, 
                'data'              => [],
                'input'             => [
                    'start' => $request->start,
                    'draw' => $request->draw,
                    'length' =>  $request->length,
                    'order' => $orderIndex,
                    'orderDir' => $orderDir,
                    'orderColumn' => $request->columns[$orderIndex]['data']
                ]
            ]);
        }
       
    }

    public function form(Request $request)
    {
        
       
        return view('pages.tleSetting.form', [
           
            'data'=>null,
            'edit' => 'no'
        ]);

    }

    public function formEdit(Request $request)
    {
        Privilege::visibleEdit($request->segments()[0],'UPDATE'); 	
        $response = $this->httpWithHeaders()
        ->get( $this->apiTms() . 'api/v1/tleSetting/get', [
            'id' => $request->id
        ])->json();
  
        return view('pages.tleSetting.form', [
            'data'=>$response['data'],
            'edit' => 'yes'
        ]);
    }

    public function store(Request $request)
    {
     	$response = $this->httpWithHeaders()
            ->post( $this->apiTms()  . 'api/v1/tleSetting/add', [
			   
                'tleId' => $request->tleId,
                'tleEftSec' => $request->tleEftSec,
                'acquirerId'  => $request->acquirerId,
                'ltmkAid'  => $request->ltmkAid,
                'vendorId'  => $request->vendorId,
                'tleVer'  => $request->tleVer,
                'kmsSecureNii'  => $request->kmsSecureNii,
                'edcSecureNii' => $request->edcSecureNii,
                'capkExponent' => $request->capkExponent,
                'capkLength' => $request->capkLength,
                'capkValue' => $request->capkValue,
                'aidLength' => $request->aidLength,
                'aidValue' => $request->aidValue,
                'encryptedField1' => $request->encryptedField1,
                'encryptedField2' => $request->encryptedField2,
                'encryptedField3' => $request->encryptedField3,
                'encryptedField4' => $request->encryptedField4,
                'encryptedField5' => $request->encryptedField5,
                'encryptedField6' => $request->encryptedField6,
                'encryptedField7' => $request->encryptedField7,
                'encryptedField8' => $request->encryptedField8,
                'encryptedField9' => $request->encryptedField9,
                'encryptedField10' => $request->encryptedField10,
                
            ])->json();
			$this->responseCode($response,'TleSetting has been added successfully');  
    }

    public function  show(Request $request)
    {
        $response = $this->httpWithHeaders()
            ->get( $this->apiTms() . 'api/v1/tleSetting/get', [
                'id' => $request->id
            ])->json();
           
		
        if($response['responseCode'] =='0000'){
         
            return response()->json(['responseCode' => 200, 'responseStatus' => 'OK', 'responseMessage' => $response['data']]);
     
          }
        else{
            return response()->json(['responseCode' => 500, 'responseStatus' => 'Failed', 'responseMessage' => $response['responseDesc']]);
        }
    }

    public function update(Request $request)
    {
       
		
        $response = $this->httpWithHeaders()
         
            ->post( $this->apiTms() . 'api/v1/tleSetting/update', [
                'tleId' => $request->tleId,
                'tleEftSec' => $request->tleEftSec,
                'acquirerId'  => $request->acquirerId,
                'ltmkAid'  => $request->ltmkAid,
                'vendorId'  => $request->vendorId,
                'tleVer'  => $request->tleVer,
                'kmsSecureNii'  => $request->kmsSecureNii,
                'edcSecureNii' => $request->edcSecureNii,
                'capkExponent' => $request->capkExponent,
                'capkLength' => $request->capkLength,
                'capkValue' => $request->capkValue,
                'aidLength' => $request->aidLength,
                'aidValue' => $request->aidValue,
                'encryptedField1' => $request->encryptedField1,
                'encryptedField2' => $request->encryptedField2,
                'encryptedField3' => $request->encryptedField3,
                'encryptedField4' => $request->encryptedField4,
                'encryptedField5' => $request->encryptedField5,
                'encryptedField6' => $request->encryptedField6,
                'encryptedField7' => $request->encryptedField7,
                'encryptedField8' => $request->encryptedField8,
                'encryptedField9' => $request->encryptedField9,
                'encryptedField10' => $request->encryptedField10,
                'id' => $request->id,
                'version' => $request->version
            ])->json();
           
			return $this->responseCode($response,'TleSetting has been update successfully');
		
      
    }

    public function delete(Request $request)
    {
      
        if(Privilege::visibleDelete($request->segments()[0],'DELETE')=='Y' )
        {
            return response()->json(['responseCode' => 401, 'responseStatus' => 'No Authorized', 'responseMessage' => 'No Authorized' ]);
        }
        
        $response = $this->httpWithHeaders()
        
        ->post( $this->apiTms() . 'api/v1/tleSetting/delete', [

            'version' => $request->version,

            'id' => $request->id,


        ])->json();
        
      
        if($response['responseCode'] =='0000'){
            return response()->json(['responseCode' => 200, 'responseStatus' => 'OK', 'responseMessage' => 'TleSetting has been deleted successfully']);
        }
        else if($response['responseCode'] =='0400'){
            return response()->json(['responseCode' => 500, 'responseStatus' => 'No Data', 'responseMessage' => $response['responseDesc'] ]);
        }
        else
        {
            return response()->json(['responseCode' => 501, 'responseStatus' => 'Exception', 'responseMessage' => $response['responseDesc']]);
        }

    }


}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Privilege;


class DistrictsController extends Controller
{
      /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
    	Privilege::visibleView($request->segments()[0],'READ');
       
        $param['pageNum']   = 1;
        $param['pageSize']   = 1000;
        
        $response = $this->httpWithHeaders()
        ->get( $this->apiTms()  . 'api/v1/city/list', $param)->json();
       
		return view('pages.district.index', [
					'city' =>  ($response['responseCode']=='0000'?$response['rows']:null),
				]);
    }

    /**
     * Country datatables
     *
     * @return type JSON country
     */
    public function list(Request $request)
    {
        // Initial Order
        $orderIndex = (int) $request->order[0]['column'];
        $orderDir = $request->order[0]['dir'];
        $orderColumn = $request->columns[$orderIndex]['data'];
		
        
        $param['pageNum']   = ($request->start / $request->length) + 1;
        $param['pageSize']   = $request->length; 
        $param['cityId']   = $request->city; 
        $param['name']   = $request->district; 
        $response = $this->httpWithHeaders()
        ->get( $this->apiTms()  . 'api/v1/district/list', $param)->json();
     
        if($response['responseCode'] === "0000")
        {
            return response()->json([
                'draw'              => $request->draw,
                'recordsTotal'      => $response['total'],
                'recordsFiltered'   => $response['total'], 
                'data'              => $response['rows'],
                'input'             => [
                    'start' => $request->start,
                    'draw' => $request->draw,
                    'length' =>  $request->length,
                    'order' => $orderIndex,
                    'orderDir' => $orderDir,
                    'orderColumn' => $request->columns[$orderIndex]['data']
                ]
            ]);
        }
        else
        {
            return response()->json([
                'draw'              => $request->draw,
                'recordsTotal'      => 0,
                'recordsFiltered'   => 0, 
                'data'              => [],
                'input'             => [
                    'start' => $request->start,
                    'draw' => $request->draw,
                    'length' =>  $request->length,
                    'order' => $orderIndex,
                    'orderDir' => $orderDir,
                    'orderColumn' => $request->columns[$orderIndex]['data']
                ]
            ]);
        }
       
    }

    public function form(Request $request)
    {
        
        $param['pageNum']   = 1;
        $param['pageSize']   = 2000;
        
        $response = $this->httpWithHeaders()
         ->send('GET',  $this->apiTms() . 'api/v1/city/list', [
            'body' => json_encode($param)
         ])->json();

     
        return view('pages.district.form', [
            'city' =>  ($response['responseCode']=='0000'?$response['rows']:null),
            'data'=>null,
            'edit' => 'no'
        ]);

    }

    public function formEdit(Request $request)
    {
        Privilege::visibleEdit($request->segments()[0],'UPDATE'); 		
        
        $param['pageNum']   = 1;
        $param['pageSize']   = 2000;
        $responseD = $this->httpWithHeaders()
         ->send('GET',  $this->apiTms() . 'api/v1/city/list', [
            'body' => json_encode($param)
         ])->json();

        $response = $this->httpWithHeaders()
        ->get( $this->apiTms() . 'api/v1/district/get', [
            'id' => $request->id
        ])->json();

        return view('pages.district.form', [
            'city' =>  ($responseD['responseCode']=='0000'?$responseD['rows']:null),
            'data' => $response['data'],
            'edit' => 'ya'
        ]);

    }

    public function store(Request $request)
    {
        
        $response = $this->httpWithHeaders()
            ->post( $this->apiTms()  . 'api/v1/district/add', [
                'city_id' => $request->city,
                'name' => $request->name
            ])->json();
           
        return $this->responseCode($response,'District has been added successfull');  
       
    }

    public function  show(Request $request)
    {
      

        $response = $this->httpWithHeaders()
            ->get( $this->apiTms() . 'api/v1/district/get', [
                'id' => $request->id
            ])->json();
           
      
        if($response['responseCode'] =='0000'){
         
            return response()->json(['responseCode' => 200, 'responseStatus' => 'OK', 'responseMessage' => $response['data']]);
     
          }
        else{
            return response()->json(['responseCode' => 500, 'responseStatus' => 'Failed', 'responseMessage' => $response['responseDesc']]);
        }
    }

    public function update(Request $request)
    {
        
       

        $response = $this->httpWithHeaders()
         
            ->post( $this->apiTms() . 'api/v1/district/update', [

                'city_id' => $request->city,
                'name' => $request->name,
                'version' => $request->version,
                'id' => $request->id,


            ])->json();
        
        return $this->responseCode($response,'District has been updated successfull');  
      
    }

    public function delete(Request $request)
    {
        
        if(Privilege::visibleDelete($request->segments()[0],'DELETE')=='Y' )
        {
            return response()->json(['responseCode' => 401, 'responseStatus' => 'No Authorized', 'responseMessage' => 'No Authorized' ]);
        }
        
        $response = $this->httpWithHeaders()
        
        ->post( $this->apiTms() . 'api/v1/district/delete', [
            'version' => $request->version,
            'id' => $request->id,

        ])->json();
           
      
        if($response['responseCode'] =='0000'){
            return response()->json(['responseCode' => 200, 'responseStatus' => 'OK', 'responseMessage' => 'District has been deleted successfully']);
        }
        else if($response['responseCode'] =='0400'){
            return response()->json(['responseCode' => 500, 'responseStatus' => 'No Data', 'responseMessage' => $response['responseDesc'] ]);
        }
        else
        {
            return response()->json(['responseCode' => 501, 'responseStatus' => 'Exception', 'responseMessage' => $response['responseDesc']]);
        }

    }

}

<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\UserGroup;
use App\Models\Setting;
use App\Models\VerificationCode;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Privilege;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Mail;
use App\Mail\MailSendingOtp;
use Carbon\Carbon;
/**
 * Description of UserController
 *
 * @author In House Dev Program
 */
class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    // public function indexx(){
		
	// 	$tenant = DB::connection('pgsql2')->table('tms_tenant')->get(); //->select('id','name')->get();
	// 	$user = DB::connection('pgsql')->table('users')
	// 	->leftjoin()
	// 	->get(); //->select('id','name')->get();
		
	// 	dd($user);
		
	// }
    public function index(Request $request)
    {
    	//Privilege::visibleView($request->segments()[0],'READ');
        
        
        //$this->authorize('READ', null);
        $param['pageNum']   = 1;
        $param['pageSize']   = 300;

        

        $tenantId = '';//'738835cc-9688-44f7-b033-1b19a12ae895';
        $signature = '';
        $refNumber = '';//'{{ref}}{{$isoTimestamp}}';
        $reqTime = '';//'{{$isoTimestamp}}';
        $x_cunsomer_username =  '';//'sss';
        

        
        $response = $this->httpWithHeaders2($tenantId,$signature,$refNumber,$reqTime,$x_cunsomer_username)
         ->send('GET',  $this->apiTms() . 'api/v1/tenant/list', [
            'body' => json_encode($param)
         ])->json();
        $tenant =  ($response['responseCode']=='0000')?$response['rows'] : null;
        $process = null;
        $userGroup = UserGroup::where('active', 'Y')->get();
        return view('pages.user.index', ['process' => $process, 'userGroup' => $userGroup, 'tenant' => $tenant]);
    }

    public function form(){
        
        
        
        $wh = "";
        $wh2 = "";
        if(session()->get('user.userGroup.name')=="Administrator") //super Admin
        {
            $wh = "Admin Tenant";
            $wh2 = true;

            //$userGroup = UserGroup::where('active', 'Y')->where('name',$wh)->get();
            $userGroup = UserGroup::where('active', 'Y')->whereIn('name', array('Admin Tenant', 'User Tenant'))->get();
            
            $param['pageNum']   = 1;
            $param['pageSize']   = 300;
            //$param['is_super'] =  $wh2;
            //$param['super_tenant_id'] =  session()->get('user.tenant_id');

            $tenantId = '';//'738835cc-9688-44f7-b033-1b19a12ae895';
            $signature = '';
            $refNumber = '';//'{{ref}}{{$isoTimestamp}}';
            $reqTime = '';//'{{$isoTimestamp}}';
            $x_cunsomer_username ='';//  'sss';
            
            $response = $this->httpWithHeaders2($tenantId,$signature,$refNumber,$reqTime,$x_cunsomer_username)
            ->get( $this->apiTms()  . 'api/v1/tenant/list', $param)->json(); 

            $tenant =  ($response['responseCode']=='0000')?$response['rows'] : null;

            return view('pages.user.form', [
            
                'userGroup'=>$userGroup,
                'data' => null,
                'tenant' => $tenant,
                'edit' => 'no'
            ]);
        }
        else if(session()->get('user.userGroup.name')=="Admin Tenant")
        {
            $wh = "User Tenant";
            $wh2 = false;

            $userGroup = UserGroup::where('active', 'Y')->where('name',$wh)->get();
            $param['pageNum']   = 1;
            $param['pageSize']   = 300;
            //$param['is_super'] =  $wh2;
            //$param['super_tenant_id'] =  session()->get('user.tenant_id');
            $param['id'] =  session()->get('user.tenant_id');

            $tenantId = '';//'738835cc-9688-44f7-b033-1b19a12ae895';
            $signature = '';
            $refNumber = '';//'{{ref}}{{$isoTimestamp}}';
            $reqTime = '';//'{{$isoTimestamp}}';
            $x_cunsomer_username =  '';//'sss';
            
            $response = $this->httpWithHeaders2($tenantId,$signature,$refNumber,$reqTime,$x_cunsomer_username)
            ->get( $this->apiTms()  . 'api/v1/tenant/list', $param)->json(); 
           
            $tenant =   ($response['responseCode']==='0000') ? $response['rows'] : null;

            return view('pages.user.form2', [
            
                'userGroup'=>$userGroup,
                'data' => null,
                'tenant' => $tenant,
                'edit' => 'no'
            ]);

        }
        
        
    }

    public function formEdit($id){
		
		$wh = "";
        $wh2 = "";
		$st = null;
        if(session()->get('user.userGroup.name')=="Administrator") //super Admin
        {
            $wh = "Admin Tenant";
            $wh2 = true;
			$st = null;

            //$userGroup = UserGroup::where('active', 'Y')->where('name',$wh)->get();
            $userGroup = UserGroup::where('active', 'Y')->whereIn('name', array('Admin Tenant', 'User Tenant'))->get();
            $param['pageNum']   = 1;
            $param['pageSize']   = 300;
            //$param['is_super'] =  $wh2;
            //$param['super_tenant_id'] = $st; 
            //$param['id'] =  session()->get('user.tenant_id');

            $tenantId = '';//'738835cc-9688-44f7-b033-1b19a12ae895';
            $signature = '';
            $refNumber = '';//'{{ref}}{{$isoTimestamp}}';
            $reqTime = '';//'{{$isoTimestamp}}';
            $x_cunsomer_username = '';// 'sss';
            
            $response = $this->httpWithHeaders2($tenantId,$signature,$refNumber,$reqTime,$x_cunsomer_username)
            ->get( $this->apiTms()  . 'api/v1/tenant/list', $param)->json(); 

            $tenant =  ($response['responseCode']=='0000')?$response['rows'] : null;
            
            $u = User::where('id',$id)->get();
        

            return view('pages.user.form', [
            
                'userGroup'=>$userGroup,
                'data' => $u,
                'tenant' => $tenant,
                'edit' => 'yes'
            ]);
        }
        else if(session()->get('user.userGroup.name')=="Admin Tenant")
        {
            $wh = "User Tenant";
            $wh2 = false;
            $st = session()->get('user.tenant_id');

            $userGroup = UserGroup::where('active', 'Y')->where('name',$wh)->get();
            $param['pageNum']   = 1;
            $param['pageSize']   = 300;
            //$param['is_super'] =  $wh2;
            //$param['super_tenant_id'] = $st; 
            $param['id'] =  session()->get('user.tenant_id');

            $tenantId = '';//'738835cc-9688-44f7-b033-1b19a12ae895';
            $signature = '';
            $refNumber = '';//'{{ref}}{{$isoTimestamp}}';
            $reqTime = '';//'{{$isoTimestamp}}';
            $x_cunsomer_username = '';// 'sss';
            
            $response = $this->httpWithHeaders2($tenantId,$signature,$refNumber,$reqTime,$x_cunsomer_username)
            ->get( $this->apiTms()  . 'api/v1/tenant/list', $param)->json(); 

            $tenant =  ($response['responseCode']=='0000')?$response['rows'] : null;
            
            $u = User::where('id',$id)->get();
        

            return view('pages.user.form2', [
            
                'userGroup'=>$userGroup,
                'data' => $u,
                'tenant' => $tenant,
                'edit' => 'yes'
            ]);
        }
        
        
    }

    /**
     * Show user profile
     *
     * @return type
     */
    public function profile()
    {
        return view('pages.user.profile');
    }

    /**
     * Change password form
     *
     * @return type
     */
    public function changePassword()
    {
        return view('pages.user.change-password');
    }

    /**
     * Setting form
     *
     * @return void
     */
    public function setting()
    {
        return view('pages.user.setting');
    }

    /**
     * Submit change password
     *
     * @param Request $request
     * @return type
     */
    public function storeChangePassword(Request $request)
    {
        
        if($request->password != $request->repassword) {
            return back()->with('error', 'Password doesn\'t match');
        }
        
        $validator = Validator::make($request->all(), [
            'password' => 'required|string|min:8|regex:/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{6,}$/',
        ]);
 
        if ($validator->fails()) {
           
            return back()->with('error2', $validator->errors());
        }

        $user = User::where('id', Auth::user()->id)->first();
        $user->password = Hash::make($request->password);
        $user->changed_password = "Y";
        if(!$user->save()) {
            return back()->with('error', 'Failed to update password. Please try again');
        }

        return back()->with('success', 'Password has been changed, Please try Signout');
    }

    /**
     * Setting
     *
     * @param Request $request
     * @return void
     */
    public function storeSetting(Request $request)
    {
        $setting = Setting::updateOrCreate(
            ['user_id' => Auth::user()->id],
            ['language' => $request->language]
        );

        if(!$setting->save()) {
            return back()->with('error', 'Failed to update setting. Please try again');
        }

        Session::put('app-locale', Auth::user()->setting->language);

        return back()->with('success', 'Setting has been update');
    }

   
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $validator = Validator::make($request->all(), [
            'email' =>'required|email'

        ]);
 
        if ($validator->fails()) {
            $a  =[   
                    "responseCode"=>501,
                    "responseMessage"=>"Format Email Salah",
                    'responseStatus' => 'Failed',                 
                ];    
                return response()->json($a);
        }
        
        
		/*if(session()->get('user.userGroup.name')=="Administrator") //super Admin
        {
            //$wh = "Admin Tenant";
            //$wh2 = true;
        }
        else if(session()->get('user.userGroup.name')=="Admin Tenant")
        {
           $u->parent_id = session()->get('user.id');
        }*/

        
        DB::beginTransaction();
        try {

            $u = new User();
            $u->email = $request->email;
            $u->name = $request->name;
            $u->password = Hash::make($request->password);
            $u->user_group_id = $request->group;
            $u->tenant_id = $request->tenant;
            $u->active = $request->active;
            $u->force_change_password = $request->force;

            if(!$u->save()) {
                return response()->json(['responseCode' => 500, 'responseStatus' => 'Failed', 'responseMessage' => 'Can\'t add user. Please try again']);
            }
            else
            {
                
                
                /* //sementara fitur ini dimatiin
				$email_to = $request->email;
                $otp = $this->generateOtp($u->id)->otp;
                $name = $request->name;
                $password = $request->password;
                
                Mail::to($request->email)->send(new MailSendingOtp($email_to, $otp, $name, $password));
                sleep(4);*/

                DB::commit();
                return response()->json(['responseCode' => 200, 'responseStatus' => 'OK', 'responseMessage' => 'User has been added successfully']);
            }

          
        } catch (\Exception $e) {

            return response()->json(['responseCode' => 500, 'responseStatus' => 'Failed', 'responseMessage' => $e->getMessage()]);
        }
		
        
    }
	
	public function generateOtp($id)
	{
		
		return VerificationCode::create([
            'user_id' => $id,
            'otp' => rand(123456, 999999),
            'expire_at' => Carbon::now()->addMinutes(10)
        ]);
	}
	

	public function store2(Request $request)
    {
        $u = new User();
        $u->email = $request->email;
        $u->name = $request->name;
        $u->password = Hash::make($request->password);
        $u->user_group_id = 5; //user tenant
        $u->tenant_id = $request->tenant;
		$u->parent_id = session()->get('user.id'); 
		
        $u->active = $request->active;

        if(!$u->save()) {
            return response()->json(['responseCode' => 500, 'responseStatus' => 'Failed', 'responseMessage' => 'Can\'t add user. Please try again']);
        }

        return response()->json(['responseCode' => 200, 'responseStatus' => 'OK', 'responseMessage' => 'User has been added successfully']);
    }
    /**
     * Display the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user, $id)
    {
        $u = $user->find($id);
        return response()->json($u);
    }

   

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        $u = $user->find($request->id);
        $u->email = $request->email;
        $u->name = $request->name;
        $u->user_group_id = $request->group;
        if(session()->get('user.userGroup.name')=="Administrator") //super Admin
        {
            //$wh = "Admin Tenant";
            $u->tenant_id = $request->tenant;
        }
       
		
        $u->active = $request->active;

        if(!empty($request->password)) {
            $u->password = Hash::make($request->password);
        }

        if(!$u->save()) {
            return response()->json(['responseCode' => 500, 'responseStatus' => 'Failed', 'responseMessage' => 'Can\'t update user. Please try again']);
        }

        return response()->json(['responseCode' => 200, 'responseStatus' => 'OK', 'responseMessage' => 'User has been updated successfully']);
    }
	
	public function update2(Request $request, User $user)
    {
        $u = $user->find($request->id);
        $u->email = $request->email;
        $u->name = $request->name;
        //$u->user_group_id = $request->group;
         
		//if(session()->get('user.userGroup.name')=="Admin Tenant")
        //{
           $u->parent_id = session()->get('user.id');
		   $u->tenant_id = $request->tenant;
        //}
		
        $u->active = $request->active;

        if(!empty($request->password)) {
            $u->password = Hash::make($request->password);
        }

        if(!$u->save()) {
            return response()->json(['responseCode' => 500, 'responseStatus' => 'Failed', 'responseMessage' => 'Can\'t update user. Please try again']);
        }

        return response()->json(['responseCode' => 200, 'responseStatus' => 'OK', 'responseMessage' => 'User has been updated successfully']);
    }

    /**
     * Update profile
     *
     * @param Request $request
     * @return type
     */
    public function updateProfile(Request $request)
    {
        // Check is email exists
        $u = User::where('email', $request->email);
        if($u->count() > 0 && ($u->first()->id != Auth::user()->id)) {
            return back()->with('error', 'Email used by another user');
        }

        $up = User::find(Auth::user()->id);
        $up->email = $request->email;
        $up->name = $request->name;

        if ($request->hasFile('file')) {
            $file = $request->file('file');
            if($file->isValid()) {
                $nameStripped = str_replace(" ", "-", $file->getClientOriginalName());
                $file->storeAs('user', $nameStripped);
                $up->avatar = $nameStripped;

                // Set session avatar
                session()->put('user.avatar', $nameStripped);
            }
        }

        if(!$up->save()) {
            return back()->with('error', 'Failed to update profile');
        }

        // Set updated session
        session()->put('user.name', $request->name);
        session()->put('user.email', $request->email);

        return back()->with('success', 'Profile has been updated');

    }


    /**
     * Activate user
     *
     * @param User $user
     * @param type $id
     * @return type
     */
    public function activateUser(User $user, $id)
    {
        $u = $user->find($id);
        $u->active = 'Y';

        if(!$u->save()) {
            return response()->json(['responseCode' => 500, 'responseStatus' => 'Failed', 'responseMessage' => 'Can\'t activate user. Please try again']);
        }

        return response()->json(['responseCode' => 200, 'responseStatus' => 'OK', 'responseMessage' => 'User has been activated successfully']);
    }

    /**
     * Inactivate user
     *
     * @param User $user
     * @param type $id
     * @return type
     */
    public function inactivateUser(User $user, $id)
    {
        $u = $user->find($id);
        $u->active = 'N';

        if(!$u->save()) {
            return response()->json(['responseCode' => 500, 'responseStatus' => 'Failed', 'responseMessage' => 'Can\'t inactivate user. Please try again']);
        }

        return response()->json(['responseCode' => 200, 'responseStatus' => 'OK', 'responseMessage' => 'User has been inactivated successfully']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function deleteUser(Request $request)
    {

        $v = VerificationCode::where('user_id',$request->id)->delete();
        
        $u = User::where('id',$request->id)->delete();
       
        if($u)
        {
            return response()->json(['responseCode' => 200, 'responseStatus' => 'OK', 'responseMessage' => 'User has been deleted successfully']);
   
        }
        else
        {
            return response()->json(['responseCode' => 500, 'responseStatus' => 'Failed', 'responseMessage' => 'Can\'t delete user. Please try again']);
        }


    }

    /**
     * User datatables
     *
     * @return type JSON user
     */
    public function datatablesx(Request $request){
		
		
		$tenant = DB::connection('pgsql2')->table('tms_tenant'); //->select('id','name')->get();
		
		$users = User::with(['userGroup'])
				->leftjoin('pgsql2.tmsapi.tms_tenant  as secondDbType', 'pgsql.users.tenant_id', '=', 'secondDbType.id');
				//->select('mysql_users.*', 'other_table.column');
		
		if(session()->get('user.userGroup.name')=="Administrator") //super Admin
        {
            $users->whereNull('parent_id');
        }
        else if(session()->get('user.userGroup.name')=="Admin Tenant")
        {
			$users->where('parent_id',session()->get('user.id'));
		}
		
		$recordTotal = $users->count();
       
        if(!empty($request->name))
        {
           
            $users->where('name', 'ILIKE', '%'.$request->name.'%');
        }
        if(!empty($request->group))
        {
            $users->where('user_group_id',  $request->group);
        }
       
        $recordFiltered = $users->count();

		
		$orderIndex = (int) $request->order[0]['column'];
        $orderDir = $request->order[0]['dir'];
        $orderColum = $request->columns[$orderIndex]['data'];
		
		$users->orderBy($orderColum, $orderDir);
        $users->skip($request->start)->take($request->length);
        
       
        $ar_user= array();
        foreach($users->get() as $data)
        {
            
            $tenantId = '';//'738835cc-9688-44f7-b033-1b19a12ae895';
            $signature = '';
            $refNumber = '';//'{{ref}}{{$isoTimestamp}}';
            $reqTime = '';//'{{$isoTimestamp}}';
            $x_cunsomer_username = '';// 'sss';
        
            $response = $this->httpWithHeaders2($tenantId,$signature,$refNumber,$reqTime,$x_cunsomer_username)
            
                ->get( $this->apiTms() . 'api/v1/tenant/get', [
                    'id' => $data->tenant_id
                ])->json();
                
                array_push($ar_user,array("id"=>$data->id,
                                        "email"=>$data->email,
                                        "name"=>$data->name,
                                        "userGroup"=>$data->userGroup->name,
                                        "tenant"=> ($response['responseCode'] =='0000') ?   $response['data'][0]['name'] : null,
                                        "active" => $data->active
                                        ));
        }  
		
		


        return response()->json([
            'draw'              => $request->draw,
            'recordsTotal'      => $recordTotal, 
            'recordsFiltered'   => $recordFiltered,
            'data'              => $ar_user,
            'input'             => [
                'start' => $request->start,
                'draw' => $request->draw,
                'length' =>  $request->length,
                'order' => $orderIndex,
                'orderDir' => $orderDir,
                'orderColumn' => $request->columns[$orderIndex]['data']
            ]
        ]);
		
	}
    public function datatables(Request $request)
    {
      
        $users = User::with(['userGroup']);
		
		if(session()->get('user.userGroup.name')=="Administrator") //super Admin
        {
            $users->whereNull('parent_id');
        }
        else if(session()->get('user.userGroup.name')=="Admin Tenant")
        {
			   //$users->where('name',session()->get('user.name'));
			   //$users->where('name',session()->get('user.userGroup.id'));
			  // $users->where('parent_id',session()->get('user.id'));
	   
			//$users->where(function(\Illuminate\Database\Eloquent\Builder $query) {
			//        $query->where('parent_id', session()->get('user.id'))->orWhere('name',session()->get('user.name'));
			//});
			$users->where('parent_id',session()->get('user.id'));
		
			
        }

        $recordTotal = $users->count();
       
        if(!empty($request->name))
        {
           
            $users->where('name', 'ILIKE', '%'.$request->name.'%');
        }
        if(!empty($request->group))
        {
            $users->where('user_group_id',  $request->group);
        }
       
        $recordFiltered = $users->count();
		
		$orderIndex = (int) $request->order[0]['column'];
        $orderDir = $request->order[0]['dir'];
        $orderColum = $request->columns[$orderIndex]['data'];
		
		
		$users->orderBy($orderColum, $orderDir);
        $users->skip($request->start)->take($request->length);
        
       
        $ar_user= array();
        foreach($users->get() as $data)
        {
            
                $tenantId = '';//'738835cc-9688-44f7-b033-1b19a12ae895';
                $signature = '';
                $refNumber = '';//'{{ref}}{{$isoTimestamp}}';
                $reqTime = '';//'{{$isoTimestamp}}';
                $x_cunsomer_username =  '';//'sss';
            

				$response = $this->httpWithHeaders2($tenantId,$signature,$refNumber,$reqTime,$x_cunsomer_username)
            
                 ->get( $this->apiTms() . 'api/v1/tenant/get', [
                     'id' => $data->tenant_id
                 ])->json();
              	array_push($ar_user,array("id"=>$data->id,
                                        "email"=>$data->email,
                                        "email_verified_at"=>($data->email_verified_at) ? "Y" : "N",
                                        "name"=>$data->name,
                                        "userGroup"=>$data->userGroup->name,
                                        "tenant"=> ($response['responseCode'] =='0000') ?   $response['data'][0]['name'] : null,
                                        "active" => $data->active,
                                        "force_change_password" => $data->force_change_password
                                        ));
        } 
		
		


        return response()->json([
            'draw'              => $request->draw,
            'recordsTotal'      => $recordTotal, 
            'recordsFiltered'   => $recordFiltered,
            'data'              => $ar_user,
            'input'             => [
                'start' => $request->start,
                'draw' => $request->draw,
                'length' =>  $request->length,
                'order' => $orderIndex,
                'orderDir' => $orderDir,
                'orderColumn' => $request->columns[$orderIndex]['data']
            ]
        ]);
    }

    
}

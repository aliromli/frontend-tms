<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Privilege;

class CitiesController extends Controller
{
     /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
    	Privilege::visibleView($request->segments()[0],'READ');
       
        $param['pageNum']   = 1;
        $param['pageSize']   = 1000;
        
      
        
        $response = $this->httpWithHeaders()
         ->send('GET',  $this->apiTms() . 'api/v1/state/list', [
            'body' => json_encode($param)
         ])->json();

       
		return view('pages.city.index', [
					'state' => ($response['responseCode']=='0000'?$response['rows']:null),
				]);
		
       
    }

    /**
     * city datatables
     *
     * @return type JSON city
     */
    public function list(Request $request)
    {
        // Initial Order
        $orderIndex = (int) $request->order[0]['column'];
        $orderDir = $request->order[0]['dir'];
        $orderColumn = $request->columns[$orderIndex]['data'];
		
        
        $param['pageNum']   = ($request->start / $request->length) + 1;
        $param['pageSize']   = $request->length; 
        $param['stateId']   = $request->state; 
        $param['name']   = $request->city; 
       

        $response = $this->httpWithHeaders()
        ->get( $this->apiTms()  . 'api/v1/city/list', $param)->json();
   

        if($response['responseCode'] == "0000")
        {
            return response()->json([
                'draw'              => $request->draw,
                'recordsTotal'      => $response['total'],
                'recordsFiltered'   => $response['total'], 
                'data'              => $response['rows'],
                'input'             => [
                    'start' => $request->start,
                    'draw' => $request->draw,
                    'length' =>  $request->length,
                    'order' => $orderIndex,
                    'orderDir' => $orderDir,
                    'orderColumn' => $request->columns[$orderIndex]['data']
                ]
            ]);
        }
        else
        {
            return response()->json([
                'draw'              => $request->draw,
                'recordsTotal'      => 0,
                'recordsFiltered'   => 0, 
                'data'              => [],
                'input'             => [
                    'start' => $request->start,
                    'draw' => $request->draw,
                    'length' =>  $request->length,
                    'order' => $orderIndex,
                    'orderDir' => $orderDir,
                    'orderColumn' => $request->columns[$orderIndex]['data']
                ]
            ]);
        }
       
    }

    public function form(Request $request)
    {
        
        $param['pageNum']   = 1;
        $param['pageSize']   = 1000;
        
        $response = $this->httpWithHeaders()
        ->get( $this->apiTms()  . 'api/v1/state/list', $param)->json();

     
        return view('pages.city.form', [
            'state' => ($response['responseCode']=='0000'?$response['rows']:null),
            'data'=>null,
            'edit' => 'no'
        ]);

    }

    public function formEdit(Request $request)
    {
        Privilege::visibleEdit($request->segments()[0],'UPDATE'); 		
        
        $response = $this->httpWithHeaders()
        ->get( $this->apiTms() . 'api/v1/city/get', [
            'id' => $request->id
        ])->json();


        $param['pageNum']   = 1;
        $param['pageSize']   = 1000;
        
        $response2 = $this->httpWithHeaders()
        ->get( $this->apiTms()  . 'api/v1/state/list', $param)->json();


        return view('pages.city.form', [
            'data' => $response['data'],
            'state' => ($response2['responseCode']=='0000'?$response2['rows']:null),
            'edit' => 'ya'
        ]);

    }

    public function store(Request $request)
    {
       
        $response = $this->httpWithHeaders()
            ->post( $this->apiTms()  . 'api/v1/city/add', [
                'states_id' => $request->state,
                'name' => $request->name
            ])->json();
           
      
            return $this->responseCode($response,'City has been added successfull');  
    }

    public function  show(Request $request)
    {
       
        $response = $this->httpWithHeaders()
            ->get( $this->apiTms() . 'api/v1/city/get', [
                'id' => $request->id
            ])->json();
           
      
        if($response['responseCode'] =='0000'){
         
            return response()->json(['responseCode' => 200, 'responseStatus' => 'OK', 'responseMessage' => $response['data']]);
     
          }
        else{
            return response()->json(['responseCode' => 500, 'responseStatus' => 'Failed', 'responseMessage' => $response['responseDesc']]);
        }
    }

    public function update(Request $request)
    {
        
        $response = $this->httpWithHeaders()
         
            ->post( $this->apiTms() . 'api/v1/city/update', [

                'states_id' => $request->state,
                'name' => $request->name,
                'version' => $request->version,
                'id' => $request->id,


            ])->json();
           
            return $this->responseCode($response,'City has been updated successfull');  
    }

    public function delete(Request $request)
    {
        
        if(Privilege::visibleDelete($request->segments()[0],'DELETE')=='Y' )
        {
            return response()->json(['responseCode' => 401, 'responseStatus' => 'No Authorized', 'responseMessage' => 'No Authorized' ]);
        }
        
        $response = $this->httpWithHeaders()
        
        ->post( $this->apiTms() . 'api/v1/city/delete', [

            'version' => $request->version,

            'id' => $request->id,


        ])->json();
           
      
        if($response['responseCode'] =='0000'){
            return response()->json(['responseCode' => 200, 'responseStatus' => 'OK', 'responseMessage' => 'City has been deleted successfully']);
        }
        else if($response['responseCode'] =='0400'){
            return response()->json(['responseCode' => 500, 'responseStatus' => 'No Data', 'responseMessage' => $response['responseDesc'] ]);
        }
        else
        {
            return response()->json(['responseCode' => 501, 'responseStatus' => 'Exception', 'responseMessage' => $response['responseDesc']]);
        }

    }


}

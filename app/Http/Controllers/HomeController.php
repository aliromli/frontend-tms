<?php

namespace App\Http\Controllers;


use App\Models\Menu;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
	
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */

    
    public function index(Request $request)
    {
        //Privilege::visibleView($request->segments()[0],'READ');
         $response = $this->httpWithHeaders()
        ->get( $this->apiTms() . 'api/v1/dashboard/terminal', [
            'id' => null
        ])->json();
      
        if($response['responseCode'] =='0000'){
            $online = $response['data']['ONLINE'];
            $offline = $response['data']['OFFLINE'];
            $disconected = $response['data']['DISCONNECTED'];
            $totalTerminal = $online + $offline + $disconected;
            //$totalTerminal =  str_replace(",00","",number_format($totalTerminal,0,",","."));
        }
        else
        {
            $online = null;
            $offline =null;
            $disconected = null;
            $totalTerminal =null;
        }
        
        return view('home', [
					'totalTerminal' => $totalTerminal,
                    'online' => $online,
                    'offline' => $offline,
                    'disconnected' => $disconected,
					'downloadTask' => 'INSTALL FMS',
					'success' => null,
                    'failed' => null,
                    'downloading' => null,
                    'installing' => null,
				]);
		
		
       
    }

    /**
     * After reset password, setup session and redirect to home
     *
     * @return void
     */
    public function setup()
    {
        // Check if user has setting language
        $appLocale = Auth::user()->setting != null ? Auth::user()->setting->language : 'en';

        // Build menus
        $userGroupActions = collect(Auth::user()->userGroup->menuActions)->pluck('id')->toArray();
        $menus = Menu::where('parent_id', null)
                ->whereHas('actions', function($q) use ($userGroupActions) { $q->whereIn('id', $userGroupActions); })
                ->orderBy('sequence')
                ->get();

        // Set session and redirect to home
        session(['user' => Auth::user(), 'menus' => $menus, 'app-locale' => $appLocale]);

        return redirect('home');
    }

	
	
	


}

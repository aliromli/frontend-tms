<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Privilege;


class StatesController extends Controller
{
     /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
    	Privilege::visibleView($request->segments()[0],'READ');
        

        $param['pageNum']   = 1;
        $param['pageSize']   = 200;
     
        $response = $this->httpWithHeaders()
         ->send('GET',  $this->apiTms() . 'api/v1/country/list', [
            'body' => json_encode($param)
         ])->json();

        return view('pages.state.index', [
					'data' => null,
                    'country' => ($response['responseCode']=='0000'?$response['rows']:null),
				]);
		
		
    }

    /**
     * Country datatables
     *
     * @return type JSON country
     */
    public function list(Request $request)
    {
        
        $orderIndex = (int) $request->order[0]['column'];
        $orderDir = $request->order[0]['dir'];
        $orderColumn = $request->columns[$orderIndex]['data'];
		
        
        $param['pageNum']   = ($request->start / $request->length) + 1;
        $param['pageSize']   = $request->length; 
        $param['countryId']   = $request->country; 
        $param['name']   = $request->state; 
        


        $response = $this->httpWithHeaders()
        ->get( $this->apiTms()  . 'api/v1/state/list', $param)->json();
     
        
        if($response['responseCode'] === "0000")
        {
            return response()->json([
                'draw'              => $request->draw,
                'recordsTotal'      => $response['total'],
                'recordsFiltered'   => $response['total'], 
                'data'              => $response['rows'],
                'input'             => [
                    'start' => $request->start,
                    'draw' => $request->draw,
                    'length' =>  $request->length,
                    'order' => $orderIndex,
                    'orderDir' => $orderDir,
                    'orderColumn' => $request->columns[$orderIndex]['data']
                ]
            ]);
        }
        else
        {
            return response()->json([
                'draw'              => $request->draw,
                'recordsTotal'      => 0,
                'recordsFiltered'   => 0, 
                'data'              => [],
                'input'             => [
                    'start' => $request->start,
                    'draw' => $request->draw,
                    'length' =>  $request->length,
                    'order' => $orderIndex,
                    'orderDir' => $orderDir,
                    'orderColumn' => $request->columns[$orderIndex]['data']
                ]
            ]);
        }
       
    }

    public function form(Request $request)
    {
        
        $param['pageNum']   = 1;
        $param['pageSize']   = 200;
     
        $response = $this->httpWithHeaders()
         ->send('GET',  $this->apiTms() . 'api/v1/country/list', [
            'body' => json_encode($param)
         ])->json();
        
        return view('pages.state.form', [
            'country' =>($response['responseCode']=='0000'?$response['rows']:null),
            'data'=>null,
            'edit' => 'no'
        ]);

    }

    public function formEdit(Request $request)
    {
        $param['pageNum']   = 1;
        $param['pageSize']   = 200;
     
        $responseC = $this->httpWithHeaders()
         ->send('GET',  $this->apiTms() . 'api/v1/country/list', [
            'body' => json_encode($param)
         ])->json();

        $response = $this->httpWithHeaders()
        ->get( $this->apiTms() . 'api/v1/state/get', [
            'id' => $request->id
        ])->json();

        return view('pages.state.form', [
            'country' => ($responseC['responseCode']=='0000'?$responseC['rows']:null),
            'data' => $response['data'],
            'edit' => 'ya'
        ]);

    }


    public function store(Request $request)
    {
        
        $response = $this->httpWithHeaders()
            ->post( $this->apiTms()  . 'api/v1/state/add', [
                'country_id' => $request->country,
                'name' => $request->name
            ])->json();
            
        return $this->responseCode($response,'State has been added successfull');  
           
      
    }

  

    public function  show(Request $request)
    {
        $tenantId = 'qualita';
        $signature = 'tes';
        $refNumber = '{{ref}}{{$isoTimestamp}}'; 
        $reqTime = '{{$timestamp}}';
        $x_cunsomer_username =  'tes';

        $response = $this->httpWithHeaders2($tenantId,$signature,$refNumber,$reqTime,$x_cunsomer_username)
            ->get( $this->apiTms() . 'api/v1/state/get', [
                'id' => $request->id
            ])->json();
           
      
        if($response['responseCode'] =='0000'){
         
            return response()->json(['responseCode' => 200, 'responseStatus' => 'OK', 'responseMessage' => $response['data']]);
     
          }
        else{
            return response()->json(['responseCode' => 500, 'responseStatus' => 'Failed', 'responseMessage' => $response['responseDesc']]);
        }
    }

    public function update(Request $request)
    {
        $response = $this->httpWithHeaders()
         
            ->post( $this->apiTms() . 'api/v1/state/update', [

                'country_id' => $request->country,
                'name' => $request->name,
                'version' => $request->version,
                'id' => $request->id,


            ])->json();
      
		return $this->responseCode($response,'State has been updated successfull');  
    }

    public function delete(Request $request)
    {
      
            $tenantId = 'qualita';
            $signature = 'tes';
            $refNumber = '{{ref}}{{$isoTimestamp}}'; 
            $reqTime = '{{$timestamp}}';
            $x_cunsomer_username =  'tes';

            $response = $this->httpWithHeaders2($tenantId,$signature,$refNumber,$reqTime,$x_cunsomer_username)
            
            ->post( $this->apiTms() . 'api/v1/state/delete', [

                'version' => $request->version,

                'id' => $request->id,


            ])->json();
           
      
        if($response['responseCode'] =='0000'){
            return response()->json(['responseCode' => 200, 'responseStatus' => 'OK', 'responseMessage' => 'State has been deleted successfully']);
        }
        else if($response['responseCode'] =='0400'){
            return response()->json(['responseCode' => 500, 'responseStatus' => 'No Data', 'responseMessage' => $response['responseDesc'] ]);
        }
        else
        {
            return response()->json(['responseCode' => 501, 'responseStatus' => 'Exception', 'responseMessage' => $response['responseDesc']]);
        }

    }



}

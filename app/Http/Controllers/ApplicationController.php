<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Privilege;

class ApplicationController extends Controller
{
     /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
    	//Privilege::visibleView($request->segments()[0],'READ');
        $param['pageNum']   = 1;
        $param['pageSize']   = 1000;
      
        $response = $this->httpWithHeaders()
        ->send('GET',  $this->apiTms() . 'api/v1/deviceModel/list', [
            'body' => json_encode($param)
        ])->json();
 
		return view('pages.application.index', [
				    'data'=>null,
                    'edit' => 'no',
                    'deviceModel'=> ($response['responseCode']=='0000'?$response['rows']:null),

				]);
		
       
    }

    /**
     * city datatables
     *
     * @return type JSON city
     */
    public function list(Request $request)
    {
        // Initial Order
        $orderIndex = (int) $request->order[0]['column'];
        $orderDir = $request->order[0]['dir'];
        $orderColumn = $request->columns[$orderIndex]['data'];
		
        
        $param['pageNum']   = ($request->start / $request->length) + 1;
        $param['pageSize']   = $request->length; 
        $param['deviceModelId']   = $request->deviceModelId; 
        $param['name']   = $request->name; 
        $param['sn']   = $request->sn; 
        
        $response = $this->httpWithHeaders()
        ->get( $this->apiTms()  . 'api/v1/application/list', $param)->json();
   

        if($response['responseCode'] == "0000")
        {
            return response()->json([
                'draw'              => $request->draw,
                'recordsTotal'      => $response['total'],
                'recordsFiltered'   => $response['total'], 
                'data'              => $response['rows'],
                'input'             => [
                    'start' => $request->start,
                    'draw' => $request->draw,
                    'length' =>  $request->length,
                    'order' => $orderIndex,
                    'orderDir' => $orderDir,
                    'orderColumn' => $request->columns[$orderIndex]['data']
                ]
            ]);
        }
        else
        {
            return response()->json([
                'draw'              => $request->draw,
                'recordsTotal'      => 0,
                'recordsFiltered'   => 0, 
                'data'              => [],
                'input'             => [
                    'start' => $request->start,
                    'draw' => $request->draw,
                    'length' =>  $request->length,
                    'order' => $orderIndex,
                    'orderDir' => $orderDir,
                    'orderColumn' => $request->columns[$orderIndex]['data']
                ]
            ]);
        }
       
    }

    public function form(Request $request)
    {
        
        return view('pages.application.form', [
          
            'data'=>null,
            'edit' => 'no'
        ]);

    }
	

    public function autoCompleteDeviceModel(Request $request){

        $search = $request->search;
        $response = null;
        if($search == ''){
            $response = $this->httpWithHeaders()
            ->get( $this->apiTms() . 'api/v1/deviceModel/list', [
                'modelName' => null
            ])->json();
           
        }else{
            $response = $this->httpWithHeaders()
            ->get( $this->apiTms() . 'api/v1/deviceModel/list', [
                'modelName' => $search
            ])->json();
        }

        $r = array();
        foreach($response['rows'] as $data){
            $r[] = array("value"=>$data['id'],"label"=>$data['model']);
        }

        return response()->json($r);

    }

    public function formEdit(Request $request)
    {
        //Privilege::visibleEdit($request->segments()[0],'SHOW'); 	
        $response = $this->httpWithHeaders()
        ->get( $this->apiTms() . 'api/v1/application/get', [
            'id' => $request->id
        ])->json();

        return view('pages.application.form', [
            'data' => $response['data'],
            'edit' => 'ya'
        ]);

    }

    public function store(Request $request)
    {
        
        $response = $this->httpWithHeaders();
        if($request->files) {
            foreach ($request->files as $key=> $file) {
                if ($request->hasFile($key)) {
                    // get Illuminate\Http\UploadedFile instance
                    $image = $request->file($key);
                    $fileName = $request->file($key)->getClientOriginalName();
                    $response = $response->attach($key, $image->get(),$fileName);
                }
            }
            $response = $response->post($this->apiTms()  . 'api/v1/application/add', $request->all());
        } else {
            $response = Http::withHeaders($headers)->post($this->apiTms()  . 'api/v1/application/add', $request->all());
        }
        return $this->responseCode($response,'Application has been added successfully');  
        
    }

  

    public function  show(Request $request)
    {
        $response = $this->httpWithHeaders()
            ->get( $this->apiTms() . 'api/v1/application/get', [
                'id' => $request->id
            ])->json();
           
      
        if($response['responseCode'] =='0000'){
         
            return response()->json(['responseCode' => 200, 'responseStatus' => 'OK', 'responseMessage' => $response['data']]);
     
          }
        else{
            return response()->json(['responseCode' => 500, 'responseStatus' => 'Failed', 'responseMessage' => $response['responseDesc']]);
        }
    }

    public function update(Request $request)
    {
        $response = $this->httpWithHeaders();
        if($request->files) {
            foreach ($request->files as $key=> $file) {
                if ($request->hasFile($key)) {
                    // get Illuminate\Http\UploadedFile instance
                    $image = $request->file($key);
                    $fileName = $request->file($key)->getClientOriginalName();
                    $response = $response->attach($key, $image->get(),$fileName);
                }
            }
            $response = $response->post($this->apiTms()  . 'api/v1/application/update', $request->all());
        } else {
            $response = Http::withHeaders($headers)->post($this->apiTms()  . 'api/v1/application/update', $request->all());
        }
       
        return $this->responseCode($response,'Application has been updated successfully'); 
           
      
    }

    public function delete(Request $request)
    {
        //if(Privilege::visibleDelete($request->segments()[0],'DELETE')=='Y' )
        //{
        //    return response()->json(['responseCode' => 401, 'responseStatus' => 'No Authorized', 'responseMessage' => 'No Authorized' ]);
        //}

        $response = $this->httpWithHeaders()
            ->post( $this->apiTms() . 'api/v1/application/delete', [
                'version' => $request->version,
                'id' => $request->id,

            ])->json();
           
      
        if($response['responseCode'] =='0000'){
            return response()->json(['responseCode' => 200, 'responseStatus' => 'OK', 'responseMessage' => 'Application has been deleted successfully']);
        }
        else if($response['responseCode'] =='0400'){
            return response()->json(['responseCode' => 500, 'responseStatus' => 'No Data', 'responseMessage' => $response['responseDesc'] ]);
        }
        else
        {
            return response()->json(['responseCode' => 501, 'responseStatus' => 'Exception', 'responseMessage' => $response['responseDesc']]);
        }

    }

    public function getApk(Request $request){

        $response = $this->httpWithHeaders()
        ->get( $this->apiTms() . 'api/v1/application/getApk', [
            'id' => $request->id,
        ])->json();

          
        return view('pages.application.formapk', [
            'data'=>$response,
            'edit' => 'no'
        ]);

    }


}

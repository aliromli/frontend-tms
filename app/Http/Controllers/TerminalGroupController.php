<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\TemporaryTerminal;
use Illuminate\Support\Facades\Http;
use Box\Spout\Reader\Common\Creator\ReaderEntityFactory;
use Privilege;

class TerminalGroupController extends Controller
{
     /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
    	Privilege::visibleView($request->segments()[0],'READ');
     	return view('pages.terminal_group.index', [
				    'data'=>null,
				]);
		
       
    }
    //autoCompleteTerminal
    public function autoCompleteTerminal(Request $request){

        $search = $request->search;
        $response = null;
        if($search == ''){
            $response = $this->httpWithHeaders()
            ->get( $this->apiTms() . 'api/v1/terminal/list', [
                'modelName' => null
            ])->json();
           
        }else{
            $response = $this->httpWithHeaders()
            ->get( $this->apiTms() . 'api/v1/terminal/list', [
                'modelName' => $search
            ])->json();
        }

        $r = array();
        foreach($response['rows'] as $data){
            $r[] = array("value"=>$data['id'],"label"=>$data['model']);
        }

        return response()->json($r);

    }

    public function list(Request $request)
    {
        // Initial Order
        $orderIndex = (int) $request->order[0]['column'];
        $orderDir = $request->order[0]['dir'];
        $orderColumn = $request->columns[$orderIndex]['data'];
		
        
        $param['pageNum']   = ($request->start / $request->length) + 1;
        $param['pageSize']   = $request->length; 
        $param['terminalId']   = $request->terminalId; 
        $param['name']   = $request->name;  
        
        $response = $this->httpWithHeaders()
        ->get( $this->apiTms()  . 'api/v1/terminalGroup/list', $param)->json();
   

        if($response['responseCode'] == "0000")
        {
            return response()->json([
                'draw'              => $request->draw,
                'recordsTotal'      => $response['total'],
                'recordsFiltered'   => $response['total'], 
                'data'              => $response['rows'],
                'input'             => [
                    'start' => $request->start,
                    'draw' => $request->draw,
                    'length' =>  $request->length,
                    'order' => $orderIndex,
                    'orderDir' => $orderDir,
                    'orderColumn' => $request->columns[$orderIndex]['data']
                ]
            ]);
        }
        else
        {
            return response()->json([
                'draw'              => $request->draw,
                'recordsTotal'      => 0,
                'recordsFiltered'   => 0, 
                'data'              => [],
                'input'             => [
                    'start' => $request->start,
                    'draw' => $request->draw,
                    'length' =>  $request->length,
                    'order' => $orderIndex,
                    'orderDir' => $orderDir,
                    'orderColumn' => $request->columns[$orderIndex]['data']
                ]
            ]);
        }
       
    }


    /**
     * city datatables
     *
     * @return type JSON city
     */
    public function listTerminal(Request $request)
    {
        // Initial Order
        $orderIndex = (int) $request->order[0]['column'];
        $orderDir = $request->order[0]['dir'];
        $orderColumn = $request->columns[$orderIndex]['data'];
		
        
        $param['pageNum']   = ($request->start / $request->length) + 1;
        $param['pageSize']   = $request->length; 
        $param['id']   = $request->id; 
       
        $response = $this->httpWithHeaders()
        ->get( $this->apiTms()  . 'api/v1/terminalGroup/listTerminal', $param)->json();
   

        if($response['responseCode'] == "0000")
        {
            return response()->json([
                'draw'              => $request->draw,
                'recordsTotal'      => $response['total'],
                'recordsFiltered'   => $response['total'], 
                'data'              => $response['rows'],
                'input'             => [
                    'start' => $request->start,
                    'draw' => $request->draw,
                    'length' =>  $request->length,
                    'order' => $orderIndex,
                    'orderDir' => $orderDir,
                    'orderColumn' => $request->columns[$orderIndex]['data']
                ]
            ]);
        }
        else
        {
            return response()->json([
                'draw'              => $request->draw,
                'recordsTotal'      => 0,
                'recordsFiltered'   => 0, 
                'data'              => [],
                'input'             => [
                    'start' => $request->start,
                    'draw' => $request->draw,
                    'length' =>  $request->length,
                    'order' => $orderIndex,
                    'orderDir' => $orderDir,
                    'orderColumn' => $request->columns[$orderIndex]['data']
                ]
            ]);
        }
       
    }
   
    public function addTerminals(Request $request)
    {
        $response = $this->httpWithHeaders()
        ->post( $this->apiTms()  . 'api/v1/terminalGroup/addTerminals', [
            'id' => $request->terminalGroupId,
            'version'=>$request->version,
            'terminalIds' =>  json_decode($request->terminalIds),
        ])->json();
       
    return $this->responseCode2($response,'Terminals has been added successfull');
 
    }

    public function form(Request $request)
    {
        // $response = $this->httpWithHeaders()
        //     ->get( $this->apiTms() . 'api/v1/terminalGroup/get', [
        //         'id' => $request->id
        //     ])->json();
      
        return view('pages.terminal_group.form', [
            'data'=> null,
            'edit' => 'no'
        ]);

    }
    public function formEdit(Request $request)
    {
        		
		Privilege::visibleEdit($request->segments()[0],'UPDATE'); 		
				
        
        $response = $this->httpWithHeaders()
        ->get( $this->apiTms() . 'api/v1/terminalGroup/get', [
            'id' => $request->id
        ])->json();
        return view('pages.terminal_group.form', [
            //'district'=> $responseDistrict['rows'] ,
            //'merchantType'=>$responseType['rows'],
            'data' => $response['data'],
            'edit' => 'ya'
        ]);

    }
    public function store(Request $request)
    {
        $response = $this->httpWithHeaders()
            ->post( $this->apiTms()  . 'api/v1/terminalGroup/add', [
                'name' => $request->name,
                'description' => $request->description,
                'terminalIds' =>  json_decode($request->terminalIds),
            ])->json();
           
        return $this->responseCode($response,'Termianl Group has been added successfull');
        
    }

    public function  show(Request $request)
    {
       
        $response = $this->httpWithHeaders()
            ->get( $this->apiTms() . 'api/v1/terminalGroup/get', [
                'id' => $request->id
            ])->json();
           
        return $this->responseCodeGet($response,'OK');
   
       
    }

    public function update(Request $request)
    {
        $response = $this->httpWithHeaders()
            ->post( $this->apiTms() . 'api/v1/terminalGroup/update', [

                'name' => $request->name,
                'description' => $request->description,
                'terminalIds' => json_decode($request->terminalIds),
                'id' => $request->id,
                'version' => $request->version
            ])->json();
		return $this->responseCode($response,'Termianl Group has been updated successfull');
    }
	
	public function detail($id)
	{
		//$response = $this->httpWithHeaders()
        //->get( $this->apiTms() . 'api/v1/terminalGroup/get', [
        //    'id' => $request->id
        //])->json();
        
        
        //$response2 = $this->httpWithHeaders()
        //->get( $this->apiTms() . 'api/v1/terminalGroup/listTerminal', [
        //    'id' => $request->id
        //])->json();
       
        return view('pages.terminal_group.detail', [
            'id'=>$id,
            //'list'=>$response2['rows'],
            
        ]);
	}
    public function importView($gid)
    {
        $response = $this->httpWithHeaders()
        ->get( $this->apiTms() . 'api/v1/terminalGroup/get', [
           'id' => $gid
        ])->json();
        
		$q  = TemporaryTerminal::where("user_name",session()->get('user.name'));
        return view('pages.terminal_group.import', [
           'id_group'=>$gid,
           'data'=>$response['data'],
           'data_temp'=>$q->where("group_id",$gid)->get(),
           'found'=>$q->where("group_id",$gid)->where("result","found")->get()->count()
        ]);
    }
	
	public function importTerminal(Request $request)
	{
		
        if ($request->hasFile('file')) {

            $file = $request->file('file');
            $gi   = $request->group_id;
            $processId = null;
            $data = null;
            if($file->isValid()) {
                $nameStripped = str_replace(" ", "-", $file->getClientOriginalName());
                $f = $file->storeAs('upload', $nameStripped,'public'); // linux

                $data = $this->actionImport($f,$gi);
				
                //$process = new Process();
                //$process->fill([
                //    'upload'    => 'mcu',
                //    'processed' => 0,
                //    'success'   => 0,
                //    'failed'    => 0,
                //    'total'     => 100,
                //    'status'    => 'ON PROGRESS'
                //]);
                //$process->save();

                //$processId = $process->id;

                //ImportMcu::dispatch('upload'.DIRECTORY_SEPARATOR.$nameStripped, $process->id)->delay(now()->addSeconds(3));
             }
             if($data)
             {
                return response()->json([
                    'responseCode' => 200,
                    'responseMessage' => 'Uploaded',
                    'processId' => $processId
                ]); 
             }
             else
             {
                return response()->json([
                    'responseCode' => 500,
                    'responseMessage' => 'Gagal Import',
                    'processId' => $processId
                ]); 
             }
             
        }
	}

    public function deleteTerminals(Request $request)
    {

        $response = $this->httpWithHeaders()
        ->post( $this->apiTms() . 'api/v1/terminalGroup/deleteTerminals', [
            'version' => $request->version,
            'id' => $request->terminalGroupId,
            'terminalIds' => json_decode($request->terminalIds)
        ])->json();

        return $this->responseCode2($response,'Terminal has been deleted successfull');
       
    }
    public function delete(Request $request)
    {
        if(Privilege::visibleDelete($request->segments()[0],'DELETE')=='Y' )
        {
            return response()->json(['responseCode' => 401, 'responseStatus' => 'No Authorized', 'responseMessage' => 'No Authorized' ]);
        }
        
        $response = $this->httpWithHeaders()
            ->post( $this->apiTms() . 'api/v1/terminalGroup/delete', [
                'version' => $request->version,
                'id' => $request->id,
        ])->json();
           
      
        if($response['responseCode'] =='0000'){
            return response()->json(['responseCode' => 200, 'responseStatus' => 'OK', 'responseMessage' => 'Terminal Group has been deleted successfully']);
        }
        else if($response['responseCode'] =='0400'){
            return response()->json(['responseCode' => 500, 'responseStatus' => 'No Data', 'responseMessage' => $response['responseDesc'] ]);
        }
        else
        {
            return response()->json(['responseCode' => 501, 'responseStatus' => 'Exception', 'responseMessage' => $response['responseDesc']]);
        }

    }

    public function actionImport($f,$gid)
    {
        try {
            $path = storage_path('app'.DIRECTORY_SEPARATOR.'public'.DIRECTORY_SEPARATOR.$f);
            $reader = ReaderEntityFactory::createReaderFromFile($path);
            $reader->open($path);

            foreach ($reader->getSheetIterator() as $sheet) {
                if($sheet->getIndex() === 0) {
                    //$this->calculateRow($sheet, $this->processId);
                    //$this->readSheet($sheet, $this->processId);
                    return $this->readSheet($sheet,$gid);
                    break;
                }
            }

            $reader->close();
            // delete file
            unlink($path);

        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    private function readSheet($sheet,$gid)
    {
        $data = array();        
        foreach ($sheet->getRowIterator() as $i => $row)
		{

            if($i == 1) {
                continue;
            }

            $r = $row->toArray();
   
            $d = $this->cekSn($r[0]);
            $result_tid = $d[0];
            $result_found = $d[1];
            array_push($data,array("user_name"=>session()->get('user.name'),"terminal_id"=>$result_tid,"group_id"=>$gid,"sn"=>$r[0],"result"=>$result_found));

        }
        //temporary_terminal
        TemporaryTerminal::where('user_name',session()->get('user.name'))->delete();
        return TemporaryTerminal::insert($data);
       
    }

    private function cekSn($sn)
    {
        $param['sn']   = $sn; 
        $response = $this->httpWithHeaders()
        ->get( $this->apiTms()  . 'api/v1/terminal/list', $param)->json();
   
        
        if($response['responseCode'] == "0000")
        {
            return [$response['rows'][0]['id'],'found'];
        }
        else
        {
            return [null,'not found'];
        }
    }


}

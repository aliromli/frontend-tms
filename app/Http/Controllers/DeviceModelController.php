<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Privilege;

class DeviceModelController extends Controller
{
     /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
		Privilege::visibleView($request->segments()[0],'READ');
        return view('pages.device_model.index', [
					'data' => null,
					//'act_create' => $this->statusActions('/device-model','CREATE','Y')
                   
				]);
		
       
    }

    /**
     * city datatables
     *
     * @return type JSON city
     */
    public function list(Request $request)
    {
        // Initial Order
        $orderIndex = (int) $request->order[0]['column'];
        $orderDir = $request->order[0]['dir'];
        $orderColumn = $request->columns[$orderIndex]['data'];
		
        
        $param['pageNum']   = ($request->start / $request->length) + 1;
        $param['pageSize']   = $request->length; 
        $param['modelName']   = $request->model; 
        $param['vendorName']   = $request->vendorName; 
        $param['vendorCountry']   = $request->vendorCountry; 
    
        
        // $tenantId = 'qualita';
        // $signature = 'tes';
        // $refNumber = '{{ref}}{{$isoTimestamp}}';
        // $reqTime = '{{$timestamp}}';
        // $x_cunsomer_username =  'tes';


        $response = $this->httpWithHeaders()
        ->get( $this->apiTms()  . 'api/v1/deviceModel/list', $param)->json();
   

        if($response['responseCode'] == "0000")
        {
            return response()->json([
                'draw'              => $request->draw,
                'recordsTotal'      => $response['total'],
                'recordsFiltered'   => $response['total'], 
                'data'              => $response['rows'],
                'input'             => [
                    'start' => $request->start,
                    'draw' => $request->draw,
                    'length' =>  $request->length,
                    'order' => $orderIndex,
                    'orderDir' => $orderDir,
                    'orderColumn' => $request->columns[$orderIndex]['data']
                ]
            ]);
        }
        else
        {
            return response()->json([
                'draw'              => $request->draw,
                'recordsTotal'      => 0,
                'recordsFiltered'   => 0, 
                'data'              => [],
                'input'             => [
                    'start' => $request->start,
                    'draw' => $request->draw,
                    'length' =>  $request->length,
                    'order' => $orderIndex,
                    'orderDir' => $orderDir,
                    'orderColumn' => $request->columns[$orderIndex]['data']
                ]
            ]);
        }
       
    }

    public function form(Request $request)
    {
        return view('pages.device_model.form', [
           
            'data'=>null,
            'edit' => 'no'
        ]);

    }

    public function formEdit(Request $request)
    {
        Privilege::visibleEdit($request->segments()[0],'UPDATE'); 
       
        $response = $this->httpWithHeaders()
            ->get( $this->apiTms() . 'api/v1/deviceModel/get', [
                'id' => $request->id
            ])->json();
      
        return view('pages.device_model.form', [
            'data' => $response['data'],
            'edit' => 'ya'
        ]);

    }

    public function store(Request $request)
    {
       
        $response = $this->httpWithHeaders()
            ->post( $this->apiTms()  . 'api/v1/deviceModel/add', [
                'model' => $request->model,
                'modelInformation' => $request->modelInformation,
                'vendorName' => $request->vendorName,
                'vendorCountry' => $request->vendorCountry,
            ])->json();
            return $this->responseCode($response,'DeviceModel has been saved successfull');
           
      
       
    }

    public function  show(Request $request)
    {
       
        $response = $this->httpWithHeaders()
            ->get( $this->apiTms() . 'api/v1/deviceModel/get', [
                'id' => $request->id
            ])->json();
           
      
        if($response['responseCode'] =='0000'){
         
            return response()->json(['responseCode' => 200, 'responseStatus' => 'OK', 'responseMessage' => $response['data']]);
     
          }
        else{
            return response()->json(['responseCode' => 500, 'responseStatus' => 'Failed', 'responseMessage' => $response['responseDesc']]);
        }
    }

    public function update(Request $request)
    {
        
       

        $response = $this->httpWithHeaders()
         
            ->post( $this->apiTms() . 'api/v1/deviceModel/update', [

                'model' => $request->model,
                'modelInformation' => $request->modelInformation,
                'vendorName' => $request->vendorName,
                'vendorCountry' => $request->vendorCountry,
                'id' => $request->id,
                'version' => $request->version


            ])->json();
            return $this->responseCode($response,'DeviceModel has been updated successfull');     
      
    }

    public function delete(Request $request)
    {
        if(Privilege::visibleDelete($request->segments()[0],'DELETE')=='Y' )
        {
            return response()->json(['responseCode' => 401, 'responseStatus' => 'No Authorized', 'responseMessage' => 'No Authorized' ]);
        }
           
        $response = $this->httpWithHeaders()
        
        ->post( $this->apiTms() . 'api/v1/deviceModel/delete', [

            'version' => $request->version,

            'id' => $request->id,


        ])->json();
           
      
        if($response['responseCode'] =='0000'){
            return response()->json(['responseCode' => 200, 'responseStatus' => 'OK', 'responseMessage' => 'Device model has been deleted successfully']);
        }
        else if($response['responseCode'] =='0400'){
            return response()->json(['responseCode' => 500, 'responseStatus' => 'No Data', 'responseMessage' => $response['responseDesc'] ]);
        }
        else
        {
            return response()->json(['responseCode' => 501, 'responseStatus' => 'Exception', 'responseMessage' => $response['responseDesc']]);
        }

    }


}

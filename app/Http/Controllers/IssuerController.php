<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use DateTime;
use Privilege;

class IssuerController extends Controller
{
     /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
    	Privilege::visibleView($request->segments()[0],'READ');
    	return view('pages.issuer.index', [
					'data' => null
				]);
	}

    /**
     * city datatables
     *
     * @return type JSON city
     */
    public function list(Request $request)
    {
        // Initial Order
        $orderIndex = (int) $request->order[0]['column'];
        $orderDir = $request->order[0]['dir'];
        $orderColumn = $request->columns[$orderIndex]['data'];
		
        
        $param['pageNum']   = ($request->start / $request->length) + 1;
        $param['pageSize']   = $request->length; 
        //$param['stateId']   = $request->state; 
        $param['name']   = $request->name; 
        
        $response = $this->httpWithHeaders()
        ->get( $this->apiTms()  . 'api/v1/issuer/list', $param)->json();
   
		//print_r($response);
		
        if($response['responseCode'] == "0000")
        {
            return response()->json([
                'draw'              => $request->draw,
                'recordsTotal'      => $response['total'],
                'recordsFiltered'   => $response['total'], 
                'data'              => $response['rows'],
                'input'             => [
                    'start' => $request->start,
                    'draw' => $request->draw,
                    'length' =>  $request->length,
                    'order' => $orderIndex,
                    'orderDir' => $orderDir,
                    'orderColumn' => $request->columns[$orderIndex]['data']
                ]
            ]);
        }
        else
        {
            return response()->json([
                'draw'              => $request->draw,
                'recordsTotal'      => 0,
                'recordsFiltered'   => 0, 
                'data'              => [],
                'input'             => [
                    'start' => $request->start,
                    'draw' => $request->draw,
                    'length' =>  $request->length,
                    'order' => $orderIndex,
                    'orderDir' => $orderDir,
                    'orderColumn' => $request->columns[$orderIndex]['data']
                ]
            ]);
        }
       
    }

    public function form(Request $request)
    {
        $param['pageNum']   = 1;
        $param['pageSize']   = 2000;
        $responseD = $this->httpWithHeaders()
         ->send('GET',  $this->apiTms() . 'api/v1/acquirer/list', [
            'body' => json_encode($param)
         ])->json();

       
        return view('pages.issuer.form', [
           
            'data'=>null,
            'edit' => 'no',
            'acquirer'=>($responseD['responseCode']=='0000'?$responseD['rows']:null),
        ]);

    }

    public function formEdit(Request $request)
    {
        $param['pageNum']   = 1;
        $param['pageSize']   = 2000;
        $responseD = $this->httpWithHeaders()
         ->send('GET',  $this->apiTms() . 'api/v1/acquirer/list', [
            'body' => json_encode($param)
         ])->json();
        $response = $this->httpWithHeaders()
        ->get( $this->apiTms() . 'api/v1/issuer/get', [
            'id' => $request->id
        ])->json();
  
        return view('pages.issuer.form', [
            'data'=>$response['data'],
            'edit' => 'yes',
            'acquirer'=>($responseD['responseCode']=='0000'?$responseD['rows']:null),
        ]);
    }

    public function store(Request $request)
    {
     	$response = $this->httpWithHeaders()
            ->post( $this->apiTms()  . 'api/v1/issuer/add', [
			    'name' => $request->name,
                'issuerId' => $request->issuerId,
                'acquirerId' => $request->acquirerId,
                'onUs' => $request->onUs,
                'cards' => json_decode($request->cards)
            ])->json();
		return	$this->responseCode($response,'Issuer has been added successfully');  
    }

    public function  show(Request $request)
    {
        $response = $this->httpWithHeaders()
            ->get( $this->apiTms() . 'api/v1/issuer/get', [
                'id' => $request->id
            ])->json();
           
		
        if($response['responseCode'] =='0000'){
         
            return response()->json(['responseCode' => 200, 'responseStatus' => 'OK', 'responseMessage' => $response['data']]);
     
          }
        else{
            return response()->json(['responseCode' => 500, 'responseStatus' => 'Failed', 'responseMessage' => $response['responseDesc']]);
        }
    }

    public function update(Request $request)
    {
       
		
        $response = $this->httpWithHeaders()
         
            ->post( $this->apiTms() . 'api/v1/issuer/update', [
                'name' => $request->name,
                'issuerId' => $request->issuerId,
                'acquirerId' => $request->acquirerId,
                'onUs' => $request->onUs, 
                'cards' => json_decode($request->cards),
                'id' => $request->id,
                'version' => $request->version
            ])->json();
           
			return $this->responseCode($response,'Issuer has been update successfully');
		
      
    }

    public function delete(Request $request)
    {
      
            $response = $this->httpWithHeaders()
            
            ->post( $this->apiTms() . 'api/v1/issuer/delete', [

                'version' => $request->version,

                'id' => $request->id,


            ])->json();
           
      
        if($response['responseCode'] =='0000'){
            return response()->json(['responseCode' => 200, 'responseStatus' => 'OK', 'responseMessage' => 'Issuer has been deleted successfully']);
        }
        else if($response['responseCode'] =='0400'){
            return response()->json(['responseCode' => 500, 'responseStatus' => 'No Data', 'responseMessage' => $response['responseDesc'] ]);
        }
        else
        {
            return response()->json(['responseCode' => 501, 'responseStatus' => 'Exception', 'responseMessage' => $response['responseDesc']]);
        }

    }


}

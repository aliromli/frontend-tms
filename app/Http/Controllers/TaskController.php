<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use App\Exports\DiagnosticExport;
use Maatwebsite\Excel\Facades\Excel;
use Privilege;

class TaskController extends Controller
{
     /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
    	Privilege::visibleView($request->segments()[0],'READ');
       
		return view('pages.task.index', [
					'data'=>null,
				]);
		
       
    }

    public function export(Request $request)
    {
    	Privilege::visibleEdit($request->segments()[0],'EXPORT'); 		
        
        $bagian = $request->bagian;

        $response = $this->httpWithHeaders()
            ->get( $this->apiTms() . 'api/v1/diagnostic/lastDiagnostic', [ 
                'sn' => 'V1E0800867',
                'terminalId' => null
            ])->json();
        
        if($response['responseCode'] == "0000")    
		{
            $data = $response['data'];  
            return Excel::download(new DiagnosticExport(
               
                $data,

                ), 'report-task.xlsx');
        }
        else
        {
            $data = null;
            
            return Excel::download(new DiagnosticExport(
               
                $data,

                ), 'report-task.xlsx');
        }
        
    }


    

}

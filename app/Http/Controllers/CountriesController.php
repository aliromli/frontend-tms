<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Config;
use Privilege;

class CountriesController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
    	Privilege::visibleView($request->segments()[0],'READ');
      	return view('pages.country.index', [
					'data' => null,
				]);
	 
    }

    /**
     * Country datatables
     *
     * @return type JSON country
     */
    public function list(Request $request)
    {
        // Initial Order
        $orderIndex = (int) $request->order[0]['column'];
        $orderDir = $request->order[0]['dir'];
        $orderColumn = $request->columns[$orderIndex]['data'];
		
        
        $param['pageNum']   = ($request->start / $request->length) + 1;
        $param['pageSize']   = $request->length; 
        $param['name']   = $request->name;
        $param['code']   = $request->code;
      

        $response = $this->httpWithHeaders()
            ->get( $this->apiTms()  . 'api/v1/country/list', $param)->json();

        if($response['responseCode'] == "0000")
        {
            return response()->json([
                'draw'              => $request->draw,
                'recordsTotal'      => $response['total'],
                'recordsFiltered'   => $response['total'],
                'data'              => $response['rows'],
                'input'             => [
                    'start' => $request->start,
                    'draw' => $request->draw,
                    'length' => $request->length,
                    'order' => $orderIndex,
                    'orderDir' => $orderDir,
                    'orderColumn' => $request->columns[$orderIndex]['data']
                ]
            ]);
        }
        else
        {
            return response()->json([
                'draw'              => $request->draw,
                'recordsTotal'      => 0,
                'recordsFiltered'   => 0, 
                'data'              => [],
                'input'             => [
                    'start' => $request->start,
                    'draw' => $request->draw,
                    'length' =>  $request->length,
                    'order' => $orderIndex,
                    'orderDir' => $orderDir,
                    'orderColumn' => $request->columns[$orderIndex]['data']
                ]
            ]);
        }
       
    }

    // public function formIndex(Request $request)
    // {
    //     return view('pages.country.formIndex');
  
    // }
	
	public function form(Request $request)
    {
        return view('pages.country.form', [
           
            'data'=>null,
            'edit' => 'no'
        ]);
  
    }

    public function formEdit(Request $request)
    {
        Privilege::visibleEdit($request->segments()[0],'UPDATE'); 		
        
        $response = $this->httpWithHeaders()
        ->get( $this->apiTms() . 'api/v1/country/get', [
            'id' => $request->id
        ])->json();

        // return view('pages.country.form', [
        //     'data' => $response['data'],
        //     'edit' => 'ya'
        // ]);

        return view('pages.country.form', [
           
            'data' => $response['data'],
            'edit' => 'ya'
        ]);

    }

    public function store(Request $request)
    {
        $param['code']   =  $request->code;
        $param['name']   =  $request->name;
		
        $response = $this->httpWithHeaders()
            ->post( $this->apiTms()  . 'api/v1/country/add', [
                'code' => $request->code,
                'name' => $request->name
            ])->json();
           
		return $this->responseCode($response,'Country has been added successfull');  
       
    }

     /**
     * Display the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function  show(Request $request)
    {
      
        $param['id']  = $request->id;
        $response = $this->httpWithHeaders()
            ->send('GET',   $this->apiTms() . 'api/v1/country/get', [
               'body' => json_encode($param)
            ])->json();

      
        if($response['responseCode'] =='0000'){
         
            return response()->json(['responseCode' => 200, 'responseStatus' => 'OK', 'responseMessage' => $response['data']]);
     
          }
        else{
            return response()->json(['responseCode' => 500, 'responseStatus' => 'Failed', 'responseMessage' => $response['responseDesc']]);
        }
    }

    public function update(Request $request)
    {
        $param['code']   =  $request->code;
        $param['name']   =  $request->name;
        
		$response = $this->httpWithHeaders()
            ->post( $this->apiTms()  . 'api/v1/country/update', [
                'code' => $request->code,
                'name' => $request->name,
                'version' => $request->version,
                'id' => $request->id,
            ])->json();
      
		return $this->responseCode($response,'Country has been updated successfull');  
      
	  
    }
    public function delete(Request $request)
    {

        
        if(Privilege::visibleDelete($request->segments()[0],'DELETE')=='Y' )
        {
            return response()->json(['responseCode' => 401, 'responseStatus' => 'No Authorized', 'responseMessage' => 'No Authorized' ]);
        }

        $response = $this->httpWithHeaders()
        ->post( $this->apiTms()  . 'api/v1/country/delete', [
            'id' => $request->id,
            'version' => $request->version
        ])->json();        

           
      
        if($response['responseCode'] =='0000'){
            return response()->json(['responseCode' => 200, 'responseStatus' => 'OK', 'responseMessage' => 'Country has been deleted successfully']);
        }
        else if($response['responseCode'] =='0400'){
            return response()->json(['responseCode' => 500, 'responseStatus' => 'No Data', 'responseMessage' => $response['responseDesc'] ]);
        }
        else
        {
            return response()->json(['responseCode' => 501, 'responseStatus' => 'Exception', 'responseMessage' => $response['responseDesc']]);
        }

    }


}

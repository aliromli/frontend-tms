<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http; 
use App\Models\TemporaryTerminalExt;
use Box\Spout\Reader\Common\Creator\ReaderEntityFactory;
use DateTime;
use Privilege;

class TerminalExtController extends Controller
{
     /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
    	Privilege::visibleView($request->segments()[0],'READ');
        return view('pages.terminalExt.index', [
					'data' => null
				]);
	}

    /**
     * city datatables
     *
     * @return type JSON city
     */
    public function list(Request $request)
    {
        // Initial Order
        $orderIndex = (int) $request->order[0]['column'];
        $orderDir = $request->order[0]['dir'];
        $orderColumn = $request->columns[$orderIndex]['data'];
		
        
        $param['pageNum']   = ($request->start / $request->length) + 1;
        $param['pageSize']   = $request->length; 
        //$param['stateId']   = $request->state; 
        $param['tid']   = $request->tid; 
        
        $response = $this->httpWithHeaders()
        ->get( $this->apiTms()  . 'api/v1/terminalExt/list', $param)->json();
   
		//print_r($response);
		
        if($response['responseCode'] == "0000")
        {
            return response()->json([
                'draw'              => $request->draw,
                'recordsTotal'      => $response['total'],
                'recordsFiltered'   => $response['total'], 
                'data'              => $response['rows'],
                'input'             => [
                    'start' => $request->start,
                    'draw' => $request->draw,
                    'length' =>  $request->length,
                    'order' => $orderIndex,
                    'orderDir' => $orderDir,
                    'orderColumn' => $request->columns[$orderIndex]['data']
                ]
            ]);
        }
        else
        {
            return response()->json([
                'draw'              => $request->draw,
                'recordsTotal'      => 0,
                'recordsFiltered'   => 0, 
                'data'              => [],
                'input'             => [
                    'start' => $request->start,
                    'draw' => $request->draw,
                    'length' =>  $request->length,
                    'order' => $orderIndex,
                    'orderDir' => $orderDir,
                    'orderColumn' => $request->columns[$orderIndex]['data']
                ]
            ]);
        }
       
    }

    public function form(Request $request)
    {
        
        $response = $this->httpWithHeaders()
        ->get( $this->apiTms() . 'api/v1/template/list', [
            'id' => $request->id
        ])->json();
  
        return view('pages.terminalExt.form', [
           
            'data'=>null,
            'template'=>$response['rows'],
            'edit' => 'no'
        ]);

    }

    public function formEdit(Request $request)
    {
        Privilege::visibleEdit($request->segments()[0],'UPDATE'); 		
        $response = $this->httpWithHeaders()
        ->get( $this->apiTms() . 'api/v1/terminalExt/get', [
            'id' => $request->id
        ])->json();
        $responseD = $this->httpWithHeaders()
        ->get( $this->apiTms() . 'api/v1/template/list', [
            'id' => $request->id
        ])->json();
  
        return view('pages.terminalExt.form', [
            'data'=>$response['data'],
            'template'=>$responseD['rows'],
            'edit' => 'yes'
        ]);
    }

    public function store(Request $request)
    {
     	$response = $this->httpWithHeaders()
            ->post( $this->apiTms()  . 'api/v1/terminalExt/add', [
			   
                'tid' =>  $request->tid,
				'mid' =>  $request->mid,
				'merchantName1' =>  $request->merchantName1,
				'merchantName2' =>  $request->merchantName2,
				'merchantName3' =>  $request->merchantName3,
                'templateId' =>  $request->templateId
            ])->json();
			return $this->responseCode($response,'TerminalExt has been added successfully');  
    }

    public function  show(Request $request)
    {
        $response = $this->httpWithHeaders()
            ->get( $this->apiTms() . 'api/v1/terminalExt/get', [
                'id' => $request->id
            ])->json();
           
		
        if($response['responseCode'] =='0000'){
         
            return response()->json(['responseCode' => 200, 'responseStatus' => 'OK', 'responseMessage' => $response['data']]);
     
          }
        else{
            return response()->json(['responseCode' => 500, 'responseStatus' => 'Failed', 'responseMessage' => $response['responseDesc']]);
        }
    }

    public function update(Request $request)
    {
       
        $response = $this->httpWithHeaders()
         
            ->post( $this->apiTms() . 'api/v1/terminalExt/update', [
                'tid' =>  $request->tid,
				'mid' =>  $request->mid,
				'merchantName1' =>  $request->merchantName1,
				'merchantName2' =>  $request->merchantName2,
				'merchantName3' =>  $request->merchantName3,
                'templateId' =>  $request->templateId,
                'id' => $request->id,
                'version' => $request->version
            ])->json();
           
			return $this->responseCode($response,'TerminalExt has been update successfully');
		
    }

    public function delete(Request $request)
    {
      
        if(Privilege::visibleDelete($request->segments()[0],'DELETE')=='Y' )
        {
            return response()->json(['responseCode' => 401, 'responseStatus' => 'No Authorized', 'responseMessage' => 'No Authorized' ]);
        } 
        $response = $this->httpWithHeaders()
            
            ->post( $this->apiTms() . 'api/v1/terminalExt/delete', [

                'version' => $request->version,

                'id' => $request->id,


        ])->json();
           
      
        if($response['responseCode'] =='0000'){
            return response()->json(['responseCode' => 200, 'responseStatus' => 'OK', 'responseMessage' => 'TerminalExt has been deleted successfully']);
        }
        else if($response['responseCode'] =='0400'){
            return response()->json(['responseCode' => 500, 'responseStatus' => 'No Data', 'responseMessage' => $response['responseDesc'] ]);
        }
        else
        {
            return response()->json(['responseCode' => 501, 'responseStatus' => 'Exception', 'responseMessage' => $response['responseDesc']]);
        }

    }

  
	public function importView($id)
	{
	    return view('pages.terminalExt.import', [
            'user_name'=>$id,
            'data_temp'=>TemporaryTerminalExt::where('user_name',$id)->get(),
            'canimp'=>TemporaryTerminalExt::where('user_name',$id)->where('status','Pass')->get()
        ]);
	}

    public function importTerminal(Request $request)
	{
		
        if ($request->hasFile('file')) {

            $file = $request->file('file');
            $userName   =  session()->get('user.name');
            $processId = null;
            $data = null;
            if($file->isValid()) {
                $nameStripped = str_replace(" ", "-", $file->getClientOriginalName());
                $f = $file->storeAs('upload', $nameStripped,'public'); // linux
                $data = $this->actionImport($f,$userName);
				
                }
             if($data)
             {
                return response()->json([
                    'responseCode' => 200,
                    'responseMessage' => 'Uploaded',
                    'processId' => $processId,
                    'un' => $userName 
                ]); 
             }
             else
             {
                return response()->json([
                    'responseCode' => 500,
                    'responseMessage' => 'Gagal Import',
                    'processId' => $processId,
                    'un' => $userName 
                ]); 
             }
             
        }
	}

    public function actionImport($f,$userName)
    {
        try {
            $path = storage_path('app'.DIRECTORY_SEPARATOR.'public'.DIRECTORY_SEPARATOR.$f);
            $reader = ReaderEntityFactory::createReaderFromFile($path);
            $reader->open($path);

            foreach ($reader->getSheetIterator() as $sheet) {
                if($sheet->getIndex() === 0) {
                    //$this->calculateRow($sheet, $this->processId);
                    //$this->readSheet($sheet, $this->processId);
                    return $this->readSheet($sheet,$userName);
                    break;
                }
            }

            $reader->close();
            // delete file
            unlink($path);

        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }
    //session()->get('user.name')
    private function readSheet($sheet,$userName)
    {
        $data = array();        
        foreach ($sheet->getRowIterator() as $i => $row)
		{

            if($i == 1) {
                continue;
            }

            $r = $row->toArray();
   
            
            //if($i>2)
            //{
            $m = $this->cekTemplateName($r[5],$data,$i);
            //}
            
            
            // $bo = true;
            // if($s[1]==0)
            // {
            //     $bo = false;
            // }
            // if($m[1]==0)
            // {
            //     $bo = false;
            // }
          
            //if($bo==true)
            //{
                array_push($data,array(
                    "user_name"=>$userName,
                    "terminal_id"=>$r[0],
                    "merchant_id"=>$r[1],
                    "merchant_name1"=>$r[2],
                    "merchant_name2"=>$r[3],
                    "merchant_name3"=>$r[4],
                    "template_name"=>$r[5],
                    "status" => $m[0],
                    "note" => '',
                    "template_id" => $m[1],
                    )
                );
   
            //}
        }
     
        TemporaryTerminalExt::where('user_name',$userName)->delete();
        return TemporaryTerminalExt::insert($data);
       
    }

    // private function cekTerminalID($tid)
    // {
    //     $param['tid']   = $tid; 
    //     $response = $this->httpWithHeaders()
    //     ->get( $this->apiTms()  . 'api/v1/terminalExt/list', $param)->json();
   
        
    //     if($response['responseCode'] == "0000")
    //     {
    //         //return [null,0]; //duplicat
    //     }
    //     else if($response['responseCode'] == "0400") 
    //     {
    //         return [$sn,1];  //not found
    //         //return [$response['rows'][0]['id'],1];
    //     }
    //}

    private function cekTemplateName($p,$data,$loop)
    {
      
        if($loop==2)
        {
            $param['name']   = $p; 
            $response = $this->httpWithHeaders()
            ->get( $this->apiTms()  . 'api/v1/template/list', $param)->json();
           
            if($response['responseCode'] == "0000") //found
            {
                return ["Pass",$response['rows'][0]['id']];
            }
            else  if($response['responseCode'] == "0400") //not tound 
            {
                return  ["Template not found",null];
            }
           
        }
        else
        {

            $found = 0;
            for($i=0; $i<count($data); $i++)
            {
                if($p==$data[$i]['template_name'])
                {
                    $found = 1;
                    break;
                }
            }
    
            if($found==1)
            {
                return ["Duplicate",null];
            }
            else
            {
                $param['name']   = $p; 
                $response = $this->httpWithHeaders()
                ->get( $this->apiTms()  . 'api/v1/template/list', $param)->json();
                
               
                if($response['responseCode'] == "0000") //found
                {
                    return ["Pass",$response['rows'][0]['id']];
                }
                else  if($response['responseCode'] == "0400") //not tound 
                {
                    return  ["Template not found",null];
                }
               
            }

        }


    }

    public function addTerminalImport(Request $request){
        

        if($request->terminals) 
        { 
            //print_r($request->terminals);
            $userName = $request->userName;
            $r_ok = array();
            $r_f = array();
            foreach(json_decode($request->terminals) as $t)
            { 
                $response = $this->httpWithHeaders()
                ->post( $this->apiTms()  . 'api/v1/terminalExt/add', [
                    'tid' => $t->tid,
                    'mid' => $t->mid,
                    'merchantName1' => $t->mn1,
                    'merchantName2' => $t->mn2,
                    'merchantName3' => $t->mn3,
                    'templateId' => $t->template_id,
                ])->json();

                if($response['responseCode']=="0000")
                {
                    array_push($r_ok,"ok");
                    TemporaryTerminalExt::where('user_name',$userName)->where("terminal_id",$t->tid)->delete();
                }
                else
                {
                    array_push($r_f,"no"); 
                }    

           
            }

            if(count($r_ok)>0)
            {
                
                $str = "Data success diImport sebanyak ".count($r_ok).". Dan Gagal Sebanyak ".count($r_f)." ";
                return response()->json(['responseCode' => '0000', 'responseStatus' => 'OK', 'responseMessage' => $str]);
			
            }
            else
            {
                
                $str = "Tidak ada data yang berhasil diimport ";
                return response()->json(['responseCode' => '0002', 'responseStatus' => 'OK', 'responseMessage' => $str]);
			
               
            }
        }
        else
        {
            return response()->json(['responseCode' => '0001', 'responseStatus' => 'Failed', 'responseMessage' => "Data Kosong"]);
			
        }
        
    }


}

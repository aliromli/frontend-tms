<?php


	namespace App\Http\Controllers;

	use Session;
	use Carbon\Carbon;
	use App\Models\User;
	use Illuminate\Http\Request;
	use App\Models\VerificationCode;
	use Illuminate\Support\Facades\Auth;

	class AuthOtpController extends Controller
	{
		
		public function verificationOtp(Request $request)
		{
			return view('auth.verify_otp',['email'=>$request->email]);		
		}
		
		public function authOtp(Request $request)
		{
			return view('auth.verify_otp_login',['email'=>$request->email]);		
		}
	
		public function otpCheck(Request $request)
		{
			#Validation
			$request->validate([
				'emailAddress' => 'required|exists:users,email',
				'otp' => 'required'
			]);

			#Validation Logic
			$user = User::where('email',$request->emailAddress)->first();
			$verificationCode   = VerificationCode::where('user_id',$user->id)->where('otp', $request->otp)->first();

			$now = Carbon::now();
			
			//echo $verificationCode->expire_at;
			//exit;
			
			if (!$verificationCode) {
				return redirect()->back()->with('error', 'Your OTP is not correct');
			    
			}
			//elseif($verificationCode && $now->isAfter($verificationCode->expire_at)){
			//	return redirect()->route('auth.otp')->with('error', 'Your OTP has been expired');
			//}
		    else
			{
				$verificationCode->update([
					'expire_at' => Carbon::now()
				]);
				
				$user->update([
					'email_verified_at' => Carbon::now()
				]);
				return redirect()->route('auth.checkSuccess')->with('success', 'Verifikasi OTP Success');
				
			}

			//$userId = User::whereId($user->id)->first();
			//$userId = User::find($user->id);
			/*
			$userId = User::where('id',$user->id)->first();

			if($userId){
				// Expire The OTP
				$verificationCode->update([
					'expire_at' => Carbon::now()
				]);

				//Auth::login($user);

				//return redirect('/home');
				//redirect()->route('auth.otp')->with('success', 'Verifikasi OTP Success !!');
				redirect()->route('otp.checkSuccess')->with('success', 'Verifikasi OTP Success');
			}

			return redirect()->route('auth.otp')->with('error', 'Your Otp is not correct 2');
			*/
			
   
		}
		
		public function otpCheckSuccess(Request $request)
		{
			return view('auth.verify_otp_success',['data'=>'']);
		}
	  
	}
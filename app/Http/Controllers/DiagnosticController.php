<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use App\Exports\DiagnosticExport;
use Maatwebsite\Excel\Facades\Excel;
use DateTime;
use Privilege;

class DiagnosticController extends Controller
{
     /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
    	Privilege::visibleView($request->segments()[0],'READ');
       
		return view('pages.diagnostic.last-diagnostic', [
					'data'=>null,
				]);
		
       
    }

    public function export(Request $request)
    {
    	Privilege::visibleView($request->segments()[0],'EXPORT');
         	
        $param  = [];
        if($request->export_cek == 'true')
        {
            $param = ['terminalIds' => null, 'date'=>null, 'terminalGroupsIds' => null];
        }
        else
        {
            
            $downloadTime = null;
            if($request->date != "")
            {
                $dst = DateTime::createFromFormat('Y-m-d', $request->date);
                //$dst = DateTime::createFromFormat('Y-m-d\TH:i:s', $request->date);
                $dst = date_create(json_decode(json_encode($dst),true)['date']);
                $downloadTime = date_format($dst,'Y-m-d');
            }

           
            $param = ['terminalIds' => json_decode($request->terminals,true), 
                    'date'=>$downloadTime,
                    'terminalGroupsIds' => json_decode($request->terminalGroups,true), 
                ];
        }
        
        
        $response = $this->httpWithHeaders()
            ->get( $this->apiTms() . 'api/v1/diagnostic/lastDiagnosticExport',$param)->json();
        
        if($response['responseCode'] == "0000")    
		{
            $data = $response['data'];  
            return Excel::download(new DiagnosticExport(
               
                $data,

                ), 'report-diagnostic.xlsx');
        }
        else
        {
            $data = null;
            
            return Excel::download(new DiagnosticExport(
               
                $data,

                ), 'report-diagnostic.xlsx');
        }
        
        
    }

   


}

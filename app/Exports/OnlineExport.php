<?php
namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithHeadings;
use DB;

class OnlineExport implements FromCollection, WithHeadings
{
    use Exportable;
	
    protected $bagian;
    protected $data;
    
	public function __construct($bagian = '', $data='')
    {
        $this->bagian = $bagian;
        $this->data = $data;
    }

    public function collection()
    {
        $kp = $this->sql();
		$a1 = array();
		$k = 0;
		foreach($kp as $data){
			
			$a1[]= [
					'Name'=> $data['name'],
					'packageName'=> $data['packageName'],
					];
			
		}
		return collect($a1);
    }

    public function headings(): array
    {
        return [
            'Name',
            'packageName',
           
        ];
    }
	
	public function sql(){
		
		return $this->data;
		//$m = DB::select("SELECT * from users");
		//return $m; 
		
	}
	
	

}

?>
<?php
namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithHeadings;
use DB;

class HeartBeatExport implements FromCollection, WithHeadings
{
    use Exportable;
	
    
    protected $data;
    
	public function __construct($data='')
    {
        $this->data = $data;
    }

    public function collection()
    {
        $kp = $this->sql();
		
		if($kp!=null)
		{
			$a1 = array();
			$k = 0;
			foreach($kp as $data){
				
				$a1[]= [
						'SN'=> $data['sn'],
						'BatteryTemp'=> $data['batteryTemp'],
						];
				
			}
			return collect($a1);
		}
		else
		{
			$a1 = array();
			
				
			$a1[]= [
						'SN'=> null,
						'BatteryTemp'=> null,
					];
				
			
			return collect($a1);
			
		}	
		
		
    }

    public function headings(): array
    {
        return [
            'SN',
            'BatteryTemp',
           
        ];
    }
	
	public function sql(){
		
		return $this->data;
	
	}
	
	

}

?>
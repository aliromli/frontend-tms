<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class MailSendingOtp extends Mailable
{
    use Queueable, SerializesModels;
	
	protected $email_to;
    protected $otp;
	protected $name;
	protected $password;
	

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($email_to='', $otp='', $name='', $password='')
    {
        $this->email_to = $email_to;
        $this->otp = $otp;
        $this->name = $name;
        $this->password = $password;
    }
    /**
     * Build the message.
     *
     * @return $this
     */
  
	public function build()
    {
        return $this->view('mail.email-otp', [
			'name' => $this->name,
			'password' => $this->password,
			'otp' =>  $this->otp,
			'email' => $this->email_to,
			'token' => 'token'
        ])
        ->subject('Email Konfirmasi OTP');
        //->from('noreply@unifiedtms.id');
    }

    
}


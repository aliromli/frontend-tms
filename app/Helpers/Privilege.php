<?php 

namespace App\Helpers;


class Privilege
{
    
	

	public static function check(string $string)
    {
        return strtoupper($string);
    }
	
	public  static function visible($url="",$action)
	{
		
		
		$segments =$url;
		if($segments !== "")
		{
				$data = json_decode(session()->get('menus'));
				$r = "N";
				foreach($data as $g){
				   
				   if(count($g->children)==0)
				   {
						$name = str_replace("/","",strtolower($g->action_url));
						if(strtolower($segments)==$name)
						{
						 	foreach($g->actions as $h)
							{
									if($h->action_type==$action and $h->is_visible=="Y") 
									{
										$r = "Y";
										break;
									}
							 }
						}
				   }
				   else
				   {
					  
						foreach($g->children as $j)
						{
							$name = str_replace("/","",strtolower($j->action_url));
							if(strtolower($segments)==$name)
							{
								foreach($j->actions as $h)
								{
									if($h->action_type==$action and $h->is_visible=="Y") 
									{
										$r = "Y";
										break;
									}
								}
							   
							}
						}
				   }
				}
				return $r;
		
		}
		else
		{
			return "";
		}
        
        
       
        
	}
	
	public  static function visibleView($url="", $action)
	{
		$segments =$url;
        
        $data = json_decode(session()->get('menus'));
        $r = true;
        foreach($data as $g){
           
           if(count($g->children)==0)
           {
                
                $name = str_replace("/","",strtolower($g->action_url));
               
                if(strtolower($segments)==$name)
                {
                   
                    foreach($g->actions as $h)
                    {
                            if($h->action_type==$action and $h->is_visible=="Y") 
                            {
                                $r =false;
								break;
                            }
                     }
                }
           }
           else
           {
              
                foreach($g->children as $j)
                {
                    
                    $name = str_replace("/","",strtolower($j->action_url));
                   
                    if(strtolower($segments)==$name)
                    {
                       
                        foreach($j->actions as $h)
                        {
                            if($h->action_type==$action and $h->is_visible=="Y") 
                            {
                                $r = false;
								break;
                            }
                        }
                       
                    }
                }
           }
        }
		
		if($r==true)
		{
			return abort(401);
		}
		else
		{
			return "N";
		}
		
		
       
        
	}
	
	public  static function visibleEdit($url="", $action)
	{
		$segments =$url;
        
        $data = json_decode(session()->get('menus'));
        $r = true;
        foreach($data as $g){
           
           if(count($g->children)==0)
           {
                
                $name = str_replace("/","",strtolower($g->action_url));
               
                if(strtolower($segments)==$name)
                {
                   
                    foreach($g->actions as $h)
                    {
                            if($h->action_type==$action and $h->is_visible=="Y") 
                            {
                                $r =false;
								break;
                            }
                     }
                }
           }
           else
           {
              
                foreach($g->children as $j)
                {
                    
                    $name = str_replace("/","",strtolower($j->action_url));
                   
                    if(strtolower($segments)==$name)
                    {
                       
                        foreach($j->actions as $h)
                        {
                            if($h->action_type==$action and $h->is_visible=="Y") 
                            {
                                $r = false;
								break;
                            }
                        }
                       
                    }
                }
           }
        }
		
		if($r==true)
		{
			return abort(401);
		}
		else
		{
			return "N";
		}
        
	}
	
	public  static function visibleDelete($url="", $action)
	{
		$segments =$url;
        
        $data = json_decode(session()->get('menus'));
        $r = true;
        foreach($data as $g){
           
           if(count($g->children)==0)
           {
                
                $name = str_replace("/","",strtolower($g->action_url));
               
                if(strtolower($segments)==$name)
                {
                   
                    foreach($g->actions as $h)
                    {
                            if($h->action_type==$action and $h->is_visible=="Y") 
                            {
                                $r =false;
								break;
                            }
                     }
                }
           }
           else
           {
              
                foreach($g->children as $j)
                {
                    
                    $name = str_replace("/","",strtolower($j->action_url));
                   
                    if(strtolower($segments)==$name)
                    {
                       
                        foreach($j->actions as $h)
                        {
                            if($h->action_type==$action and $h->is_visible=="Y") 
                            {
                                $r = false;
								break;
                            }
                        }
                       
                    }
                }
           }
        }
		
		if($r==true)
		{
			return "Y"; //no authorized
		}
		else
		{
			return "N";
		}
		
       
        
	}
}